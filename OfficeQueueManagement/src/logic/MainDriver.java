package logic;
import java.util.HashMap;
import java.util.Vector;

import view.Gui;

public class MainDriver {

	public static void main(String[] args) {

		HashMap<String,Service> availableServices = new HashMap<String, Service>();
		availableServices.put("A",new Service("Account","A",5));
		availableServices.put("P",new Service("Package","P",10));

		Vector<Gate> availableGates = new Vector<Gate>();
		availableGates.add(new Gate(1, "A"));
		availableGates.add(new Gate(2, "P"));
		availableGates.add(new Gate(3, "A", "P"));
		
		Office o=new Office(availableServices, availableGates);

		new Gui(o);

	}

}
