package logic;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to manage a service
 * @author crix9
 */

public class Service {
	
	private String serviceName;
	private String letterTicket;
	private Integer lastTicketNumber;
	private Integer estimatedTime;
	List<Ticket> pendingTickets;
	
	
	/**
	 * Constructor to initialize the service, with name,letter and estimated time
	 * @param serviceName
	 * @param letterTicket
	 * @param estimatedTime
	 */

	public Service(String serviceName, String letterTicket, Integer estimatedTime) {
		this.serviceName = serviceName;
		this.letterTicket = letterTicket;
		this.pendingTickets=new ArrayList<Ticket>();
		this.estimatedTime=estimatedTime;
		this.lastTicketNumber=0;
	}
	
	/** 
	 * @return the letter associated to the service
	 */
	
	public String getLetterTicket() {
		return letterTicket;
	}
	
	/**
	 * Set the letter associated to the service
	 * @param letterTicket
	 * @return if the operation is successful or not 
	 */
	
	public boolean setLetterTicket(String letterTicket) {
		if(letterTicket.length()==1) {
			this.letterTicket = letterTicket;
			return true;
		}
		return false;
	}
		
	/**
	 * Add a new ticket in the list
	 * @return the ticket added in the list
	 */
	
	public Ticket addTicket() {
		Ticket t=new Ticket(letterTicket,this.lastTicketNumber++);
		pendingTickets.add(t);
		return t;
	}
		
	/**
	 * Get the first inserted ticket, and remove it from the list of pending tickets
	 * @return the first inserted ticket that wait to be served, null in case of error
	 */
	
	public Ticket getNextTicket() {
		if(!pendingTickets.isEmpty())
			return pendingTickets.remove(0);
		return null;
	}
	
	/**
	 * @return the name of the service
	 */
	
	public String getServiceName() {
		return serviceName;
	}
	
	/**
	 * Set the name of the service
	 * @param serviceName
	 * @return true if the value is not null, false otherwise
	 */
	
	public boolean setServiceName(String serviceName) {
		if(serviceName != null) {
			this.serviceName = serviceName;
			return true;
		}
		return false;
	}
		
	/**
	 * @return the whole list of pending tickets
	 */
	
	public List<Ticket> getPendingTickets(){
		return pendingTickets;
	}
	
	/**
	 * Reset the number for this service, restart to zero
	 */
	
	public void resetNumber() {
		this.lastTicketNumber=0;
		pendingTickets.clear();
	}
	
	/**
	 * @return the actual number of the queue
	 */
	
	public Integer getActualNumber() {
		return this.lastTicketNumber;
	}
		
	/**
	 * @return  the estimated time for this service
	 */
	
	public Integer getEstimatedTime() {
		return this.estimatedTime;
	}
	
}
