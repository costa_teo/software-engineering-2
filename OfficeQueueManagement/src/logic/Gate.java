package logic;

import java.util.Arrays;
import java.util.Vector;

public class Gate {

	private Integer gateNumber;
	private Vector<String> services;
	private String lastTicket;

	public Gate(Integer gateNumber, String ... services) {
		this.gateNumber = gateNumber;
		this.services = new Vector<String>();
		Arrays.asList(services).forEach(s -> addService(s));
	}

	public String getLastTicket() {
		return lastTicket;
	}

	public void setLastTicket(String ticket) {
	    String pattern= "^[A-Z]{1} \\d{3}$";
	    if(ticket != null && ticket.matches(pattern))
			lastTicket = ticket;
	}

	public Integer getGateNumber() {
		return gateNumber;
	}

	public Vector<String> getServices() {
		return services;
	}

	public void addService(String service) {
		if(service != null && !services.contains(service))
			this.services.add(service);
	}
}
