package logic;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Office class to manage the whole office
 * @author crix9
 */

public class Office {
		
	private Map<String,Service> services;
	private Vector<Gate> gates;

	
	/**
	 * Constructor to initialize the map of services and the vector of gates
	 * @param services
	 * @param gates
	 */
	
	public Office(HashMap<String,Service> services, Vector<Gate> gates) {
		if(services==null) this.services=new HashMap<String, Service>();
		else this.services=services;
		if(gates==null) this.gates=new Vector<Gate>();
		else this.gates = gates;
	}
		
	/**
	 * Call service method that create a new ticket for that service and put it in a queue
	 * @param serviceLetter
	 * @return the ticket added or null if error
	 */
	
	public Ticket addTicket(String serviceLetter) {
		if(serviceLetter==null) return null;
		Service serv=services.get(serviceLetter);
		if(serv != null) return serv.addTicket();
		return null;
	}
	
	/** 
	 * @param gate
	 * @return the next ticket to call for the one of the services managed by the gate
	 */
	
	public Ticket ticketCallNext(Gate gate) {
		
		if(gate==null) return null;
		
		Vector<String> serviceskey = gate.getServices();
		
		if(serviceskey!=null) {
			
			Vector<Service> servicesLocal=new Vector<Service>();
			
			for(String s: serviceskey) {
				if(services.get(s)!= null)
					servicesLocal.add(services.get(s));
			}
			
			if (!servicesLocal.isEmpty()) {
				// I search for the next ticket, following the given algorithm (biggest queue with minimum time if equal lenght)
				Service nextService = servicesLocal.get(0);
				
				for(Service service : servicesLocal) {
					//biggest queue
					if(service.getPendingTickets().size() > nextService.getPendingTickets().size()) {
						nextService = service;
					} else if(service.getPendingTickets().size() == nextService.getPendingTickets().size()) {
						//or minimum time if even
						if(service.getEstimatedTime() < nextService.getEstimatedTime()) {
							nextService = service;
						}
					}
				}
				
				Ticket t = nextService.getNextTicket();
				if(t!=null)
					gate.setLastTicket(t.toString());
				return t; 
			}
		}
		
		return null;
	}
	
	/**
	 * @return the map<String,Service>, all the services managed by the office
	 */
	
	public Map<String,Service> getServices() {
		return services;
	}
	
	/**
	 * @return the size of the actual service queue
	 */
	
	public Integer numberOfServices() {
		return services.size();
	}
	
	/**
	 * @return the vector of all gates
	 */
	
	public Vector<Gate> getGates(){
		return gates;
	}
	
}
