package logic;

import java.lang.Integer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Ticket {

	private String letter; 
	private Integer number; 
	private String date;
		
	
	public Ticket(String letter, Integer number) {
		this.number = number;
		this.letter = letter;
	    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yy");
	    DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
	    LocalDateTime d = LocalDateTime.now();
	    date = d.format(dateFormatter) + " at " + d.format(timeFormatter);
	}


	/**
	 * @return the corresponding string. For Example "A 012"
	 */
	
	public String toString() {
		return letter + " " + String.format("%3d", number).replace(" ", "0");
	}
	
	/**
	 * 
	 * @return the date corresponding to the creation of the object. Format is "dd/MM/yy at HH:mm"
	 */
	public String dateTimeString() {
		return date;
	}
	
	/**
	 * 
	 * @return return the letter associated with the ticket
	 */
	public String getLetter() {
		return letter;
	}
}
