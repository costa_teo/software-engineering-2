package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import logic.Office;
import logic.Ticket;

public class Gui {
	JFrame f1;
	JFrame f3;
	JFrame f4;
	public Gui(Office o){  
		
		JPanel p1Container=new JPanel();
		p1Container.setLayout(new BoxLayout(p1Container, BoxLayout.LINE_AXIS));
		JPanel p1 = new JPanel(new GridLayout(o.numberOfServices()+1, 1, 10, 10));
		p1.setBorder(new EmptyBorder(20,20,20,20));
		JPanel p2 = new JPanel(new GridBagLayout());
		p2.setAlignmentY(Component.CENTER_ALIGNMENT);
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		
		p4.setLayout(new GridLayout(10, 1));
		BoxLayout boxlayout = new BoxLayout(p3, BoxLayout.X_AXIS);
		p3.setLayout(boxlayout);
		
		//Panel 1 --> user choose the service
		JLabel label1= new JLabel("Please choose one of the two services:");
		
		p1.add(label1);
		o.getServices().forEach((serviceLetter,service) -> {
			JButton serviceButton = new JButton(service.getServiceName());
			p1.add(serviceButton);
			
		
			serviceButton.addActionListener(new ActionListener(){
			      public void actionPerformed(ActionEvent e){
			    	
			    	  Ticket toDisplay = o.addTicket(serviceLetter); 
			    	  if(toDisplay != null) {
			    		  	//Panel 2 --> ticket
			    		  	JPanel printTicket= new JPanel();
			    		  	printTicket.setLayout(new GridLayout(3,1));
			    		  	printTicket.setBackground(Color.WHITE);
			    		  	printTicket.setBorder(new LineBorder(Color.BLACK));
			    		  	
			    		  	//Label for ID ticket
			    		  	JLabel ticketNumber = new JLabel(toDisplay.toString());
			    		  	ticketNumber.setBackground(Color.RED);
			    		  	ticketNumber.setOpaque(true);
			    		  	ticketNumber.setHorizontalAlignment(SwingConstants.CENTER);
			    		  	ticketNumber.setFont(new Font("Courier New", Font.BOLD, 30));
			    		  	
			    		  	//Label for date
			    		  	JLabel ticketDate = new JLabel(toDisplay.dateTimeString());
			    		  	ticketDate.setFont(new Font("Courier New", Font.ITALIC, 20));
			    		  	ticketDate.setBorder(new EmptyBorder(10,10,10,10));
			    		  	
			    		  	//Logo management
			    		  	BufferedImage logo = null;
							try {
								logo = ImageIO.read(new File("./src/logo.jpg"));
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			    		    JLabel labelLogo = new JLabel(new ImageIcon(logo));
			    		    printTicket.add(ticketNumber);
			    		  	printTicket.add(ticketDate);
			    		    printTicket.add(labelLogo);
							
							p2.removeAll();
					    	p2.add(printTicket);
					    	p2.updateUI();
			    	  }
			      }
			    });
		});
		
		
		o.getGates().forEach((gate)->{
			JPanel cp = new JPanel();
			cp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder()));
			cp.setLayout(new GridLayout(3, 1));
			cp.add(new JLabel("Gate " + gate.getGateNumber(), SwingConstants.CENTER));
			JLabel label = new JLabel("", SwingConstants.CENTER);
			label.setText(gate.getLastTicket());
			label.setFont(label.getFont().deriveFont(30f));
			label.setBackground(Color.white);
			label.setOpaque(true);
			label.setBorder(new LineBorder(Color.BLACK));
				
			cp.add(label);
		
			JButton serviceButton = new JButton("call next");
			cp.add(serviceButton);
			serviceButton.addActionListener(new ActionListener(){
			      public void actionPerformed(ActionEvent e){
			    	
			    	  Ticket toDisplay = o.ticketCallNext(gate);
			    	
			    	  if(toDisplay != null) {
			  				JLabel ticketLabel = new JLabel("", SwingConstants.CENTER);
			  				ticketLabel.setText(toDisplay.toString() + " at Gate" +gate.getGateNumber());
			  				ticketLabel.setFont(ticketLabel.getFont().deriveFont(20f));
							ticketLabel.setBackground(Color.GREEN);
							ticketLabel.setOpaque(true);
							ticketLabel.setBorder(new LineBorder(Color.BLACK));
			  				

							if(p4.getComponentCount()>=10) {
								p4.remove(9);
							}
							
							//Update colors of labels in the panel 4
							Component[] components=p4.getComponents();
							for (Component c : components) {
								if(c instanceof JLabel)
									if(c.getBackground().equals(Color.GREEN) && ((JLabel)c).getText().contains("at Gate"+gate.getGateNumber()))
										c.setBackground(Color.WHITE);
							}
							
							p4.add(ticketLabel,0);
					    	p4.updateUI();
					    	label.setText(toDisplay.toString());
					    	label.updateUI();
			    	  }
			      }
			    });
			p3.add(cp);
		});
		
		p1Container.add(p1);
		p1Container.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Panel user"));
		p2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Panel ticket"));
		
		f1=new JFrame("Client Interface");
		f3=new JFrame("Operator Interface");
		f4=new JFrame("Clients Panel");

		f1.setLocation(0, 250);
		f3.setLocation(600, 250);
		f4.setLocation(900, 150);
		
		f1.setLayout(new GridLayout(1,2));
		
		f1.add(p1Container);
		f1.add(p2);
		f3.add(p3);
		f4.add(p4);
		
		
		f4.setMinimumSize(new Dimension(300, 600));
		f3.setMinimumSize(new Dimension(300, 300));
		f1.setMinimumSize(new Dimension(600, 300));
		
		f1.setVisible(true); //making the frame visible  
		f3.setVisible(true); //making the frame visible  
		f4.setVisible(true); //making the frame visible  
	}  
}
