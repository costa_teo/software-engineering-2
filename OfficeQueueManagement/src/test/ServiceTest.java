package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import logic.Service;
import logic.Ticket;

class ServiceTest {

	@Test
	void testGetLetterTicket() {
		try {
			Service ps = new Service("Package","P",10);
			assertTrue(ps.getLetterTicket().compareTo("P") == 0);
		}
		catch(Exception e) {
			fail();
		}	
	}
	
	@Test
	void testAddTicket() {
		try {
			Service ps = new Service("Package","P",10);
			ps.addTicket();
			ps.addTicket();
			ps.addTicket();
			ps.addTicket();
			assertEquals(4,ps.getPendingTickets().size());
		}
		catch(Exception e) {
			fail();
		}
	}

	@Test
	void testGetNextTicket() {
		try {
			Service s = new Service("Package","P",10);
			s.addTicket();
			s.addTicket();
			
			Ticket t1 = s.getNextTicket();
			assertTrue(t1 != null);
			assertEquals("P", t1.getLetter());
			Ticket t2 = s.getNextTicket();
			assertTrue(t2 != null);
			assertEquals("P", t2.getLetter());
			assertTrue(s.getNextTicket() == null);
		}
		catch(Exception e) {
			fail();
		}
	}
	
	@Test
	void testSetLetterTicket() {
		try {
			Service ps = new Service("Package","P",10);
			ps.setLetterTicket("Test");
			assertTrue(ps.getLetterTicket().compareTo("P") == 0);
		}
		catch(Exception e) {
			fail();
		}	
	}

	@Test
	void testGetServiceName() {
		try {
			Service s = new Service("Test","P",10);
			assertTrue(s.getServiceName().compareTo("Test") == 0);
		}
		catch(Exception e) {
			fail();
		}	
	}

	@Test
	void testSetServiceName() {
		try {
			Service s = new Service("Package","P",10);
			s.setServiceName("Name");
			assertTrue(s.getServiceName().compareTo("Name") == 0);
		}
		catch(Exception e) {
			fail();
		}	
	}

	@Test
	void testGetPendingTickets() {
		try {
			Service s = new Service("Package","P",10);
			s.addTicket();
			s.addTicket();
			s.addTicket();
			assertEquals(3, s.getPendingTickets().size());
		}
		catch(Exception e) {
			fail();
		}	
	}

	@Test
	void testResetNumber() {
		try {
			Service s = new Service("Package","P",10);
			s.addTicket();
			s.addTicket();
			s.addTicket();
			s.resetNumber();
			assertEquals(0, s.getPendingTickets().size());
		}
		catch(Exception e) {
			fail();
		}	
	}

	@Test
	void testGetActualNumber() {
		try {
			Service s = new Service("Package","P",10);
			s.addTicket();
			s.addTicket();
			assertEquals(2, s.getActualNumber());
		}
		catch(Exception e) {
			fail();
		}	
	}
	
	@Test
	void testConstructor01() {
		try {
			Service s = new Service(null,"P",10);
			assertEquals(null, s.getServiceName());
		}
		catch(Exception e) {
			fail();
		}	
	}
	
	@Test
	void testConstructor02() {
		try {
			Service s = new Service("ServiceName",null,10);
			assertEquals(null, s.getLetterTicket());
		}
		catch(Exception e) {
			fail();
		}	
	}
	
	@Test
	void testConstructor03() {
		try {
			Service s = new Service("ServiceName","L",null);
			assertEquals(null,s.getEstimatedTime());
		}
		catch(Exception e) {
			fail();
		}	
	}
}
