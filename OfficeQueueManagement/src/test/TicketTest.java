package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import logic.Ticket;

class TicketTest {

	@Test
	void testToString() {
		Ticket t = new Ticket("A", 138);
		if(!t.toString().equals("A 138")) {
			fail("To string wrong format. Expected A 138 but was: " + t.toString());
		}
		
	}
	
	@Test
	void testGetLetter() {
		Ticket t = new Ticket("E", 1);
		if(!t.getLetter().equals("E")) {
			fail("Ticket getLetter wrong. Expected A but was: " + t.getLetter());	
		}
	}

}
