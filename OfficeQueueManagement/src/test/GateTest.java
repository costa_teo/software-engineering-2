package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Vector;

import org.junit.jupiter.api.Test;

import logic.Gate;
import logic.Office;
import logic.Service;

class GateTest {

	@Test
	void testGetLastTicket() {
		HashMap<String,Service> availableServices = new HashMap<String, Service>();
		availableServices.put("A",new Service("Account","A",5));
		availableServices.put("P",new Service("Package","P",10));
		Gate g=new Gate(1, "A", "P");
		Vector<Gate> availableGates = new Vector<Gate>();
		availableGates.add(g);
		Office o= new Office(availableServices, availableGates);
		o.addTicket("A");
		String t0=g.getLastTicket();
		assertNull(t0);
		o.ticketCallNext(g);
		String t1=g.getLastTicket();
		assertEquals("A 000", t1);
	}
	
	@Test
	void testSetLastTicket() {
		Gate g=new Gate(1, "A", "P");
		g.setLastTicket("A 000");
		assertEquals("A 000", g.getLastTicket());
		g.setLastTicket("A 001");
		g.setLastTicket(null);
		assertEquals("A 001", g.getLastTicket());
		g.setLastTicket("A 002");
		g.setLastTicket("A 00A");
		assertEquals("A 002", g.getLastTicket());
	}
	
	@Test
	void testGetGateNumber() {
		Gate g=new Gate(1, "A", "P");
		assertEquals(1, g.getGateNumber());
	}
	
	
	@Test
	void testGetServices() {
		Gate g=new Gate(1, "A", "P");
		Vector<String> services=g.getServices();
		
		assertEquals(2, services.size());
		assertEquals("A", services.get(0));
		assertEquals("P", services.get(1));
	}
	
	@Test
	void testAddService() {
		Gate g=new Gate(1, "A", "P");
		g.addService(null);
		assertEquals(2, g.getServices().size());
		g.addService("A");
		assertEquals(2, g.getServices().size());
		g.addService("H");
		Vector<String> services=g.getServices();
		assertEquals(3, services.size());
		assertEquals("H", services.get(2));
	}

}
