package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Vector;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import logic.Gate;
import logic.Office;
import logic.Service;
import logic.Ticket;


class OfficeTest {

	private Office o;

	@BeforeEach
	public void setUp() {

		HashMap<String,Service> availableServices = new HashMap<String, Service>();
		availableServices.put("A",new Service("Account","A",5));
		availableServices.put("P",new Service("Package","P",10));

		Vector<Gate> availableGates = new Vector<Gate>();
		availableGates.add(new Gate(1, "A"));
		availableGates.add(new Gate(2, "P"));
		availableGates.add(new Gate(3, "A", "P"));

		this.o=new Office(availableServices, availableGates);

	}

	@Test
	void testAddTicket01() {
		try {
			Ticket t = o.addTicket("A");
			assertEquals("A 000", t.toString());

			t = o.addTicket("A");
			assertEquals("A 001", t.toString());

			t = o.addTicket("P");
			assertEquals("P 000", t.toString());

			t = o.addTicket("P");
			assertEquals("P 001", t.toString());
		} catch(Exception e) {
			fail();
		}
	}

	@Test
	void testAddTicket02() {
		try {
			Ticket t = o.addTicket("NULL");
			assertNull(t);
		} catch(Exception e) {
			fail();
		}
	}

	@Test
	void testAddTicket03() {
		try {
			Ticket t = o.addTicket(null);
			assertNull(t);
		} catch(Exception e) {
			fail();
		}
	}

	@Test
	void testGetServices01() {
		try {
			assertEquals(2, o.getServices().size());
		} catch(Exception e) {
			fail();
		}
	}

	@Test
	void testGetServices02() {
		try {
			Office o2 = new Office(null, null);
			assertEquals(0, o2.getServices().size());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testGetGates01() {
		try {
			assertEquals(3, o.getGates().size());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testGetGates02() {
		try {
			Office o2 = new Office(null, null);
			assertEquals(0, o2.getGates().size());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testNumberOfServices01() {
		try {
			assertEquals(2, o.numberOfServices());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testNumberOfServices02() {
		try {
			Office o2 = new Office(null, null);
			assertEquals(0, o2.numberOfServices());
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
	void testTicketCallNext01() {
		o.addTicket("A");
		o.addTicket("P");
		o.addTicket("A");
		Ticket t=o.ticketCallNext(o.getGates().get(2));
		assertEquals("A 000",t.toString());		
	}

	@Test
	void testTicketCallNext02() {
		o.addTicket("A");
		o.addTicket("P");
		o.addTicket("A");
		Ticket t=o.ticketCallNext(o.getGates().get(1));
		assertEquals("P 000",t.toString());
	}

	@Test
	void testTicketCallNext03() {
		o.addTicket("A");
		o.addTicket("P");
		o.addTicket("A");
		Ticket t=o.ticketCallNext(o.getGates().get(0));	
		assertEquals("A 000",t.toString());		
	}

	@Test
	void testTicketCallNext04() {	
		o.addTicket("A");
		o.addTicket("P");
		o.addTicket("A");
		Ticket t=o.ticketCallNext(o.getGates().get(2));
		t=o.ticketCallNext(o.getGates().get(2));
		assertEquals("A 001",t.toString());	
	}

	@Test
	void testTicketCallNext05() {	
		o.addTicket("A");
		o.addTicket("P");
		o.addTicket("A");
		Ticket t=o.ticketCallNext(o.getGates().get(2));
		t=o.ticketCallNext(o.getGates().get(2));
		t=o.ticketCallNext(o.getGates().get(2));
		assertEquals("P 000",t.toString());		
	}

	@Test
	void testTicketCallNext06() {
		o.addTicket("A");
		o.addTicket("P");
		o.addTicket("A");
		o.addTicket("P");
		Ticket t=o.ticketCallNext(o.getGates().get(2));
		assertEquals("A 000",t.toString());	
	}

	@Test
	void testTicketCallNext07() {
		o.addTicket("A");
		o.addTicket("A");
		Ticket t=o.ticketCallNext(o.getGates().get(1));
		assertNull(t);
	}

	@Test
	void testTicketCallNext08() {
		Ticket t=o.ticketCallNext(o.getGates().get(1));
		assertNull(t);		
	}

	@Test
	void testTicketCallNext09() {
		try {
			Ticket t = o.ticketCallNext(null);
			assertNull(t);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}
