

>Copyright © 2019 Matteo Costa Politecnico di Torino DAUIN department. \
>All rights reserved. This guide and the accompanying materials
>are available for free.\
> Let's go offer a coffee to the authors.

Contributors: 
1. [Matteo Costa](https://gitlab.com/costa_teo) - Initial API and Docker implementation
 


# useful commands
### Important Dockerfile is a file without extension.

## These commands are used in the same directory where is put Dockerfile.


1. build a new image by lunching the script in the Dockerfile
    > docker build -t imageName .

1. run without portmapping (will not work)
    > docker run imageName

1. run with mapping (important)
    > docker run -p 8080:8080 se2polito/team-c

1. get from repository
    > docker pull se2polito/team-c

1. push to repository (you must be logged)
    > docker push se2polito/team-c

1. the list of images installed in you pc   
    > docker images


## Come fare il package:
1. Cambiare configurazione in application.properites, il path del db e abilitare se si vuole la console dall'esterno.

1. Fare il [deploy di angular](https://angular.io/guide/deployment) e copiare tutti i file appena compilati nella cartella (vuota o svuotata) del server src/main/resources/public.
1. Compilare il server java
    1. Pulire cartella targer 
        > maven clean

    1. Fare il package: (se i test non passano non funziona, fixxa i test prima, [disabilitali](https://maven.apache.org/plugins-archives/maven-surefire-plugin-2.12.4/examples/skipping-test.html) o eliminali)
        > maven build package 

1. Verra creata la cartella **target** dove:

    1. trovi il file *artifactId-version.war* [esempio *SMS-backend-0.0.1-SNAPSHOT.war*], artifactId e version sono specificati nel maven. (se non hai un .war imponilo nel maven)

    1. trovi una cartella artifactId-version [esempio SMS-backend-0.0.1-SNAPSHOT].

1. copia il file .war e la cartella specificata nella cartella dove è presente il Dockerfile.

1. probabilmente dovrai aggiustare o inserire anche il file del database. In application.properties è specificato il path al file del db, fai si che sia raggiungibile lo stesso path relativo dalla root di docker (senza uscire dalla root, solo sotto cartelle o root stessa). 
1. Per Docker lancia i comandi: 
    > docker build -t se2polito/team-c .
    
    > docker run -p 443:443 se2polito/team-c

1. Se funziona tutto 
    > docker push se2polito/team-c

