# Story 1
## Use case: Know marks of his/her child

## Scope: System (parent side)

## Level: user-goal

## Intention in context:
The parent wants to see and know what are the marks of his/her child and the average mark for every subject.

## Primary actor: parent

## Stakeholders’ interests:
- Parents want to stay informed of their children's grades and averages.
- Teacher wants that parents are able to see their child's grades
- School principal wants to keep informed families about children's trend.

## Precondition:
- The parent must be registered to the system.
- The parent should have at least one child in a school class.

## Success guarantees:
- Grades and averages of the selected child are shown divided by subject.

## Main Success Scenario
1. The parent inserts his/her credentials to login
2. The system verifies and accepts parent's credentials
3. The parent selects one of his/her child
4. The parent goes in grades section of his/her child
5. The system provides the list of subjects
6. The parent selects a subject to see all the grades for that subject and the average mark
7. The system provides the list of grades for selected subject in cronological order

## Extensions:
1A. The parent inserts wrong credentials
  - The system doesn't accept user's credentials.
  - The use case continues at step 1.

3A. The parent selects the wrong child
  - The parent can come back to the child selection.
  - The use case continues at step 3.

6A. The parent selects the wrong subject
  - The parent can come back to the subject selection.
  - The use case continues at step 6.


_____________

# Story 2
## Use case: Record daily lecture topics

## Scope: System (Teacher and Lecture Information)

## Level: user-goal

## Intention in context:
- The data of the lecture topics are saved in the system.

## Primary actor: Teacher


## Stakeholders’ interests:
- The teacher wants to save lecture topics in the system.
- The parent would like to access the topics.
- The student want to get information about lectures.

## Precondition:
- The teacher must have a teacher account and he/she must own at least a teaching subject and a class related to it.


## Success guarantees:
- The lecture topics inserted by the teacher are saved in the system.


## Main Success Scenario

1. The teacher inserts his/her credentials to login
2. The system verifies and accepts teacher's credentials
3. The teacher selects to record the lecture topic
4. The system provides the possibility to view actual lecture topics or to insert new ones
5. The teacher selects to insert a new lecture topic
6. The system provides the possibility to leave a description selecting a class and a subject
7. The teacher selects filters and fills the form
8. The system confirms the operation and saves data

## Extensions:
1A. The teacher inserts wrong credentials
  - The system doesn't accept user's credentials.
  - The use case continues at step 1.

5A. The teacher selects to view lecture topics
  - The system provide the view of teacher's lecture topics.
  - The use case ends.

7A. The teacher select an invalid data (last week day, future time, holidays)
  - The system rejects the date and shows an error.
  - The use case continues at step 7.



______________


# Story 3
## Use case: Enable access to parents

## Scope: System Account

## Level: user-goal

## Intention in context:
The administrative wants to provide access to parents.

## Primary actor:  Administrative officer

## Stakeholders’ interests:
- The administrative officer wants to save personal data about parents in the system.
- The parent want credential to access the system.

## Precondition:
- The administrative officer has an account, required information about parents are available.


## Success guarantees:
- The parents information are saved and the parents receive an email with credentials to access the system.

## Main Success Scenario
1. The administrative officer inserts his/her credentials to login
2. The system verifies and accepts administrative officer's credentials
3. The administrative officer selects the registration of a parent service
4. The administrative officer inserts parents' fiscal ID
5. The system provide the possibility to enter other information of parents
6. The administrative officer inserts all information about parents
7. The system verifies and saves data
8. The system sends an email to parents' email with credentials

## Extensions:

1A. The administrative officer inserts wrong credentials
- The system doesn't accept user's credentials.
- The use case continues at step 1.

4A. The administrative officer inserts a wrong fiscal ID
  - The system verify that the fiscal ID does not exist and open a new parent form.
  - The system provide the possibility to remove the inserted fiscalID.
  - The new parent form is removed.
  - The use case continues at step 4.


4B. The system verifies successfully the fiscal IDs and doesn't show the additional form
- The use case continues at step 7.



8A. The mail system has an error and is not able to send an email to parents
- An error message is shown  
    - The system try again
        - The emal are sent
        - The use case complete successfully

    * The system try again
        - The email are not sent
        - An error message is shown
        - The use case fails

______________


# Story 4
## Use case: Enroll students

## Scope: System accounts

## Level: User-goal

## Intention in context:
Administrative officer wants to save data about the new student in the system. Moreover, he wants to link the student to his parents.  


## Primary actor: Administrative officer


## Stakeholders’ interests:
- The parent would like to link his account to the new student in order to access the student page.
- The student want to be enrolled in the system

## Precondition:
- The administrative officer has an account, required information about parents and student are available.


## Success guarantees:
- The student information are saved and link together with parents account.

## Main Success Scenario
1. The administrative officer inserts his/her credentials to login
2. The system verifies and accepts administrative officer's credentials
3. The administrative officer select the registration of a parent service
4. The administrative officer inserts parents fiscal ID
5. The system verifies the fiscal ID
6. The administrative officer inserts student information
7. The system verifies and saves data

## Extensions:

1A. The administrative officer inserts wrong credentials
    - The system doesn't accept user's credentials.
    - An error message is shown.
    - The use case continues at step 1.

4A. The administrative officer inserts the new parents's fiscal ID
    - The system verify that the fiscal ID does not exist.
    - The system provides an additional form for parents information.
    - The use case continues at step 6.

6A. The administrative officer inserts the new student's fiscal ID
    - The system verifies that the fiscal ID does not exist.
    - The system provides an additional form for student information.
    - The use case continues at step 7.
