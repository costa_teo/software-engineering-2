import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  secret: boolean = false;

  constructor() {
    //
  }

  doSurprise() {
    this.secret = !this.secret;
    if(this.secret) document.body.style.backgroundColor = "#002060";
    if(!this.secret) document.body.style.backgroundColor = "#E0FFFF";
  }
}
