import { ServiceBoxData } from './service-box-data';
import { Injectable } from '@angular/core';
import { Routes } from 'src/app/core/routes';
import { ROLES } from '../../constants';
import { Router } from '@angular/router';

@Injectable()
export class ServiceBoxService {

    private allServices: Array<{ role: string, services: Array<ServiceBoxData> }>;

    constructor() {
        
        let parentServices: Array<ServiceBoxData> = new Array<ServiceBoxData>();
        parentServices.push(
            new ServiceBoxData("Grades", Routes.PARENT_GRADES),
            new ServiceBoxData("Assignments", Routes.PARENT_ASSIGNMENTS),
            new ServiceBoxData("Support Material", Routes.PARENT_SUPPORT_MATERIAL),
            new ServiceBoxData("Attendances", Routes.PARENT_ATTENDANCES),
            new ServiceBoxData("Timetable", Routes.PARENT_TIMETABLE)
        );

        let teacherServices: Array<ServiceBoxData> = new Array<ServiceBoxData>();
        teacherServices.push(
            new ServiceBoxData("Lecture Topics", Routes.TEACHER_TOPICS),
            new ServiceBoxData("Students Grades", Routes.TEACHER_GRADES),
            new ServiceBoxData("Students Attendances", Routes.TEACHER_ABSENCES),
            new ServiceBoxData("Class Assignments", Routes.TEACHER_ASSIGNMENTS),
            new ServiceBoxData("Support Material", Routes.TEACHER_SUPPORT_MATERIAL),
            new ServiceBoxData("Timetable", Routes.TEACHER_TIMETABLE)
        );

        let administratorServices:Array<ServiceBoxData> = new Array<ServiceBoxData>();
        administratorServices.push(
            new ServiceBoxData("Add Users", Routes.ADMINISTRATOR_ADD_USER),
            new ServiceBoxData("Classes Composition", Routes.ADMINISTRATOR_ENTER_CLASS_COMPOSITION),
            new ServiceBoxData("General Communication", Routes.ADMINISTRATOR_GENERAL_COMMUNICATION),
            new ServiceBoxData("Assign Teacher", Routes.ADMINISTRATOR_ASSIGN_TEACHER),
            new ServiceBoxData("Manage Timetables", Routes.ADMINISTRATOR_MANAGE_TIMETABLES),
        );

        this.allServices = new Array<{ role: string, services: Array<ServiceBoxData> }>();
        this.allServices.push({ role: ROLES.PARENT, services: parentServices }, { role: ROLES.TEACHER, services: teacherServices }, { role: ROLES.ADMINISTRATOR, services: administratorServices });
    }

    public get parentServices(): Array<ServiceBoxData> {
        return this.allServices.find(obj => {
            return obj.role == ROLES.PARENT;
        }).services;
    }

    public get teacherServices(): Array<ServiceBoxData> {
        return this.allServices.find(obj => {
            return obj.role == ROLES.TEACHER;
        }).services;
    }

    public get administratorServices(): Array<ServiceBoxData> {
        
        let adminServices = new Array<ServiceBoxData>();
        this.allServices.find(obj => {
            return obj.role == ROLES.ADMINISTRATOR;
        }).services.forEach(service =>{
            adminServices.push(service) //make a copy
        });


        let currentUser = JSON.parse(localStorage.getItem('currentUser')); //SUPER ADMINISTRATOR SERVICES
        if(currentUser != null){
            if(currentUser.token.split("_")[2] == 'TRUE'){
                adminServices.push(new ServiceBoxData("Add Account", Routes.SUPER_ADMINISTRATOR_ADD_ACCOUNT));
            }
        }
        return adminServices
    }

    public getServicesByRole(role: string): Array<ServiceBoxData> {
        if (!role) return null;

        if (role == ROLES.PARENT) return this.parentServices;
        if (role == ROLES.TEACHER) return this.teacherServices;
        if (role == ROLES.ADMINISTRATOR) return this.administratorServices;
    }
}