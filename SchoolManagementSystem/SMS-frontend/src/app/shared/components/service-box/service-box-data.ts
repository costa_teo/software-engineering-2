export class ServiceBoxData {
    public serviceName: string;
    public redirectionPath: string;

    constructor(service: string, path: string) {
        this.serviceName = service;
        this.redirectionPath = path;
    }
}