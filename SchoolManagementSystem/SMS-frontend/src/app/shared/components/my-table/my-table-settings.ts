export interface Settings {

    /**
     * The list of properties of each column, in the preferred order
     */
    columns: Array<{
        /**
         * The name to display on the header
         */
        columnName: string,
        /**
         * The name of the property
         */
        attributeName: string,
        /**
         * If true, the elements of this column will be displayed in a readonly textarea
         */
        isEditable?: boolean,
        /**
         * If true, the elements of this column will be displayed in a writable textarea
         * !!!ONLY ONE COLUMN CAN HAVE THIS!!! (ask Coscia M to implement for more columns)
         */
        isInsertable?:boolean,
        /**
         * If true, sorting is allowed on this column ('hasSorting' must be true too)
         */
        isSortable?: boolean
    }>;

    /**
     * If true, info button is showed
     */
    hasInfo?:boolean;

    /**
     * If true, edit button is showed
     */
    hasEdit?: boolean;

    /**
     * If true, delete button is showed
     */
    hasDelete?: boolean;

    /**
     * If true, save buttons is showed
     */
    hasInsertion?:boolean;

    /**
     * If true, sorting options are enabled
     */
    hasSorting?: boolean;

    /**
     * If true, pagination is enabled
     */
    hasPagination?:boolean;

    /**
     * The number of rows per page. If not set, default is 5 ('hasPagination' must be true too)
     */
    elementsPerPage?: number;
}