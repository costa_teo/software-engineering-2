import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Settings } from './my-table-settings';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'my-table',
  templateUrl: './my-table.component.html',
  styleUrls: ['./my-table.component.scss']
})
export class MyTableComponent implements OnInit {

  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;

  @Input() protected data: Array<any>;
  @Input() protected settings: Settings;

  @Output() protected onDelete: EventEmitter<number> = new EventEmitter<number>();
  @Output() protected onEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() protected onInsert: EventEmitter<any> = new EventEmitter<any>();
  @Output() protected onInfo: EventEmitter<number> = new EventEmitter<number>();

  private rowToDelete: number;

  protected rowToEdit: number;
  protected isEditing: boolean;
  protected dataToEdit: any;

  // Pagination
  private elementsPerPage: number;
  private currentPage: number = 1;
  protected pages: Array<number>;
  protected elementsToShow: Array<any>;

  //Sorting
  protected currentSortings: Map<string, string>;
  protected originalSorting: Array<any>;

  constructor() { }

  ngOnInit() {
    // Pagination Setup
    if (this.settings && this.settings.hasPagination && this.settings.elementsPerPage) {
      this.elementsPerPage = this.settings.elementsPerPage;
    } else if (this.settings && this.settings.hasPagination) {
      this.elementsPerPage = 5;
    } else {
      this.elementsPerPage = Math.max(this.data.length, 1);
    }

    if (this.data) {
      this.data = JSON.parse(JSON.stringify(this.data));
      this.pages = Array(Math.ceil(this.data.length / this.elementsPerPage)).fill(0).map((x, i) => i + 1);
      this.elementsToShow = this.data.slice(0, this.elementsPerPage);
    }

    // Sorting Setup
    if (this.settings && this.settings.columns && this.settings.hasSorting) {
      this.currentSortings = new Map<string, string>();
      this.settings.columns.forEach(element => {
        this.currentSortings.set(element.attributeName, 'none');
      });

      if (this.data) {
        this.originalSorting = new Array<any>();
        this.data.forEach((el, i) => {
          el['__id__'] = i;
          this.originalSorting.push(el);
        });
      }
    }
  }

  // Start Pagination
  protected hasPrevious(): boolean {
    return this.currentPage > 1;
  }

  protected hasNext(): boolean {
    return this.currentPage < this.pages.length;
  }

  protected goPrevious() {
    if (this.currentPage < 2) return;

    this.goToPage(this.currentPage - 1);
  }

  protected goToPage(pageNum: number) {
    this.elementsToShow = this.data.slice((pageNum - 1) * this.elementsPerPage, (pageNum) * this.elementsPerPage);
    this.currentPage = pageNum;
  }

  protected goNext() {
    if (this.currentPage > this.pages.length - 1) return;

    this.goToPage(this.currentPage + 1);
  }
  //End Pagination

  //Start Sorting
  sortRowsByColumn(attributeName: string) {
    if (!this.settings.hasSorting) return;

    let currSort: string = this.currentSortings.get(attributeName);
    this.currentSortings.forEach((value, key) => {
      if (key != attributeName) this.currentSortings.set(key, 'none');
    });

    if (currSort == 'none') this.sortDesc(attributeName);
    else if (currSort == 'desc') this.sortAsc(attributeName);
    else if (currSort == 'asc') this.sortNone(attributeName);

    this.goToPage(this.currentPage);
  }

  sortDesc(attributeName: string) {
    this.data.sort((a, b) => {
      return (a[attributeName] > b[attributeName]) ? -1 : (a[attributeName] === b[attributeName]) ? 0 : 1;
    });

    this.currentSortings.set(attributeName, 'desc');
  }

  sortAsc(attributeName: string) {
    this.data.sort((a, b) => {
      return (a[attributeName] > b[attributeName]) ? 1 : (a[attributeName] === b[attributeName]) ? 0 : -1;
    });

    this.currentSortings.set(attributeName, 'asc');
  }

  sortNone(attributeName: string) {
    this.data = JSON.parse(JSON.stringify(this.originalSorting));

    this.currentSortings.set(attributeName, 'none');
  }

  getSortingIcon(attributeName: string): string {
    let currSort: string = this.currentSortings.get(attributeName);

    if (currSort == 'none') return 'glyphicon-sort';
    else if (currSort == 'desc') return 'glyphicon-sort-by-attributes-alt';
    else if (currSort == 'asc') return 'glyphicon-sort-by-attributes';
  }
  //End Sorting

  //Start Info
  protected doInfo(index: number) {
    this.isEditing = false;
    let rowIndex = (this.currentPage - 1) * this.elementsPerPage + index;
    let originalIndex = this.originalSorting.findIndex(el => el['__id__'] == this.data[rowIndex]['__id__']);

    this.onInfo.emit(originalIndex);
  }
  //End Info

  //Start Edit
  protected doEdit(index: number) {
    let rowIndex = (this.currentPage - 1) * this.elementsPerPage + index;
    this.rowToEdit = this.data[rowIndex]['__id__'];
    this.dataToEdit = JSON.parse(JSON.stringify(this.data[rowIndex]));
    this.isEditing = true;
  }

  protected doConfirmUpdate() {
    let currentIndex = this.data.findIndex(el => el['__id__'] == this.rowToEdit);
    let originalIndex = this.originalSorting.findIndex(el => el['__id__'] == this.rowToEdit);

    this.data[currentIndex] = this.dataToEdit;
    this.originalSorting[originalIndex] = JSON.parse(JSON.stringify(this.dataToEdit));

    this.goToPage(this.currentPage);
    this.onEdit.emit({ value: this.dataToEdit, index: originalIndex });
    this.doCancelUpdate();
  }

  protected doCancelUpdate() {
    this.dataToEdit = null;
    this.rowToEdit = null;
    this.isEditing = false;
  }
  //End Edit

  //Start Delete
  protected doDelete(index: number) {
    let rowIndex = (this.currentPage - 1) * this.elementsPerPage + index;
    this.rowToDelete = this.data[rowIndex]['__id__'];
    this.modal.show("Do you want to delete the selected element?");
  }

  protected doConfirmDelete(event: boolean) {
    let currentIndex = this.data.findIndex(el => el['__id__'] == this.rowToDelete);
    let originalIndex = this.originalSorting.findIndex(el => el['__id__'] == this.rowToDelete);

    this.data.splice(currentIndex, 1);
    this.originalSorting.splice(originalIndex, 1);

    this.pages = Array(Math.ceil(this.data.length / this.elementsPerPage)).fill(0).map((x, i) => i + 1);
    this.goToPage(this.elementsToShow.length == 1 ? this.currentPage - 1 : this.currentPage);
    this.onDelete.emit(originalIndex);
    this.rowToDelete = null;
  }
  //End Delete

  //Start Insert
  protected doSaveInsertion() {
    let filledRows = new Array<any>();
    let columnToInsert: string = this.settings.columns.find(col => col.isInsertable).attributeName;
    this.data.forEach((el, i) => {
      if (el[columnToInsert]) {
        filledRows.push(el)
      }
    });
    this.onInsert.emit(filledRows);
  }
  //End Insert

}
