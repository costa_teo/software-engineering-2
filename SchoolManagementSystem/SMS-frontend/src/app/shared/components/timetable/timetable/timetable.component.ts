import { Component, OnInit, Input } from '@angular/core';
import { TimetableRow } from './timetableRow';
import { CONSTANTS } from 'src/app/shared/constants';

@Component({
  selector: 'timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.scss']
})
export class TimetableComponent implements OnInit {

  @Input() protected data: Array<TimetableRow>;
  
  subjectColor = [];
  days = CONSTANTS.days;
  workingHours = CONSTANTS.workingHours;
  
  constructor() { }
  
  ngOnInit() {
    this.setColor();
  }


  setColor() {
    let lastColorTaken = 0;

    if(!this.data)
      return;

    this.data = this.data.slice(0,6)

    while(this.data.length < 6 ){
      this.data.push(new TimetableRow)
    }

    let i = 0;
    for(let row of this.data){
      for(let d of [row.monday, row.tuesday, row.wednesday, row.thursday, row.friday]){
          if(d && this.subjectColor[d] == null){
            this.subjectColor[d] = CONSTANTS.SUBJECT_COLORS[lastColorTaken++%CONSTANTS.SUBJECT_COLORS.length]
          }
      }
      i++; if(i>=6) break;
    }
  }



}



