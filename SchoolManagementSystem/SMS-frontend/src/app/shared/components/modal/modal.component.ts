import { Component, OnInit, Output, EventEmitter } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output() private onConfirm: EventEmitter<boolean> = new EventEmitter<boolean>();

  protected bodyMessage: string;

  constructor() { }

  ngOnInit() {
  }

  show(bodyMessage: string) {
    this.bodyMessage = bodyMessage;
    $("#my-modal").modal('show');
  }

  protected doConfirm() {
    this.onConfirm.emit(true);
  }

}
