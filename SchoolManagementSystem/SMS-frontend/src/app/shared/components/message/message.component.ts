import { Component, OnInit } from '@angular/core';

export class MESSAGE_TYPE {
  static ERROR: string = "alert-danger";
  static WARNING: string = "alert-warning";
  static SUCCESS: string = "alert-success";
}

@Component({
  selector: 'message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  protected toShow: boolean = false;
  protected messageText: string;
  protected messageType: MESSAGE_TYPE = "alert-danger";

  private interval = 0;

  constructor() { }

  ngOnInit() {
    //
  }

  showMessage(message: string, type: MESSAGE_TYPE) {
    this.messageType = type;
    this.messageText = message;
    window.scrollTo(0, 0);
    this.toShow = true;
    this.startTimer();
  }

  private startTimer() {
    window.clearTimeout(this.interval);
    this.interval = window.setTimeout(() => {
      this.toShow = false;
      this.messageText = "";
    }, 7000);
  }

}
