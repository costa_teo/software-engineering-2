import { DatePipe } from '@angular/common';
import * as XLSX from 'src/lib/xlsx.full.min.js'
import { Observable } from 'rxjs';


export class ROLES {
    static PARENT: string = "PARENT";
    static TEACHER: string = "TEACHER";
    static ADMINISTRATOR: string = "ADMINISTRATOR";
}


export class GRADES {
    static valuesList: Array<{ label: string, value: number }> = [
        { label: "1", value: 1 },
        { label: "1+", value: 1.25 },
        { label: "1.5", value: 1.5 },
        { label: "1/2", value: 1.75 },
        { label: "2-", value: 1.75 },
        { label: "2", value: 2 },
        { label: "2+", value: 2.25 },
        { label: "2.5", value: 2.5 },
        { label: "2/3", value: 2.75 },
        { label: "3-", value: 2.75 },
        { label: "3", value: 3 },
        { label: "3+", value: 3.25 },
        { label: "3.5", value: 3.5 },
        { label: "3/4", value: 3.75 },
        { label: "4-", value: 3.75 },
        { label: "4", value: 4 },
        { label: "4+", value: 4.25 },
        { label: "4.5", value: 4.5 },
        { label: "4/5", value: 4.75 },
        { label: "5-", value: 4.75 },
        { label: "5", value: 5 },
        { label: "5+", value: 5.25 },
        { label: "5.5", value: 5.5 },
        { label: "5/6", value: 5.75 },
        { label: "6-", value: 5.75 },
        { label: "6", value: 6 },
        { label: "6+", value: 6.25 },
        { label: "6.5", value: 6.5 },
        { label: "6/7", value: 6.75 },
        { label: "7-", value: 6.75 },
        { label: "7", value: 7 },
        { label: "7+", value: 7.25 },
        { label: "7.5", value: 7.5 },
        { label: "7/8", value: 7.75 },
        { label: "8-", value: 7.75 },
        { label: "8", value: 8 },
        { label: "8+", value: 8.25 },
        { label: "8.5", value: 8.5 },
        { label: "8/9", value: 8.75 },
        { label: "9-", value: 8.75 },
        { label: "9", value: 9 },
        { label: "9+", value: 9.25 },
        { label: "9.5", value: 9.5 },
        { label: "9/10", value: 9.75 },
        { label: "10-", value: 9.75 },
        { label: "10", value: 10 },
        { label: "10 cum laude", value: 10 },
    ];
}

export class TIMESLOTS {
    static valuesList: Array<{ label: string, value: number }> = [
        { label: "8", value: 8 },
        { label: "9", value: 9 },
        { label: "10", value: 10 },
        { label: "11", value: 11 },
        { label: "12", value: 12 },
    ];
}

export class CONSTANTS {

    static days = ["Mon", "Thu", "Wed", "Tus", "Fri"]
    
    static workingHours = [
      "08:00",
      "09:00",
      "10:00",
      "11:00",
      "12:00",
      "13:00",
    ]

    static SUBJECT_COLORS = [
        "#c70000",
        "#c77100",
        "#c7ba00",
        "#88c700",
        "#35c700",
        "#00c799",
        "#009cc7",
        "#009cc7",
        "#1b00c7",
        "#7b00c7",
        "#c7008f",
      ]
  static TIMETABLE_TEMPLATE_URL: string = "timetable-template.xlsx";
  static CLASS_COMPOSITION_TEMPLATE_URL: string = "class-composition-template.xlsx"; 

    static convertDateToString(date: Date): string {
        var datePipe = new DatePipe("en-UK")
        return datePipe.transform(date, 'yyyy/MM/dd')
    }

    static useExcel(file: File): Observable<any> {
        //var fileUpload = <any> document.getElementById("excelFile");
        return Observable.create((observer) => {
        
            //Validate whether File is valid Excel file.
            var regex = /^(.*)+(.xls|.xlsx)$/;
            if (regex.test(file.name)) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();

                    //For Browsers other than IE.
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e: any) {
                            observer.next(CONSTANTS.ProcessExcel(e.target.result));
                            observer.complete();
                        };
                        reader.readAsBinaryString(file);
                    } else {
                        //For IE Browser.
                        reader.onload = function (e: any) {
                            var data = "";
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            observer.next(CONSTANTS.ProcessExcel(data));
                            observer.complete();
                        };
                        reader.readAsArrayBuffer(file);
                    }
                } else {
                    observer.error("This browser does not support HTML5");
                    observer.complete();
                }
            } else {
                alert("Please upload a valid Excel file.");
                observer.error("Please upload a valid Excel file.");
                observer.complete();
            }
        })
    }


    private static ProcessExcel(data) {
        //Read the Excel File data.
        var workbook = XLSX.read(data, {
            type: 'binary'
        });

        //Fetch the name of First Sheet.
        var firstSheet = workbook.SheetNames[0];

        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

        return excelRows;

    };

}


