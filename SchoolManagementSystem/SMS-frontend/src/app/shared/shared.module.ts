import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LoaderMainComponent } from './components/loader-main/loader-main.component';
import { LoaderSmallComponent } from './components/loader-small/loader-small.component';
import { ServiceBoxService } from './components/service-box/service-box.service';
import { MessageComponent } from './components/message/message.component';
import { MyTableComponent } from './components/my-table/my-table.component';
import { ModalComponent } from './components/modal/modal.component';
import { ServiceBoxComponent } from './components/service-box/service-box.component';
import { SharedService } from './shared.service';
import { TimetableComponent } from './components/timetable/timetable/timetable.component';

@NgModule({
    declarations: [
        LoaderMainComponent,
        LoaderSmallComponent,
        MessageComponent,
        MyTableComponent,
        ModalComponent,
        ServiceBoxComponent,
        TimetableComponent
    ],
    exports: [
        LoaderMainComponent,
        LoaderSmallComponent,
        MessageComponent,
        MyTableComponent,
        ModalComponent,
        ServiceBoxComponent,
        TimetableComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
    providers: [ServiceBoxService, SharedService]
})
export class SharedModule { }