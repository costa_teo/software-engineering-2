import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Routes } from '../core/routes';
import { Observable } from 'rxjs';
import { GeneralCommunication } from '../entities/generalCommunication';
import { SupportMaterial } from '../entities/supportMaterial';

@Injectable({
    providedIn: 'root'
})
export class SharedService {

    private baseURL: string;

    constructor(private http: HttpClient) {
        this.baseURL = Routes.baseURL + '/user';
    }

    public downloadAttachment(id: number): Observable<Blob> {
        let param = new HttpParams().set('id', id.toString());
        return this.http.get(this.baseURL + '/downloadAttachment', { params: param, responseType: 'blob' });
    }

    public downloadSupportMaterial(id: number): Observable<Blob> {
          let param = new HttpParams().set('id', id.toString());
          return this.http.get(this.baseURL + '/downloadSupportMaterial', { params: param, responseType: 'blob' });
      }

    public  getSMaterialByClassAndSubject(classId:number, subjectId:number){
        let httpParams = {
            params: new HttpParams()
              .set('classId', classId.toString())
              .set('subjectId', subjectId.toString())
          };
          return this.http.get<Array<SupportMaterial>>(this.baseURL + "/getSMaterialByClassIdAndSubjectId", httpParams);
    }

    public getGeneralCommunications(): Observable<Array<GeneralCommunication>>{
    return this.http.get<Array<GeneralCommunication>>(this.baseURL + '/getGeneralCommunications');
  }
}