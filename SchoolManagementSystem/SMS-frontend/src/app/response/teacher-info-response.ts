export class TeacherInfoResponse {

    public name: string;
    public surname: string;
    public id: number

    constructor(name:string, surname:string, id: number){
        this.name=name;
        this.surname=surname;
        this.id = id;
    }

}