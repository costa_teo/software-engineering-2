export class StudentInfoResponse {

    public fiscalCode: string;
    public name: string;
    public surname: string;
    public gender: string;
    public birthdate: string;
    public id: number
    public entryLevel:number

    constructor(name:string, surname:string, id: number){
        this.name=name;
        this.surname=surname;
        this.id = id;
    }

}