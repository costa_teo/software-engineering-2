import { Attendance } from '../entities/attendance';


export class AttendanceByStudentResponse{
    public length: number;
    public attendances: Array<Attendance>;
}