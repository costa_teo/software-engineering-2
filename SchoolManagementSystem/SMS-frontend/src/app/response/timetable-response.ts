import { TimetableRow } from '../shared/components/timetable/timetable/timetableRow'

export class TimetableResponse {
    
    public timetable: Array<TimetableRow>
    public className: string

    constructor() {
    }
}