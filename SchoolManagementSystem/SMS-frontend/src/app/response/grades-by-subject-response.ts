import { Grade } from '../entities/grade';

export class GradesBySubjectResponse {
    public average: number;
    public subject: string;
    public teacher: string
    public grades : Array<Grade>;
    
    constructor() {
        
    }
}