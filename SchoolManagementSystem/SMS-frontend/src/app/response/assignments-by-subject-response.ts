import { Assignment } from '../entities/assignment';

export class AssignmentsBySubjectResponse{
    public subject:string;
    public teacher:string;
    public assignments: Array<Assignment>;
}