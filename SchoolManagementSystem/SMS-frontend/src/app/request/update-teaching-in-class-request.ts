import { Class } from '../entities/class';
import { TeacherInfoResponse } from '../response/teacher-info-response';
import { Subject } from '../entities/subject';


export class UpdateTeachingInClassRequest {
    public classId: number;
    public subjectId: number;
    public teacher : TeacherInfoResponse;
    
    constructor(c: number, s: number, teacher: TeacherInfoResponse){
        this.classId = c;
        this.subjectId = s;
        this.teacher = teacher;
    }
}