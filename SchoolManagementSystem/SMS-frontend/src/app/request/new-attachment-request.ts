export class NewAttachmentRequest {
    public name: string;
    public file: File;
    public assignment_ref: number;
}