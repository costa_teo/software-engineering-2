import { Student } from '../entities/student';
import { Parent } from '../entities/parent';

export class AddParentStudentRequest {
    private parent1: Parent;
    private parent2: Parent;
    private student: Student;
    constructor(parent1: Parent, parent2: Parent, student: Student){
        this.parent1 = parent1;
        this.parent2 = parent2;
        this.student = student;
    }
}