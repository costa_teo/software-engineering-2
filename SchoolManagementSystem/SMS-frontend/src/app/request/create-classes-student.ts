

export class CreateClassesStudent {
    private student: String;
    private classe: String;

    constructor(student: String, classe: String){
        this.student = student;
        this.classe = classe;
    }
}