import { NewGrade } from '../entities/newGrade';

export class InsertGradeRequest {
    public grades: Array <NewGrade>;
    public description: string;
    public data: string;
    public timeslot: number;
    public subjectId: number;
    public teacherId: number;

    constructor(grades: Array <NewGrade>, description: string, data: string, timeslot: number, subjectId: number, teacherId: number){
        this.grades=grades;
        this.description=description;
        this.data=data;
        this.timeslot=timeslot;
        this.subjectId=subjectId;
        this.teacherId=teacherId;
    }

}