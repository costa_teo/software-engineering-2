import { Class } from '../entities/class';
import { StudentInfoResponse } from '../response/student-info-response';

export class UpdateClassRequest {
    public c: Class;
    public students : Array<StudentInfoResponse>;
    
    constructor(c: Class, students: Array<StudentInfoResponse>){
        this.c = c;
        this.students = students;
    }
}