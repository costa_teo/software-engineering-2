export class NewSupportMaterialRequest {
    public name: string;
    public file: File;
    public subject_ref: number;
    public class_ref: number;
}