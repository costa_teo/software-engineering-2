import { TimetableRow } from '../shared/components/timetable/timetable/timetableRow'

export class TimeTableRequest {
    
    public timetable: Array<TimetableRow>
    public classId: number

    constructor() {
    }
}