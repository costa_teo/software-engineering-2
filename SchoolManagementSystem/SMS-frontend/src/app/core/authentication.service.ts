import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../entities/user';
import { Routes } from './routes';
import { Router } from '@angular/router';
import { ROLES } from '../shared/constants';
import { LoginRequest } from '../request/login-request';

@Injectable()
export class AuthenticationService implements HttpInterceptor {

    private currentUserSubject: BehaviorSubject<User>;
    private baseURL: string;

    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();

        this.baseURL = Routes.baseURL + '/authentication';
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(request: LoginRequest): Observable<User> {
        return this.http.post<User>(this.baseURL + '/login', request);
    }

    saveUser(user: User){
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));        
        this.currentUserSubject.next(user);
        this.redirectToUserHome();     
    }

    logout() {
        if (!this.currentUserSubject.value) {
            this.router.navigateByUrl(Routes.HOME_LOGIN);
            return;
        }

        this.http.post(this.baseURL + '/logout', this.currentUserSubject.value, { observe: 'response' }).subscribe(
            data => {
                localStorage.removeItem('currentUser');
                this.currentUserSubject.next(null);
                this.router.navigateByUrl(Routes.HOME_LOGIN);
            },
            err => {
                console.error("TODO");
            }
        );
    }

    authenticateUser(): Observable<number> {
        return this.http.get<number>(this.baseURL + '/authenticateUser');
    }

    /**
     * 
     * Do NOT call this method, internal only
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (JSON.parse(localStorage.getItem('currentUser')) && JSON.parse(localStorage.getItem('currentUser')).token) {
            request = request.clone(
                {
                    setHeaders: {
                        token: JSON.parse(localStorage.getItem('currentUser')).token
                    }
                }
            );
        }

        return next.handle(request);
    }

    redirectToUserHome() {
        let role: string = JSON.parse(localStorage.getItem('currentUser')).role;
        if (role == ROLES.PARENT) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
        } else if (role == ROLES.TEACHER) {
            this.router.navigateByUrl(Routes.TEACHER_HOME);
        } else if (role == ROLES.ADMINISTRATOR) {
            this.router.navigateByUrl(Routes.ADMINISTRATOR_HOME);
        } else {
            return;
        }
    }

    public changePassword(newPassword: string): Observable<any>{
       return this.http.post(this.baseURL + '/change-password', newPassword, { observe: 'response' })
      }

}