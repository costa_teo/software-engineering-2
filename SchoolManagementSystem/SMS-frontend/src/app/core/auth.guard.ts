import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Routes } from './routes';
import { ROLES } from '../shared/constants';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const currentUser = this.authenticationService.currentUserValue;

        if (!currentUser || !currentUser.role || !currentUser.token) {
            this.router.navigateByUrl(Routes.HOME_LOGIN);
            return false;
        }

        if (route.url[0].path == 'parent' && currentUser.role == ROLES.PARENT) {
            return true;
        }

        if (route.url[0].path == 'teacher' && currentUser.role == ROLES.TEACHER) {
            return true;
        }

        if (route.url[0].path == 'administrator' && currentUser.role == ROLES.ADMINISTRATOR) {
            return true;
        }

        this.router.navigateByUrl(Routes.HOME_LOGIN);
        return false;
    }

}