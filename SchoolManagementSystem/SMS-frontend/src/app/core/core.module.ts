import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar/navbar.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        NavbarComponent,
    ],
    exports: [
        NavbarComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
    ],
    providers: []
})
export class CoreModule { }
