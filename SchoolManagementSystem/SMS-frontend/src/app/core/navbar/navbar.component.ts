import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { ParentService } from 'src/app/modules/parent/parent.service';
import { User } from 'src/app/entities/user';
import { ServiceBoxService } from 'src/app/shared/components/service-box/service-box.service';
import { ServiceBoxData } from 'src/app/shared/components/service-box/service-box-data';
import { Routes } from '../routes';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  @Output() protected onSurprise: EventEmitter<any> = new EventEmitter<any>();

  public isCollapsed: boolean = false;
  public user: User;
  public services: Array<ServiceBoxData> = new Array<ServiceBoxData>();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private parentService: ParentService,
    private serviceBoxService: ServiceBoxService) { }

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(user => {
      this.user = user;

      if(user) {
        this.services = this.serviceBoxService.getServicesByRole(user.role);
      }
    });   
  }

  toggleNavbar() {
    this.isCollapsed = !this.isCollapsed;
  }

  goToHome() {
    this.parentService.setStudent(null);
    this.authenticationService.redirectToUserHome();
  }

  goToAction(serviceNumber: number) {
    this.router.navigateByUrl(this.services[serviceNumber].redirectionPath);
  }

  doLogout() {
    this.parentService.setStudent(null);
    this.authenticationService.logout();
    this.user = null;
    this.services = new Array<ServiceBoxData>();
  }

  doChangePassoword(){
    this.router.navigateByUrl(Routes.CHANGE_PASSWORD);
  }

  easterEgg() {
    this.onSurprise.emit();
  }

}
