import { environment } from 'src/environments/environment';

export class Routes {
    public static baseURL: string = environment.serverBaseURL;
    public static templates: string = environment.serverBaseURL + '/template';


    //HOME ROUTES
    public static HOME_LOGIN: string = "login";
    public static CHANGE_PASSWORD: string = "change-password";

    //PARENT ROUTES
    public static PARENT_HOME: string = "parent/home";
    public static PARENT_CHOICE: string = "parent/choice";
    public static PARENT_GRADES: string = "parent/grades";
    public static PARENT_ASSIGNMENTS: string= "parent/assignments";
    public static PARENT_ATTENDANCES: string = "parent/attendances";
    public static PARENT_SUPPORT_MATERIAL: string = "parent/supportMaterial";
    public static PARENT_TIMETABLE: string = "parent/timetable";

    //TEACHER ROUTES
    public static TEACHER_HOME: string = "teacher/home";
    public static TEACHER_TOPICS: string = "teacher/topics";
    public static TEACHER_GRADES: string = "teacher/grades";
    public static TEACHER_ABSENCES: string = "teacher/attendances";
    public static TEACHER_ASSIGNMENTS: string = "teacher/assignments";
    public static TEACHER_SUPPORT_MATERIAL: string = "teacher/supportMaterial"
    public static TEACHER_TIMETABLE: string = "teacher/timetable";

   
    //ADMIN ROUTES
    public static ADMINISTRATOR_HOME: string = "administrator/home";
    public static ADMINISTRATOR_ADD_USER: string = "administrator/addUser";
    public static ADMINISTRATOR_ENTER_CLASS_COMPOSITION: string="administrator/enterClassComposition"
    public static ADMINISTRATOR_GENERAL_COMMUNICATION: string="administrator/generalCommunication"
    public static ADMINISTRATOR_ASSIGN_TEACHER: string="administrator/assignTeacher"
    public static ADMINISTRATOR_MANAGE_TIMETABLES: string="administrator/manageTimetables"
    
    //SUPER ADMIN
    public static SUPER_ADMINISTRATOR_ADD_ACCOUNT: string="administrator/addAccount"
}