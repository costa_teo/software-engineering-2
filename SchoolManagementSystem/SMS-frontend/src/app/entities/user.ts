export class User {
    
    public id: number;
    public name: string;
    public surname: string;
    public fiscalCode: string;
    public gender: string;
    public birthdate: string;
    public telephoneN: string;
    public role: string;
    public email: string;
    public token: string;
    public superAdmin: boolean;

    constructor(){
        this.telephoneN = "";
        this.name = "";
        this.surname = "";
        this.email = "";
        this.fiscalCode = "";
        this.gender = "";
        this.role=""
    }

}