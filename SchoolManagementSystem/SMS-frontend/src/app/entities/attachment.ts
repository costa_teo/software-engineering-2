export class Attachment {

    public fileName: string;
    public id: number

    constructor(name:string, id: number){
        this.fileName=name;
        this.id = id;
    }

}