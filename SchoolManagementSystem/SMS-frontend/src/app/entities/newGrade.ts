export class NewGrade {

    public studentId:number
    public name: string;
    public surname: String;
    public value: number;

    constructor(studentId:number, name:string, surname:string, value:number){
        this.studentId=studentId;
        this.name=name;
        this.surname=surname;
        this.value=value;
    }

}