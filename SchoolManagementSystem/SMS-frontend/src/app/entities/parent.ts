export class Parent {

    public fiscalCode: string
    public name: string;
    public surname: string;
    public gender: string;
    public birthdate: string;
    public telephoneN: string;
    public email: string;

    constructor(fiscalCode: string){  
        this.fiscalCode = fiscalCode;
        this.name = "";
        this.surname = "";
        this.gender = "";
        this.birthdate = null;
        this.telephoneN = "";
        this.email = "";
    }

}