export class Grade {

    public id: number;
    public date : string;
    public subject : string;
    public teacher : string;                          
    public value : number;
    public description: string;
    public timeslot: number;

    constructor(id: number, date: string, timeslot: number, subject: string, teacher: string, value: number ,description: string) {
        this.id = id;
        this.date = date; 
        this.subject = subject; 
        this.teacher = teacher;                           
        this.value = value;
        this.description = description; 
        this.timeslot = timeslot
    }
}