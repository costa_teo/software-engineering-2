export class Student {

    public fiscalCode: string;
    public name: string;
    public surname: string;
    public gender: string;
    public birthdate: string;
    public id: number;
    public entryLevel:number;

    constructor(){
        this.fiscalCode = "";
        this.name = "";
        this.surname = "";
        this.gender = "";
        this.birthdate = null;
        this.id = -1;
        this.entryLevel = 0;
    }

}