import { Attendance } from './attendance';

export class StudentAttendance  {
    
     studentId: number;
	 studentName: string;
	 attendance1: Attendance;
	 attendance2: Attendance;


    constructor(studentId: number, studentName: string, attendance1: Attendance, attendance2: Attendance) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.attendance1 = attendance1;
        this.attendance2 = attendance2;        
    }
}