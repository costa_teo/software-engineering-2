export class Teacher {

    public id: number;
    public fiscalId: string
    public gender: string;
    public bDate: Date;
    public telephoneN: number;

    constructor(fiscalId: string){  
        this.fiscalId = fiscalId;
    }

}