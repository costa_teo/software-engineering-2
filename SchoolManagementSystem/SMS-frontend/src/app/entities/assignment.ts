export class Assignment {

    public id: number
    public date: string;
    public description: string;
    public subjectName: string;
    public teacherName: string;
    public className: string;
    public hasAttachment: boolean;

    constructor(id: number, date?: string, description?: string, sName?: string, tName?: string, cName?: string,  hasAttachment?:boolean){
        this.id=id;
        this.date=date;
        this.description=description;
        this.subjectName=sName;
        this.teacherName=tName;
        this.className=cName;
        this.hasAttachment=hasAttachment;
    }

}