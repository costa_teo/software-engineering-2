export class Lecture {

    public id:number
    public date: string;
    public className: string;
    public teacher: string;
    public subject: string;
    public description: string;
    public timeSlot: number


    constructor(id:number, date:string, className : string, subject : string, descritpion : string, teacher :string, timeSlot:number){
        this.date= date;
        this.className=className;
        this.subject=subject;
        this.description=descritpion;
        this.teacher=teacher;
        this.id = id;
        this.timeSlot = timeSlot
    }

}