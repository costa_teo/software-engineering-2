export class Attendance {

    date: string;
    timeslot: number
    type: AttendanceType // (absence, enter, exit)


    constructor(date: string, timeslot: number, type: AttendanceType){
        this.date = date;
        this.timeslot = timeslot;
        this.type = type;
    }

 
}

export enum AttendanceType {
    ABSENCE = "Absence" , 
    ENTER = "Enter" , 
    EXIT = "Exit" }
