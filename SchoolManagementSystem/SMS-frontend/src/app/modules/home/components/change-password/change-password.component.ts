import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { User } from 'src/app/entities/user';
import { LoginRequest } from 'src/app/request/login-request';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})

export class ChangePasswordComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  loaded: boolean;
  newPassword1 : string;
  newPassword2 : string;
  currentPassword : string;
  

  constructor(private authenticationService: AuthenticationService) { }

  
  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if(!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
        }
      },
      err => {
        console.error("User not logged");
        this.authenticationService.logout();
      }
    );
  }


  changePassword(){
    this.loaded = false;
    this.authenticationService.changePassword(this.newPassword1).subscribe(
      data => {
          this.childMessage.showMessage("Password updated", MESSAGE_TYPE.SUCCESS);
          window.setTimeout(() => {
            this.authenticationService.redirectToUserHome();            
          }, 2000);
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
      }
    );
  }


}

