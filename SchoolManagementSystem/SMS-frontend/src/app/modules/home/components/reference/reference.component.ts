import { Component, OnInit } from '@angular/core';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';

import { CONSTANTS } from 'src/app/shared/constants';
import { TimetableRow } from 'src/app/shared/components/timetable/timetable/timetableRow';

@Component({
  selector: 'app-reference',
  templateUrl: './reference.component.html',
  styleUrls: ['./reference.component.scss']
})
export class ReferenceComponent implements OnInit {

  mockData: Array<any> = [
    { 'c1': 'Element 1.1', 'c2': 'C', 'c3': 'Element 1.3', 'c4': 'Element 1.4' },
    { 'c1': 'Element 2.1', 'c2': 'A', 'c3': 'Element 2.3', 'c4': 'Element 2.4', 'isEditable': false, 'isDeletable': false },
    { 'c1': 'Element 3.1', 'c2': 'B', 'c3': 'Element 3.3', 'c4': 'Element 3.4', 'isEditable': false }
  ];

  mockSettings: Settings = {
    columns: [{
      columnName: 'COL 1',
      attributeName: 'c1',
      isEditable: true,
      isSortable: true
    }, {
      columnName: 'COL 2',
      attributeName: 'c2',
      isSortable: true
    }, {
      columnName: 'COL 3',
      attributeName: 'c3',
      isEditable: true
    }, {
      columnName: 'COL 4',
      attributeName: 'c4'
    }],
    hasEdit: true,
    hasDelete: true,
    hasSorting: true,
    elementsPerPage: 2
  };

  timetabledata: Array<TimetableRow> =  [
    {monday: "storia1", tuesday: "geografia", wednesday: "matematica", thursday: "chimica", friday:  "fisica"},
    {monday: "storia1", tuesday: "geografia", wednesday: "matematica", thursday: "chimica", friday:  "fisica"},
    {monday: "storia1", tuesday: "geografia", wednesday: "matematica", thursday: "chimica", friday:  "fisica"},
    {monday: "storia1", tuesday: "geografia", wednesday: "matematica", thursday: "chimica", friday:  "fisica"},
    {monday: "storia1", tuesday: "geografia", wednesday: "matematica", thursday: "chimica", friday:  "fisica"},
    {monday: "storia1", tuesday: "geografia", wednesday: "matematica", thursday: "chimica", friday:  "fisica"},
  ]

  public loaded: boolean;

  constructor() { }

  ngOnInit() {
    this.loaded = true;
  }

  myFuncEdit(updatedElement: { value: any, index: number }) {
    this.mockData[updatedElement.index] = updatedElement.value;
  }

  myFuncDel(index: number) {
    this.mockData.splice(index, 1);
  }

/** this is what you neeed for excel */
  useExcel(file: File){
    CONSTANTS.useExcel(file).subscribe(
      data=> console.log(data),
      error => {
        console.log(error);
      }
    )
    
  }
  /** end excel  */



  

}


