import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { User } from 'src/app/entities/user';
import { LoginRequest } from 'src/app/request/login-request';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';

@Component({
  selector: 'app-home-login',
  templateUrl: './home-login.component.html',
  styleUrls: ['./home-login.component.scss']
})
export class HomeLoginComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  public username: string;
  public password: string;
  public role: string;
  public roleButton: Array<Boolean>;
  public loaded: boolean;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.loaded = false;
    const user: User = this.authenticationService.currentUserValue;

    if (user) {
      this.authenticationService.redirectToUserHome();
    }
    this.roleButton=Array<Boolean>(3).fill(false);
    this.loaded = true;
  }

  login() {
    this.loaded = false;
    let loginRequest: LoginRequest = new LoginRequest(this.username, this.password, this.role)
    this.authenticationService.login(loginRequest).subscribe(
      user => {
        this.authenticationService.saveUser(user);
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Wrong credentials!", MESSAGE_TYPE.ERROR);
      }
    );
  }

}
