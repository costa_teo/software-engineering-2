import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Routes } from 'src/app/core/routes';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { User } from 'src/app/entities/user';

@Injectable()
export class HomeService {

  private baseURL: string;

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService) {
    this.baseURL = Routes.baseURL + '/user';
  }

  public firstService(user: User): Observable<any> {
    return this.http.post(this.baseURL + '/insert', user);
  }

  public secondService(num: number): Observable<User> {
    let httpParams = { params: new HttpParams().set('id', num.toString()) };
    return this.http.get<User>(this.baseURL + '/get', httpParams);
  }

  public thirdService(): Observable<number> {
    return this.http.get<number>(this.baseURL + '/thirdService');
  }

  public fourthService(): Observable<User> {
    return this.http.get<User>(this.baseURL + '/fourthService');
  }
}
