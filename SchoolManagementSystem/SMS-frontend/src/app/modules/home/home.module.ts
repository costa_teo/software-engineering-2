import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HomeLoginComponent } from './components/home-login/home-login.component';
import { HomeService } from './home.service';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReferenceComponent } from './components/reference/reference.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';

@NgModule({
  declarations: [
    HomeLoginComponent,
    ReferenceComponent,
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    HomeRoutingModule
  ],
  providers: [HomeService]
})
export class HomeModule { }
