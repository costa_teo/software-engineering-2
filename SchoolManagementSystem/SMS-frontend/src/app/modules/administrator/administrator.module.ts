import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdministratorRoutingModule } from './administrator-routing.module';
import { AdministratorHomeComponent } from './components/administrator-home/administrator-home.component';
import { AdministratorAddUserComponent } from './components/administrator-add-user/administrator-add-user.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdministratorEnterClassCompositionComponent } from './components/administrator-enter-class-composition/administrator-enter-class-composition.component';
import { AdministratorGeneralCommunicationComponent } from './components/administrator-general-communication/administrator-general-communication.component';
import { AdministratorAssignTeacherComponent } from './components/administrator-assign-teacher/administrator-assign-teacher.component';
import { AdministratorAddAccountComponent } from './components/administrator-add-account/administrator-add-account.component';
import { AdministratorManageTimetablesComponent } from './components/administrator-manage-timetables/administrator-manage-timetables.component';

@NgModule({
  declarations: [AdministratorHomeComponent, AdministratorAddUserComponent, 
    AdministratorEnterClassCompositionComponent, AdministratorGeneralCommunicationComponent,
     AdministratorAssignTeacherComponent, AdministratorAddAccountComponent, AdministratorManageTimetablesComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    AdministratorRoutingModule
  ]
})
export class AdministratorModule { }
