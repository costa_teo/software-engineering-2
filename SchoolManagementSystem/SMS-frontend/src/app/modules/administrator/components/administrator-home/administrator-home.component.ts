import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Router } from '@angular/router';
import { Routes } from 'src/app/core/routes';
import { ServiceBoxData } from 'src/app/shared/components/service-box/service-box-data';
import { ServiceBoxService } from 'src/app/shared/components/service-box/service-box.service';
import { GeneralCommunication } from 'src/app/entities/generalCommunication';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-administrator-home',
  templateUrl: './administrator-home.component.html',
  styleUrls: ['./administrator-home.component.scss']
})
export class AdministratorHomeComponent implements OnInit {
  
  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;
  
  public loaded: boolean;
  public loadingTable: boolean;
  public generalCommunications: Array<GeneralCommunication>;
  public services: Array<ServiceBoxData> = new Array<ServiceBoxData>();

  GCViewTableSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Description',
      attributeName: 'description',
    }],
    hasSorting: true,
    elementsPerPage: 10
  }

  constructor(
    private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private router: Router,
    private serviceBoxService: ServiceBoxService) { }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if(!data) {
          this.authenticationService.logout();
        } else {
          this.services = this.serviceBoxService.administratorServices;
          this.loadGC();
          this.loaded = true;
        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      }
    );
  }

  
  loadGC(){
    this.loadingTable=true;
    this.sharedService.getGeneralCommunications().subscribe(
      data => {
        if (data instanceof Array) {
          this.generalCommunications = data;
          this.loadingTable = false;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  selectService(route: string) {
    this.router.navigateByUrl(route);
  }
}