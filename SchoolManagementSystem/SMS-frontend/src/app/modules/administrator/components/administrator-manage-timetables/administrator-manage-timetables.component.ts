import { Component, OnInit, ViewChild } from '@angular/core';
import { AdministratorService } from '../../administrator.service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { CONSTANTS } from 'src/app/shared/constants';
import { TimetableRow } from 'src/app/shared/components/timetable/timetable/timetableRow';
import { TeacherInfoResponse } from 'src/app/response/teacher-info-response';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { TimeTableRequest } from 'src/app/request/timetable-request';
import { Class } from 'src/app/entities/class';

@Component({
  selector: 'app-administrator-manage-timetables',
  templateUrl: './administrator-manage-timetables.component.html',
  styleUrls: ['./administrator-manage-timetables.component.scss']
})
export class AdministratorManageTimetablesComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;


  timetabledata: Array<TimetableRow>

  public loaded: boolean;
  public timetableFlag: boolean;
  public teacherOrClassChoice = null;
  public teacherOrClassList: { name: string, id: number }[] = null;
  public selectedTeacherOrClassId: number = null;
  public loadedTeacherOrClassList: boolean = true;
  public timetabledataLoaded: boolean = true;

  public selectedClassId: number = null
  public classList: Class[]

  constructor(private authenticationService: AuthenticationService,
    private administratorService: AdministratorService) {

  }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.downloadTeacherList()
          this.loaded = true;
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  downloadTeacherList() {
    this.administratorService.getAllClasses().subscribe(data => {
      this.classList = data;
      this.loaded = true
    }, err => {
      this.childMessage.showMessage("Unexpeced error", MESSAGE_TYPE.ERROR)
    })
  }

  downloadTemplate() {
    this.administratorService.downloadTemplate(CONSTANTS.TIMETABLE_TEMPLATE_URL);
  }

  useExcel(file: File) {
    this.timetableFlag = false;
    CONSTANTS.useExcel(file).subscribe(rows => {
      this.timetabledata = rows;
      this.timetableFlag = true
    },
      error => {
        console.log(error);
      })

  }

  loadTeachersOrClasses() {
    this.selectedTeacherOrClassId = null
    this.teacherOrClassList = []
    this.loadedTeacherOrClassList = false;
    this.timetableFlag = false;

    if (this.teacherOrClassChoice == "class") {
      this.teacherOrClassList = this.classList
      this.loadedTeacherOrClassList = true;
    } else {
      this.administratorService.getAllTeachers().subscribe(data => {
        data.forEach(teacher => {
          this.teacherOrClassList.push({ name: teacher.name + " " + teacher.surname, id: teacher.id })
          this.loadedTeacherOrClassList = true
        })
      }, err => {
        this.childMessage.showMessage("Unexpeced error", MESSAGE_TYPE.ERROR);
        this.loadedTeacherOrClassList = true;
      });
    }
  }


loadTeacherOrClassTimetable(){
  this.timetableFlag = false;
  this.timetabledataLoaded = false;
  this.timetabledata = []

  if (this.teacherOrClassChoice == "teacher") {
    this.administratorService.getTimetableByTeacher(this.selectedTeacherOrClassId).subscribe(data => {
      this.timetabledata = data
      this.timetabledataLoaded = true;
      this.timetableFlag = true;
    }, erro => {
      this.childMessage.showMessage("Unexpeced error", MESSAGE_TYPE.ERROR);
      this.timetabledataLoaded = true;
    });

  } else {
    this.administratorService.getTimetableByClass(this.selectedTeacherOrClassId).subscribe(data => {
      this.timetabledata = data
      this.timetabledataLoaded = true;
      this.timetableFlag = true;
    }, error => {
      this.childMessage.showMessage("Unexpeced error", MESSAGE_TYPE.ERROR);
      this.timetabledataLoaded = true;
    });
  }

  this.timetableFlag = true
}

cleanData(){
  this.selectedClassId = null;
  this.timetabledata = null;
  this.timetableFlag = false;
  this.teacherOrClassChoice = null;
  this.teacherOrClassList = null;
  this.selectedTeacherOrClassId = null;
}

cancelNewTimetable(){
  this.timetabledata = null;
  this.timetableFlag = false;
}

saveNewTimetable(){
  let req = new TimeTableRequest();
  req.classId = this.selectedClassId
  req.timetable = this.timetabledata
  this.administratorService.addClassTimetable(req).subscribe(data => {
    this.childMessage.showMessage("Timetable saved on server", MESSAGE_TYPE.SUCCESS);
  }, err => {
    this.childMessage.showMessage(err.error.message, MESSAGE_TYPE.ERROR);
    console.error(err);
  });
}

  }
