import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Parent } from 'src/app/entities/parent';
import { Student } from 'src/app/entities/student';
import { AdministratorService } from '../../administrator.service';
import { AddParentStudentRequest } from 'src/app/request/add-parent-student-request';
import { Routes } from 'src/app/core/routes';
import { Router } from '@angular/router';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { CONSTANTS } from 'src/app/shared/constants';


@Component({
  selector: 'app-administrator-add-user',
  templateUrl: './administrator-add-user.component.html',
  styleUrls: ['./administrator-add-user.component.scss']
})
export class AdministratorAddUserComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  public loaded: boolean;
  public isShow1: boolean;
  public isShow2: boolean;
  public isShow3: boolean;
  public exist1: boolean;
  public exist2: boolean;
  public exist3: boolean;
  public checked1: boolean;
  public checked2: boolean;
  public checked3: boolean;
  public p2Edit: boolean;
  public p1: Parent;
  public p2: Parent;
  public s: Student;

  constructor(
    private authenticationService: AuthenticationService,
    private administratorService: AdministratorService,
    private router: Router) {

  }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.p1 = new Parent("");
          this.p1.gender="M";
          this.p2 = new Parent("");
          this.p2.gender="F";
          this.s = new Student();
          this.s.gender="M"
          this.isShow1 = false;
          this.isShow2 = false;
          this.isShow3 = false;
          this.exist1 = false;
          this.exist2 = false;
          this.exist3 = false;
          this.checked1 = false;
          this.checked2 = false;
          this.checked3 = false;
          this.p2Edit = false;
          this.loaded = true;

        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      }
    );
  }

  editClick() {
    this.p2Edit = !this.p2Edit;
  }

  checkP1() {
    this.loaded = false;
    let parent1FiscalId = this.p1.fiscalCode;
    if (parent1FiscalId == null || parent1FiscalId == "" ) {
      this.isShow1 = false;
      this.loaded = true;
      this.childMessage.showMessage("PARENT 1 INSERT FISCALCODE", MESSAGE_TYPE.ERROR)
      return;
    }
    if(!parent1FiscalId.match(/[A-Za-z0-9]{16}/)){
      this.isShow1 = false;
      this.loaded = true;
      this.childMessage.showMessage("PARENT 1 INSERT FISCALCODE OF 16 CHARACTERS", MESSAGE_TYPE.ERROR)
      return;
    }
    this.administratorService.checkParent(parent1FiscalId).subscribe(
      data => {
        this.isShow1 = !data;
        this.exist1 = data;
        this.checked1 = true;
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        console.error("TODO");
        this.childMessage.showMessage("Unexpected errror", MESSAGE_TYPE.ERROR)
      }
    )
  }
  checkP2() {
    this.loaded = false;
    let parent2FiscalId = this.p2.fiscalCode;
    if (parent2FiscalId == null || parent2FiscalId == "") {
      this.isShow2 = false;
      this.loaded = true;
      this.childMessage.showMessage("PARENT 2 INSERT FISCALCODE", MESSAGE_TYPE.ERROR)
      return;
    }
    if (!parent2FiscalId.match(/[A-Za-z0-9]{16}/)) {
      this.isShow2 = false;
      this.loaded = true;
      this.childMessage.showMessage("PARENT 2 INSERT FISCALCODE OF 16 CHARACTERS", MESSAGE_TYPE.ERROR)
      return;
    }
    this.administratorService.checkParent(parent2FiscalId).subscribe(
      data => {
        this.isShow2 = !data;
        this.exist2 = data;
        this.checked2 = true;
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Unexpected errror", MESSAGE_TYPE.ERROR)
        console.error("TODO");
      }
    )
  }

  checkS() {
    this.loaded = false;
    let studentFiscalId = this.s.fiscalCode;
    if (studentFiscalId == null || studentFiscalId == "") {
      this.isShow3 = false;
      this.loaded = true;
      this.childMessage.showMessage("STUDENT INSERT FISCALCODE", MESSAGE_TYPE.ERROR)
      return;
    }
    if (!studentFiscalId.match(/[A-Za-z0-9]{16}/)) {
      this.isShow3 = false;
      this.loaded = true;
      this.childMessage.showMessage("STUDENT INSERT FISCALCODE OF 16 CHARACTERS", MESSAGE_TYPE.ERROR)
      return;
    }
    this.administratorService.checkStudent(studentFiscalId).subscribe(
      data => {
        this.isShow3 = !data;
        this.exist3 = data;
        this.checked3 = true;
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        console.error("TODO");
        this.childMessage.showMessage("Unexpected errror", MESSAGE_TYPE.ERROR)
      }
    )
  }

  saveButton() {
    if (this.p2Edit) {
      let ok1: boolean = false;
      let ok2: boolean = false;
      let ok3: boolean = false;
      if (this.checked1 == true && this.checked2 == true && this.checked3 == true) {
        if (!this.exist1) {
          if (this.p1.fiscalCode != "" && this.p1.name != "" && this.p1.surname != "" && this.p1.telephoneN != ""
            && this.p1.email != "" && this.p1.gender != "" && this.p1.birthdate != null
            && this.p1.email.match(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+/)
            && this.p1.fiscalCode.match(/[A-Za-z0-9]{16}/) && this.p1.fiscalCode != this.p2.fiscalCode
            && this.p1.fiscalCode != this.s.fiscalCode) {
            ok1 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }
        else {
          if (this.p1.fiscalCode != "" && this.p1.fiscalCode != this.p2.fiscalCode
            && this.p1.fiscalCode != this.s.fiscalCode && this.p1.fiscalCode.match(/[A-Za-z0-9]{16}/)) {
            ok1 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }

        if (!this.exist2) {
          if (this.p2.fiscalCode != "" && this.p2.name != "" && this.p2.surname != "" && this.p2.telephoneN != ""
            && this.p2.email != "" && this.p2.gender != "" && this.p2.birthdate != null
            && this.p2.email.match(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+/)
            && this.p2.fiscalCode.match(/[A-Za-z0-9]{16}/)
            && this.p2.fiscalCode != this.p1.fiscalCode
            && this.p2.fiscalCode != this.s.fiscalCode) {
            ok2 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }
        else {
          if (this.p2.fiscalCode != "" && this.p2.fiscalCode != this.p1.fiscalCode
            && this.p2.fiscalCode != this.s.fiscalCode && this.p2.fiscalCode.match(/[A-Za-z0-9]{16}/)) {
            ok2 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }

        if (!this.exist3) {
          if (this.s.fiscalCode != "" && this.s.name != "" && this.s.surname != "" && this.s.gender != ""
            && this.s.birthdate != null && this.s.fiscalCode.match(/[A-Za-z0-9]{16}/)
            && this.s.fiscalCode != this.p1.fiscalCode
            && this.s.fiscalCode != this.p2.fiscalCode) {
            ok3 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }
        else {
          if (this.s.fiscalCode != "" && this.s.fiscalCode != this.p1.fiscalCode
            && this.s.fiscalCode != this.p2.fiscalCode && this.s.fiscalCode.match(/[A-Za-z0-9]{16}/)) {
            ok3 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }

        if (ok1 == true && ok2 == true && ok3 == true) {
          this.p1.birthdate = CONSTANTS.convertDateToString(new Date(this.p1.birthdate))
          this.p2.birthdate = CONSTANTS.convertDateToString(new Date(this.p2.birthdate))
          this.s.birthdate = CONSTANTS.convertDateToString(new Date(this.s.birthdate))
          this.sendemail();
        }
        else {
          this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
        }
      }
    }
    else {
      let ok1: boolean = false;
      let ok3: boolean = false;
      if (this.checked1 == true && this.checked3 == true) {
        if (!this.exist1) {
          if (this.p1.fiscalCode != "" && this.p1.name != "" && this.p1.surname != "" && this.p1.telephoneN != ""
            && this.p1.email != "" && this.p1.gender != "" && this.p1.birthdate != null
            && this.p1.email.match(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+/)
            && this.p1.fiscalCode.match(/[A-Za-z0-9]{16}/) && this.p1.fiscalCode != this.p2.fiscalCode
            && this.p1.fiscalCode != this.s.fiscalCode) {
            ok1 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }
        else {
          if (this.p1.fiscalCode != "" && this.p1.fiscalCode != this.p2.fiscalCode
            && this.p1.fiscalCode != this.s.fiscalCode && this.p1.fiscalCode.match(/[A-Za-z0-9]{16}/)) {
            ok1 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }
        if (!this.exist3) {
          if (this.s.fiscalCode != "" && this.s.name != "" && this.s.surname != "" && this.s.gender != ""
            && this.s.birthdate != null && this.s.fiscalCode.match(/[A-Za-z0-9]{16}/)
            && this.s.fiscalCode != this.p1.fiscalCode
            && this.s.fiscalCode != this.p2.fiscalCode) {
            ok3 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }
        else {
          if (this.s.fiscalCode != "" && this.s.fiscalCode != this.p1.fiscalCode 
           && this.p2.fiscalCode != this.s.fiscalCode && this.s.fiscalCode.match(/[A-Za-z0-9]{16}/)) {
            ok3 = true;
          }
          else {
            this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
          }
        }

        if (ok1 == true && ok3 == true) {
          this.p1.birthdate = CONSTANTS.convertDateToString(new Date(this.p1.birthdate))
          this.p2.birthdate = CONSTANTS.convertDateToString(new Date(this.p2.birthdate))
          this.s.birthdate = CONSTANTS.convertDateToString(new Date(this.s.birthdate))
          this.sendemail();
        }
        else {
          this.childMessage.showMessage("Wrong or missing fields", MESSAGE_TYPE.ERROR)
        }
      }
    }
  }

  sendemail() {
    let before = new Date();
    let parent2:Parent;
    this.loaded = false
    if(this.p2Edit){
      parent2=this.p2;
    }
    else{
      parent2=null;
    }
    let addParentStudentRequest: AddParentStudentRequest = new AddParentStudentRequest(this.p1, parent2, this.s)
    this.administratorService.saveParents(addParentStudentRequest).subscribe(
      user => {
        this.router.navigateByUrl(Routes.ADMINISTRATOR_ADD_USER);

        let after = new Date();
        console.log((after.valueOf() - before.valueOf()) / 1000)
        this.childMessage.showMessage("Email sent", MESSAGE_TYPE.SUCCESS)
        this.p1.birthdate = null;
        this.p1.email = ""
        this.p1.fiscalCode = ""
        this.p1.gender = ""
        this.p1.name = ""
        this.p1.surname = ""
        this.p1.telephoneN = ""
        this.p2.birthdate = null;
        this.p2.email = ""
        this.p2.fiscalCode = ""
        this.p2.gender = ""
        this.p2.name = ""
        this.p2.surname = ""
        this.p2.telephoneN = ""
        this.s.birthdate = null
        this.s.fiscalCode = ""
        this.s.gender = ""
        this.s.name = ""
        this.s.surname = ""
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Unexpected errror", MESSAGE_TYPE.ERROR)
        console.error("TODO");

      }
    );
  }


}