import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { GeneralCommunication } from 'src/app/entities/generalCommunication';
import { AdministratorService } from '../../administrator.service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { CONSTANTS } from 'src/app/shared/constants';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-administrator-general-communication',
  templateUrl: './administrator-general-communication.component.html',
  styleUrls: ['./administrator-general-communication.component.scss']
})
export class AdministratorGeneralCommunicationComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;
  
  //html view
  loaded: boolean;
  loadingTable: boolean;
  showTable: boolean = false;
  allowedDates: Array<string>;

  //data
  generalCommunications: Array<GeneralCommunication>;
  newGC: GeneralCommunication;


  constructor(private authenticationService: AuthenticationService, 
    private administrationService: AdministratorService,
    private sharedService: SharedService) { }

  GCViewTableSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Description',
      attributeName: 'description',
      isEditable: true
    }],
    hasEdit: true,
    hasDelete: true,
    hasSorting: true,
    elementsPerPage: 10
  }

  ngOnInit() {
    this.generalCommunications = [];
    this.newGC = new GeneralCommunication(0, "", "");

    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        }
        this.loaded = true;
        this.computeValidDates();
      },
      err => {
        console.error(err);
        this.authenticationService.logout();
      }
    );
  }

  loadGC(){
    this.showTable=false;
    this.loadingTable=true;
    this.sharedService.getGeneralCommunications().subscribe(
      data => {
        if (data instanceof Array) {
          this.generalCommunications = data;
          
          var minDate = new Date(this.allowedDates[0]);
          var maxDate = new Date(this.allowedDates[this.allowedDates.length - 1])
          for (var g of this.generalCommunications) {
            var gcDate = new Date(g.date)
            g['isEditable'] = (minDate.valueOf() <= gcDate.valueOf() && gcDate.valueOf() <= maxDate.valueOf())
            g['isDeletable'] = g['isEditable'];
          }
          this.loadingTable = false;
          this.showTable = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  cancelInsertedData() {
    this.newGC = new GeneralCommunication(0, "", "");
  }

  insertCommunication(){
    this.loaded = false;
    this.administrationService.insertGeneralCommunication(this.newGC).subscribe(
      data => {
        this.loaded = true;
        this.childComponent.showMessage("Communication correctly inserted", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
        this.loaded=true;
      }
    );
  }

  updateCommunication(wrapper:any){
    let updatedCommunication = wrapper.value
    let index = wrapper.index
    this.generalCommunications[index] = updatedCommunication
    this.administrationService.updateCommunication(updatedCommunication).subscribe(
      data => {
        this.childComponent.showMessage("Communication correctly updated", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );
  }

  deleteCommunication(index: number){
      let communication = this.generalCommunications[index]
      this.loaded = false;
      this.administrationService.deleteCommunication(communication).subscribe(
        data => {
          this.childComponent.showMessage("Communication correctly deleted", MESSAGE_TYPE.SUCCESS);
          this.generalCommunications.splice(index, 1);
          this.loaded = true;
        },
        err => {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error(err);
        }
      );
  }

  
  computeValidDates() {
    let now = new Date();
    let i = (now.getDay() == 0) ? 7 : now.getDay();

    let firstMonday = new Date();
    firstMonday.setDate(now.getDate() - i + 1);

    this.allowedDates = [];
    for (let j = 0; j < i; j++) {
      this.allowedDates.push(CONSTANTS.convertDateToString(firstMonday));
      firstMonday.setDate(firstMonday.getDate() + 1);
    }
  }

}
