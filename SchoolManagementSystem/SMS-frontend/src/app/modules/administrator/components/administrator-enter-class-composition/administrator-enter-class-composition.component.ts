import { Component, OnInit, ViewChild } from '@angular/core';
import { Student } from 'src/app/entities/student';
import { Class } from 'src/app/entities/class';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { AdministratorService } from '../../administrator.service';
import { Router } from '@angular/router';
import { UpdateClassRequest } from 'src/app/request/update-class-request';
import { StudentInfoResponse } from 'src/app/response/student-info-response';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { MESSAGE_TYPE, MessageComponent } from 'src/app/shared/components/message/message.component';
import { CONSTANTS } from 'src/app/shared/constants';
import { CreateClassesStudent } from 'src/app/request/create-classes-student';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-administrator-enter-class-composition',
  templateUrl: './administrator-enter-class-composition.component.html',
  styleUrls: ['./administrator-enter-class-composition.component.scss']
})

export class AdministratorEnterClassCompositionComponent implements OnInit {

  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  public loaded: Boolean;
  public modified: Boolean;
  public selectedClass: Class;
  public studentsFree: Array<StudentInfoResponse>;
  public studentsInClass: Array<StudentInfoResponse>;
  public classes: Array<Class>;
  public classToCreate: number;

  constructor(private authenticationService: AuthenticationService,
    private administratorService: AdministratorService,
    private router: Router) {
  }

  ngOnInit() {

    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
          this.modified = false;
          this.studentsFree = [];
          this.studentsInClass = [];
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  loadFreeStudents() {
    this.selectedClass = null;
    this.administratorService.getStudentsInClass(-1).subscribe(
      data => {
        if (data instanceof Array) {
          this.studentsFree = data;
        } else {
          console.error("TODO");
        }
      },
      err => {
        console.error(err);
      });

    this.administratorService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;
        } else {
          console.error("TODO");
        }
      },
      err => {
        console.error(err);
      });
  }

  controlModified() {
    if (this.modified) this.cancel();
  }

  loadStudentsInClass() {
    this.administratorService.getStudentsInClass(this.selectedClass.id).subscribe(
      data => {

        if (data instanceof Array) {
          this.studentsInClass = data;
        } else {
          console.error("TODO");
        }

      },
      err => {
        console.error(err);

      });
  }

  addStudent(student: Student) {
    this.modified = true;
    this.studentsInClass.push(student)
    this.studentsFree.splice(this.studentsFree.indexOf(student), 1);
  }

  removeStudent(student: Student) {
    this.modified = true;
    this.studentsFree.push(student)
    this.studentsInClass.splice(this.studentsInClass.indexOf(student), 1);
  }

  updateClass() {

    this.loaded = false;
    let updateClassRequest: UpdateClassRequest = new UpdateClassRequest(this.selectedClass, this.studentsInClass)

    this.administratorService.updateClass(updateClassRequest).subscribe(
      data => {
        this.childMessage.showMessage("The class has been modified successfully", MESSAGE_TYPE.SUCCESS);
        this.studentsInClass = [];
        this.modified = false
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Server error,  try again", MESSAGE_TYPE.ERROR);
      }
    )

  }

  cancel() {
    this.modal.show("Are you sure to delete the unsaved modifications?");
  }

  createClass() {
    this.loaded = false;
    this.administratorService.createClass(this.classToCreate).subscribe(
      data => {
        this.childMessage.showMessage("You have created the class " + data, MESSAGE_TYPE.SUCCESS);
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Server error,  try again", MESSAGE_TYPE.ERROR);
      }
    )
  }
  doConfirm() {
    this.loaded = false;
    this.modified = false;
    this.childMessage.showMessage("CLEANED", MESSAGE_TYPE.SUCCESS);
    this.loadFreeStudents();
    this.studentsInClass = [];
    this.selectedClass = null;
    this.loaded = true;

  }

  useExcel(file: File) {
    CONSTANTS.useExcel(file).subscribe(rows=>{
      let classesStudents = new Array<CreateClassesStudent>();
      for (let i = 0; i < rows.length; i++) {
        classesStudents.push(new CreateClassesStudent(rows[i].Student, rows[i].Class))
        
      }
      console.log(classesStudents)
      this.sendClassesStudents(classesStudents)
    },
    error=> {
      console.log(error);
    })

  }


  sendClassesStudents(classesStudents: CreateClassesStudent[]) {

    this.loaded = false;
    this.administratorService.createExcelClasses(classesStudents).subscribe(
      data => {
          this.loaded = true;
          this.childMessage.showMessage("Classes have been correctly created", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage((err.error.message)? err.error.message : "Server error,  try again", MESSAGE_TYPE.ERROR);
      }
    )

  }

  downloadTemplate(){
    this.administratorService.downloadTemplate(CONSTANTS.CLASS_COMPOSITION_TEMPLATE_URL);
  }

}
