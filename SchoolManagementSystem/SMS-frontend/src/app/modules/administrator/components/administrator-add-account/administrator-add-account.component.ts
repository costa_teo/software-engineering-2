import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Parent } from 'src/app/entities/parent';
import { Student } from 'src/app/entities/student';
import { AdministratorService } from '../../administrator.service';
import { AddParentStudentRequest } from 'src/app/request/add-parent-student-request';
import { Routes } from 'src/app/core/routes';
import { Router } from '@angular/router';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { User } from 'src/app/entities/user';
import { CONSTANTS } from 'src/app/shared/constants';


@Component({
  selector: 'app-administrator-add-account',
  templateUrl: './administrator-add-account.component.html',
  styleUrls: ['./administrator-add-account.component.scss']
})
export class AdministratorAddAccountComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  loaded: boolean;
  public roleButton: Array<Boolean>;
  public u: User;

  constructor(
    private authenticationService: AuthenticationService,
    private administratorService: AdministratorService,
    private router: Router) {
    this.u = new User();
    this.u.gender = "M"
  }

  ngOnInit() {
    this.loaded = false;
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.u.role = 'TEACHER'
          this.roleButton = [true, false, false]
          this.loaded = true;

        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      }
    );
  }

  checkFiledsToEnableButton(){
    if(this.u.telephoneN.match(/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/)
      && this.u.email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?).*$/))
      return true;
      else{
        return false;
      }
  }
  addAccount() {

    if(!this.u.fiscalCode.match(/[A-Za-z0-9]{16}/)){
      this.childMessage.showMessage("INSERT A FISCALCODE OF 16 CHARACTERS", MESSAGE_TYPE.ERROR);
      return;
    }
    if(!this.u.email.match(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+/)){
      this.childMessage.showMessage("INSERT A CORRECT EMAIL", MESSAGE_TYPE.ERROR);
      return;
    }
    if(!this.u.telephoneN.match(/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/)){
      this.childMessage.showMessage("INSERT A CORRECT TELEPHONE NUMBER", MESSAGE_TYPE.ERROR);
      return;
    }


    this.loaded = false;
    this.u.birthdate = CONSTANTS.convertDateToString(new Date(this.u.birthdate))
    this.administratorService.addAccount(this.u).subscribe(
      data => {
        this.loaded = true;
        this.u.fiscalCode = "";
        this.u.birthdate = null;
        this.u.email = "";
        this.u.gender = "";
        this.u.name = "";
        this.u.surname = "";
        this.u.telephoneN = "";
        this.childMessage.showMessage("Saved", MESSAGE_TYPE.SUCCESS)
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Unexpected error", MESSAGE_TYPE.ERROR)
      }
    );
  }

  selectRole(role: string) {
    switch (role) {
      case "TEACHER":
        this.roleButton = [true, false, false]
        this.u.role = 'TEACHER'
        break;

      case "ADMINISTRATOR":
        this.roleButton = [false, true, false]
        this.u.role = 'ADMINISTRATOR'
        break;

      case "PRINCIPAL":
        this.roleButton = [false, false, true]
        this.u.role = 'PRINCIPAL'
        break;

      default:
        break;
    }
  }
}