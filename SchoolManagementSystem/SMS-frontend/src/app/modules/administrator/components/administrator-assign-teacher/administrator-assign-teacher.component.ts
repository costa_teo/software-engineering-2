import { Component, OnInit, ViewChild } from '@angular/core';
import { Class } from 'src/app/entities/class';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { AdministratorService } from '../../administrator.service';
import { Router } from '@angular/router';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { MESSAGE_TYPE, MessageComponent } from 'src/app/shared/components/message/message.component';
import { Subject } from 'src/app/entities/subject';
import { TeacherInfoResponse } from 'src/app/response/teacher-info-response';
import { UpdateTeachingInClassRequest } from 'src/app/request/update-teaching-in-class-request';

@Component({
  selector: 'app-administrator-assign-teacher',
  templateUrl: './administrator-assign-teacher.component.html',
  styleUrls: ['./administrator-assign-teacher.component.scss']
})

export class AdministratorAssignTeacherComponent implements OnInit {

  @ViewChild(ModalComponent, { static: false }) modal: ModalComponent;

  @ViewChild(MessageComponent, { static: false }) childMessage: MessageComponent;

  public loaded: Boolean;
  public modified: Boolean;
  public selectedClass: Class;
  public selectedSubject: Subject;
  public teachersFree: Array<TeacherInfoResponse>;
  public teacherInClass: TeacherInfoResponse;
  public classes: Array<Class>;
  public subjects: Array<Subject>;

  constructor(private authenticationService: AuthenticationService,
    private administratorService: AdministratorService,
    private router: Router) {
  }

  ngOnInit() {

    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
          this.modified = false;
          this.teachersFree = [];
          this.classes = [];
          this.subjects = [];
          this.loadSubjects();
          this.loadClasses();
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  loadFreeTeachers() {
    this.teachersFree=[];
    this.administratorService.getAllTeachers().subscribe(
      data => {
        if (data instanceof Array) {
          this.teachersFree = data;
        } else {
          console.error("TODO");
        }
      },
      err => {
        console.error(err);
      });
    this.loadTeacherInClass();
  }

  controlModified() {
    if (this.modified) this.cancel();
  }

  loadTeacherInClass() {

   if(this.selectedSubject){
      this.administratorService.getTeacherByClassAndSubject(this.selectedClass.id, this.selectedSubject.id).subscribe(
        data => {
          this.teacherInClass = data;
        },
        err => {
          console.error(err);
        });
    }
  }

  addTeacher(teacher: TeacherInfoResponse) {

    this.modified = true;
    this.teacherInClass=teacher;
    
  }

  removeTeacher() {

    this.modified = true;
    this.teacherInClass=null;

  }

  updateTeachingInClass() {

    this.loaded = false;
    let updateTeachingInClassRequest: UpdateTeachingInClassRequest = new UpdateTeachingInClassRequest(this.selectedClass.id, this.selectedSubject.id, this.teacherInClass)
    this.administratorService.updateTeachingInClass(updateTeachingInClassRequest).subscribe(
      data => {
        this.childMessage.showMessage("The teaching in class has been modified successfully", MESSAGE_TYPE.SUCCESS);
        this.teacherInClass=null;
        this.teachersFree = [];
        this.selectedClass=null;
        this.selectedSubject=null;
        this.modified = false
        this.loaded = true;
      },
      err => {
        this.loaded = true;
        this.childMessage.showMessage("Server error,  try again", MESSAGE_TYPE.ERROR);
      }
    );
  }

  cancel() {
    this.modal.show("Are you sure to delete the unsaved modifications?");
  }

  doConfirm() {

    this.loaded = false;
    this.modified = false;
    this.loadTeacherInClass();
    this.loadFreeTeachers();
    this.childMessage.showMessage("CLEANED", MESSAGE_TYPE.SUCCESS);
    this.loaded = true;

  }

  loadSubjects() {    
    this.selectedSubject=null;
    this.administratorService.getAllSubjects().subscribe(
      data => {
        if (data instanceof Array) {
          this.subjects = data;
        } else {
          console.error("TODO");
        }
      },
      err => {
        console.error(err);
      }); 
  }

  loadClasses() {
    this.selectedClass = null;
    this.administratorService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;
        } else {
          console.error("TODO");
        }
      },
      err => {
        console.error(err);
      });
  }
}
