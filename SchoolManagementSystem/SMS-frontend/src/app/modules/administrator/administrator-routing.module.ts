import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/auth.guard';
import { AdministratorHomeComponent } from './components/administrator-home/administrator-home.component';
import { AdministratorAddUserComponent } from './components/administrator-add-user/administrator-add-user.component';
import { AdministratorEnterClassCompositionComponent } from './components/administrator-enter-class-composition/administrator-enter-class-composition.component';
import { AdministratorGeneralCommunicationComponent } from './components/administrator-general-communication/administrator-general-communication.component';
import { AdministratorAssignTeacherComponent } from './components/administrator-assign-teacher/administrator-assign-teacher.component';
import { AdministratorAddAccountComponent } from './components/administrator-add-account/administrator-add-account.component';
import { AdministratorManageTimetablesComponent } from './components/administrator-manage-timetables/administrator-manage-timetables.component';

const routes: Routes = [
    {
        path: 'administrator', canActivate: [AuthGuard], children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: AdministratorHomeComponent },
            { path: 'addUser', component: AdministratorAddUserComponent },
            { path: 'enterClassComposition', component: AdministratorEnterClassCompositionComponent},
            { path: 'generalCommunication', component: AdministratorGeneralCommunicationComponent},
            { path: 'assignTeacher', component: AdministratorAssignTeacherComponent},
            { path: 'addAccount', component: AdministratorAddAccountComponent},
            { path: 'manageTimetables', component: AdministratorManageTimetablesComponent},
            { path: '**', redirectTo: 'home' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AdministratorRoutingModule { }
