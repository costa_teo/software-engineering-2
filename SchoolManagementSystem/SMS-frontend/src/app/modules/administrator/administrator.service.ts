import { HttpClient, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Routes } from 'src/app/core/routes';
import { Observable } from 'rxjs';
import { AddParentStudentRequest } from 'src/app/request/add-parent-student-request';
import { UpdateClassRequest } from 'src/app/request/update-class-request';
import { Student } from 'src/app/entities/student';
import { Class } from 'src/app/entities/class';
import { StudentInfoResponse } from 'src/app/response/student-info-response';
import { CreateClassesStudent } from 'src/app/request/create-classes-student';
import { GeneralCommunication } from 'src/app/entities/generalCommunication';
import { Subject } from 'src/app/entities/subject';
import { TeacherInfoResponse } from 'src/app/response/teacher-info-response';
import { Teacher } from 'src/app/entities/teacher';
import { UpdateTeachingInClassRequest } from 'src/app/request/update-teaching-in-class-request';
import { User } from 'src/app/entities/user';
import { TimetableRow } from 'src/app/shared/components/timetable/timetable/timetableRow';
import { TimeTableRequest } from 'src/app/request/timetable-request';


@Injectable({
  providedIn: 'root'
})
export class AdministratorService {
  

  private baseURL: string;

  constructor(
    private http: HttpClient) {
    this.baseURL = Routes.baseURL + '/administrator';
  }

  checkParent(request: string): Observable<boolean> {
    let httpParams = { params: new HttpParams().set('id', request.toString()) };
    return this.http.get<boolean>(this.baseURL + '/checkParent', httpParams);
  }

  checkStudent(request: string): Observable<boolean> {
    let httpParams = { params: new HttpParams().set('id', request.toString()) };
    return this.http.get<boolean>(this.baseURL + '/checkStudent', httpParams);
  }

  saveParents(request: AddParentStudentRequest): Observable<void> {
    return this.http.post<void>(this.baseURL + '/addParentStudent', request);
  }

  updateClass(request: UpdateClassRequest): Observable<any> {
    return this.http.post<any>(this.baseURL + '/updateClass', request, { observe: 'response' });
  }

  getStudentsInClass(request: number): Observable<Array<StudentInfoResponse>> {
    let httpParams = { params: new HttpParams().set('id', request.toString()) };
    return this.http.get<Array<Student>>(this.baseURL + '/getStudentsInClass', httpParams);
  }

  getAllClasses(): Observable<Array<Class>> {
    return this.http.get<Array<Class>>(this.baseURL + '/getAllClasses');
  }

  createClass(request:number) : Observable<String> {
    return this.http.post<String>(this.baseURL + '/createClass', request, {responseType:'text' as 'json'});
  } 

  createExcelClasses(request:Array<CreateClassesStudent>) : Observable<any> {
    return this.http.post<any>(this.baseURL + '/createExcelClasses', request, { observe: 'response' });
  }

  insertGeneralCommunication(newGC: GeneralCommunication): Observable<any> {
    return this.http.post<any>(this.baseURL + '/insertGeneralCommunication', newGC);
  }

  updateCommunication(updatedGC: GeneralCommunication): Observable<any> {
    return this.http.post<any>(this.baseURL + '/updateCommunication', updatedGC);
  }

  deleteCommunication(deletedGC: GeneralCommunication): Observable<any>{
    return this.http.post<any>(this.baseURL + '/deleteCommunication', deletedGC);
  }

  getAllSubjects(): Observable<Array<Subject>> {
    return this.http.get<Array<Subject>>(this.baseURL + '/getAllSubjects');
  }

  getTeacherByClassAndSubject(classId: number, subjectId: number): Observable<TeacherInfoResponse> {
    let httpParams = { params: new HttpParams()
      .set('classId', classId.toString())
        .set('subjectId', subjectId.toString())
    };
    return this.http.get<TeacherInfoResponse>(this.baseURL + '/getTeacherByClassAndSubject', httpParams);
  }

  getAllTeachers() : Observable<Array<TeacherInfoResponse>> {
    return this.http.get<Array<TeacherInfoResponse>>(this.baseURL + '/getAllTeachers');
  }

  updateTeachingInClass(request: UpdateTeachingInClassRequest): Observable<any> {
    return this.http.post<any>(this.baseURL + '/updateTeachingInClass', request, { observe: 'response' });
  }

  addAccount(request: User): Observable<any> {
    return this.http.post<any>(this.baseURL + '/addAccount', request, { observe: 'response' });
  }

  downloadTemplate(name: string){
    window.open(Routes.templates + "/" +name)
  }

  getTimetableByClass(teacherId: number): Observable<Array<TimetableRow>>{
    let httpParams = { params: new HttpParams().set('classId', teacherId.toString()) };
    return this.http.get<Array<TimetableRow>>(this.baseURL + '/getTimetableByClass', httpParams);
  }

  getTimetableByTeacher(classId: number): Observable<Array<TimetableRow>>{
    let httpParams = { params: new HttpParams().set('teacherId', classId.toString()) };
    return this.http.get<Array<TimetableRow>>(this.baseURL + '/getTimetableByTeacher', httpParams);
  }

  addClassTimetable(request: TimeTableRequest): Observable<any> {
    return this.http.post<any>(this.baseURL + '/addClassTimetable', request, { observe: 'response' });    
  }

}

