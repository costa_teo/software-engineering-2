import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/auth.guard';
import { TeacherHomeComponent } from './components/teacher-home/teacher-home.component';
import { TeacherTopicsComponent } from './components/teacher-topics/teacher-topics.component';
import { TeacherGradesComponent } from './components/teacher-grades/teacher-grades.component';
import { TeacherAttendancesComponent } from './components/teacher-attendances/teacher-attendances.component';
import { TeacherAssignmentsComponent } from './components/teacher-assignments/teacher-assignments.component';
import { TeacherSupportMaterialComponent } from './components/teacher-support-material/teacher-support-material.component';
import { TeacherTimetableComponent } from './components/teacher-timetable/teacher-timetable.component';

const routes: Routes = [
    {
        path: 'teacher', canActivate: [AuthGuard], children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: TeacherHomeComponent },
            { path: 'topics', component: TeacherTopicsComponent},
            { path: 'grades', component: TeacherGradesComponent},
            { path: 'attendances', component: TeacherAttendancesComponent},
            { path: 'assignments', component: TeacherAssignmentsComponent},
            { path: 'supportMaterial', component: TeacherSupportMaterialComponent},
            { path: 'timetable', component: TeacherTimetableComponent},

            { path: '**', redirectTo: 'home' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class TeacherRoutingModule { }
