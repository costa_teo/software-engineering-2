import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherHomeComponent } from './components/teacher-home/teacher-home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TeacherRoutingModule } from './teacher-routing.module';
import { TeacherTopicsComponent } from './components/teacher-topics/teacher-topics.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TeacherGradesComponent } from './components/teacher-grades/teacher-grades.component';
import { TeacherAttendancesComponent } from './components/teacher-attendances/teacher-attendances.component';
import { TeacherAssignmentsComponent } from './components/teacher-assignments/teacher-assignments.component';
import { TeacherSupportMaterialComponent } from './components/teacher-support-material/teacher-support-material.component';
import { TeacherTimetableComponent } from './components/teacher-timetable/teacher-timetable.component';

@NgModule({
  declarations: [TeacherHomeComponent, TeacherTopicsComponent, 
                 TeacherGradesComponent, TeacherAttendancesComponent,
                 TeacherAssignmentsComponent, TeacherSupportMaterialComponent, TeacherTimetableComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    TeacherRoutingModule,
  ]
})
export class TeacherModule { }
