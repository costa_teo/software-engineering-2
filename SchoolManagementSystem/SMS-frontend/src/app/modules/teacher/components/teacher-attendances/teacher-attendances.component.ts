import { Component, OnInit, ViewChild } from '@angular/core';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { Class } from 'src/app/entities/class';
import { Subject } from 'src/app/entities/subject';
import { StudentInfoResponse } from 'src/app/response/student-info-response';
import { TeacherService } from '../../teacher-service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { CONSTANTS } from 'src/app/shared/constants';
import { StudentAttendance } from 'src/app/entities/studentAttendance';
import { ThrowStmt } from '@angular/compiler';
import { Attendance, AttendanceType } from 'src/app/entities/attendance';

@Component({
  selector: 'app-teacher-attendances',
  templateUrl: './teacher-attendances.component.html',
  styleUrls: ['./teacher-attendances.component.scss']
})
export class TeacherAttendancesComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;

  subjects: Array<Subject>;
  classes: Array<Class>;
  validDates: Array<string>;

  studentsAttendance: Array<StudentAttendance>


  loaded: boolean;
  loadedStudents: boolean;
  loadedClasses: boolean;
  selectedClass: Class;
  selectedDate: string;


  constructor(
    private teacherService: TeacherService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
          this.loadClasses();
          this.computeValidDates();
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  loadClasses() {
    this.loadedStudents = false;

    this.loadedClasses = false
    this.teacherService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;

        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.log("TODO");
        }
        this.loadedClasses = true
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadStudents() {
    if(this.selectedDate == null || this.selectedClass == null)
      return

    this.loadedStudents = false;

    this.teacherService.loadStudentsAttendanceByClassIdAndDate(this.selectedClass.id, this.selectedDate).subscribe(
      data => {
        if (data instanceof Array) {
          this.studentsAttendance = data
          this.studentsAttendance.forEach(a => {
            if(a.attendance1 == null){
              a.attendance1 = new Attendance(this.selectedDate, null, AttendanceType.ENTER)
              a['late'] = false;    
              a['present'] = true;
            }else{
              if(a.attendance1.type == AttendanceType.ABSENCE){
                a['present'] = false;
                a['late'] = false;
              }else{
                a['present'] = true;
                a['late'] = true;
              }
            }

            if(a.attendance2 == null){
              a.attendance2 = new Attendance(this.selectedDate, null, AttendanceType.EXIT)
              a['early'] = false;
            }else{
              a['early'] = true;
            }
          })


          this.loadedStudents = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }


  computeValidDates() {
    let now = new Date();
    let i = (now.getDay() == 0) ? 7 : now.getDay();

    let firstMonday = new Date();
    firstMonday.setDate(now.getDate() - i + 1);

    i = (i < 6) ? i : 5;

    this.validDates = [];

    for (let j = 0; j < i; j++) {
      this.validDates.push(CONSTANTS.convertDateToString(firstMonday))
      firstMonday.setDate(firstMonday.getDate() + 1);
    }
  }

  changePresentAttendance(attendance: StudentAttendance) {
    if (attendance['present'] == false) {
      attendance['early'] = false;
      attendance['late'] = false;
    }
  }

  cancelInsertedData(){
    this.loadStudents();
  }

  insertAbsences(){
    let studentAttendanceRequest = new Array<StudentAttendance>();
    
    this.studentsAttendance.forEach(a => {
      let b1 = new StudentAttendance(a.studentId, a.studentName, null, null)

      
      if(a['late'] && a.attendance1.timeslot != null){
        b1.attendance1 = new Attendance(this.selectedDate, a.attendance1.timeslot, AttendanceType.ENTER)
      }        

      
      if(a['early']&& a.attendance2.timeslot != null){
        b1.attendance2 = new Attendance(this.selectedDate, a.attendance2.timeslot, AttendanceType.EXIT)
      }

      if(!a['present']){
        b1.attendance1 = new Attendance(this.selectedDate, null, AttendanceType.ABSENCE)
      }
      studentAttendanceRequest.push(b1);
    })

    this.loaded = false;
    this.teacherService.insertAbsences(studentAttendanceRequest, this.selectedClass.id, this.selectedDate).subscribe(
      ok=>{
        this.childComponent.showMessage("Saved", MESSAGE_TYPE.SUCCESS);
        this.loaded = true;
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        this.loaded = true;
      }
    )
  }

  lateChanged(attendance: StudentAttendance){
    if(attendance.attendance1.timeslot > attendance.attendance2.timeslot){
      attendance.attendance2.timeslot = null;
    }
  }

  lateCheckChange(attendance: StudentAttendance){
    if(attendance['late']==false){
      attendance.attendance1.timeslot = null;
    }
  }

}
