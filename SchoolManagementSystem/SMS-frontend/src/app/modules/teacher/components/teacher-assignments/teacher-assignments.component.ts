import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService } from '../../teacher-service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Subject } from 'src/app/entities/subject';
import { Class } from 'src/app/entities/class';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { Assignment } from 'src/app/entities/assignment';
import { Attachment } from 'src/app/entities/attachment';

import * as FileSaver from 'file-saver';
import { NewAttachmentRequest } from 'src/app/request/new-attachment-request';
import { CONSTANTS } from 'src/app/shared/constants';

@Component({
  selector: 'app-teacher-assignments',
  templateUrl: './teacher-assignments.component.html',
  styleUrls: ['./teacher-assignments.component.scss']
})
export class TeacherAssignmentsComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;

  today = new Date();

  selectedSubject: Subject;
  selectedClass: Class;
  selectedDate: Date;
  selectedAssignment: Assignment;
  newfileName: string;

  selectdTimeslot: number;
  description: string;

  subjects: Array<Subject>;
  classes: Array<Class>;
  assignmentsData: Array<Assignment>;
  attachmentsData: Array<Attachment>;

  loaded: boolean;
  loadedSubjects: boolean = true;
  loadedClasses: boolean;
  loadedAssignments: boolean;
  loadAttachments: boolean;
  canUpload: boolean;

  constructor(
    private teacherService: TeacherService,
    private authenticationService: AuthenticationService) { }

  //to change
  insertGradesSettings: Settings = {
    columns: [{
      columnName: 'Name',
      attributeName: 'name',
      isSortable: true
    }, {
      columnName: 'Surname',
      attributeName: 'surname',
    }, {
      columnName: 'Grade',
      attributeName: 'value',
      isInsertable: true,
    }],
    hasEdit: false,
    hasDelete: false,
    hasInsertion: true,
    hasSorting: true,
  }

  assignmentsSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Description',
      attributeName: 'description',
      isEditable: true
    }],
    hasInfo: true,
    hasEdit: true,
    hasDelete: true,
    hasSorting: true,
    elementsPerPage: 10
  };

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
          this.loadClasses();
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  loadClasses() {
    this.subjects = []

    this.loadedClasses = false
    this.teacherService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;

        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.log("TODO");
        }
        this.loadedClasses = true
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSubjects() {
    this.loadedSubjects = false;
    this.selectedSubject = null;

    this.teacherService.getSubjectsByClassId(this.selectedClass.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.subjects = data;
          this.loadedSubjects = true
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadAssignmentsData() {
    this.loadedAssignments = false;

    this.teacherService.getAssignmentByClassAndSubject(this.selectedClass.id, this.selectedSubject.id).subscribe(
      data => {
        this.assignmentsData = data;
        this.loadedAssignments = true;
      },
      erro => {
        this.assignmentsData = [];
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
        this.loadedAssignments = true;
      });
  }

  loadAttachmentsData(index: number) {
    this.selectedAssignment = this.assignmentsData[index];
    this.loadAttachments = false;

    this.teacherService.getAttachmentsByAssignmentId(this.assignmentsData[index].id).subscribe(
      data => {
        this.attachmentsData = data
        this.loadAttachments = true;
      },
      err => {
        this.assignmentsData = [];
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
        this.loadAttachments = true;
      });
  }

  downloadAttachment(att: Attachment) {
    this.teacherService.downloadAttachment(att.id).subscribe(blob => {
      FileSaver.saveAs(blob, att.fileName)
    });
  }

  uploadAttachment(files: FileList) {
    this.loaded = false;
    this.canUpload = false;

    let request: NewAttachmentRequest = new NewAttachmentRequest();
    request.name = files.item(0).name;
    request.file = files.item(0);
    request.assignment_ref = this.selectedAssignment.id;

    this.teacherService.uploadAttachment(request).subscribe(
      data => {
        this.childComponent.showMessage("File correctly uploaded", MESSAGE_TYPE.SUCCESS);
        this.loaded = true;
      }, err => {
        this.loaded = true;
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
      });
  }

  deleteAttachment(att: Attachment) {
    this.teacherService.deleteAttachment(att).subscribe(
      data => {
        this.childComponent.showMessage("Attachment correctly deleted", MESSAGE_TYPE.SUCCESS);
        this.attachmentsData.splice(this.attachmentsData.indexOf(att), 1)
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
      });
  }

  deleteAssignment(index: number) {
    this.teacherService.deleteAssignment(this.assignmentsData[index].id).subscribe(data => {
      this.childComponent.showMessage("Assignment correctly deleted", MESSAGE_TYPE.SUCCESS);
    }, err => {
      this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
    });
  }

  updateAssignment(ob: { index: number, value: Assignment }) {
    this.teacherService.updateAssignment(ob.value).subscribe(data => {
      this.childComponent.showMessage("Saved", MESSAGE_TYPE.SUCCESS);
      this.assignmentsData[ob.index] = ob.value
    }, err => {
      this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
    })
  }

  resetSelectedData() {
    this.canUpload = false;
    this.selectedClass = null;
    this.selectdTimeslot = null;
    this.selectedDate = null;
    this.selectedSubject = null;
    this.selectedAssignment = null
    this.description = null;
    this.subjects = []
  }

  cancel() {
    this.resetSelectedData();
  }

  confirmNewAssignment() {
    let a: Assignment = new Assignment(-1, CONSTANTS.convertDateToString(this.selectedDate), this.description, this.selectedSubject.name, null, this.selectedClass.name, false);

    this.teacherService.insertAssignment(a).subscribe(
      data => {
        this.selectedAssignment = new Assignment(data);
        this.canUpload = true;
        this.childComponent.showMessage("Assignment inserted correctly", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
      });
  }

}
