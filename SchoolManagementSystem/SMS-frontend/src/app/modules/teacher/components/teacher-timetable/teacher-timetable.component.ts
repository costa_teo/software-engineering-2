import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService } from '../../teacher-service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { Student } from 'src/app/entities/student';
import { TimetableRow } from 'src/app/shared/components/timetable/timetable/timetableRow';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Routes } from 'src/app/core/routes';

@Component({
  selector: 'app-teacher-timetable',
  templateUrl: './teacher-timetable.component.html',
  styleUrls: ['./teacher-timetable.component.scss']
})
export class TeacherTimetableComponent implements OnInit {
  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;
  
  public loaded: Boolean;
  public student: Student;
  timetableData: TimetableRow[];

  constructor(private authenticationService: AuthenticationService,
    private teacherService: TeacherService) { }

    ngOnInit() {
      this.authenticationService.authenticateUser().subscribe(
        data => {
          if (!data) {
            this.authenticationService.logout();
          } else {
            this.loadTimetable();
          }
        },
        err => {
          this.authenticationService.logout();
        }
      );
    }

  loadTimetable() {
    this.teacherService.getTimetable().subscribe(
      data=> {
        this.timetableData = data;
        this.loaded = true;
      }, err=>{
        this.childComponent.showMessage("Unexpeced error", MESSAGE_TYPE.ERROR)
      }
    )
  }
    
    
}




