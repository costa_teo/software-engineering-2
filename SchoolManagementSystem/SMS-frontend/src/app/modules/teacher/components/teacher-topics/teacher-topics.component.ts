import { Component, OnInit, ViewChild } from '@angular/core';
import { Lecture } from 'src/app/entities/lecture';
import { Class } from 'src/app/entities/class';
import { Subject } from 'src/app/entities/subject';
import { TeacherService } from '../../teacher-service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { CONSTANTS } from 'src/app/shared/constants';

@Component({
  selector: 'app-teacher-topics',
  templateUrl: './teacher-topics.component.html',
  styleUrls: ['./teacher-topics.component.scss']
})
export class TeacherTopicsComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;

  //html view
  loadedSubjects: boolean = true;
  loadedClasses: boolean = false;
  loaded: boolean;
  loadingTable: boolean
  showTable: boolean = false;

  isViewEdit: boolean;
  allowedDates: Array<string>;

  //data
  lectures: Array<Lecture>;
  classes: Array<Class>;
  subjects: Array<Subject>;
  selectedClass: Class;
  selectedSubject: Subject;
  newLecture: Lecture

  constructor(private authenticationService: AuthenticationService,
    private teacherService: TeacherService) {

  }

  lecturesViewTableSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Timeslot',
      attributeName: 'timeSlot',
    }, {
      columnName: 'Class name',
      attributeName: 'className',
    }, {
      columnName: 'Description',
      attributeName: 'description',
      isEditable: true,
    }, {
      columnName: 'Subject',
      attributeName: 'subject',
    }],
    hasEdit: true,
    hasDelete: true,
    hasSorting: true,
    elementsPerPage: 10
  }

  ngOnInit() {
    this.lectures = [];
    this.subjects = [];
    this.newLecture = new Lecture(0, "", "", "", "", "", null)

    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        }
        this.loaded = true;
      },
      err => {
        console.error(err);
        this.authenticationService.logout();
      }
    );

    this.computeValidDates();
  }

  loadedAllClasses() {
    this.loadedClasses = false
    this.teacherService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;
          if (this.isViewEdit) this.classes.unshift(new Class("All classes", -1));
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.log("TODO");
        }
        this.loadedClasses = true
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSubjects() {
    this.loadedSubjects = false;

    this.selectedSubject = null;
    this.teacherService.getSubjectsByClassId(this.selectedClass.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.subjects = data;
          if (this.isViewEdit) this.subjects.unshift(new Subject("All subjects", -1));
          this.loadedSubjects = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  computeValidDates() {
    let now = new Date();
    let i = (now.getDay() == 0) ? 7 : now.getDay();

    let firstMonday = new Date();
    firstMonday.setDate(now.getDate() - i + 1);

    i = (i < 6) ? i : 5;

    this.allowedDates = [];
    for (let j = 0; j < i; j++) {
      this.allowedDates.push(CONSTANTS.convertDateToString(firstMonday));
      firstMonday.setDate(firstMonday.getDate() + 1);
    }
  }

  updateLecture(wrapper: any) {
    let updatedLecture = wrapper.value
    let index = wrapper.index
    this.lectures[index] = updatedLecture
    this.teacherService.updateLecture(updatedLecture).subscribe(
      data => {
        this.childComponent.showMessage("Lecture correctly updated", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );
  }

  loadLectures() {
    this.showTable = false;
    this.loadingTable = true;

    this.teacherService.getLecturesByClassIdAndSubjectId(this.selectedSubject.id, this.selectedClass.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.lectures = data;

          var minDate = new Date(this.allowedDates[0]);
          var maxDate = new Date(this.allowedDates[this.allowedDates.length - 1])
          for (var l of this.lectures) {
            var lectureDate = new Date(l.date)
            l['isEditable'] = (minDate.valueOf() <= lectureDate.valueOf() && lectureDate.valueOf() <= maxDate.valueOf())
            l['isDeletable'] = l['isEditable'];
          }
          this.loadingTable = false;
          this.showTable = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  insertLecture() {
    this.loaded = false;
    this.newLecture.subject = this.selectedSubject.name
    this.newLecture.className = this.selectedClass.name
    console.log(this.newLecture)
    this.teacherService.insertLectureTopics(this.newLecture).subscribe(
      data => {
        this.newLecture = data;
        this.newLecture['isEditable'] = true;
        this.lectures = JSON.parse(JSON.stringify(this.lectures));
        this.lectures.push(this.newLecture);
        this.loaded = true;

        this.childComponent.showMessage("Lecture correctly inserted", MESSAGE_TYPE.SUCCESS);

      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );
  }

  deleteLecture(index: number) {
    let lecture = this.lectures[index]
    this.loaded = false;
    this.teacherService.deleteLecture(lecture).subscribe(
      data => {
        this.childComponent.showMessage("Lecture correctly deleted", MESSAGE_TYPE.SUCCESS);
        this.lectures.splice(index, 1);
        this.loaded = true;
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );
  }

  cancelInsertedData() {
    this.selectedClass = null
    this.selectedSubject = null
    this.showTable = false
    this.newLecture = new Lecture(0, "", "", "", "", "", null)
  }

  setIsViewEditvalue(value: boolean) {
    this.isViewEdit = value
    this.classes = []
    this.subjects = []
    this.cancelInsertedData();
    this.loadedAllClasses();
  }

}

