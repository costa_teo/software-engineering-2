import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService } from '../../teacher-service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Subject } from 'src/app/entities/subject';
import { Class } from 'src/app/entities/class';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { Student } from 'src/app/entities/student';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { StudentInfoResponse } from 'src/app/response/student-info-response';
import { Grade } from 'src/app/entities/grade';
import { InsertGradeRequest } from 'src/app/request/insert-grade-request';
import { NewGrade } from 'src/app/entities/newGrade';
import { CONSTANTS } from 'src/app/shared/constants';


@Component({
  selector: 'app-teacher-grades',
  templateUrl: './teacher-grades.component.html',
  styleUrls: ['./teacher-grades.component.scss']
})
export class TeacherGradesComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;

  selectedSubject: Subject;
  selectedClass: Class;
  selectedDate: string;
  selectdTimeslot: number;
  selectedStudent: Student;
  description: string;


  subjects: Array<Subject>;
  classes: Array<Class>;
  students: Array<StudentInfoResponse>;
  validDates: Array<string>;
  viewStudentGrades: Array<Grade>;


  loaded: boolean;
  loadedStudentData: boolean;
  loadedSubjects: boolean = true;
  loadedStudents: boolean;
  loadedClasses: boolean;

  newGrades: Array<NewGrade> = [];

  constructor(
    private teacherService: TeacherService,
    private authenticationService: AuthenticationService) { }

  insertGradesSettings: Settings = {
    columns: [{
      columnName: 'Name',
      attributeName: 'name',
      isSortable: true
    }, {
      columnName: 'Surname',
      attributeName: 'surname',
    }, {
      columnName: 'Grade',
      attributeName: 'value',
      isInsertable: true,
    }],
    hasEdit: false,
    hasDelete: false,
    hasInsertion: true,
    hasSorting: true,
  }

  viewGradesSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Time slot',
      attributeName: 'timeslot',
    }, {
      columnName: 'Description',
      attributeName: 'description',
      isEditable: true
    }, {
      columnName: 'Grade',
      attributeName: 'value',
      isEditable: true
    }],
    hasEdit: true,
    hasDelete: true,
    hasSorting: true,
    elementsPerPage: 10
  };

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
          this.loadClasses();
          this.computeValidDates();
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  loadClasses() {
    this.loadedStudents = false;
    this.loadedStudentData = false;
    this.subjects = []
    this.students = []

    this.loadedClasses = false
    this.teacherService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;

        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.log("TODO");
        }
        this.loadedClasses = true
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSubjects() {
    this.loadedSubjects = false;
    this.loadedStudents = false;
    this.loadedStudentData = false;
    this.selectedSubject = null;
    this.selectedStudent = null

    this.teacherService.getSubjectsByClassId(this.selectedClass.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.subjects = data;
          this.loadedSubjects = true
          this.loadStudents();
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadStudents() {
    this.loadedStudents = false;
    this.loadedStudentData = false;

    this.teacherService.loadStudentsByClassId(this.selectedClass.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.students = data;
          this.newGrades = new Array();
          this.students.forEach(s => {
            this.newGrades.push(new NewGrade(s.id, s.name, s.surname, null));
          })
          this.loadedStudents = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadStudentData(student: Student) {
    this.loadedStudentData = false;
    this.selectedStudent = student;
    this.teacherService.getGradesByStudentAndSubject(student.id, this.selectedSubject.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.viewStudentGrades = data;

          var minDate = new Date(this.validDates[0]);
          var maxDate = new Date(this.validDates[this.validDates.length -1]) 
          this.viewStudentGrades.forEach(grade => {
            var gradeDate = new Date(grade.date)
            grade['isEditable'] = (minDate.valueOf() <= gradeDate.valueOf() && gradeDate.valueOf() <= maxDate.valueOf())
            grade['isDeletable'] = grade['isEditable'];
          })
          this.loadedStudentData = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  computeValidDates() {
    let now = new Date();
    let i = (now.getDay() == 0) ? 7 : now.getDay();

    let firstMonday = new Date();
    firstMonday.setDate(now.getDate() - i + 1);

    i = (i < 6) ? i : 5;

    this.validDates = [];

    for (let j = 0; j < i; j++) {
      this.validDates.push(CONSTANTS.convertDateToString(firstMonday))
      firstMonday.setDate(firstMonday.getDate() + 1);
    }
  }

  saveNewGrades(newGrades: Array<NewGrade>) {
    if(!this.selectdTimeslot || !this.selectedDate || !this.selectedSubject){
      this.childComponent.showMessage("Please, fill all the fields", MESSAGE_TYPE.WARNING)
      return;
    }

    let gradeRequest = new  InsertGradeRequest(newGrades, this.description, this.selectedDate, this.selectdTimeslot, this.selectedSubject.id, -1 );
    this.teacherService.insertGrades(gradeRequest).subscribe(
      data => {
        this.childComponent.showMessage("Grades correctly inserted", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );

  }

  updateGrade(wrapper: any) {
    let updatedGrade = wrapper.value;
    this.viewStudentGrades[wrapper.index] = updatedGrade
    this.teacherService.updateGrade(updatedGrade).subscribe(
      data => {
        this.childComponent.showMessage("Grade correctly updated", MESSAGE_TYPE.SUCCESS);
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );
  }

  deleteGrade(index: number) {
    let deletedGrade = this.viewStudentGrades[index];
    this.teacherService.deleteGrade(deletedGrade).subscribe(
      data => {
        this.childComponent.showMessage("Grade correctly deleted", MESSAGE_TYPE.SUCCESS);
        this.viewStudentGrades.splice(index, 1);
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      }
    );
  }

  resetSelectedData() {
    this.selectedClass = null;
    this.selectdTimeslot = null;
    this.selectedDate = null;
    this.selectedStudent = null;
    this.selectedSubject = null;
    this.description = null;
    this.loadedStudents = false;
    this.students = []
    this.subjects = []
  }

}
