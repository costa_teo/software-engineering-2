import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService } from '../../teacher-service';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Subject } from 'src/app/entities/subject';
import { Class } from 'src/app/entities/class';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { Assignment } from 'src/app/entities/assignment';
import { Attachment } from 'src/app/entities/attachment';

import * as FileSaver from 'file-saver';
import { SupportMaterial } from 'src/app/entities/supportMaterial';
import { NewSupportMaterialRequest } from 'src/app/request/new-support-material-request';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-teacher-support-material',
  templateUrl: './teacher-support-material.component.html',
  styleUrls: ['./teacher-support-material.component.scss']
})
export class TeacherSupportMaterialComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;

  today = new Date();

  selectedSubject: Subject;
  selectedClass: Class = null;

  subjects: Array<Subject>;
  classes: Array<Class>;

  supportMaterials: Array<SupportMaterial>;
  loadedSupportMaterial: boolean;

  loaded: boolean;
  loadedSubjects: boolean = true;
  loadedClasses: boolean;


  constructor(
    private teacherService: TeacherService,
    private authenticationService: AuthenticationService) { }


  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.loaded = true;
          this.loadClasses();
        }
      },
      err => {
        this.authenticationService.logout();
      }
    );
  }

  loadClasses() {
    this.subjects = []

    this.loadedClasses = false
    this.teacherService.getAllClasses().subscribe(
      data => {
        if (data instanceof Array) {
          this.classes = data;

        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        }
        this.loadedClasses = true
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSubjects() {
    this.loadedSubjects = false;
    this.selectedSubject = null;

    this.teacherService.getSubjectsByClassId(this.selectedClass.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.subjects = data;
          this.loadedSubjects = true
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSupportMaterials() {
    this.loadedSupportMaterial = false;

    this.teacherService.getSMaterialByClassAndSubject(this.selectedClass.id, this.selectedSubject.id).subscribe(
      data => {
        this.supportMaterials = data
        this.loadedSupportMaterial = true;
      },
      err => {
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
        this.loadedSupportMaterial = true;
      });
  }

  downloadSupportMaterial(supportMaterial: SupportMaterial) {
    this.teacherService.downloadSupportMaterial(supportMaterial.id).subscribe(blob => {
      FileSaver.saveAs(blob, supportMaterial.fileName)
    });
  }

  uploadSupportMaterial(files: FileList) {
    this.loaded = false;

    let request: NewSupportMaterialRequest = new NewSupportMaterialRequest();
    request.name = files.item(0).name;
    request.file = files.item(0);
    request.class_ref = this.selectedClass.id;
    request.subject_ref = this.selectedSubject.id;

    this.teacherService.uploadSupportMaterial(request).subscribe(
      data => {
        this.childComponent.showMessage("File correctly uploaded", MESSAGE_TYPE.SUCCESS);
        this.loaded = true;
        this.loadSupportMaterials();
      }, err => {
        this.loaded = true;
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
      });
  }

  deleteSupportMaterial(supportMaterial: SupportMaterial) {
    this.loaded = false
    this.teacherService.deleteSupportMaterial(supportMaterial).subscribe(
      data => {
        this.childComponent.showMessage("Attachment correctly deleted", MESSAGE_TYPE.SUCCESS);
        this.supportMaterials.splice(this.supportMaterials.indexOf(supportMaterial), 1)
        this.loaded = true
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        this.loaded = true
      });
  }



}
