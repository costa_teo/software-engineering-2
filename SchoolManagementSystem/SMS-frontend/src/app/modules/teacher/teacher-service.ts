import { Injectable } from '@angular/core';
import { Routes } from 'src/app/core/routes';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Subject } from 'src/app/entities/subject';
import { Class } from 'src/app/entities/class';
import { Lecture } from 'src/app/entities/lecture';
import { StudentInfoResponse } from 'src/app/response/student-info-response';
import { Grade } from 'src/app/entities/grade';
import { InsertGradeRequest } from 'src/app/request/insert-grade-request';
import { StudentAttendance } from 'src/app/entities/studentAttendance';
import { Assignment } from 'src/app/entities/assignment';
import { Attachment } from 'src/app/entities/attachment';
import { SharedService } from 'src/app/shared/shared.service';
import { NewAttachmentRequest } from 'src/app/request/new-attachment-request';
import { SupportMaterial } from 'src/app/entities/supportMaterial';
import { NewSupportMaterialRequest } from 'src/app/request/new-support-material-request';
import { TimetableRow } from 'src/app/shared/components/timetable/timetable/timetableRow';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private baseURL: string;

  constructor(
    private http: HttpClient,
    private sharedService: SharedService) {
    this.baseURL = Routes.baseURL + '/teacher';
  }

  public getAllSubjects(): Observable<Array<Subject>> {
    return this.http.get<Array<Subject>>(this.baseURL + "/getAllSubjects");
  }

  public getAllClasses(): Observable<Array<Class>> {
    return this.http.get<Array<Class>>(this.baseURL + "/getAllClasses");
  }

  public getSubjectsByClassId(id: number): Observable<Array<Subject>> {
    let httpParams = { params: new HttpParams().set('id', id.toString()) };
    return this.http.get<Array<Subject>>(this.baseURL + "/getSubjectsByClassId", httpParams);
  }

  public getLecturesByClassIdAndSubjectId(subjectId: number, classId: number): Observable<Array<Lecture>> {
    let httpParams = {
      params: new HttpParams()
        .set('classId', classId.toString())
        .set('subjectId', subjectId.toString())
    };
    return this.http.get<Array<Lecture>>(this.baseURL + "/getLecturesByClassIdAndSubjectId", httpParams);
  }

  public deleteLecture(badLecture: Lecture): Observable<any> {
    return this.http.post(this.baseURL + '/deleteLecture', badLecture, { observe: 'response' });
  }

  public insertLectureTopics(newLecture: Lecture): Observable<Lecture> {
    return this.http.post<Lecture>(this.baseURL + '/insertLectureTopics', newLecture);
  }

  public updateLecture(updateLecture: Lecture): Observable<any> {
    return this.http.post<any>(this.baseURL + '/updateLecture', updateLecture, { observe: 'response' });
  }

  public loadStudentsByClassId(id: number): Observable<Array<StudentInfoResponse>> {
    let httpParams = { params: new HttpParams().set('id', id.toString()) };
    return this.http.get<Array<StudentInfoResponse>>(this.baseURL + "/getStudentsByClassId", httpParams);
  }

  public getGradesByStudentAndSubject(studentId: number, subjectId: number): Observable<Array<Grade>> {
    let httpParams = {
      params: new HttpParams()
        .set('studentId', studentId.toString())
        .set('subjectId', subjectId.toString())
    };
    return this.http.get<Array<Grade>>(this.baseURL + "/getGradesByStudentAndSubject", httpParams);
  }

  public insertGrades(newGrades: InsertGradeRequest): Observable<any> {
    return this.http.post<any>(this.baseURL + '/insertGrades', newGrades, { observe: 'response' });
  }

  public updateGrade(updatedGrade: Grade): Observable<any> {
    return this.http.post<any>(this.baseURL + '/updateGrade', updatedGrade, { observe: 'response' });
  }

  public deleteGrade(deletedGrade: Grade): Observable<any> {
    return this.http.post<any>(this.baseURL + '/deleteGrade', deletedGrade, { observe: 'response' });
  }

  public insertAbsences(studentsAttendance: Array<StudentAttendance>, classId: number, date: string): Observable<any> {
    let httpParams = new HttpParams()
      .set('classId', classId.toString())
      .set('date', date);
    return this.http.post<any>(this.baseURL + '/insertAttendance', studentsAttendance, { observe: 'response', params: httpParams });
  }

  public loadStudentsAttendanceByClassIdAndDate(classId: number, selectedDate: string): Observable<any> {
    let httpParams = {
      params: new HttpParams()
        .set('classId', classId.toString())
        .set('date', selectedDate)
    };
    return this.http.get<Array<StudentAttendance>>(this.baseURL + "/getStudentsAttendanceByClassIdAndDate", httpParams);
  }

  public getAssignmentByClassAndSubject(classId: number, subjectId: number): Observable<Array<Assignment>> {
    let httpParams = {
      params: new HttpParams()
        .set('classId', classId.toString())
        .set('subjectId', subjectId.toString())
    };
    return this.http.get<Array<Assignment>>(this.baseURL + "/getAssignmentByClassIdAndSubjectId", httpParams);
  }

  public getAttachmentsByAssignmentId(assignmentid: number): Observable<Array<Attachment>> {
    let httpParams = {
      params: new HttpParams().set('assignmentid', assignmentid.toString())
    }
    return this.http.get<Array<Attachment>>(this.baseURL + "/getAttachmentsByAssignmentId", httpParams);
  }

  public updateAssignment(updateAssignment: Assignment): Observable<any> {
    return this.http.post<any>(this.baseURL + '/updateAssignment', updateAssignment, { observe: 'response' });
  }

  public deleteAssignment(deletedAssignmentIndex: number): Observable<any> {
    return this.http.post<any>(this.baseURL + '/deleteAssignment', deletedAssignmentIndex, { observe: 'response' });
  }

  public downloadAttachment(id: number): Observable<Blob> {
    return this.sharedService.downloadAttachment(id);
  }

  public downloadSupportMaterial(smId: number): Observable<Blob> {
    return this.sharedService.downloadSupportMaterial(smId);
  }

  public uploadAttachment(request: NewAttachmentRequest): Observable<any>{
    let fd: FormData = new FormData();
    fd.append('name', request.name);
    fd.append('file', request.file);
    fd.append('assignment_ref', request.assignment_ref.toString());
    return this.http.post<any>(this.baseURL + '/uploadAttachment', fd, { observe: 'response' });
  }

  public uploadSupportMaterial(request: NewSupportMaterialRequest): Observable<any>{
    let fd: FormData = new FormData();
    fd.append('name', request.name);
    fd.append('file', request.file);
    fd.append('class_ref', request.class_ref.toString());
    fd.append('subject_ref', request.subject_ref.toString());
    return this.http.post<any>(this.baseURL + '/uploadSupportMaterial', fd, { observe: 'response' });
  }

  public insertAssignment(a: Assignment): Observable<number> {
    return this.http.post<number>(this.baseURL + '/insertAssignment', a);
  }

  public deleteAttachment(deleteAttachment: Attachment):Observable<any> {
    return this.http.post<any>(this.baseURL + '/deleteAttachment', deleteAttachment,  {observe: 'response' });
  }

  public deleteSupportMaterial(supportMaterial: SupportMaterial):Observable<any> {
    return this.http.post<any>(this.baseURL + '/deleteSupportMaterial', supportMaterial,  {observe: 'response' });
  }

  public getSMaterialByClassAndSubject(classId: number, subjectId: number): Observable<Array<SupportMaterial>> {
    return this.sharedService.getSMaterialByClassAndSubject(classId, subjectId);
  }

  public getTimetable(): Observable<Array<TimetableRow>> {
    return this.http.get<Array<TimetableRow>>(this.baseURL + "/getTimetable");
  }


}
