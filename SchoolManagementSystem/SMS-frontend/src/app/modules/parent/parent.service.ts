import { Injectable } from '@angular/core';
import { Grade } from 'src/app/entities/grade';
import { Routes } from 'src/app/core/routes';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { StudentInfoResponse } from 'src/app/response/student-info-response';
import { Student } from 'src/app/entities/student';
import { GradesBySubjectResponse } from 'src/app/response/grades-by-subject-response';
import { AssignmentsBySubjectResponse } from 'src/app/response/assignments-by-subject-response';
import { Attachment } from 'src/app/entities/attachment';
import { Attendance } from 'src/app/entities/attendance';
import { SharedService } from 'src/app/shared/shared.service';
import { SupportMaterial } from 'src/app/entities/supportMaterial';
import { Subject } from 'src/app/entities/subject';
import { Class } from 'src/app/entities/class';
import { TimetableResponse } from 'src/app/response/timetable-response';

@Injectable({
  providedIn: 'root'
})
export class ParentService {
  
  private baseURL: string;
  private static selectedStudent: Student;
  
  constructor(
    private http: HttpClient,
    private sharedService: SharedService) {
      this.baseURL = Routes.baseURL + '/parent';
    }
    
    public getGrades(id: number): Observable<Array<GradesBySubjectResponse>> {
      let httpParams = { params: new HttpParams().set('id', id.toString()) };
      return this.http.get<Array<GradesBySubjectResponse>>(this.baseURL + "/getGrades", httpParams);
    }
    
    public getStudents(): Observable<Array<StudentInfoResponse>> {
      return this.http.get<Array<StudentInfoResponse>>(this.baseURL + "/getStudents");
    }
    
    public setStudent(student: Student) {
      ParentService.selectedStudent = student;
    }
    
    public getStudent(): Student {
      return ParentService.selectedStudent;
    }
    
    public getAssignments(id: number): Observable<Array<AssignmentsBySubjectResponse>> {
      let httpParams = { params: new HttpParams().set('id', id.toString()) };
      return this.http.get<Array<AssignmentsBySubjectResponse>>(this.baseURL + "/getAssignments", httpParams);
    }
    
    public getAttachments(id: number): Observable<Array<Attachment>> {
      let httpParams = { params: new HttpParams().set('id', id.toString()) };
      return this.http.get<Array<Attachment>>(this.baseURL + "/getAttachments", httpParams);
    }
    
    public downloadAttachment(id: number): Observable<Blob> {
      return this.sharedService.downloadAttachment(id);
    }
    
    public getAttendance(id: number): Observable<Array<Attendance>> {
      let httpParams = { params: new HttpParams().set('id', id.toString()) };
      return this.http.get<Array<Attendance>>(this.baseURL + "/getAttendance", httpParams);
    }
    
    public downloadSupportMaterial(smId: number): Observable<Blob> {
      return this.sharedService.downloadSupportMaterial(smId);
    }
    
    public getSMaterialByClassAndSubject(classId: number, subjectId: number): Observable<Array<SupportMaterial>> {
      return this.sharedService.getSMaterialByClassAndSubject(classId, subjectId);
    }
    
    public getSubjectsByClassId(id: number): Observable<Array<Subject>> {
      let httpParams = { params: new HttpParams().set('id', id.toString()) };
      return this.http.get<Array<Subject>>(this.baseURL + "/getSubjectsByClassId", httpParams);
    }
    
    public getClassByStudentId(id: number): Observable<Class> {
      let httpParams = { params: new HttpParams().set('id', id.toString()) };
      return this.http.get<Class>(this.baseURL + "/getClassByStudentId", httpParams);
    }
    
    getTimetableByStudentId(studentId: number): Observable<TimetableResponse> {
      let httpParams = { params: new HttpParams().set('studentId', studentId.toString()) };
      return this.http.get<TimetableResponse>(this.baseURL + "/getTimetableByStudentId", httpParams);
    }

  }
  