import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParentHomeComponent } from './components/parent-home/parent-home.component';
import { ParentChooseChildComponent } from './components/parent-choose-child/parent-choose-child.component';
import { ParentGradesComponent } from './components/parent-grades/parent-grades.component';
import { FormsModule } from '@angular/forms';
import { ParentRoutingModule } from './parent-routing.module';
import { ParentService } from './parent.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { ParentAssignmentsComponent } from './components/parent-assignments/parent-assignments.component';
import { ParentAttendancesComponent } from './components/parent-attendances/parent-attendances.component';
import { ParentDownloadSupportMaterialComponent } from './components/parent-download-support-material/parent-download-support-material.component';
import { ParentTimetableComponent } from './components/parent-timetable/parent-timetable.component';

@NgModule({
  declarations: [
    ParentHomeComponent,
    ParentChooseChildComponent,
    ParentGradesComponent,
    ParentAssignmentsComponent,
    ParentAttendancesComponent,
    ParentDownloadSupportMaterialComponent,
    ParentTimetableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ParentRoutingModule,
  ],
  providers: [ParentService]
})
export class ParentModule { }
