import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParentHomeComponent } from './components/parent-home/parent-home.component';
import { ParentGradesComponent } from './components/parent-grades/parent-grades.component';
import { AuthGuard } from 'src/app/core/auth.guard';
import { ParentChooseChildComponent } from './components/parent-choose-child/parent-choose-child.component';
import { ParentAssignmentsComponent } from './components/parent-assignments/parent-assignments.component';
import { ParentAttendancesComponent } from './components/parent-attendances/parent-attendances.component';
import { ParentDownloadSupportMaterialComponent } from './components/parent-download-support-material/parent-download-support-material.component';
import { ParentTimetableComponent } from './components/parent-timetable/parent-timetable.component';


const routes: Routes = [
    {
        path: 'parent', canActivate: [AuthGuard], children: [
            { path: '', redirectTo: 'choice', pathMatch: 'full' },
            { path: 'choice', component: ParentChooseChildComponent},
            { path: 'home', component: ParentHomeComponent },
            { path: 'grades', component: ParentGradesComponent},
            { path: 'services', component: ParentChooseChildComponent},
            { path: 'assignments', component: ParentAssignmentsComponent},
            { path: 'attendances', component: ParentAttendancesComponent},
            { path: 'supportMaterial', component: ParentDownloadSupportMaterialComponent},
            { path: 'timetable', component: ParentTimetableComponent},
            { path: '**', redirectTo: 'choice' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class ParentRoutingModule { }
