import { Component, OnInit, ViewChild } from '@angular/core';
import { Routes } from 'src/app/core/routes';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { ParentService } from '../../parent.service';
import { Student } from 'src/app/entities/student';
import { Router } from '@angular/router';
import { Subject } from 'src/app/entities/subject';
import { MESSAGE_TYPE, MessageComponent } from 'src/app/shared/components/message/message.component';
import { SupportMaterial } from 'src/app/entities/supportMaterial';
import * as FileSaver from 'file-saver';
import { Class } from 'src/app/entities/class';

@Component({
  selector: 'app-parent-download-support-material',
  templateUrl: './parent-download-support-material.component.html',
  styleUrls: ['./parent-download-support-material.component.scss']
})
export class ParentDownloadSupportMaterialComponent implements OnInit {
  
  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;
  
  student: Student;
  class: Class;
  selectedSubject: Subject;
  subjects: Array<Subject>;
  loadedSupportMaterial: boolean;
  supportMaterials:Array<SupportMaterial>;
  loaded: boolean;

  constructor(private authenticationService: AuthenticationService,
    private parentService: ParentService,private router: Router) { }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.student = this.parentService.getStudent();
          if (!this.student) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
          }else{
            this.loadClass();
          }
        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      });
  }

  loadClass() {
    this.parentService.getClassByStudentId(this.student.id).subscribe(
      data => {  
          this.class = data;
          this.loadSubjects();   
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSubjects() {
    this.selectedSubject = null;

    this.parentService.getSubjectsByClassId(this.class.id).subscribe(
      data => {
        if (data instanceof Array) {
          this.subjects = data;
          this.loaded = true;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  loadSupportMaterials() {
    this.loadedSupportMaterial = false;

    this.parentService.getSMaterialByClassAndSubject(this.class.id, this.selectedSubject.id).subscribe(
      data => {
        this.supportMaterials = data 
        this.loadedSupportMaterial = true;
      },
      err => {
        this.childComponent.showMessage("Unexpected Error", MESSAGE_TYPE.ERROR);
        this.loadedSupportMaterial = true;
      });
  }

  downloadSupportMaterial(supportMaterial:SupportMaterial){
    this.parentService.downloadSupportMaterial(supportMaterial.id).subscribe(blob => {
      FileSaver.saveAs(blob, supportMaterial.fileName)
    });
  }

}
