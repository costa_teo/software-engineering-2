import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { ParentService } from '../../parent.service';
import { Router } from '@angular/router';
import { Student } from 'src/app/entities/student';
import { Routes } from 'src/app/core/routes';
import { AssignmentsBySubjectResponse } from 'src/app/response/assignments-by-subject-response';
import { Assignment } from 'src/app/entities/assignment';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { Attachment } from 'src/app/entities/attachment';

import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-parent-assignments',
  templateUrl: './parent-assignments.component.html',
  styleUrls: ['./parent-assignments.component.scss']
})
export class ParentAssignmentsComponent implements OnInit {

  public student: Student;
  public loaded: Boolean;
  public assignments: AssignmentsBySubjectResponse[];
  public hidden: Boolean[];
  public loadedAttachments: Boolean[];
  public attachments: Array<Array<Attachment>>;
  public previousIndexAttachments: Array<number>;

  constructor(private authenticationService: AuthenticationService,
    private parentService: ParentService,
    private router: Router) { }
    
    assignmentSettings: Settings = {
      columns: [{
        columnName: 'Date',
        attributeName: 'date',
        isSortable: true
      }, {
        columnName: 'Description',
        attributeName: 'description',
        isSortable: true
      }],
      hasInfo: true,
      hasSorting: true,
      elementsPerPage: 8
    };
  
   ngOnInit() {
   this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.student = this.parentService.getStudent();
          if (!this.student) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
          } else {
            this.loadAssignments(this.student.id);
          }
        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      });
  }

  loadAssignments(id: number) {
    
    this.parentService.getAssignments(id).subscribe(
      data => {
        if (data instanceof Array) {
          this.assignments = data;
          this.hidden = Array<Boolean>(this.assignments.length).fill(true);
          this.loadedAttachments= Array<Boolean>(this.assignments.length).fill(false);
          this.attachments=new Array(this.assignments.length);
          this.previousIndexAttachments= new Array(this.assignments.length);
          this.loaded = true;
        } else {
          console.log("TODO data not valid");
        }
      },
      err => {
        console.error("TODO, id not valid");
      });
  }

  loadAttachments(index: number, a : Array<Assignment>, i: number){
    if(this.loadedAttachments[i]==true && this.previousIndexAttachments[i]==index){
      this.loadedAttachments[i]=false;
    }
    else{
      this.parentService.getAttachments(a[index].id).subscribe(
        data => {
          if (data instanceof Array) {
            this.attachments[i] = data;
            this.previousIndexAttachments[i]=index;
            this.loadedAttachments[i]= true;
          } else {
            console.log("TODO data not valid");
          }
        },
        err => {
          console.error("TODO, id not valid");
        });
    }
  }

  downloadAttachment(a:Attachment){
    this.parentService.downloadAttachment(a.id).subscribe(blob => {      
      FileSaver.saveAs(blob, a.fileName)
    });
  }

}
