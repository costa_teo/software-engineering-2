import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { ParentService } from '../../parent.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Routes } from 'src/app/core/routes';
import { Student } from 'src/app/entities/student';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { TimetableRow } from 'src/app/shared/components/timetable/timetable/timetableRow';

@Component({
  selector: 'app-parent-timetable',
  templateUrl: './parent-timetable.component.html',
  styleUrls: ['./parent-timetable.component.scss']
})
export class ParentTimetableComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;
  
  public loaded: Boolean;
  public student: Student;
  timetableData: TimetableRow[];
  className: string;

  constructor(private authenticationService: AuthenticationService,
    private parentService: ParentService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.student = this.parentService.getStudent();
          if (!this.student) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
          } else {
            this.loadTimetableStudent(this.student.id);
          }
        }
      },
      err => {
        this.authenticationService.logout();
      });
    }

  loadTimetableStudent(studentId: number) {
    this.parentService.getTimetableByStudentId(studentId).subscribe(
      data=> {
        this.timetableData = data.timetable;
        this.className = data.className;
        this.loaded = true;
      }, err=>{
        this.childComponent.showMessage("Unexpeced error", MESSAGE_TYPE.ERROR)
      }
    )
  }
    
    
}




