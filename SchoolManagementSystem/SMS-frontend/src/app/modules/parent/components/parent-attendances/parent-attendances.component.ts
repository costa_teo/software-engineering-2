import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { ParentService } from '../../parent.service';
import { Router } from '@angular/router';
import { Routes } from 'src/app/core/routes';
import { Student } from 'src/app/entities/student';
import { TIMESLOTS } from 'src/app/shared/constants';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';
import { Attendance, AttendanceType } from 'src/app/entities/attendance';

@Component({
  selector: 'app-parent-attendances',
  templateUrl: './parent-attendances.component.html',
  styleUrls: ['./parent-attendances.component.scss']
})
export class ParentAttendancesComponent implements OnInit {

  public absenceArray: Array<Attendance>;
  public exitArray: Array<Attendance>;
  public enterArray: Array<Attendance>;
  public timeslotsLookup = TIMESLOTS.valuesList;
  public loaded: Boolean;
  public hidden1: Boolean;
  public hidden2: Boolean;
  public hidden3: Boolean;
  public student: Student;

  constructor(private authenticationService: AuthenticationService,
    private parentService: ParentService,
    private router: Router) { 
      this.hidden1 = true;
      this.hidden2 = true;
      this.hidden3 = true;
    }

  attendanceSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Timeslot',
      attributeName: 'timeslot'
    }],
    hasSorting: true,
    elementsPerPage: 8
  };
  attendanceAbsenceSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }],
    hasSorting: true,
    elementsPerPage: 8
  };

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.student = this.parentService.getStudent();
          if (!this.student) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
          } else {
            this.loadAttendancies(this.student.id);
          }
        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      });
  }

  loadAttendancies(id: number) {
    this.parentService.getAttendance(id).subscribe(
      data => {
        let absenceArray: Array<Attendance> = new Array<Attendance>();
        let enterArray: Array<Attendance> = new Array<Attendance>();
        let exitArray: Array<Attendance> = new Array<Attendance>();
        data.forEach(function (value) {
          if(value.type == AttendanceType.ABSENCE){
            absenceArray.push(value);
            
          }
          else if(value.type == AttendanceType.ENTER){
            enterArray.push(value);
          }
          else if(value.type == AttendanceType.EXIT){
            exitArray.push(value);
          }
        });
        
        this.absenceArray = absenceArray;
        this.exitArray = exitArray;
        this.enterArray = enterArray;
        this.loaded = true;
      },
      err =>{
        console.log(err)
        //TODO this.messagechild("errore")
      }
    );

  }

  getTimeslotLabel(currValue: number): string {
    return this.timeslotsLookup.find(el => {
      return el.value == currValue;
    }).label;
  }
}



