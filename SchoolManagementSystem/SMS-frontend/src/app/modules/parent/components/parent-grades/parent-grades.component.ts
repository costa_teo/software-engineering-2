import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { ParentService } from '../../parent.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Routes } from 'src/app/core/routes';
import { Student } from 'src/app/entities/student';
import { GradesBySubjectResponse } from 'src/app/response/grades-by-subject-response';
import { GRADES } from 'src/app/shared/constants';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';

@Component({
  selector: 'app-parent-grades',
  templateUrl: './parent-grades.component.html',
  styleUrls: ['./parent-grades.component.scss']
})
export class ParentGradesComponent implements OnInit {

  public gradesArray: Array<GradesBySubjectResponse>;
  public gradesLookup = GRADES.valuesList;
  public loaded: Boolean;
  public hidden: Boolean[];
  public student: Student;

  constructor(private authenticationService: AuthenticationService,
    private parentService: ParentService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  gradeSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Grade',
      attributeName: 'value',
    }, {
      columnName: 'Description',
      attributeName: 'description',
      isSortable: true
    }],
    hasSorting: true,
    elementsPerPage: 8
  };

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.student = this.parentService.getStudent();
          if (!this.student) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
          } else {
            this.loadGrades(this.student.id);
          }
        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      });
  }

  loadGrades(id: number) {
    this.parentService.getGrades(id).subscribe(
      data => {
        if (data instanceof Array) {
          this.gradesArray = data;
          this.hidden = Array<Boolean>(this.gradesArray.length).fill(true);
          this.loaded = true;
        } else {
          this.gradesArray = [];
          this.hidden = [];
          this.loaded = true;
        }
      },
      err => {
        console.error("TODO, id not valid");
      });
  }

  getGradeLabel(currValue: number): string {
    return this.gradesLookup.find(el => {
      return el.value == currValue;
    }).label;
  }
}



