import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/entities/student';
import { ParentService } from '../../parent.service';
import { Router } from '@angular/router';
import { Routes } from 'src/app/core/routes';

@Component({
  selector: 'app-parent-choose-child',
  templateUrl: './parent-choose-child.component.html',
  styleUrls: ['./parent-choose-child.component.scss']
})
export class ParentChooseChildComponent implements OnInit {

  loaded: boolean;
  students: Student[];

  constructor(
    private parentService: ParentService,
    private router: Router) { }

  ngOnInit() {

    this.parentService.getStudents().subscribe(
      data => {
        if (data instanceof Array) {
          this.students = data;
        } else {
          console.log("TODO data not valid");
        }
      },
      err => {
        console.error("TODO, id not valid");
      });

    this.loaded = true;
  }

  chooseChild(student: Student) {
    this.parentService.setStudent(student);
    this.router.navigateByUrl(Routes.PARENT_HOME)
  }
}
