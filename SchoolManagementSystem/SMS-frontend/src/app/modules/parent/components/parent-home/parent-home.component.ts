import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from 'src/app/core/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ParentService } from '../../parent.service';
import { Routes } from 'src/app/core/routes';
import { Student } from 'src/app/entities/student';
import { ServiceBoxService } from 'src/app/shared/components/service-box/service-box.service';
import { ServiceBoxData } from 'src/app/shared/components/service-box/service-box-data';
import { GeneralCommunication } from 'src/app/entities/generalCommunication';
import { MessageComponent, MESSAGE_TYPE } from 'src/app/shared/components/message/message.component';
import { SharedService } from 'src/app/shared/shared.service';
import { Settings } from 'src/app/shared/components/my-table/my-table-settings';

@Component({
  selector: 'app-parent-home',
  templateUrl: './parent-home.component.html',
  styleUrls: ['./parent-home.component.scss']
})
export class ParentHomeComponent implements OnInit {

  @ViewChild(MessageComponent, { static: false }) childComponent: MessageComponent;
  
  public loaded: boolean;
  public loadingTable: boolean;
  public generalCommunications: Array<GeneralCommunication>;

  public student: Student;
  public services: Array<ServiceBoxData> = new Array<ServiceBoxData>();

  constructor(
    private authenticationService: AuthenticationService,
    private sharedService: SharedService,
    private parentService: ParentService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private serviceBoxService: ServiceBoxService) { }

  ngOnInit() {
    this.authenticationService.authenticateUser().subscribe(
      data => {
        if (!data) {
          this.authenticationService.logout();
        } else {
          this.student = this.parentService.getStudent();
          if (!this.student) {
            this.router.navigateByUrl(Routes.PARENT_CHOICE);
          }
          this.services = this.serviceBoxService.parentServices;
          this.loadGC();
          this.loaded = true;
        }
      },
      err => {
        console.error("TODO");
        this.authenticationService.logout();
      }
    );
  }

  GCViewTableSettings: Settings = {
    columns: [{
      columnName: 'Date',
      attributeName: 'date',
      isSortable: true
    }, {
      columnName: 'Description',
      attributeName: 'description',
    }],
    hasSorting: true,
    elementsPerPage: 10
  }

  loadGC(){
    this.loadingTable=true;
    this.sharedService.getGeneralCommunications().subscribe(
      data => {
        if (data instanceof Array) {
          this.generalCommunications = data;
          this.loadingTable = false;
        } else {
          this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
          console.error("TODO");
        }
      },
      err => {
        this.childComponent.showMessage("Unexpected error", MESSAGE_TYPE.ERROR);
        console.error(err);
      });
  }

  selectService(route: string) {
    this.router.navigateByUrl(route);
  }

}
