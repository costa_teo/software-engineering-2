import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from './modules/home/home.module';
import { TeacherModule } from './modules/teacher/teacher.module';
import { AdministratorModule } from './modules/administrator/administrator.module';
import { ParentModule } from './modules/parent/parent.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './core/authentication.service';
import { AuthGuard } from './core/auth.guard';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import {HashLocationStrategy, LocationStrategy} from '@angular/common'

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    HomeModule,
    ParentModule,
    TeacherModule,
    AdministratorModule,
    AppRoutingModule //AppRoutingModule VA PER ULTIMO!!! (Per il redirect)  
  ],
  providers: [
    AuthGuard,
    AuthenticationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationService,
      multi: true
    },{
      provide: LocationStrategy, 
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
