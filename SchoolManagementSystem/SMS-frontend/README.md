

>Copyright © 2019 Matteo Costa Politecnico di Torino DAUIN department. \
>All rights reserved. This guide and the accompanying materials
>are available for free.\
> Let's go offer a coffee to the authors.

Contributors: 
1. [Matteo Costa](https://gitlab.com/costa_teo) - Deployment tutorial

# if problem install
npm install angular-datatables --save

# How do I deploy the application?

1. run command 
    > ng build --prod

2. if there are any error fix it, all protected and private object must be declared public or without visibility key [private, protected]

3. copy the content of dist/SMS-frontend inside:
    >SchoolManagementSystem\SMS-backend\src\main\resources\public

4. open file, and comments all Development environment rows and uncomment Deployment rows
    >SchoolManagementSystem\SMS-backend\src\main\resources\public\application.properties

5. run in eclipse 
    > maven build package

6. fix test if does not pass

7. from **target\SMS-backend-\[version\]\WEB-INF\classes** move the myDB.mv.db file into targer 

8. **from target dir** *copy SMS-backend-\[version\].war*, *myDB.mv.db* and *SMS-backend- \[version\]* directory in docker or another server envirenment.


9. lounch it with docker or with java command
    > ENTRYPOINT \["java", "-jar", "SMS.\[version\].war"\]

10. the application runs on port 443, with https. Have fun!