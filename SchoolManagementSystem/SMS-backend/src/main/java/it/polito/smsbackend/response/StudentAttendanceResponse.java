package it.polito.smsbackend.response;

import it.polito.smsbackend.dto.Attendance;

public class StudentAttendanceResponse {
	private Integer studentId;
	private String studentName;
	private Attendance attendance1;
	private Attendance attendance2;
	
	
	public StudentAttendanceResponse(Integer studentId, String studentName, Attendance attendance1, Attendance attendance2) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.attendance1 = attendance1;
		this.attendance2 = attendance2;
	}


	public Integer getStudentId() {
		return studentId;
	}


	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}


	public String getStudentName() {
		return studentName;
	}


	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}


	public Attendance getAttendance1() {
		return attendance1;
	}


	public void setAttendance1(Attendance attendance1) {
		this.attendance1 = attendance1;
	}


	public Attendance getAttendance2() {
		return attendance2;
	}


	public void setAttendance2(Attendance attendance2) {
		this.attendance2 = attendance2;
	}
	
	
	

}
