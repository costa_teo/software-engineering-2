package it.polito.smsbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.AttendanceDAO;
import it.polito.smsbackend.dto.Attendance;

public interface AttendanceRepository extends CrudRepository<AttendanceDAO, Integer> {

	@Query(value = "SELECT new it.polito.smsbackend.dto.Attendance(a.id, a.date, a.timeslot, a.type, a.studentRef) FROM AttendanceDAO a WHERE a.studentRef = :studentId ORDER BY a.type, a.date", nativeQuery = false)
	List<Attendance> getAttendanceById(@Param("studentId") Integer studentId);

	@Query(value = "SELECT new it.polito.smsbackend.dto.Attendance(a.id, a.date, a.timeslot, a.type, a.studentRef) "
			+ "FROM AttendanceDAO a, StudentDAO s "
			+ "WHERE s.classRef = :classId AND "
			+ "a.studentRef = s.id AND "
			+ "a.date= :date "
			+ "ORDER BY s.surname", nativeQuery = false)
	List<Attendance> getAttendanceByClassIdAndDate(@Param("classId") Integer classId, @Param("date") String date);

	@Query(value = "SELECT * "
			+ "FROM ATTENDANCE a, STUDENT s "
			+ "WHERE s.class_ref = :classId AND "
			+ "a.student_ref = s.id AND "
			+ "a.date= :date "
			+ "ORDER BY s.surname", nativeQuery = true)
	List<AttendanceDAO> getAttendanceDAOByClassIdAndDate(Integer classId, String date);

	
	
}
