package it.polito.smsbackend.common;

public enum AttendanceType {
	Absence("Absence"), 
	Enter("Enter"), 
    Exit("Exit");

private String type;

AttendanceType(String type) {
this.type = type;
}

public String getType() {
return type;
} 
}
