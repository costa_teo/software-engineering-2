package it.polito.smsbackend.request;

public class CreateClassesStudentRequest {
	
	private String student;
	private String classe;
	
	public CreateClassesStudentRequest(String student, String classe) {
		this.student = student;
		this.classe = classe;
	}
	
	public String getClasse() {
		return classe;
	}
	public void setClasse(String classe) {
		this.classe = classe;
	}
	public String getStudent() {
		return student;
	}
	public void setStudent(String student) {
		this.student = student;
	}
}
