package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		name="TIMETABLE",
		uniqueConstraints={
				@UniqueConstraint(columnNames = {"day_of_week", "time_slot", "teach_sub_class_ref"})}
		)
public class TimeTableDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "timetable_generator")
	@SequenceGenerator(name="timetable_generator", sequenceName = "timetable_seq", allocationSize=1)
	private Integer id;
	@Column(nullable = false, name="day_of_week")
	private Integer dayOfWeek;
	@Column(nullable = false, name="time_slot")
	private Integer timeSlot;
	@Column(nullable = false, name = "teach_sub_class_ref")
	private Integer teachSubClassRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(Integer dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public Integer getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(Integer timeSlot) {
		this.timeSlot = timeSlot;
	}

	public Integer getTeachSubClassRef() {
		return teachSubClassRef;
	}

	public void setTeachSubClassRef(Integer teachSubClassRef) {
		this.teachSubClassRef = teachSubClassRef;
	}

}
