package it.polito.smsbackend.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginRequest {
	
	private String username;
	private String password;
	private String role;
	
	public LoginRequest(
			@JsonProperty("username") String username,
			@JsonProperty("password") String password,
			@JsonProperty("role") String role) {
		this.username = username;
		this.password = password;
		this.setRole(role);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

}
