package it.polito.smsbackend.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.TimeTableDAO;

public interface TimeTableRepository extends CrudRepository<TimeTableDAO, Integer> {
	@Modifying
	@Query(value = "DELETE FROM TIMETABLE t WHERE t.TEACH_SUB_CLASS_REF IN ("
			+ "SELECT tsc.ID FROM TEACHER_SUBJECT_CLASS tsc WHERE tsc.CLASS_REF = :cId)", nativeQuery = true)
	void deleteByClassId(@Param("cId") Integer classId);
	
	@Query(value = "SELECT * FROM TIMETABLE t, TEACHER_SUBJECT_CLASS tsc "
			+ "WHERE t.TEACH_SUB_CLASS_REF = tsc.id AND "
			+ "tsc.CLASS_REF = :cId "
			+ "ORDER BY t.TIME_SLOT", nativeQuery = true)
	List<TimeTableDAO> getByClassIdOrderByTimeSlot(@Param("cId") Integer classId);
	
	@Query(value = "SELECT * FROM TIMETABLE t, TEACHER_SUBJECT_CLASS tsc "
			+ "WHERE t.TEACH_SUB_CLASS_REF = tsc.id AND "
			+ "tsc.TEACHER_REF = :tId "
			+ "ORDER BY t.TIME_SLOT", nativeQuery = true)
	List<TimeTableDAO> getByTeacherIdOrderByTimeSlot(@Param("tId") Integer teacherId);
	
	@Query(value = "SELECT COUNT(*) FROM TIMETABLE t, TEACHER_SUBJECT_CLASS tsc "
			+ "WHERE t.TEACH_SUB_CLASS_REF = tsc.ID "
			+ "GROUP BY t.DAY_OF_WEEK, t.TIME_SLOT, tsc.TEACHER_REF", nativeQuery = true)
	List<Integer> getCountGroupByTimeAndDayAndTeacher();
}
