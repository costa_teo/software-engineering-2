package it.polito.smsbackend.repository;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.LectureDAO;
import it.polito.smsbackend.dto.Lecture;

public interface LectureRepository extends CrudRepository<LectureDAO, Integer> {

	@Query(value = "SELECT new it.polito.smsbackend.dto.Lecture(l.id, l.date, c.name, u.name, s.name, l.description, l.timeSlot) " +
			"FROM LectureDAO l, TeacherSubjectClassDAO tsc, SubjectDAO s, TeacherDAO t, ClassDAO c, UserDAO u " +
			"WHERE l.teachSubClassRef = tsc.id AND " +
			"s.id = tsc.subjectRef AND " +
			"c.id = tsc.classRef AND " +
			"(:subjectId IS NULL OR tsc.subjectRef = :subjectId) AND " +
			"(:classId IS NULL OR tsc.classRef = :classId) AND " +
			"tsc.teacherRef = t.id AND " +
			"t.userRef = u.id AND " +
			"u.id = :userId", nativeQuery = false)
	ArrayList<Lecture> getLecturesByUserIdAndByOptionalSubjectIdAndByOptionalClassId(@Param("userId") Integer userId, @Param("subjectId") Integer subjectId, @Param("classId") Integer classId);

	@Modifying
	@Transactional
	@Query(value="INSERT into LECTURE (id, date, time_slot, description, teach_sub_class_ref) "
			+ "VALUES (lecture_seq.nextval, :date, :slot, :desc, :ref)", nativeQuery=true)
	void insertLectureTopics(@Param("date") String date, @Param("slot") Integer slot, @Param("ref") Integer ids, @Param("desc") String description);

	@Modifying
	@Transactional
	@Query(value = "UPDATE LECTURE " + 
			"SET DESCRIPTION = :desc " + 
			"WHERE id = :id",
			nativeQuery=true)
	void updateLecture(@Param ("id") Integer lId, @Param ("desc") String desc);

	@Modifying
	@Transactional
	@Query(value = "DELETE FROM LECTURE WHERE id = :id"
	, nativeQuery=true)
	void deleteLecture(@Param("id") Integer lectureId);
}
