package it.polito.smsbackend.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import it.polito.smsbackend.dto.Grade;

public class GradesBySubjectResponse {
	private BigDecimal average;
    private String subject;
    private String teacher;
    private  List<Grade> grades;
	
    public BigDecimal getAverage() {
		return average;
	}
	
    public void setAverage(BigDecimal average) {
		this.average = average;
	}
	
    public String getSubject() {
		return subject;
	}
	
    public void setSubject(String subject) {
		this.subject = subject;
	}
	
    public String getTeacher() {
		return teacher;
	}
	
    public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	
    public List<Grade> getGrades() {
		return grades;
	}
	
    public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}
	
	public void addGrades(Grade grade) {
		if(grades == null) {
			grades = new ArrayList<>();
		}
		this.grades.add(grade);
	}
	
}
