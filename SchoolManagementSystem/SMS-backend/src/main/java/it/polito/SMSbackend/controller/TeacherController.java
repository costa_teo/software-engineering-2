package it.polito.smsbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.dao.AssignmentDAO;
import it.polito.smsbackend.dto.Assignment;
import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.Grade;
import it.polito.smsbackend.dto.Lecture;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.SupportMaterial;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.request.InsertGradeRequest;
import it.polito.smsbackend.request.NewAttachmentRequest;
import it.polito.smsbackend.request.NewSupportMaterialRequest;
import it.polito.smsbackend.response.StudentAttendanceResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.service.AuthenticationService;
import it.polito.smsbackend.service.TeacherService;

@RestController
@RequestMapping(value = "teacher")
@CrossOrigin
public class TeacherController {

	private final TeacherService teacherService;
	private final AuthenticationService authenticationService;

	@Autowired
	public TeacherController(TeacherService teacherService, AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
		this.teacherService = teacherService;
	}

	@GetMapping(value = "getAllClasses")
	public List<Class> getAllClasses(@RequestHeader("token") String token)  {
		Integer userId = authenticationService.authenticateUser(token);
		return teacherService.getAllClasses(userId);
	}

	@GetMapping(value = "getSubjectsByClassId")
	public List<Subject> getSubjectsByClassId(@RequestHeader("token") String token, @RequestParam("id") Integer classId)  {
		Integer userId = authenticationService.authenticateUser(token);
		return teacherService.getSubjectsByClassId(userId, classId == -1? null : classId);
	}

	@GetMapping(value = "getLecturesByClassIdAndSubjectId")
	public List<Lecture> getLecturesByClassIdAndSubjectId(@RequestHeader("token") String token, @RequestParam("classId") Integer classId, @RequestParam("subjectId") Integer subjectId)  {
		Integer userId = authenticationService.authenticateUser(token);
		return teacherService.getLecturesByClassIdAndSubjectId(userId, classId == -1? null : classId, subjectId == -1 ? null : subjectId);
	}

	@PostMapping(value = "insertLectureTopics")
	public Lecture insertLectureTopics(@RequestHeader("token") String token, @RequestBody Lecture request) {
		Integer userId = authenticationService.authenticateUser(token);
		return teacherService.insertLectureTopics(userId, request);
	}

	@PostMapping(value = "updateLecture")
	public void updateLecture(@RequestHeader("token") String token, @RequestBody Lecture updatedLecture) {
		Integer userId = authenticationService.authenticateUser(token);
		teacherService.updateLecture(userId, updatedLecture);
	}

	@PostMapping(value = "deleteLecture")
	public void deleteLecture(@RequestHeader("token") String token, @RequestBody Lecture request) {
		Integer userId = authenticationService.authenticateUser(token);
		teacherService.deleteLecture(userId, request);
	}

	@GetMapping(value = "getStudentsByClassId")
	public List<StudentInfoResponse> getStudentsByClassId(@RequestHeader("token") String token, @RequestParam("id") Integer classId)  {
		authenticationService.authenticateUser(token);
		return teacherService.getStudentsByClassId(classId);
	}
	
	@GetMapping(value = "getGradesByStudentAndSubject")
	public List<Grade>getGradesByStudentAndSubject(@RequestHeader("token") String token, @RequestParam("studentId") Integer studentId, @RequestParam("subjectId") Integer subjectId)  {
		authenticationService.authenticateUser(token);
		return teacherService.getGradesByStudentAndSubject(studentId, subjectId);
	}
	
	@PostMapping(value = "insertGrades")
	public void insertGrades(@RequestHeader("token") String token, @RequestBody InsertGradeRequest request) {
		Integer userId = authenticationService.authenticateUser(token);
		teacherService.insertGrades(userId, request);
	}
	
	@PostMapping(value = "updateGrade")
	public void updateGrade(@RequestHeader("token") String token, @RequestBody Grade request) {
		authenticationService.authenticateUser(token);
		teacherService.updateGrade(request);
	}
	
	@PostMapping(value = "deleteGrade")
	public void deleteGrade(@RequestHeader("token") String token,  @RequestBody Grade request) {
		authenticationService.authenticateUser(token);
		teacherService.deleteGrade(request);
	}
	
	@GetMapping(value = "getStudentsAttendanceByClassIdAndDate")
	public List<StudentAttendanceResponse> getStudentsAttendanceByClassIdAndDate(@RequestHeader("token") String token, 
			@RequestParam("classId") Integer classId,
			@RequestParam("date") String date)  {
		authenticationService.authenticateUser(token);
		return teacherService.getStudentsAttendanceByClassIdAndDate(classId, date);
	}
	
	@PostMapping(value = "insertAttendance")
	public void deleteGrade(@RequestHeader("token") String token,
			@RequestParam("date") String date, 
			@RequestParam("classId") Integer classId,
			@RequestBody List<StudentAttendanceResponse> attendances) {
		authenticationService.authenticateUser(token);
		teacherService.insertAttendances(attendances, classId, date);
	}
	
	@GetMapping(value = "getAssignmentByClassIdAndSubjectId")
	public List<AssignmentDAO> getAssignmentByClassIdAndSubjectId(@RequestHeader("token") String token, 
			@RequestParam("classId") Integer classId,
			@RequestParam("subjectId") Integer subjectId)  {
		Integer userId = authenticationService.authenticateUser(token);
		return teacherService.getAssignmentByClassIdAndSubjectId(userId, classId, subjectId);
	}
	
	@GetMapping(value = "getAttachmentsByAssignmentId")
	public List<Attachment> getAttachmentsByAssignmentId(@RequestHeader("token") String token, 
			@RequestParam("assignmentid") Integer assignmentid)  {
		authenticationService.authenticateUser(token);
		return teacherService.getAttachmentsByAssignmentId(assignmentid);
	}
	
	@PostMapping(value = "updateAssignment")
	public void updateAssignment(@RequestHeader("token") String token, @RequestBody Assignment request) {
		authenticationService.authenticateUser(token);
		teacherService.updateAssignment(request);
	}
	
	@PostMapping(value = "deleteAssignment")
	public void deleteAssignment(@RequestHeader("token") String token,  @RequestBody Integer index) {
		authenticationService.authenticateUser(token);
		teacherService.deleteAssignment(index);
	}
	
	@PostMapping(value = "insertAssignment")
	public Integer deleteGrade(@RequestHeader("token") String token,  @RequestBody Assignment request) {
		Integer userId=authenticationService.authenticateUser(token);
		return teacherService.insertAssignment(userId, request);
	}
	
	@PostMapping(value = "uploadAttachment")
	public void uploadAttachment(@RequestHeader("token") String token,  @ModelAttribute NewAttachmentRequest request) {
		authenticationService.authenticateUser(token);
		teacherService.uploadAttachment(request);
	}
	
	@PostMapping(value = "deleteAttachment")
	public void deleteAttachment(@RequestHeader("token") String token,  @RequestBody Attachment a) {
		authenticationService.authenticateUser(token);
		teacherService.deleteAttachment(a.getId());
	}
	
	@PostMapping(value = "deleteSupportMaterial")
	public void deleteSupportMaterial(@RequestHeader("token") String token,  @RequestBody SupportMaterial s) {
		authenticationService.authenticateUser(token);
		teacherService.deleteSupportMaterial(s.getId());
	}

	@PostMapping(value = "uploadSupportMaterial")
	public void uploadSupportMaterial(@RequestHeader("token") String token,  @ModelAttribute NewSupportMaterialRequest request) {
		authenticationService.authenticateUser(token);
		
		Integer userId = Integer.valueOf(token.split("_")[0]);
		
		teacherService.uploadSupportMaterial(userId, request);
	}
	
	
	@GetMapping(value = "getTimetable")
	public List<TimeTableRow> getTimetable(@RequestHeader("token") String token)  {
		Integer userId = authenticationService.authenticateUser(token);
		
		return teacherService.getTimetableByUserId(userId); 
	}
	
}
