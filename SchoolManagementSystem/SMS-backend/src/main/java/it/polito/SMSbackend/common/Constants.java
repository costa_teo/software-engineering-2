package it.polito.smsbackend.common;

public final class Constants {
	
	private Constants() {
		//
	}
	
	public static final String PARENT = "PARENT";
	public static final String TEACHER = "TEACHER";
	public static final String ADMINISTRATOR = "ADMINISTRATOR";
	public static final String PRINCIPAL = "PRINCIPAL";
}
