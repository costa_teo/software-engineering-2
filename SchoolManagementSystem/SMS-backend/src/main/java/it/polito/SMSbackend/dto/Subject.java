package it.polito.smsbackend.dto;

import it.polito.smsbackend.dao.SubjectDAO;

public class Subject {

	private Integer id;
	private String name;

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public SubjectDAO map() {
		SubjectDAO s=new SubjectDAO();
		s.setId(id);
		s.setName(name);
		return s;
	}

}

