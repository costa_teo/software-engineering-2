package it.polito.smsbackend.response;

public class TeacherInfoResponse {
	
	private String name;
	private String surname;
	private Integer id;
	
	public TeacherInfoResponse(String name, String surname, Integer id) {
		this.name = name;
		this.surname = surname;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
}
