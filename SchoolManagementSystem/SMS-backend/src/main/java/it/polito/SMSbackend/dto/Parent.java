package it.polito.smsbackend.dto;

import it.polito.smsbackend.dao.ParentDAO;

public class Parent {

	private Integer id;
	private String fiscalCode;
	private String gender;
	private String birthdate;
	private String telephoneN;
	private String name;
	private String surname;
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getTelephoneN() {
		return telephoneN;
	}

	public void setTelephoneN(String telephoneN) {
		this.telephoneN = telephoneN;
	}


	public ParentDAO map() {
		ParentDAO p = new ParentDAO();
		p.setBirthdate(this.birthdate);
		p.setFiscalCode(this.fiscalCode);
		p.setGender(this.gender);
		p.setId(this.id);
		p.setTelephoneN(this.telephoneN);

		return p;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


}

