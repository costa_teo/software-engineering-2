package it.polito.smsbackend.dto;

import java.math.BigDecimal;

public class NewGrade {
	
	private Integer studentId;
	private String name;
	private String surname;
	private BigDecimal value;
	
	public NewGrade(Integer studentId, String name, String surname, BigDecimal value) {
		this.studentId = studentId;
		this.name = name;
		this.surname = surname;
		this.value = value;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
	
	

}
