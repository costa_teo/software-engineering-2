package it.polito.smsbackend.dto;

import java.math.BigDecimal;

public class Grade {
	
	private Integer id;
	private BigDecimal value;
	private String subject;
	private String teacher;
	private String date;
	private String description;
	private Integer timeslot;
	
	public Grade() {}

	/**
	 * THIS CONSTRUCTOR IS USED FOR QUERIES ONLY. PLEASE USE GETTERS/SETTERS TO MANIPULATE NORMAL OBJECT
	 */
	public Grade(Integer id, BigDecimal value, String subject, String teacher, String date, String description, Integer timeslot) {
		this.id = id;
		this.value = value;
		this.subject = subject;
		this.teacher = teacher;
		this.date = date;
		this.description = description;
		this.setTimeslot(timeslot);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(Integer timeslot) {
		this.timeslot = timeslot;
	}
	
}
