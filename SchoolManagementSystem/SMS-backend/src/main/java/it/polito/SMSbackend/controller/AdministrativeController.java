package it.polito.smsbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import it.polito.smsbackend.request.AddParentStudentRequest;
import it.polito.smsbackend.request.CreateClassesStudentRequest;
import it.polito.smsbackend.request.TimeTableRequest;
import it.polito.smsbackend.request.UpdateClassRequest;
import it.polito.smsbackend.request.UpdateTeachingInClassRequest;
import it.polito.smsbackend.service.AdministrativeService;
import it.polito.smsbackend.service.AuthenticationService;
import it.polito.smsbackend.service.UserService;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TeacherInfoResponse;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.dto.User;

@RestController
@RequestMapping(value = "administrator")
@CrossOrigin
public class AdministrativeController {

	private final AdministrativeService administrativeService;
	private final AuthenticationService authenticationService;
	private final UserService userService;

	@Autowired
	public AdministrativeController(AdministrativeService administrativeService,
			AuthenticationService authenticationService, UserService userService) {
		this.administrativeService = administrativeService;
		this.authenticationService = authenticationService;
		this.userService = userService;
	}

	@GetMapping(value = "checkParent")
	public boolean checkParent(@RequestHeader("token") String token, @RequestParam("id") String parentFiscalId) {
		authenticationService.authenticateUser(token);
		return administrativeService.checkIfParentExists(parentFiscalId);
	}

	@GetMapping(value = "checkStudent")
	public boolean checkStudent(@RequestHeader("token") String token, @RequestParam("id") String studentFiscalId) {
		authenticationService.authenticateUser(token);
		return administrativeService.checkIfStudentExists(studentFiscalId);
	}

	@PostMapping(value = "addParentStudent")
	public void checkStudent(@RequestHeader("token") String token,
			@RequestBody AddParentStudentRequest parentsStudent) {
		authenticationService.authenticateUser(token);
		administrativeService.addParentsStudent(parentsStudent);
	}

	@GetMapping(value = "getStudentsInClass")
	public List<StudentInfoResponse> getStudentsInClass(@RequestHeader("token") String token,
			@RequestParam("id") Integer classId) {
		authenticationService.authenticateUser(token);
		return administrativeService.getStudentsInClass(classId == -1 ? null : classId);
	}

	@GetMapping(value = "getAllClasses")
	public List<Class> getAllClasses(@RequestHeader("token") String token) {
		authenticationService.authenticateUser(token);
		return administrativeService.getAllClasses();
	}

	@PostMapping(value = "createClass")
	public String createClass(@RequestHeader("token") String token, @RequestBody Integer classNumber) {
		authenticationService.authenticateUser(token);
		return administrativeService.createClass(classNumber);
	}

	@PostMapping(value = "updateClass")
	public void updateClass(@RequestHeader("token") String token, @RequestBody UpdateClassRequest request) {
		authenticationService.authenticateUser(token);
		administrativeService.updateClass(request);
	}

	@PostMapping(value = "createExcelClasses")
	public void createExcelClasses(@RequestHeader("token") String token,
			@RequestBody List<CreateClassesStudentRequest> request) {
		authenticationService.authenticateUser(token);
		administrativeService.createExcelClasses(request);
	}

	@PostMapping(value = "insertGeneralCommunication")
	public void insertGeneralCommunication(@RequestHeader("token") String token,
			@RequestBody GeneralCommunication request) {
		authenticationService.authenticateUser(token);
		administrativeService.insertGeneralCommunications(request);
	}

	@PostMapping(value = "updateCommunication")
	public void updateGeneralCommunication(@RequestHeader("token") String token,
			@RequestBody GeneralCommunication request) {
		authenticationService.authenticateUser(token);
		administrativeService.updateGeneralCommunications(request);
	}

	@PostMapping(value = "deleteCommunication")
	public void deleteGeneralCommunication(@RequestHeader("token") String token,
			@RequestBody GeneralCommunication request) {
		authenticationService.authenticateUser(token);
		administrativeService.deleteGeneralCommunications(request);
	}

	@GetMapping(value = "getAllSubjects")
	public List<Subject> getAllSubjects(@RequestHeader("token") String token) {
		authenticationService.authenticateUser(token);
		return administrativeService.getAllSubjects();
	}

	@GetMapping(value = "getTeacherByClassAndSubject")
	public TeacherInfoResponse getTeacherByClassAndSubject(@RequestHeader("token") String token,
			@RequestParam("subjectId") Integer subjectId, @RequestParam("classId") Integer classId) {
		authenticationService.authenticateUser(token);
		return administrativeService.getTeacherByClassAndSubject(subjectId, classId);
	}
	
	@GetMapping(value = "getAllTeachers")
	public List<TeacherInfoResponse> getAllTeachers(@RequestHeader("token") String token){
		authenticationService.authenticateUser(token);
		return administrativeService.getAllTeachers();
	}
	
	@PostMapping(value = "updateTeachingInClass")
	public void updateTeachingInClass(@RequestHeader("token") String token, @RequestBody UpdateTeachingInClassRequest request) {
		authenticationService.authenticateUser(token);
		administrativeService.updateTeachingInClass(request);
	}
	
	@PostMapping(value = "addAccount")
	public void addAccount(@RequestHeader("token") String token, @RequestBody User request) {
		authenticationService.authenticateUser(token);
		administrativeService.addAccount(request);
	}
	
	
	@PostMapping(value = "addClassTimetable")
	public void addClassTimetable(@RequestHeader("token") String token, @RequestBody TimeTableRequest request) {
		authenticationService.authenticateUser(token);
		administrativeService.addClassTimetable(request);
	}
	
	
	@GetMapping(value = "getTimetableByClass")
	public List<TimeTableRow> getTimetableByClass(@RequestHeader("token") String token,
			@RequestParam("classId") Integer classId){
		authenticationService.authenticateUser(token);
		return userService.getTimetableByClassId(classId);
	}
	
	@GetMapping(value = "getTimetableByTeacher")
	public List<TimeTableRow> getTimetableByTeacher(@RequestHeader("token") String token,
			@RequestParam("teacherId") Integer teacherId){
		authenticationService.authenticateUser(token);
		return userService.getTimetableByTeacherId(teacherId);
	}
	
	
}
