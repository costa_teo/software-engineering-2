package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.TeacherDAO;
import it.polito.smsbackend.response.TeacherInfoResponse;

public interface TeacherRepository extends CrudRepository<TeacherDAO, Integer> {

	@Query(value = "SELECT t.id FROM TEACHER t, USER u WHERE u.id = :userId AND u.id = t.user_ref", nativeQuery = true)
	Integer getTeacherIdByUserId(@Param("userId") Integer userId);

	@Query(value = "Select distinct new it.polito.smsbackend.response.TeacherInfoResponse(u.name, u.surname, t.id) "
			+ "from TeacherDAO t, TeacherSubjectClassDAO tsc, UserDAO u "
			+ "WHERE t.id=tsc.teacherRef and t.userRef=u.id "
			+ "AND (:subjectId IS NULL OR tsc.subjectRef=:subjectId) AND (:classId IS NULL OR tsc.classRef=:classId)", nativeQuery = false)
	List<TeacherInfoResponse> getTeachersByClassIdAndSubjectId(@Param("classId") Integer classId, @Param("subjectId") Integer subjectId);

	@Modifying
	@Transactional
	@Query(value="INSERT INTO TEACHER (id, birthdate, fiscal_code, gender, telephonen, user_ref) "
			+ "VALUES (teacher_seq.nextval, :birthdate, :fiscal_code, :gender, :telephone_number, :user_ref)", nativeQuery = true)
	void insertTeacher(@Param("birthdate") String birthdate, @Param("fiscal_code") String fiscalCode,
						@Param("gender") String gender, @Param("telephone_number") String telephoneNumber, 
						@Param("user_ref") Integer userRef);
	
}
