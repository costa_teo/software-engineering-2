package it.polito.smsbackend.dto;

public class Attachment {

	private Integer id;
	private String fileName;
	
	public Attachment(Integer id, String fileName) {
		this.id = id;
		this.fileName = fileName;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
