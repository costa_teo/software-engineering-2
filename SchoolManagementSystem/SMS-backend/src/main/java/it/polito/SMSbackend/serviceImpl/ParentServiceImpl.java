package it.polito.smsbackend.serviceimpl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dao.ClassDAO;
import it.polito.smsbackend.dao.StudentDAO;
import it.polito.smsbackend.dao.SubjectDAO;
import it.polito.smsbackend.dto.Assignment;
import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Attendance;
import it.polito.smsbackend.dto.Grade;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.repository.AssignmentRepository;
import it.polito.smsbackend.repository.AttachmentRepository;
import it.polito.smsbackend.repository.AttendanceRepository;
import it.polito.smsbackend.repository.ClassRepository;
import it.polito.smsbackend.repository.GradeRepository;
import it.polito.smsbackend.repository.StudentRepository;
import it.polito.smsbackend.repository.SubjectRepository;
import it.polito.smsbackend.response.AssignmentsBySubjectResponse;
import it.polito.smsbackend.response.GradesBySubjectResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TimeTableResponse;
import it.polito.smsbackend.service.ParentService;
import it.polito.smsbackend.service.UserService;

@Service
public class ParentServiceImpl implements ParentService {

	private final UserService userService;

	private final GradeRepository gradeRepository;
	private final StudentRepository studentRepository;
	private final AssignmentRepository assignmentRepository;
	private final AttachmentRepository attachmentRepository;
	private final AttendanceRepository attendanceRepository;
	private final ClassRepository classRepository;
	private final SubjectRepository subjectRepository;

	@Autowired
	public ParentServiceImpl(UserService userService, GradeRepository gradeRepository, 
			StudentRepository studentRepository,
			AssignmentRepository assignmentRepository,
			AttachmentRepository attachmentRepository,
			AttendanceRepository attendanceRepository,
			ClassRepository classRepository,
			SubjectRepository subjectRepository) {
		this.userService = userService;
		this.gradeRepository = gradeRepository;
		this.studentRepository = studentRepository;
		this.assignmentRepository = assignmentRepository;
		this.attachmentRepository = attachmentRepository;
		this.attendanceRepository = attendanceRepository; 
		this.classRepository = classRepository;
		this.subjectRepository=subjectRepository;
	}

	@Override
	public ArrayList<GradesBySubjectResponse> getGrades(Integer studentId) {

		ArrayList<GradesBySubjectResponse> grades = new ArrayList<>();

		if(studentId == null || studentId <= 0 ) {
			return grades;
		}

		ArrayList<Grade> flatGrades = gradeRepository.getGradesByStudentOrderBySubject(studentId);

		if(flatGrades == null || flatGrades.isEmpty()) {
			return grades;
		}

		if(flatGrades.size() == 1) {
			GradesBySubjectResponse gsr = new GradesBySubjectResponse();
			gsr.setGrades(flatGrades);
			gsr.setSubject(flatGrades.get(0).getSubject());
			gsr.setTeacher(flatGrades.get(0).getTeacher());
			gsr.setAverage(flatGrades.get(0).getValue());
			grades.add(gsr);

			return grades;
		}

		//Cycle over flatGrades to split into 'grades' by subject (remember that query results are ordered by subject)
		GradesBySubjectResponse tmp = new GradesBySubjectResponse();
		tmp.addGrades(flatGrades.get(0));
		tmp.setSubject(flatGrades.get(0).getSubject());
		tmp.setTeacher(flatGrades.get(0).getTeacher());


		BigDecimal tmpAverage = flatGrades.get(0).getValue();

		for(int i = 1; i < flatGrades.size(); i++) {
			if(flatGrades.get(i).getSubject().equals(flatGrades.get(i-1).getSubject())) {
				tmp.addGrades(flatGrades.get(i));
				tmpAverage = tmpAverage.add(flatGrades.get(i).getValue());
			} else {
				tmp.setAverage(tmpAverage.divide(new BigDecimal(tmp.getGrades().size()), RoundingMode.HALF_UP));
				grades.add(tmp);

				tmp = new GradesBySubjectResponse();
				tmp.addGrades(flatGrades.get(i));
				tmp.setSubject(flatGrades.get(i).getSubject());
				tmp.setTeacher(flatGrades.get(i).getTeacher());
				tmpAverage = flatGrades.get(i).getValue();

			}
		}
		tmp.setAverage(tmpAverage.divide(new BigDecimal(tmp.getGrades().size()), RoundingMode.HALF_UP));
		grades.add(tmp);

		return grades;
	}

	@Override
	public ArrayList<StudentInfoResponse> getStudents(Integer userId) {

		if(userId == null || userId<=0) { 
			return new ArrayList<>();
		}
		List<StudentDAO> studentsDAO = studentRepository.getStudentsByUserId(userId);

		ArrayList<StudentInfoResponse> resp = new ArrayList<>();
		for(StudentDAO sDAO : studentsDAO) {
			StudentInfoResponse s = new StudentInfoResponse();
			s.setId(sDAO.getId());
			s.setName(sDAO.getName());
			s.setSurname(sDAO.getSurname());
			resp.add(s);
		}
		return resp;
	}

	@Override
	public ArrayList<AssignmentsBySubjectResponse> getAssignments(Integer studentId) {

		if(studentId==null || studentId<=0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		if(studentRepository.checkStudentById(studentId)==0) return new ArrayList<>();

		ArrayList<AssignmentsBySubjectResponse> responses=new ArrayList<>();
		AssignmentsBySubjectResponse ar=new AssignmentsBySubjectResponse();

		List<Assignment> assignments= assignmentRepository.getAssignmentsByStudentOrderBySubjectAndTeacher(studentId);

		if(!assignments.isEmpty()) {
			ar.setTeacher(assignments.get(0).getTeacherName());
			ar.setSubject(assignments.get(0).getSubjectName());

			for(Assignment a : assignments) {
				if(a.getSubjectName().compareTo(ar.getSubject())==0 && a.getTeacherName().compareTo(ar.getTeacher())==0) {
					if(attachmentRepository.checkAttachmentById(a.getId())!=0) a.setHasAttachment(true);
					ar.addAssignment(a);
				}else {
					responses.add(ar);
					ar=new AssignmentsBySubjectResponse();
					ar.setTeacher(a.getTeacherName());
					ar.setSubject(a.getSubjectName());
					if(attachmentRepository.checkAttachmentById(a.getId())!=0) a.setHasAttachment(true);
					ar.addAssignment(a);
				}
			}

			responses.add(ar);
		}
		return responses;
	}

	@Override
	public ArrayList<Attachment> getAttachments(Integer assignmentId) {

		if(assignmentId==null || assignmentId<=0 ) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		if(assignmentRepository.checkAssignmentById(assignmentId)==0) return new ArrayList<>();

		return attachmentRepository.getAttachmentsByAssignmentIdOrderByName(assignmentId);
	}

	@Override
	public List<Attendance> getAttendances(Integer studentId) {

		if(studentId == null || studentId <= 0 ) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		return attendanceRepository.getAttendanceById(studentId);
	}

	@Override
	public Class getClass(Integer studentId) {

		if(studentId == null || studentId <= 0 ) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		if (studentRepository.findById(studentId).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		ClassDAO c= classRepository.getClassByStudentId(studentId);
		Class cl= new Class();
		cl.setId(c.getId());
		cl.setName(c.getName());

		return cl;
	}

	@Override
	public ArrayList<Subject> getSubjectsByClassId(Integer classId) {
		ArrayList<Subject> subjects = new ArrayList<>();

		if (classId== null || classId<=0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		if (classRepository.findById(classId).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		ArrayList<SubjectDAO> subjectsDAO = subjectRepository.getSubjectsByClassId(classId);

		for (SubjectDAO s : subjectsDAO) {
			subjects.add(s.map());
		}

		return subjects;
	}

	@Override
	public TimeTableResponse getTimetableByStudentId(Integer studentId) {
		if(studentId == null || studentId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		ClassDAO c;

		try {
			c = classRepository.getClassByStudentId(studentId);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if(c == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		TimeTableResponse resp = new TimeTableResponse();
		resp.setClassName(c.getName());
		resp.setTimetable(userService.getTimetableByClassId(c.getId()));

		return resp;
	}

}
