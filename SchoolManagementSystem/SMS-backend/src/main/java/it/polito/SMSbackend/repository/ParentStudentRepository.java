package it.polito.smsbackend.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.ParentStudentDAO;

public interface ParentStudentRepository extends CrudRepository<ParentStudentDAO, Integer> {
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO PARENT_STUDENT (id, parent_ref, student_ref) "
			+ "VALUES (PS_seq.nextval, :parent_ref, :student_ref)", nativeQuery=true)
	void insertStudentParent(@Param("parent_ref") Integer parentRef, @Param("student_ref") Integer studentRef);
	
	@Query(value = "SELECT COUNT(*) FROM PARENT_STUDENT ps WHERE ps.parent_ref = :parent_ref AND ps.student_ref = :student_ref", nativeQuery = true)
	Integer checkParentStudent(@Param("parent_ref") Integer parentRef, @Param("student_ref") Integer studentRef);
}
