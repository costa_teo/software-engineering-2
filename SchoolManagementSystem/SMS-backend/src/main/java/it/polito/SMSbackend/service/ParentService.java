package it.polito.smsbackend.service;

import java.util.ArrayList;
import java.util.List;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Attendance;
import it.polito.smsbackend.response.AssignmentsBySubjectResponse;
import it.polito.smsbackend.response.GradesBySubjectResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TimeTableResponse;

public interface ParentService {
	
	ArrayList<GradesBySubjectResponse> getGrades(Integer studentId);

	ArrayList<StudentInfoResponse> getStudents(Integer userId);

	ArrayList<AssignmentsBySubjectResponse> getAssignments(Integer studentId);

	ArrayList<Attachment> getAttachments(Integer assignmentId);

	List<Attendance> getAttendances(Integer studentId);

	Class getClass(Integer studentId);

	List<Subject> getSubjectsByClassId(Integer integer);
	
	TimeTableResponse getTimetableByStudentId(Integer studentId);
}
