package it.polito.smsbackend.serviceimpl;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dao.GeneralCommunicationDAO;
import it.polito.smsbackend.dao.TimeTableDAO;
import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.SupportMaterial;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.repository.AttachmentRepository;
import it.polito.smsbackend.repository.ClassRepository;
import it.polito.smsbackend.repository.GeneralCommunicationRepository;
import it.polito.smsbackend.repository.SubjectRepository;
import it.polito.smsbackend.repository.SupportMaterialRepository;
import it.polito.smsbackend.repository.TeacherRepository;
import it.polito.smsbackend.repository.TimeTableRepository;
import it.polito.smsbackend.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final AttachmentRepository attachmentRepository;
	private final GeneralCommunicationRepository generalCommunicationRepository;
	private final SupportMaterialRepository supportMaterialRepository;
	private final ClassRepository classRepository;
	private final SubjectRepository subjectRepository;
	private final TeacherRepository teacherRepository;
	private final TimeTableRepository timeTableRepository;

	@Autowired
	public UserServiceImpl(AttachmentRepository attachmentRepository, GeneralCommunicationRepository generalCommunicationRepository,
			SupportMaterialRepository supportMaterialRepository, ClassRepository classRepository, SubjectRepository subjectRepository,
			TeacherRepository teacherRepository, TimeTableRepository timeTableRepository) {
		this.attachmentRepository = attachmentRepository;
		this.generalCommunicationRepository= generalCommunicationRepository;
		this.supportMaterialRepository= supportMaterialRepository;
		this.classRepository = classRepository;
		this.subjectRepository = subjectRepository;
		this.teacherRepository = teacherRepository;
		this.timeTableRepository = timeTableRepository;
	}

	@Override
	public Blob downloadAttachment(Integer attachmentId) {
		if(attachmentId==null || attachmentId<=0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		Blob b =  attachmentRepository.getAttachmentById(attachmentId);

		if(b == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		return b;
	}
	
	@Override
	public List<GeneralCommunication> getGeneralCommunications() {
		List<GeneralCommunicationDAO> gcDAO = (List<GeneralCommunicationDAO>) generalCommunicationRepository.findAll();
		List<GeneralCommunication> gcs = new ArrayList<>();
		for (GeneralCommunicationDAO gc : gcDAO)
			gcs.add(gc.map());
		return gcs;
	}

	@Override
	public Blob downloadSupportMaterial(Integer supportMaterialId) {
		if(supportMaterialId==null || supportMaterialId<=0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		Blob b =  supportMaterialRepository.getSupportMaterialById(supportMaterialId);

		if(b == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		return b;
	}
	
	@Override
	public List<SupportMaterial> getSMaterialByClassIdAndSubjectId(Integer classId, Integer subjectId){
		if (classId == null || classId <= 0 || subjectId == null || subjectId <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		return this.supportMaterialRepository.getSupportMaterialByClassAndSubject(classId, subjectId);

	}
	
	@Override
	public List<TimeTableRow> getTimetableByClassId(Integer classId) {
		if(classId == null || classId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if(classRepository.findById(classId).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Selected class does not exist");

		List<TimeTableRow> rows = new ArrayList<>();
		List<TimeTableDAO> lessons = new ArrayList<>();

		try {
			lessons = timeTableRepository.getByClassIdOrderByTimeSlot(classId);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		int timeS = 0;
		TimeTableRow row = new TimeTableRow();
		for(TimeTableDAO ttd : lessons) {
			if(ttd.getTimeSlot() != timeS) {
				timeS++;
				rows.add(row);
				row = new TimeTableRow();
			}

			String subjectName;

			try {
				subjectName = subjectRepository.getNameByTSCId(ttd.getTeachSubClassRef());
			} catch(Exception e) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			switch(ttd.getDayOfWeek()) {
			case 0:
				row.setMonday(subjectName);
				break;
			case 1:
				row.setTuesday(subjectName);
				break;
			case 2:
				row.setWednesday(subjectName);
				break;
			case 3:
				row.setThursday(subjectName);
				break;
			case 4:
				row.setFriday(subjectName);
				break;
			default:
				break; //Should not happen
			}
		}

		rows.add(row);
		
		return rows;
	}
	
	@Override
	public List<TimeTableRow> getTimetableByTeacherId(Integer teacherId) {
		if(teacherId == null || teacherId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if(teacherRepository.findById(teacherId).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Selected teacher does not exist");

		List<TimeTableRow> rows = new ArrayList<>();
		List<TimeTableDAO> lessons = new ArrayList<>();

		try {
			lessons = timeTableRepository.getByTeacherIdOrderByTimeSlot(teacherId);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		int timeS = 0;
		TimeTableRow row = new TimeTableRow();
		for(TimeTableDAO ttd : lessons) {
			if(ttd.getTimeSlot() != timeS) {
				timeS++;
				rows.add(row);
				row = new TimeTableRow();
			}

			String subjectName;
			String className;

			try {
				subjectName = subjectRepository.getNameByTSCId(ttd.getTeachSubClassRef());
				className = classRepository.getNameByTSCId(ttd.getTeachSubClassRef());
			} catch(Exception e) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
			}

			switch(ttd.getDayOfWeek()) {
			case 0:
				row.setMonday(className + ": " + subjectName);
				break;
			case 1:
				row.setTuesday(className + ": " + subjectName);
				break;
			case 2:
				row.setWednesday(className + ": " + subjectName);
				break;
			case 3:
				row.setThursday(className + ": " + subjectName);
				break;
			case 4:
				row.setFriday(className + ": " + subjectName);
				break;
			default:
				break; //Should not happen
			}
		}
		
		rows.add(row);

		return rows;
	}
	
}
