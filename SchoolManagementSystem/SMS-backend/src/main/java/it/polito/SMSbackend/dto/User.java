package it.polito.smsbackend.dto;

import it.polito.smsbackend.dao.UserDAO;

public class User {

	private Integer id;
	private String name;
	private String surname;
	private String fiscalCode;
	private String gender;
	private String birthdate;
	private String telephoneN;
	private String email;
	private String role;
	private String token;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UserDAO map() {
		UserDAO u = new UserDAO();
		u.setId(id);
		u.setName(name);
		u.setSurname(surname);
		u.setRole(role);
		return u;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephoneN() {
		return telephoneN;
	}

	public void setTelephoneN(String telephoneN) {
		this.telephoneN = telephoneN;
	}
}
