package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		name="TEACHER_SUBJECT_CLASS",
		uniqueConstraints={
				@UniqueConstraint(columnNames = {"teacher_ref", "subject_ref", "class_ref"})}
		)
public class TeacherSubjectClassDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TSC_generator")
	@SequenceGenerator(name="TSC_generator", sequenceName = "TSC_seq", allocationSize=1)
	private Integer id;
	@Column(nullable = false, name = "teacher_ref")
	private Integer teacherRef;
	@Column(nullable = false, name = "subject_ref")
	private Integer subjectRef;
	@Column(nullable = false, name = "class_ref")
	private Integer classRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTeacherRef() {
		return teacherRef;
	}

	public void setTeacherRef(Integer teacherRef) {
		this.teacherRef = teacherRef;
	}

	public Integer getSubjectRef() {
		return subjectRef;
	}

	public void setSubjectRef(Integer subjectRef) {
		this.subjectRef = subjectRef;
	}

	public Integer getClassRef() {
		return classRef;
	}

	public void setClassRef(Integer classRef) {
		this.classRef = classRef;
	}

}
