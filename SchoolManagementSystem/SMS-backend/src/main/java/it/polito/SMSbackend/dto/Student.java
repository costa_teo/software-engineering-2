package it.polito.smsbackend.dto;

public class Student {

	private Number id;
	private String name;
	private String surname;
	private String fiscalCode;
	private String gender;
	private String birthdate;
	private Integer entryLevel;

	public Number getId() {
		return id;
	}

	public void setId(Number id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public Integer getEntryLevel() {
		return entryLevel;
	}

	public void setEntryLevel(Integer entryLevel) {
		this.entryLevel = entryLevel;
	}
}
