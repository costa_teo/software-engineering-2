package it.polito.smsbackend.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.UserDAO;

public interface UserRepository extends CrudRepository<UserDAO, Integer> {

	@Query(value="SELECT * FROM USER u WHERE u.password = :password AND ( u.username = :username OR u.email = :email ) AND role = :role ", nativeQuery = true)
	UserDAO getUserByCredentials(@Param("email") String email, @Param("username") String username, @Param("password") String password, @Param("role") String role);
	
	@Modifying
	@Query(value="INSERT INTO USER (id, email, name, password, role, surname, username) "
			+ "VALUES (user_seq.nextval, :email, :name, :password, :role, :surname, :username)", nativeQuery = true)
	void insertUser(@Param("email") String email, @Param("name") String name, @Param("password") String password,
						@Param("role") String role, @Param("surname") String surname, @Param("username") String username);

	@Query(value="SELECT MAX(id) FROM USER", nativeQuery = true)
	Integer getLastUserId();
	
	@Modifying
	@Transactional
	@Query(value="UPDATE USER SET PASSWORD = :newPassword WHERE ID = :userId", nativeQuery = true)
	void changePassword(@Param("userId") Integer userId, @Param("newPassword") String newPassword );
	
	@Query(value="SELECT username FROM USER " + 
			"WHERE id = (select  max(id) from USER)", nativeQuery = true)
	String getLastUsername();

	UserDAO findByUsername(String username);
}
