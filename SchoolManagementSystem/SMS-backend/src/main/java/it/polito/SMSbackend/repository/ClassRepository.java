package it.polito.smsbackend.repository;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import it.polito.smsbackend.dao.ClassDAO;

public interface ClassRepository extends CrudRepository<ClassDAO, Integer> {
	
	@Query(value = "SELECT * FROM CLASS c, TEACHER_SUBJECT_CLASS tsc, TEACHER t WHERE c.id = tsc.class_ref AND tsc.teacher_ref = t.id AND t.user_ref = :id", nativeQuery = true)
	ArrayList<ClassDAO> getTeacherClassByUserId(@Param("id") Integer id);

	@Query(value= "SELECT * FROM CLASS", nativeQuery=true)
	ArrayList<ClassDAO> getAllClasses();
	
	@Query(value= "SELECT * FROM CLASS WHERE name = :name", nativeQuery=true)
	ClassDAO getClassByName(@Param("name") String name);

	@Modifying
	@Transactional
	@Query(value="INSERT into CLASS (id, name) "
			+ "VALUES (class_seq.nextval, :name)", nativeQuery=true)
	void createNewClass(@Param("name") String newName);
	
	@Query(value= "SELECT * FROM Class c WHERE c.id = (SELECT s.class_ref FROM Student s WHERE s.id = :id)", nativeQuery=true)
	ClassDAO getClassByStudentId(@Param("id") Integer id);
	
	@Query(value = "SELECT c.NAME FROM CLASS c, TEACHER_SUBJECT_CLASS tsc "
			+ "WHERE c.ID = tsc.CLASS_REF AND "
			+ "tsc.ID = :tscId", nativeQuery = true)
	String getNameByTSCId(@Param("tscId") Integer tscId);

}
