package it.polito.smsbackend.dao;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="SUPPORT_MATERIAL")
public class SupportMaterialDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SM_generator")
	@SequenceGenerator(name="SM_generator", sequenceName = "SM_seq", allocationSize=1)
	private Integer id;
	@Lob
	private Blob file;
	@Column(nullable = false, name = "file_name")
	private String fileName;
	private String date;
	@Column(nullable = false, name = "teach_sub_class_ref")
	private Integer teachSubClassRef;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Blob getFile() {
		return file;
	}

	public void setFile(Blob file) {
		this.file = file;
	}

	public Integer getTeachSubClassRef() {
		return teachSubClassRef;
	}

	public void setTeachSubClassRef(Integer teachSubClassRef) {
		this.teachSubClassRef = teachSubClassRef;
	}
}
