package it.polito.smsbackend.request;

import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

public class NewAttachmentRequest {

	private String name;
	private MultipartFile file;
	private Integer assignment_ref;
	
	public NewAttachmentRequest(String name, MultipartFile file, String assignment_ref) {
		super();
		this.name = name;
		this.file = file;
		try {
			this.assignment_ref = Integer.parseInt(assignment_ref);
		}
		catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public MultipartFile getFile() {
		return file;
	}
	
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	public Integer getAssignment_ref() {
		return assignment_ref;
	}
	
	public void setAssignment_ref(Integer assignmentRef) {
		this.assignment_ref = assignmentRef;
	}

}
