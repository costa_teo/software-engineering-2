package it.polito.smsbackend.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.TeacherSubjectClassDAO;

public interface TeacherSubjectClassRepository extends CrudRepository<TeacherSubjectClassDAO, Integer> {

	@Query(value = "SELECT tsc.id FROM TEACHER_SUBJECT_CLASS tsc, SUBJECT s, CLASS c, TEACHER t "
			+ "WHERE tsc.subject_ref = s.id AND " + "tsc.class_ref = c.id AND " + "tsc.teacher_ref = t.id AND "
			+ "s.name = :subName AND " + "c.name = :className AND " + "t.user_ref = :userId", nativeQuery = true)
	Integer getIdByUserIdAndSubjectNameAndClassName(@Param("userId") Integer userId, @Param("subName") String subName,
			@Param("className") String className);

	@Query(value = "SELECT ID FROM TEACHER_SUBJECT_CLASS"
			+ " WHERE CLASS_REF=:cId AND TEACHER_REF=:tId AND SUBJECT_REF=:sId", nativeQuery = true)
	Integer getId(@Param("tId") Integer teacherId, @Param("sId") Integer subjectId, @Param("cId") Integer classId);

	@Query(value = "SELECT ID from TEACHER_SUBJECT_CLASS WHERE CLASS_REF=:cId and SUBJECT_REF=:sId", nativeQuery=true)
	Integer getIdByClassAndSubject(@Param ("cId") Integer classId, @Param("sId") Integer subjectId);
	
	@Query(value = "SELECT ID from TEACHER_SUBJECT_CLASS "
			+ "WHERE CLASS_REF=:cId AND "
			+ "SUBJECT_REF=:sId AND "
			+ "TEACHER_REF=:tId", nativeQuery=true)
	Integer getIdByClassAndSubjectAndTeacher(@Param ("cId") Integer classId, @Param("sId") Integer subjectId, @Param("tId") Integer teacherId);
	
	@Query(value = "SELECT tsc.ID FROM TEACHER_SUBJECT_CLASS tsc, SUBJECT s "
			+ "WHERE tsc.CLASS_REF = :cId AND "
			+ "tsc.SUBJECT_REF = s.ID AND "
			+ "s.NAME = :subj_name", nativeQuery=true)
	Integer getIdByClassIdAndSubjectName(@Param("cId") Integer classId, @Param("subj_name") String subjectName);
}
