package it.polito.smsbackend.dto;

import it.polito.smsbackend.dao.ClassDAO;

public class Class {

	private Integer id;
	private String name;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ClassDAO map() {
		ClassDAO s=new ClassDAO();
		s.setId(id);
		s.setName(name);
		return s;
	}
}
