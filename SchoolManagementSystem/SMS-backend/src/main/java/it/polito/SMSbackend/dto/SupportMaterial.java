package it.polito.smsbackend.dto;

public class SupportMaterial {
	
	private Integer id;
	private String fileName;
	private String subjectName;
	private String teacherName;
	private String date;
	
	public SupportMaterial() {}

	public SupportMaterial(Integer id, String filename, String subjectname, String teachername, String date) {
		this.fileName = filename;
		this.subjectName = subjectname;
		this.teacherName = teachername;
		this.date = date;	
		this.id = id;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
}
