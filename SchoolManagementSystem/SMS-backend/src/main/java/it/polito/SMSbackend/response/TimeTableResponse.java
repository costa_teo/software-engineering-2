package it.polito.smsbackend.response;

import java.util.List;
import it.polito.smsbackend.dto.TimeTableRow;

public class TimeTableResponse {
	
	private List<TimeTableRow> timetable;
	private String className;
	
	public List<TimeTableRow> getTimetable() {
		return timetable;
	}
	
	public void setTimetable(List<TimeTableRow> timetable) {
		this.timetable = timetable;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

}
