package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ADMINISTRATOR")
public class AdministratorDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "administrator_generator")
	@SequenceGenerator(name="administrator_generator", sequenceName = "administrator_seq", allocationSize=1)
	private Integer id;
	@Column(unique = true, nullable = false)
	private String fiscalCode;
	private String gender;
	private String birthdate;
	@Column(nullable = false, name = "user_ref")
	private Integer userRef;
	@Column(name = "super_admin")
	private Boolean superAdmin;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public Integer getUserRef() {
		return userRef;
	}

	public void setUserRef(Integer userRef) {
		this.userRef = userRef;
	}

	public Boolean getSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(Boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
	
}
