package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import it.polito.smsbackend.dto.Class;

@Entity
@Table(name="CLASS")
public class ClassDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "class_generator")
	@SequenceGenerator(name="class_generator", sequenceName = "class_seq", allocationSize=1)
	private Integer id;
	@Column(unique = true, nullable = false)
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Class map() {
		Class s=new Class();
		s.setId(id);
		s.setName(name);
		return s;
	}

}
