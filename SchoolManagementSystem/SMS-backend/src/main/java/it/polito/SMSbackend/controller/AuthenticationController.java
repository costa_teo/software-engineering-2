package it.polito.smsbackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.request.LoginRequest;
import it.polito.smsbackend.service.AuthenticationService;

@RestController
@RequestMapping(value = "authentication")
@CrossOrigin
public class AuthenticationController {
	
	private final AuthenticationService authenticationService;
	
	@Autowired
	public AuthenticationController(AuthenticationService authenticationService) {
		this.authenticationService = authenticationService;
	}
	
	@PostMapping(value = "login")
	public User login(@RequestBody LoginRequest request)  {
		return authenticationService.login(request.getUsername(), request.getPassword(), request.getRole());
	}
	
	@PostMapping(value = "logout")
	public void logout(@RequestBody User user) {
		authenticationService.logout(user);
	}
	
	@GetMapping(value = "authenticateUser")
	public Integer authenticateUser(@RequestHeader("token") String token)  {
		return authenticationService.authenticateUser(token);
	}
	
	@PostMapping(value = "change-password")
	public void changePassword(@RequestHeader("token") String token, @RequestBody String newPassword)  {
		Integer userId = authenticationService.authenticateUser(token);
		if(userId == null) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}
		authenticationService.changePassword(userId, newPassword);
		
	}
}
