package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import it.polito.smsbackend.dto.GeneralCommunication;

@Entity
@Table(name="GENERAL_COMMUNICATION")
public class GeneralCommunicationDAO {
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GC_generator")
		@SequenceGenerator(name="GC_generator", sequenceName = "GC_seq", allocationSize=1)
		private Integer id;
		@Column(nullable = false)
		private String date;
		@Column(nullable = false)
		private String description;
		
		public GeneralCommunicationDAO() {
			//Empty
		}
		
		public GeneralCommunicationDAO(Integer id, String date, String description) {
			this.id=id;
			this.date=date;
			this.description=description;
		}
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}

		public GeneralCommunication map() {
			GeneralCommunication g=new GeneralCommunication();
			g.setId(this.id);
			g.setDate(this.date);
			g.setDescription(this.description);
			return g;
		}

}
