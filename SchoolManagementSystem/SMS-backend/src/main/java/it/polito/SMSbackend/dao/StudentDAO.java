package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="STUDENT")
public class StudentDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_generator")
	@SequenceGenerator(name="student_generator", sequenceName = "student_seq", allocationSize=1)
	private Integer id;
	@Column(unique = true, nullable = false)
	private String fiscalCode;
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private String surname;
	private String gender;
	private String birthdate;
	private Integer entryLevel;
	@Column(name = "class_ref")
	private Integer classRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public Integer getEntryLevel() {
		return entryLevel;
	}

	public void setEntryLevel(Integer entryLevel) {
		this.entryLevel = entryLevel;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

}
