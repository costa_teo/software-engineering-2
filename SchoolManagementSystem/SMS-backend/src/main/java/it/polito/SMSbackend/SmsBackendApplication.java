package it.polito.smsbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.polito.smsbackend.emailsystem.EmailSystem;


@SpringBootApplication
public class SmsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsBackendApplication.class, args);
		EmailSystem.connect();
		
		
	}

}
