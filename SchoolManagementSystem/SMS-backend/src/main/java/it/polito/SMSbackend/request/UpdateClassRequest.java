package it.polito.smsbackend.request;

import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.dto.Class;

public class UpdateClassRequest {
	
	private Class c;
    private StudentInfoResponse[] students;
    
	public UpdateClassRequest(Class c, StudentInfoResponse[] students) {
		this.c = c;
		this.students = students;
	}

	public Class getC() {
		return c;
	}

	public void setC(Class c) {
		this.c = c;
	}

	public StudentInfoResponse[] getStudents() {
		return students;
	}

	public void setStudents(StudentInfoResponse[] students) {
		this.students = students;
	}

}
