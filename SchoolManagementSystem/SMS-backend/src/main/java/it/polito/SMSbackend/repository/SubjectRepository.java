package it.polito.smsbackend.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.SubjectDAO;

public interface SubjectRepository extends CrudRepository<SubjectDAO, Integer>{

	@Query(value = "SELECT s.id, s.name FROM SUBJECT s, TEACHER_SUBJECT_CLASS tsc, TEACHER t " +
			"WHERE s.id = tsc.subject_ref AND tsc.teacher_ref = t.id AND " +
			"t.user_ref = :userId AND (:classId IS NULL OR tsc.class_ref = :classId) " +
			"GROUP BY s.id, s.name", nativeQuery = true)
	ArrayList<SubjectDAO> getTeacherSubjectsByUserIdAndOptionalClassId(@Param("userId") Integer userId, @Param("classId") Integer classId);

	@Query(value = "SELECT s.id, s.name FROM SUBJECT s, TEACHER_SUBJECT_CLASS tsc " +
			"WHERE s.id = tsc.subject_ref AND tsc.class_ref = :classId ", nativeQuery = true)
	ArrayList<SubjectDAO> getSubjectsByClassId(Integer classId);

	@Query(value = "SELECT s.NAME FROM SUBJECT s, TEACHER_SUBJECT_CLASS tsc "
			+ "WHERE s.ID = tsc.SUBJECT_REF AND "
			+ "tsc.ID = :tscId", nativeQuery = true)
	String getNameByTSCId(@Param("tscId") Integer tscId);
}
