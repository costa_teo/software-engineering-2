package it.polito.smsbackend.service;

import java.util.ArrayList;
import java.util.List;

import it.polito.smsbackend.dao.AssignmentDAO;
import it.polito.smsbackend.dto.Assignment;
import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.Grade;
import it.polito.smsbackend.dto.Lecture;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.request.InsertGradeRequest;
import it.polito.smsbackend.request.NewAttachmentRequest;
import it.polito.smsbackend.request.NewSupportMaterialRequest;
import it.polito.smsbackend.response.StudentAttendanceResponse;
import it.polito.smsbackend.response.StudentInfoResponse;

public interface TeacherService {

	ArrayList<Class> getAllClasses(Integer userId);

	ArrayList<Subject> getSubjectsByClassId(Integer userId, Integer classId);
	
	ArrayList<Lecture> getLecturesByClassIdAndSubjectId(Integer userId, Integer classId, Integer subjectId);

	Lecture insertLectureTopics(Integer userId, Lecture l);
	
	void updateLecture(Integer userId, Lecture request);

	void deleteLecture(Integer userId, Lecture request);

	ArrayList<StudentInfoResponse> getStudentsByClassId(Integer classId);

	ArrayList<Grade> getGradesByStudentAndSubject(Integer studentId, Integer subjectId);

	void insertGrades(Integer userId, InsertGradeRequest request);

	void updateGrade(Grade request);

	void deleteGrade(Grade request);

	List<StudentAttendanceResponse> getStudentsAttendanceByClassIdAndDate(Integer classId, String date);

	void insertAttendances(List<StudentAttendanceResponse> attendances, Integer classId, String date);

	List<AssignmentDAO> getAssignmentByClassIdAndSubjectId(Integer userId, Integer classId, Integer subjectId);

	List<Attachment> getAttachmentsByAssignmentId(Integer assignmentid);

	void updateAssignment(Assignment request);

	void deleteAssignment(Integer index);
	
	void uploadAttachment(NewAttachmentRequest request);

	Integer insertAssignment(Integer userId, Assignment request);

	void deleteAttachment(Integer index);
	
	void deleteSupportMaterial(Integer index);
	
	void uploadSupportMaterial(Integer userId, NewSupportMaterialRequest request);
	
	List<TimeTableRow> getTimetableByUserId(Integer userId);

}
