package it.polito.smsbackend.dao;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="ATTACHMENT")
public class AttachmentDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attachment_generator")
	@SequenceGenerator(name="attachment_generator", sequenceName = "attachment_seq", allocationSize=1)
	private Integer id;
	private String fileName;
	@Column(nullable = false, name = "assignment_ref")
	private Integer assignmentRef;
	@Lob
	private Blob file;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getAssignmentRef() {
		return assignmentRef;
	}

	public void setAssignmentRef(Integer assignmentRef) {
		this.assignmentRef = assignmentRef;
	}

	public Blob getFile() {
		return file;
	}
	
	public void setFile(Blob file) {
		this.file = file;
	}
		
	
}
