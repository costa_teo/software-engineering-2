package it.polito.smsbackend.response;

import java.util.ArrayList;
import java.util.List;

import it.polito.smsbackend.dto.Assignment;

public class AssignmentsBySubjectResponse {

	private String subject;
	private String teacher;
	private  List<Assignment> assignments;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public List<Assignment> getAssignments() {
		return assignments;
	}
	public void setAssignments(List<Assignment> assignments) {
		this.assignments = assignments;
	}

	public void addAssignment(Assignment assignment) {
		if(this.assignments == null) {
			this.assignments = new ArrayList<>();
		}
		this.assignments.add(assignment);
	}
}
