package it.polito.smsbackend.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.AssignmentDAO;
import it.polito.smsbackend.dto.Assignment;

public interface AssignmentRepository extends CrudRepository<AssignmentDAO, Integer> {


	/**
	 * NativeQuery = false to use JPQL. Grade needs a constructor with returned values in exact order
	 */
	
	@Query(value = "SELECT new it.polito.smsbackend.dto.Assignment(a.id, a.date, a.description, u.name, sub.name, c.name, false) " +
			"FROM AssignmentDAO a, SubjectDAO sub, TeacherDAO t, UserDAO u, TeacherSubjectClassDAO tsc, StudentDAO s, ClassDAO c " +
			"WHERE tsc.teacherRef=t.id AND " +
			"sub.id = tsc.subjectRef AND " +
			"u.id = t.userRef AND " +
			"u.role = 'TEACHER' AND " +
			"a.teachSubClassRef=tsc.id AND " +
			"s.id=:studentId AND " +
			"s.classRef=tsc.classRef AND " +
			"s.classRef=c.id " +
			"ORDER BY sub.name, u.name", nativeQuery = false)
	ArrayList<Assignment> getAssignmentsByStudentOrderBySubjectAndTeacher(@Param("studentId") Integer studentId);
	
	@Query(value="SELECT COUNT(*) FROM ASSIGNMENT a WHERE a.id = :id", nativeQuery = true)
	Integer checkAssignmentById(Integer id);
	
	@Query(value = "SELECT * " +
			"FROM ASSIGNMENT a " +
			"WHERE a.teach_sub_class_ref=:tscId " +
			"ORDER BY a.date", nativeQuery = true)
	List<AssignmentDAO> getAssignmentsTeacherSubjectClassId(@Param("tscId") Integer tscId);
	
	@Query(value = "SELECT new it.polito.smsbackend.dto.Assignment(a.id, a.date, a.description, u.name, sub.name, c.name, false) " +
			"FROM AssignmentDAO a, SubjectDAO sub, TeacherDAO t, UserDAO u, TeacherSubjectClassDAO tsc, ClassDAO c " +
			"WHERE tsc.teacherRef=t.id AND " +
			"c.id=tsc.classRef AND " +
			"sub.id = tsc.subjectRef AND " +
			"u.id = t.userRef AND " +
			"u.role = 'TEACHER' AND " +
			"a.teachSubClassRef=tsc.id AND " +
			"a.id=:id " +
			"ORDER BY sub.name, u.name", nativeQuery = false)
	Assignment getAssignmentById(@Param("id") Integer id);
	
	
}
