package it.polito.smsbackend.request;
import it.polito.smsbackend.dto.Parent;
import it.polito.smsbackend.dto.Student;

public class AddParentStudentRequest {
	private Parent parent1;
	private Parent parent2;
	private Student student;
	
	public Parent getParent1() {
		return parent1;
	}
	public Parent getParent2() {
		return parent2;
	}
	
	public void setParents(Parent parent) {
		this.parent1 = parent;
	}
	public void setParents2(Parent parent) {
		this.parent2 = parent;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
}
