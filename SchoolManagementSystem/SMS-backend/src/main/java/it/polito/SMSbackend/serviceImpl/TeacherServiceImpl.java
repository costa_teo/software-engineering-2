package it.polito.smsbackend.serviceimpl;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.common.AttendanceType;
import it.polito.smsbackend.dao.AssignmentDAO;
import it.polito.smsbackend.dao.AttendanceDAO;
import it.polito.smsbackend.dao.ClassDAO;
import it.polito.smsbackend.dao.GradeDAO;
import it.polito.smsbackend.dao.StudentDAO;
import it.polito.smsbackend.dao.SubjectDAO;
import it.polito.smsbackend.dto.Assignment;
import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Attendance;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.Grade;
import it.polito.smsbackend.dto.Lecture;
import it.polito.smsbackend.dto.NewGrade;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.repository.AssignmentRepository;
import it.polito.smsbackend.repository.AttachmentRepository;
import it.polito.smsbackend.repository.AttendanceRepository;
import it.polito.smsbackend.repository.ClassRepository;
import it.polito.smsbackend.repository.GradeRepository;
import it.polito.smsbackend.repository.LectureRepository;
import it.polito.smsbackend.repository.StudentRepository;
import it.polito.smsbackend.repository.SubjectRepository;
import it.polito.smsbackend.repository.SupportMaterialRepository;
import it.polito.smsbackend.repository.TeacherRepository;
import it.polito.smsbackend.repository.TeacherSubjectClassRepository;
import it.polito.smsbackend.request.InsertGradeRequest;
import it.polito.smsbackend.request.NewAttachmentRequest;
import it.polito.smsbackend.request.NewSupportMaterialRequest;
import it.polito.smsbackend.response.StudentAttendanceResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.service.TeacherService;
import it.polito.smsbackend.service.UserService;

@Service
public class TeacherServiceImpl implements TeacherService {

	private final UserService userService;

	private final TeacherRepository teacherRepository;
	private final SupportMaterialRepository supportMaterialRepository;
	private final SubjectRepository subjectRepository;
	private final ClassRepository classRepository;
	private final TeacherSubjectClassRepository teacherSubjectClassRepository;
	private final LectureRepository lectureRepository;
	private final StudentRepository studentRepository;
	private final GradeRepository gradeRepository;
	private final AttachmentRepository attachmentRepository;
	private final AttendanceRepository attendanceRepository;
	private final AssignmentRepository assignmentRepository;


	@Autowired
	public TeacherServiceImpl(UserService userService, TeacherRepository teacherRepository, SubjectRepository subjectRepository,
			ClassRepository classRepository, TeacherSubjectClassRepository teacherSubjectClassRepository,
			LectureRepository lectureRepository, StudentRepository studentRepository, GradeRepository gradeRepository,
			AttachmentRepository attachmentRepository, AttendanceRepository attendanceRepository,
			AssignmentRepository assignmentRepository, SupportMaterialRepository supportMaterialRepository) {
		this.userService = userService;
		this.teacherRepository = teacherRepository;
		this.subjectRepository = subjectRepository;
		this.classRepository = classRepository;
		this.teacherSubjectClassRepository = teacherSubjectClassRepository;
		this.lectureRepository = lectureRepository;
		this.studentRepository = studentRepository;
		this.gradeRepository = gradeRepository;
		this.attachmentRepository = attachmentRepository;
		this.attendanceRepository = attendanceRepository;
		this.assignmentRepository = assignmentRepository;
		this.supportMaterialRepository = supportMaterialRepository;
	}

	@Override
	public ArrayList<Class> getAllClasses(Integer userId) {
		ArrayList<Class> classes = new ArrayList<>();

		if (userId == null || userId <= 0) {
			return classes;
		}

		ArrayList<ClassDAO> classesDAO = classRepository.getTeacherClassByUserId(userId);
		if (classesDAO == null || classesDAO.isEmpty()) {
			return classes;
		}
		for (ClassDAO c : classesDAO) {
			classes.add(c.map());
		}

		return classes;
	}

	/**
	 * @param classId can be null
	 */
	@Override
	public ArrayList<Subject> getSubjectsByClassId(Integer userId, Integer classId) {
		ArrayList<Subject> subjects = new ArrayList<>();

		if (userId == null || userId <= 0) {
			return subjects;
		}

		ArrayList<SubjectDAO> subjectsDAO = subjectRepository.getTeacherSubjectsByUserIdAndOptionalClassId(userId,
				classId);

		if (subjectsDAO == null || subjectsDAO.isEmpty()) {
			return subjects;
		}

		for (SubjectDAO s : subjectsDAO) {
			subjects.add(s.map());
		}
		return subjects;
	}

	/**
	 * @param subjectId can be null
	 * @param classId   can be null
	 */
	@Override
	public ArrayList<Lecture> getLecturesByClassIdAndSubjectId(Integer userId, Integer classId, Integer subjectId) {
		ArrayList<Lecture> lectures = new ArrayList<>();

		if (userId == null || userId <= 0) {
			return lectures;
		}

		lectures = lectureRepository.getLecturesByUserIdAndByOptionalSubjectIdAndByOptionalClassId(userId, subjectId,
				classId);

		return lectures;
	}

	@Override
	public Lecture insertLectureTopics(Integer userId, Lecture lecture) {

		if (userId == null || lecture == null || lecture.getClassName() == null || lecture.getDate() == null
				|| lecture.getDescription() == null
				|| lecture.getSubject() == null /* || lecture.getTeacher() == null */ || lecture.getTimeSlot() < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		Integer tscRef = teacherSubjectClassRepository.getIdByUserIdAndSubjectNameAndClassName(userId,
				lecture.getSubject(), lecture.getClassName());

		if (tscRef == null || !checkLectureDate(lecture.getDate())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		lectureRepository.insertLectureTopics(lecture.getDate(), lecture.getTimeSlot(), tscRef,
				lecture.getDescription());
		return lecture;
	}

	@Override
	public void updateLecture(Integer userId, Lecture updatedLecture) {
		if (userId > 0 && updatedLecture != null) {
			this.lectureRepository.updateLecture(updatedLecture.getId(), updatedLecture.getDescription());
		}
	}

	@Override
	public void deleteLecture(Integer userId, Lecture lecture) {
		if (userId <= 0 || lecture == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if (Boolean.FALSE.equals(checkLectureDate(lecture.getDate()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		this.lectureRepository.deleteLecture(lecture.getId());
	}

	// STORY 6
	@Override
	public ArrayList<StudentInfoResponse> getStudentsByClassId(Integer classId) {
		if (classId == null || classId < 0) {
			return new ArrayList<>();
		}
		List<StudentDAO> studentsDAO = studentRepository.getStudentsByClassId(classId);
		ArrayList<StudentInfoResponse> resp = new ArrayList<>();
		for (StudentDAO sDAO : studentsDAO) {
			StudentInfoResponse s = new StudentInfoResponse();
			s.setId(sDAO.getId());
			s.setName(sDAO.getName());
			s.setSurname(sDAO.getSurname());
			resp.add(s);
		}
		return resp;
	}

	/**
	 * Util method to check date validity
	 */
	private Boolean checkLectureDate(String lectureDate) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			// This parse can throw exception, but it's fine.
			LocalDate date = LocalDate.parse(lectureDate, formatter);
			LocalDate now = LocalDate.now();
			LocalDate lastSunday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));

			if (Boolean.TRUE.equals((date.isAfter(lastSunday) && (date.isBefore(now) || date.isEqual(now))))) {
				return Boolean.TRUE;
			}

			return Boolean.FALSE;
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	private void checkDateFormat(String date) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			// This parse can throw exception, but it's fine.
			LocalDate.parse(date, formatter);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	private void checkDateNotFuture(String stringDate) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			// This parse can throw exception, but it's fine.
			LocalDate date = LocalDate.parse(stringDate, formatter);
			LocalDate now = LocalDate.now();
			if (date.isAfter(now))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	private void checkDateNotPast(String stringDate) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			// This parse can throw exception, but it's fine.
			LocalDate date = LocalDate.parse(stringDate, formatter);
			LocalDate now = LocalDate.now();
			if (date.isEqual(now) || date.isBefore(now))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public ArrayList<Grade> getGradesByStudentAndSubject(Integer studentId, Integer subjectId) {
		if (studentId == null || subjectId == null || studentId < 0 || subjectId < 0)
			return new ArrayList<>();

		return gradeRepository.getGradesByStudentAndSubject(studentId, subjectId);
	}

	@Override
	public void insertGrades(Integer userId, InsertGradeRequest request) {

		if (Boolean.FALSE.equals(checkLectureDate(request.getData())))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		Integer teacherId = teacherRepository.getTeacherIdByUserId(userId);

		ArrayList<GradeDAO> grades = new ArrayList<>();
		for (NewGrade g : request.getGrades()) {
			GradeDAO gDAO = new GradeDAO();
			gDAO.setDate(request.getData());
			gDAO.setDescription(request.getDescription());
			gDAO.setStudentRef(g.getStudentId());
			gDAO.setSubjectRef(request.getSubjectId());
			gDAO.setTimeSlot(request.getTimeslot());
			gDAO.setTeacherRef(teacherId);
			if (g.getValue().floatValue() > 10 || g.getValue().floatValue() < 0)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			gDAO.setValue(g.getValue());
			grades.add(gDAO);
		}
		gradeRepository.saveAll(grades);
	}

	@Override
	public void updateGrade(Grade request) {
		if (request == null || request.getId() == null || request.getValue() == null || request.getId() < 0
				|| request.getValue().floatValue() < 0 || request.getValue().floatValue() > 10)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if (Boolean.FALSE.equals(checkLectureDate(request.getDate()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		gradeRepository.updateGrade(request.getId(), request.getDescription(), request.getValue());
	}

	@Override
	public void deleteGrade(Grade request) {
		if (request == null || request.getId() == null || request.getId() < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if (Boolean.FALSE.equals(checkLectureDate(request.getDate()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		gradeRepository.deleteById(request.getId());
	}

	@Override
	public List<StudentAttendanceResponse> getStudentsAttendanceByClassIdAndDate(Integer classId, String date) {
		if (classId == null || classId <= 0 || date == null || date.isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		checkDateNotFuture(date);

		Map<Integer, String> studentsNames = new HashMap<>();

		List<StudentDAO> students = studentRepository.getStudentsByClassId(classId);
		students.forEach(s ->
		studentsNames.put(s.getId(), s.getName() + " " + s.getSurname())
				);

		Map<Integer, StudentAttendanceResponse> studentAttendances = new HashMap<>();
		for (StudentDAO s : students) {
			StudentAttendanceResponse sa = new StudentAttendanceResponse(s.getId(), studentsNames.get(s.getId()), null, null);
			studentAttendances.put(s.getId(), sa);
		}

		List<Attendance> attendances = attendanceRepository.getAttendanceByClassIdAndDate(classId, date);
		for (Attendance a : attendances) {
			if (a.getType() != AttendanceType.Exit) {
				studentAttendances.get(a.getStudentId()).setAttendance1(a);
			} else {
				studentAttendances.get(a.getStudentId()).setAttendance2(a);
			}
		}

		return studentAttendances.values().stream().collect(Collectors.toList());
	}

	@Override
	public void insertAttendances(List<StudentAttendanceResponse> attendances, Integer classId, String date) {
		if (attendances == null || attendances.isEmpty() || classId == null || Boolean.FALSE.equals(checkLectureDate(date)))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if (classRepository.findById(classId).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		checkStudentAttendances(attendances);

		List<AttendanceDAO> listAttendance = new ArrayList<>();
		attendances.forEach(a -> {
			if (a.getAttendance1() != null) {
				a.getAttendance1().setStudentId(a.getStudentId());
				listAttendance.add(a.getAttendance1().map());

			}
			if (a.getAttendance2() != null) {
				a.getAttendance2().setStudentId(a.getStudentId());
				listAttendance.add(a.getAttendance2().map());
			}
		});

		List<AttendanceDAO> toRemove = attendanceRepository.getAttendanceDAOByClassIdAndDate(classId, date);

		attendanceRepository.deleteAll(toRemove);
		attendanceRepository.saveAll(listAttendance);

	}

	private void checkStudentAttendances(List<StudentAttendanceResponse> attendances) {
		attendances.forEach(a -> {
			Attendance enter = a.getAttendance1();
			Attendance exit = a.getAttendance2();
			if (enter != null && enter.getType() == AttendanceType.Exit) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			}

			if ((exit != null && enter != null) &&  (exit.getType() != AttendanceType.Exit || enter.getType() == AttendanceType.Absence))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		});
	}

	@Override
	public List<AssignmentDAO> getAssignmentByClassIdAndSubjectId(Integer userId, Integer classId, Integer subjectId) {
		if (classId == null || classId <= 0 || subjectId == null || subjectId <= 0 || userId == null || userId<=0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);


		Integer teacherId = teacherRepository.getTeacherIdByUserId(userId);
		if (teacherId == null)
			return new ArrayList<>();

		Integer tscId = teacherSubjectClassRepository.getId(teacherId, subjectId, classId);
		if (tscId == null)
			return new ArrayList<>();

		return assignmentRepository.getAssignmentsTeacherSubjectClassId(tscId);
	}

	@Override
	public List<Attachment> getAttachmentsByAssignmentId(Integer assignmentid) {
		if (assignmentid == null || assignmentid <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		return attachmentRepository.getAttachmentsByAssignmentIdOrderByName(assignmentid);
	}

	@Override
	public void updateAssignment(Assignment request) {

		if (request == null || request.getId() == null || request.getId() <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		checkDateFormat(request.getDate());

		if(Boolean.TRUE.equals(assignmentRepository.findById(request.getId()).isEmpty()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		AssignmentDAO a;

		try {
			Optional<AssignmentDAO> opt = this.assignmentRepository.findById(request.getId());
			if(opt.isPresent())
				a = opt.get();
			else
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		a.setDescription(request.getDescription());

		this.assignmentRepository.save(a);

	}

	@Override
	public void deleteAssignment(Integer index) {
		if (index == null || index <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		if(Boolean.TRUE.equals(assignmentRepository.findById(index).isEmpty()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		this.attachmentRepository.deleteByAssignmentId(index);
		this.assignmentRepository.deleteById(index);
	}

	@Override
	public void uploadAttachment(NewAttachmentRequest request) {
		if (request == null || request.getName() == null || request.getName().isBlank()
				|| request.getAssignment_ref() == null || request.getAssignment_ref() < 0
				|| request.getFile() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		try {
			attachmentRepository.insertAttachment(request.getAssignment_ref(), request.getFile().getBytes(),
					request.getName());
		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public Integer insertAssignment(Integer userId, Assignment request) {
		if (request == null || request.getDate() == null || request.getSubjectName() == null
				|| request.getClassName() == null || userId == null || request.getSubjectName().isBlank()
				|| request.getClassName().isBlank()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		checkDateFormat(request.getDate());
		checkDateNotPast(request.getDate()); 

		Integer tscId = teacherSubjectClassRepository.getIdByUserIdAndSubjectNameAndClassName(userId,
				request.getSubjectName(), request.getClassName());
		AssignmentDAO a=new AssignmentDAO();
		a.setDate(request.getDate());
		a.setDescription(request.getDescription());
		a.setTeachSubClassRef(tscId);
		return assignmentRepository.save(a).getId();
	}

	@Override
	public void deleteAttachment(Integer index) {
		if (index == null || index <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		if(Boolean.TRUE.equals(attachmentRepository.findById(index).isEmpty()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		this.attachmentRepository.deleteById(index);

	}

	@Override
	public void deleteSupportMaterial(Integer index) {
		if (index == null || index <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		if(Boolean.TRUE.equals(supportMaterialRepository.findById(index).isEmpty()))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		this.supportMaterialRepository.deleteById(index);
	}

	@Override
	public void uploadSupportMaterial(Integer userId, NewSupportMaterialRequest request) {
		if (request == null || request.getName() == null || request.getName().isBlank()
				|| request.getClass_ref() == null || request.getSubject_ref() < 0
				|| request.getFile() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		try {
			Integer teachId = teacherRepository.getTeacherIdByUserId(userId);

			Integer tscId = teacherSubjectClassRepository.
					getIdByClassAndSubjectAndTeacher(request.getClass_ref(), request.getSubject_ref(), teachId);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			String nowStringDate = LocalDate.now().format(formatter);

			supportMaterialRepository.insertSupportMaterial(nowStringDate, tscId, request.getFile().getBytes(),
					request.getName());
		} catch (IOException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public List<TimeTableRow> getTimetableByUserId(Integer userId) {
		if(userId == null || userId < 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		Integer teacherId;

		try {
			teacherId = teacherRepository.getTeacherIdByUserId(userId);
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if(teacherId == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		return userService.getTimetableByTeacherId(teacherId);

	}
}
