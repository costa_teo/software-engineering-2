package it.polito.smsbackend.request;

import it.polito.smsbackend.response.TeacherInfoResponse;
public class UpdateTeachingInClassRequest {
	
	private Integer classId;
	private Integer subjectId;
	private TeacherInfoResponse teacher;
	
	public UpdateTeachingInClassRequest(Integer c, Integer s, TeacherInfoResponse teacher) {
		this.classId = c;
		this.subjectId= s;
		this.teacher = teacher;
	}
	
	
	public TeacherInfoResponse getTeacher() {
		return teacher;
	}
	public void setTeacher(TeacherInfoResponse teacher) {
		this.teacher = teacher;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public Integer getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}

}
