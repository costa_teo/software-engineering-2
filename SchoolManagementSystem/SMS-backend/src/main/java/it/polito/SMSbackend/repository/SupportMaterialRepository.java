package it.polito.smsbackend.repository;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import it.polito.smsbackend.dao.SupportMaterialDAO;
import it.polito.smsbackend.dto.SupportMaterial;

public interface SupportMaterialRepository extends CrudRepository<SupportMaterialDAO, Integer> {

	@Query(value="SELECT new it.polito.smsbackend.dto.SupportMaterial(sm.id, sm.fileName, s.name, u.name, sm.date) "
				+ "FROM SupportMaterialDAO sm, TeacherSubjectClassDAO tsc, SubjectDAO s, TeacherDAO t, UserDAO u "
				+ "WHERE sm.teachSubClassRef=tsc.id AND tsc.classRef=:classId "
				+ "AND tsc.subjectRef=:subjectId AND tsc.subjectRef=s.id AND tsc.teacherRef=t.id AND t.userRef=u.id", nativeQuery = false)
	ArrayList<SupportMaterial> getSupportMaterialByClassAndSubject(@Param("classId") Integer classId,
											@Param("subjectId") Integer subjectId);
	
	@Query(value = "SELECT * FROM SUPPORT_MATERIAL", nativeQuery = true)
	List<SupportMaterialDAO> getAllSupportMaterial(Integer classId, String date);
	
	@Query(value="SELECT file FROM SUPPORT_MATERIAL sm WHERE sm.id = :id", nativeQuery = true)
	Blob getSupportMaterialById(Integer id);
	
	@Modifying
	@Transactional
	@Query(value="INSERT into SUPPORT_MATERIAL (id,date,file,file_name, teach_sub_class_ref) "
			+ "VALUES (SM_seq.nextval, :nowdate, :file, :name, :tscId)", nativeQuery=true)
	void insertSupportMaterial(@Param("nowdate") String nowdate, @Param("tscId") Integer tscId,
								@Param("file") byte[] file,@Param("name") String name);
}
