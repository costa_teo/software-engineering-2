package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
		name="PARENT_STUDENT",
		uniqueConstraints={
				@UniqueConstraint(columnNames = {"parent_ref", "student_ref"})}
		)
public class ParentStudentDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PS_generator")
	@SequenceGenerator(name="PS_generator", sequenceName = "PS_seq", allocationSize=1)
	private Integer id;
	@Column(nullable = false, name = "parent_ref")
	private Integer parentRef;
	@Column(nullable = false, name = "student_ref")
	private Integer studentRef;

	public Integer getParentRef() {
		return parentRef;
	}

	public void setParentRef(Integer parentRef) {
		this.parentRef = parentRef;
	}

	public Integer getStudentRef() {
		return studentRef;
	}

	public void setStudentRef(Integer studentRef) {
		this.studentRef = studentRef;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
