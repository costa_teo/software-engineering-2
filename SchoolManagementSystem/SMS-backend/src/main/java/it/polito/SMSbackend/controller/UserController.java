package it.polito.smsbackend.controller;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.SupportMaterial;
import it.polito.smsbackend.service.AuthenticationService;
import it.polito.smsbackend.service.UserService;

@RestController
@RequestMapping(value = "user")
@CrossOrigin
public class UserController {
	
	private final UserService userService;
	private final AuthenticationService authenticationService;
	
	@Autowired
	public UserController(UserService userService, AuthenticationService authenticationService) {
		this.userService = userService;
		this.authenticationService = authenticationService;
	}
	
	@GetMapping(value = "downloadAttachment",
			produces = MediaType.ALL_VALUE)
	public @ResponseBody byte[] downloadAttachment(@RequestHeader("token") String token, @RequestParam("id") Integer attachmentId) throws SQLException  {
		authenticationService.authenticateUser(token);
		Blob b = userService.downloadAttachment(attachmentId);
		return b.getBytes(0, (int) b.length());
	}
	
	@GetMapping(value = "downloadSupportMaterial",
			produces = MediaType.ALL_VALUE)
	public @ResponseBody byte[] downloadSupportMaterial(@RequestHeader("token") String token, @RequestParam("id") Integer id) throws SQLException  {
		authenticationService.authenticateUser(token);
		Blob b = userService.downloadSupportMaterial(id);
		return b.getBytes(0, (int) b.length());
	}
	
	@GetMapping(value = "getSMaterialByClassIdAndSubjectId")
	public List<SupportMaterial> getSMaterialByClassIdAndSubjectId(@RequestHeader("token") String token, 
			@RequestParam("classId") Integer classId,
			@RequestParam("subjectId") Integer subjectId)  {
			authenticationService.authenticateUser(token);
		return userService.getSMaterialByClassIdAndSubjectId(classId, subjectId);
	}
	
	@GetMapping(value = "getGeneralCommunications")
	public List<GeneralCommunication> getGeneralCommmunications(@RequestHeader("token") String token) {
		authenticationService.authenticateUser(token);
		return userService.getGeneralCommunications();
	}

}
