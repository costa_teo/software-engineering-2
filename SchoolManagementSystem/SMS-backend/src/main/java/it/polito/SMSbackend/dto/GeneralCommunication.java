package it.polito.smsbackend.dto;

import it.polito.smsbackend.dao.GeneralCommunicationDAO;

public class GeneralCommunication {
	
	private Integer id;
	private String description;
	private String date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public GeneralCommunicationDAO map() {
		GeneralCommunicationDAO g=new GeneralCommunicationDAO();
		g.setDate(this.date);
		g.setId(this.id);
		g.setDescription(this.description);
		return g;
	}
}
