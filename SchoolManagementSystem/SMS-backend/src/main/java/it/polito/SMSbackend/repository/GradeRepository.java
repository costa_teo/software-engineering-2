package it.polito.smsbackend.repository;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.GradeDAO;
import it.polito.smsbackend.dto.Grade;

public interface GradeRepository extends CrudRepository<GradeDAO, Integer> {

	/**
	 * NativeQuery = false to use JPQL. Grade needs a constructor with returned values in exact order
	 */
	@Query(value = "SELECT new it.polito.smsbackend.dto.Grade(g.id, g.value, sub.name, u.name, g.date, g.description, g.timeSlot) " +
			"FROM GradeDAO g, SubjectDAO sub, TeacherDAO t, UserDAO u " +
			"WHERE sub.id = g.subjectRef AND " +
			"t.id = g.teacherRef AND " +
			"u.id = t.userRef AND " +
			"g.studentRef = :id AND " +
			"u.role = 'TEACHER' " +
			"ORDER BY sub.name", nativeQuery = false)
	ArrayList<Grade> getGradesByStudentOrderBySubject(@Param("id") Integer id);

	@Query(value= "SELECT new it.polito.smsbackend.dto.Grade(g.id, g.value, sub.name, u.name, g.date, g.description, g.timeSlot) " +
			"FROM GradeDAO g, SubjectDAO sub, TeacherDAO t, UserDAO u " +
			"WHERE t.id = g.teacherRef AND " +
			"u.id = t.userRef AND " +
			"sub.id = :subjectId AND " +
			"g.studentRef = :studentId AND " +
			"g.subjectRef = :subjectId AND " +
			"u.role = 'TEACHER'", nativeQuery=false)
	ArrayList<Grade> getGradesByStudentAndSubject(@Param("studentId") Integer studentId, @Param("subjectId") Integer subjectId);

	@Modifying
	@Transactional
	@Query(value= "UPDATE GRADE SET description=:desc, value=:val WHERE id=:idG", nativeQuery=true)
	void updateGrade(@Param("idG") Integer id, @Param("desc") String description, @Param("val") BigDecimal value);
}
