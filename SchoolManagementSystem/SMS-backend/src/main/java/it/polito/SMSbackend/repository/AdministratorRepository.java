package it.polito.smsbackend.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.AdministratorDAO;

public interface AdministratorRepository extends CrudRepository<AdministratorDAO, Integer> {

	@Modifying
	@Transactional
	@Query(value="INSERT INTO ADMINISTRATOR (id, birthdate, fiscal_code, gender, user_ref, super_admin) "
			+ "VALUES (administrator_seq.nextval, :birthdate, :fiscal_code, :gender, :user_ref, :super_admin)", nativeQuery = true)
	void insertAdministrator(@Param("birthdate") String birthdate, @Param("fiscal_code") String fiscalCode,
						@Param("gender") String gender, @Param("user_ref") Integer userRef, @Param("super_admin") Boolean superAdmin);

	@Query(value= "SELECT * FROM ADMINISTRATOR a WHERE a.user_ref = :user_ref", nativeQuery=true)
	AdministratorDAO getAdministratorByUserRef(@Param("user_ref") Integer userRef);

}
