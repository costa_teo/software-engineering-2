package it.polito.smsbackend.dto;

public class Lecture {
	private Integer id;
	private String date;
	private String className;
	private String teacher;
	private String subject;
	private String description;
	private Integer timeSlot;

	public Lecture() { }

	public Lecture(Integer id, String date, String className, String teacher, String subject, String description,
			Integer timeSlot) {
		this.id = id;
		this.date = date;
		this.className = className;
		this.teacher = teacher;
		this.subject = subject;
		this.description = description;
		this.timeSlot = timeSlot;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getTimeSlot() {
		return timeSlot;
	}
	public void setTimeSlot(Integer timeSlot) {
		this.timeSlot = timeSlot;
	}

}
