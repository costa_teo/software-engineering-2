package it.polito.smsbackend.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.ParentDAO;

public interface ParentRepository extends CrudRepository<ParentDAO, Integer> {

	@Query(value="SELECT * FROM PARENT p WHERE p.fiscal_code = :fiscalCode", nativeQuery = true)
	ParentDAO getParentByFiscalCode(String fiscalCode);
	
	@Query(value="SELECT COUNT(*) FROM PARENT p WHERE p.fiscal_code = :fiscalCode", nativeQuery = true)
	Integer checkParentByFiscalId(String fiscalCode);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO PARENT (id, birthdate, fiscal_code, gender, telephonen, user_ref) "
			+ "VALUES (parent_seq.nextval, :birthdate, :fiscal_code, :gender, :telephone_number, :user_ref)", nativeQuery = true)
	void insertParent(@Param("birthdate") String birthdate, @Param("fiscal_code") String fiscalCode,
						@Param("gender") String gender, @Param("telephone_number") String telephoneNumber, 
						@Param("user_ref") Integer userRef);
}
