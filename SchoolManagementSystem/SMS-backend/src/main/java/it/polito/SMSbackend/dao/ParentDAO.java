package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="PARENT")
public class ParentDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "parent_generator")
	@SequenceGenerator(name="parent_generator", sequenceName = "parent_seq", allocationSize=1)
	private Integer id;
	@Column(unique = true, nullable = false)
	private String fiscalCode;
	private String gender;
	private String birthdate;
	private String telephoneN;
	@Column(nullable = false, name = "user_ref")
	private Integer userRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getTelephoneN() {
		return telephoneN;
	}

	public void setTelephoneN(String telephoneN) {
		this.telephoneN = telephoneN;
	}

	public Integer getUserRef() {
		return userRef;
	}

	public void setUserRef(Integer userRef) {
		this.userRef = userRef;
	}
	
}
