package it.polito.smsbackend.serviceimpl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dao.AdministratorDAO;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.repository.AdministratorRepository;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.service.AuthenticationService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private final UserRepository userRepository;
	private final AdministratorRepository administratorRepository;
	
	@Autowired
	public AuthenticationServiceImpl(UserRepository userRepository, AdministratorRepository administratorRepository) {
		this.userRepository = userRepository;
		this.administratorRepository = administratorRepository;
	}
	
	@Override
	public User login(String username, String password, String role) {
		//MOCK
		//Improve token generation if you want
		//Right now, token corresponds to its own duration (20 minutes)
		LocalDateTime date = LocalDateTime.now().plusMinutes(20);
		
		UserDAO uDAO = this.userRepository.getUserByCredentials(username, username, password, role);
		if(uDAO == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		User u = uDAO.map();
		
		if(role.equals("ADMINISTRATOR")) {
			AdministratorDAO administratorDAO = this.administratorRepository.getAdministratorByUserRef(uDAO.getId());
			if(Boolean.TRUE.equals(administratorDAO.getSuperAdmin())) {
				u.setToken(u.getId()+"_" + date.toString()+"_" +"TRUE");
				return u;
			}
		}
		
		u.setToken(u.getId()+"_" + date.toString()+"_" +"FALSE");
		return u;
	}

	@Override
	public void logout(User user) {
		//
	}

	@Override
	public Integer authenticateUser(String token) {
		if(token.isEmpty() || !token.contains("_")) return null;

		Integer userId = Integer.valueOf(token.split("_")[0]);
		LocalDateTime date = LocalDateTime.parse(token.split("_")[1]);

		if(date.isBefore(LocalDateTime.now())) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}else {
			return userId;
		}
	}

	@Override
	public void changePassword(Integer userId, String newPassword) {
		this.userRepository.changePassword(userId, newPassword);
	}

}
