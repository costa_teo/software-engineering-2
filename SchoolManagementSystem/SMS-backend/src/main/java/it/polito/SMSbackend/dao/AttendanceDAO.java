package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ATTENDANCE")
public class AttendanceDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attendance_generator")
	@SequenceGenerator(name="attendance_generator", sequenceName = "attendance_seq", allocationSize=1)
	private Integer id;
	private String date;
	private Integer timeslot;
	private String type;
	@Column(nullable = false, name = "student_ref")
	private Integer studentRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public Integer getStudentRef() {
		return studentRef;
	}

	public void setStudentRef(Integer studentRef) {
		this.studentRef = studentRef;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getTimeslot() {
		return timeslot;
	}

	public void setTimeslot(Integer timeslot) {
		this.timeslot = timeslot;
	}
}
