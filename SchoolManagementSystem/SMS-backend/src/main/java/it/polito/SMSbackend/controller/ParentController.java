package it.polito.smsbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Attendance;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.response.AssignmentsBySubjectResponse;
import it.polito.smsbackend.response.GradesBySubjectResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TimeTableResponse;
import it.polito.smsbackend.service.AuthenticationService;
import it.polito.smsbackend.service.ParentService;

@RestController
@RequestMapping(value = "parent")
@CrossOrigin
public class ParentController {

	private final ParentService parentService;
	private final AuthenticationService authenticationService;

	@Autowired
	public ParentController(ParentService parentService, AuthenticationService authenticationService) {
		this.parentService = parentService;
		this.authenticationService = authenticationService;
	}
	
	@GetMapping(value = "getGrades")
	public List<GradesBySubjectResponse> getGrades(@RequestHeader("token") String token, @RequestParam("id") Integer studentId) {
		authenticationService.authenticateUser(token);
		return parentService.getGrades(studentId);
	}
	
	@GetMapping(value = "getStudents")
	public List<StudentInfoResponse> getStudents(@RequestHeader("token") String token)  {
		Integer userId = authenticationService.authenticateUser(token);
		return parentService.getStudents(userId);
	}
	
	@GetMapping(value = "getAssignments")
	public List<AssignmentsBySubjectResponse> getAssignments(@RequestHeader("token") String token, @RequestParam("id") Integer studentId)  {
		authenticationService.authenticateUser(token);
		return parentService.getAssignments(studentId);
	}
	
	@GetMapping(value = "getAttachments")
	public List<Attachment> getAttachments(@RequestHeader("token") String token, @RequestParam("id") Integer assignmentId) {
		authenticationService.authenticateUser(token);
		return parentService.getAttachments(assignmentId);
	}
	
	@GetMapping(value = "getAttendance")
	public List<Attendance> getAttendances(@RequestHeader("token") String token, @RequestParam("id") Integer studentId)  {
		authenticationService.authenticateUser(token);
		return parentService.getAttendances(studentId);
	}
	
	@GetMapping(value = "getClassByStudentId")
	public Class getClassByStudentId(@RequestHeader("token") String token, @RequestParam("id") Integer studentId) {
		authenticationService.authenticateUser(token);
		return parentService.getClass(studentId);
	}

	@GetMapping(value = "getSubjectsByClassId")
	public List<Subject> getSubjectsByClassId(@RequestHeader("token") String token, @RequestParam("id") Integer classId)  {
		authenticationService.authenticateUser(token);
		return parentService.getSubjectsByClassId(classId);
	}

	@GetMapping(value = "getTimetableByStudentId")
	public TimeTableResponse getTimetableByStudentId(@RequestHeader("token") String token, @RequestParam("studentId") Integer studentId)  {
		authenticationService.authenticateUser(token);
		return parentService.getTimetableByStudentId(studentId);
	}
	
	
	
}
