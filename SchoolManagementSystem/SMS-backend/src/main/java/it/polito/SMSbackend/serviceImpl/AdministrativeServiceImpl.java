package it.polito.smsbackend.serviceimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.common.Constants;
import it.polito.smsbackend.dao.ClassDAO;
import it.polito.smsbackend.dao.GeneralCommunicationDAO;
import it.polito.smsbackend.dao.ParentDAO;
import it.polito.smsbackend.dao.StudentDAO;
import it.polito.smsbackend.dao.SubjectDAO;
import it.polito.smsbackend.dao.TeacherSubjectClassDAO;
import it.polito.smsbackend.dao.TimeTableDAO;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.Parent;
import it.polito.smsbackend.dto.Student;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.emailsystem.EmailSystem;
import it.polito.smsbackend.repository.AdministratorRepository;
import it.polito.smsbackend.repository.ClassRepository;
import it.polito.smsbackend.repository.GeneralCommunicationRepository;
import it.polito.smsbackend.repository.ParentRepository;
import it.polito.smsbackend.repository.ParentStudentRepository;
import it.polito.smsbackend.repository.StudentRepository;
import it.polito.smsbackend.repository.SubjectRepository;
import it.polito.smsbackend.repository.TeacherRepository;
import it.polito.smsbackend.repository.TeacherSubjectClassRepository;
import it.polito.smsbackend.repository.TimeTableRepository;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.request.AddParentStudentRequest;
import it.polito.smsbackend.request.CreateClassesStudentRequest;
import it.polito.smsbackend.request.TimeTableRequest;
import it.polito.smsbackend.request.UpdateClassRequest;
import it.polito.smsbackend.request.UpdateTeachingInClassRequest;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TeacherInfoResponse;
import it.polito.smsbackend.service.AdministrativeService;

@Service
public class AdministrativeServiceImpl implements AdministrativeService {

	private final ParentRepository parentRepository;
	private final StudentRepository studentRepository;
	private final UserRepository userRepository;
	private final ClassRepository classRepository;
	private final ParentStudentRepository parentStudentRepository;
	private final GeneralCommunicationRepository generalCommunicationRepository;
	private final SubjectRepository subjectRepository;
	private final TeacherRepository teacherRepository;
	private final TeacherSubjectClassRepository teacherSubjectClassRepository;
	private final AdministratorRepository administratorRepository;
	private final TimeTableRepository timeTableRepository;

	@Autowired
	public AdministrativeServiceImpl(ParentRepository parentRepository, StudentRepository studentRepository,
			UserRepository userRepository, ClassRepository classRepository,
			ParentStudentRepository parentStudentRepository,
			GeneralCommunicationRepository generalCommunicationRepository, SubjectRepository subjectRepository,
			TeacherRepository teacherRepository, TeacherSubjectClassRepository teacherSubjectClassRepository,
			AdministratorRepository administratorRepository,
			TimeTableRepository timeTableRepository) {
		this.parentRepository = parentRepository;
		this.studentRepository = studentRepository;
		this.userRepository = userRepository;
		this.classRepository = classRepository;
		this.parentStudentRepository = parentStudentRepository;
		this.generalCommunicationRepository = generalCommunicationRepository;
		this.subjectRepository = subjectRepository;
		this.teacherRepository = teacherRepository;
		this.teacherSubjectClassRepository = teacherSubjectClassRepository;
		this.administratorRepository = administratorRepository;
		this.timeTableRepository = timeTableRepository;
	}

	@Override
	public boolean checkIfParentExists(String token) {
		if (token == null)
			return false;
		Integer exist = this.parentRepository.checkParentByFiscalId(token);

		return exist == 1;
	}

	@Override
	public boolean checkIfStudentExists(String token) {
		if (token == null)
			return false;
		Integer exist = this.studentRepository.checkStudentByFiscalId(token);

		return exist == 1;
	}

	@Override
	public void addParentsStudent(AddParentStudentRequest parentsStudent) {

		if ((parentsStudent.getParent1() == null && parentsStudent.getParent2() == null) || parentsStudent.getStudent() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} else if ((parentsStudent.getParent1() != null && parentsStudent.getParent2() != null) && parentsStudent
				.getParent1().getFiscalCode().compareTo(parentsStudent.getParent2().getFiscalCode()) == 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}

		Parent p1 = parentsStudent.getParent1();
		Parent p2 = parentsStudent.getParent2();
		Student s = parentsStudent.getStudent();

		final Pattern datePattern = Pattern.compile("[1-2][0-9]{3}/[0-1][0-9]/[0-3][0-9]");
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

		if (p1 != null && p2 != null) {
			Integer p1Exist = this.parentRepository.checkParentByFiscalId(p1.getFiscalCode());
			Integer p2Exist = this.parentRepository.checkParentByFiscalId(p2.getFiscalCode());
			Integer sExist = this.studentRepository.checkStudentByFiscalId(s.getFiscalCode());

			if (p1.getFiscalCode() == null || p1.getFiscalCode().isBlank()|| p2.getFiscalCode() == null
					|| p2.getFiscalCode().isBlank() || s.getFiscalCode() == null || s.getFiscalCode().isBlank())
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			if (sExist == 0) {
				if (s.getBirthdate() == null)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean dates = datePattern.matcher(s.getBirthdate()).matches();

				if (Boolean.FALSE.equals(dates))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(s.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}

				if (s.getGender() == null || s.getGender().isBlank() || s.getSurname() == null || s.getSurname().isBlank()
						|| s.getName() == null || s.getName().isBlank() || (!s.getGender().equals("M") && !s.getGender().equals("F"))
						|| s.getEntryLevel() == null || s.getEntryLevel() <= 0 || s.getEntryLevel() >= 11)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			}

			if (p1Exist == 0) {
				if (p1.getBirthdate() == null)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean datep1 = datePattern.matcher(p1.getBirthdate()).matches();

				if (Boolean.FALSE.equals(datep1))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(p1.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}

				if (p1.getEmail() == null || p1.getEmail().isBlank() || p1.getGender() == null || p1.getGender().isBlank()
						|| p1.getName() == null || p1.getName().isBlank() || p1.getSurname() == null
						|| p1.getSurname().isBlank() || p1.getTelephoneN() == null
						|| (!p1.getGender().equals("M") && !p1.getGender().equals("F"))
						|| !p1.getTelephoneN().matches("^[0-9]{10,12}$")) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}

			}
			if (p2Exist == 0) {
				if (p2.getBirthdate() == null || p2.getBirthdate().isBlank())
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean datep2 = datePattern.matcher(p2.getBirthdate()).matches();

				if (Boolean.FALSE.equals(datep2))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(p2.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}
				if (p2.getEmail() == null || p2.getEmail().isBlank() || p2.getGender() == null || p2.getGender().isBlank()
						|| p2.getName() == null || p2.getName().isBlank() || p2.getSurname() == null
						|| p2.getSurname().isBlank() || p2.getTelephoneN() == null
						|| (!p2.getGender().equals("M") && !p2.getGender().equals("F")) || !p2.getTelephoneN()
						.matches("^[0-9]{10,12}$")) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}
			}

			if (p1Exist == 0) {
				if (!p1.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+"))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				Integer lastIdInteger = this.userRepository.getLastUserId();
				lastIdInteger++;

				String passwordHash = Long.toString(new java.util.Random().nextLong() & Long.MAX_VALUE, 36);

				this.userRepository.insertUser(p1.getEmail(), p1.getName(), passwordHash, Constants.PARENT, p1.getSurname(),
						"U" + lastIdInteger);
				UserDAO user1 = this.userRepository.getUserByCredentials(p1.getEmail(), "U" + lastIdInteger,
						passwordHash, Constants.PARENT);
				this.parentRepository.insertParent(p1.getBirthdate(), p1.getFiscalCode(), p1.getGender(),
						p1.getTelephoneN(), user1.getId());
				EmailSystem.sendEmail(p1.getEmail(), "U" + lastIdInteger, passwordHash);
			}
			if (p2Exist == 0) {
				if (!p2.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+"))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				Integer lastIdInteger2 = this.userRepository.getLastUserId();
				lastIdInteger2++;

				String passwordHash2 = Long.toString(new java.util.Random().nextLong() & Long.MAX_VALUE, 36);

				this.userRepository.insertUser(p2.getEmail(), p2.getName(), passwordHash2, Constants.PARENT, p2.getSurname(),
						"U" + lastIdInteger2);
				UserDAO user2 = this.userRepository.getUserByCredentials(p2.getEmail(), "U" + lastIdInteger2,
						passwordHash2, Constants.PARENT);
				this.parentRepository.insertParent(p2.getBirthdate(), p2.getFiscalCode(), p2.getGender(),
						p2.getTelephoneN(), user2.getId());
				EmailSystem.sendEmail(p2.getEmail(), "U" + lastIdInteger2, passwordHash2);
			}
			if (sExist == 0) {
				this.studentRepository.insertStudent(s.getBirthdate(), null, s.getEntryLevel(), s.getFiscalCode(),
						s.getGender(), s.getName(), s.getSurname());
			}

			StudentDAO sFiscalCode = this.studentRepository.getStudentByFiscalCode(s.getFiscalCode());
			ParentDAO p1FiscalCode = this.parentRepository.getParentByFiscalCode(p1.getFiscalCode());
			ParentDAO p2FiscalCode = this.parentRepository.getParentByFiscalCode(p2.getFiscalCode());

			if (this.parentStudentRepository.checkParentStudent(p1FiscalCode.getId(), sFiscalCode.getId()) == 0) {
				this.parentStudentRepository.insertStudentParent(p1FiscalCode.getId(), sFiscalCode.getId());
			}
			if (this.parentStudentRepository.checkParentStudent(p2FiscalCode.getId(), sFiscalCode.getId()) == 0) {
				this.parentStudentRepository.insertStudentParent(p2FiscalCode.getId(), sFiscalCode.getId());
			}
		} else if (p1 != null && p2 == null) {
			Integer p1Exist = this.parentRepository.checkParentByFiscalId(p1.getFiscalCode());
			Integer sExist = this.studentRepository.checkStudentByFiscalId(s.getFiscalCode());

			if (p1.getFiscalCode() == null || p1.getFiscalCode().isBlank() || s.getFiscalCode() == null
					|| s.getFiscalCode().isBlank())
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

			if (sExist == 0) {
				if (s.getBirthdate() == null)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean dates = datePattern.matcher(s.getBirthdate()).matches();

				if (Boolean.FALSE.equals(dates))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(s.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}

				if (s.getBirthdate() == null || s.getGender() == null || s.getGender().isBlank() || s.getSurname() == null
						|| s.getSurname().isBlank() || s.getName() == null || s.getName().isBlank()
						|| (!s.getGender().equals("M") && !s.getGender().equals("F")) || s.getEntryLevel() == null
						|| s.getEntryLevel() >= 11 || s.getEntryLevel() <= 0)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			}
			if (p1Exist == 0) {
				if (p1.getBirthdate() == null)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean datep1 = datePattern.matcher(p1.getBirthdate()).matches();

				if (Boolean.FALSE.equals(datep1))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(p1.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}
				if (p1.getEmail() == null || p1.getEmail().isBlank() || p1.getGender() == null || p1.getGender().isBlank()
						|| p1.getName() == null || p1.getName().isBlank() || p1.getSurname() == null
						|| p1.getSurname().isBlank() || p1.getTelephoneN() == null
						|| (!p1.getGender().equals("M") && !p1.getGender().equals("F"))
						|| !p1.getTelephoneN().matches("^[0-9]{10,12}$")) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}
			}

			if (p1Exist == 0) {
				if (!p1.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+"))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				Integer lastIdInteger = this.userRepository.getLastUserId();
				lastIdInteger++;

				String passwordHash = Long.toString(new java.util.Random().nextLong() & Long.MAX_VALUE, 36);

				this.userRepository.insertUser(p1.getEmail(), p1.getName(), passwordHash, Constants.PARENT, p1.getSurname(),
						"U" + lastIdInteger);
				UserDAO user1 = this.userRepository.getUserByCredentials(p1.getEmail(), "U" + lastIdInteger,
						passwordHash, Constants.PARENT);
				this.parentRepository.insertParent(p1.getBirthdate(), p1.getFiscalCode(), p1.getGender(),
						p1.getTelephoneN(), user1.getId());
				EmailSystem.sendEmail(p1.getEmail(), "U" + lastIdInteger, passwordHash);
			}
			if (sExist == 0) {
				this.studentRepository.insertStudent(s.getBirthdate(), null, s.getEntryLevel(), s.getFiscalCode(),
						s.getGender(), s.getName(), s.getSurname());
			}

			StudentDAO sFiscalCode = this.studentRepository.getStudentByFiscalCode(s.getFiscalCode());
			ParentDAO p1FiscalCode = this.parentRepository.getParentByFiscalCode(p1.getFiscalCode());

			if (this.parentStudentRepository.checkParentStudent(p1FiscalCode.getId(), sFiscalCode.getId()) == 0) {
				this.parentStudentRepository.insertStudentParent(p1FiscalCode.getId(), sFiscalCode.getId());
			}
		} else if (p1 == null && p2 != null) {
			Integer p2Exist = this.parentRepository.checkParentByFiscalId(p2.getFiscalCode());
			Integer sExist = this.studentRepository.checkStudentByFiscalId(s.getFiscalCode());

			if (p2.getFiscalCode() == null || p2.getFiscalCode().isBlank() || s.getFiscalCode() == null
					|| s.getFiscalCode().isBlank())
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

			if (sExist == 0) {
				if (s.getBirthdate() == null)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean dates = datePattern.matcher(s.getBirthdate()).matches();

				if (Boolean.FALSE.equals(dates))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(s.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}

				if (s.getBirthdate() == null || s.getGender() == null || s.getGender().isBlank() || s.getSurname() == null
						|| s.getSurname().isBlank() || s.getName() == null || s.getName().isBlank()
						|| (!s.getGender().equals("M") && !s.getGender().equals("F")) || s.getEntryLevel() == null
						|| s.getEntryLevel() >= 11 || s.getEntryLevel() <= 0)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			}
			if (p2Exist == 0) {
				if (p2.getBirthdate() == null)
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				boolean datep2 = datePattern.matcher(p2.getBirthdate()).matches();

				if (Boolean.FALSE.equals(datep2))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

				LocalDate date = LocalDate.parse(p2.getBirthdate(), formatter);
				String nowString = LocalDate.now().format(formatter);
				LocalDate now = LocalDate.parse(nowString, formatter);

				if (Boolean.TRUE.equals((date.isAfter(now)))) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}
				if (p2.getEmail() == null || p2.getEmail().isBlank() || p2.getGender() == null || p2.getGender().isBlank()
						|| p2.getName() == null || p2.getName().isBlank() || p2.getSurname() == null
						|| p2.getSurname().isBlank() || p2.getTelephoneN() == null
						|| (!p2.getGender().equals("M") && !p2.getGender().equals("F"))
						|| !p2.getTelephoneN().matches("^[0-9]{10,12}$")) {

					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				}
			}

			if (p2Exist == 0) {
				if (!p2.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+"))
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
				Integer lastIdInteger2 = this.userRepository.getLastUserId();
				lastIdInteger2++;

				String passwordHash2 = Long.toString(new java.util.Random().nextLong() & Long.MAX_VALUE, 36);

				this.userRepository.insertUser(p2.getEmail(), p2.getName(), passwordHash2, Constants.PARENT, p2.getSurname(),
						"U" + lastIdInteger2);
				UserDAO user2 = this.userRepository.getUserByCredentials(p2.getEmail(), "U" + lastIdInteger2,
						passwordHash2, Constants.PARENT);
				this.parentRepository.insertParent(p2.getBirthdate(), p2.getFiscalCode(), p2.getGender(),
						p2.getTelephoneN(), user2.getId());
				EmailSystem.sendEmail(p2.getEmail(), "U" + lastIdInteger2, passwordHash2);
			}
			if (sExist == 0) {
				this.studentRepository.insertStudent(s.getBirthdate(), null, s.getEntryLevel(), s.getFiscalCode(),
						s.getGender(), s.getName(), s.getSurname());
			}

			StudentDAO sFiscalCode = this.studentRepository.getStudentByFiscalCode(s.getFiscalCode());
			ParentDAO p2FiscalCode = this.parentRepository.getParentByFiscalCode(p2.getFiscalCode());

			if (this.parentStudentRepository.checkParentStudent(p2FiscalCode.getId(), sFiscalCode.getId()) == 0) {
				this.parentStudentRepository.insertStudentParent(p2FiscalCode.getId(), sFiscalCode.getId());
			}
		}

	}

	@Override
	public ArrayList<StudentInfoResponse> getStudentsInClass(Integer classId) {
		ArrayList<StudentInfoResponse> r = new ArrayList<>();
		List<StudentDAO> students = studentRepository.getStudentsByClassId(classId);
		for (StudentDAO s : students) {
			StudentInfoResponse sR = new StudentInfoResponse();
			sR.setId(s.getId());
			sR.setName(s.getName());
			sR.setSurname(s.getSurname());
			r.add(sR);
		}
		return r;
	}

	@Override
	public ArrayList<Class> getAllClasses() {
		ArrayList<ClassDAO> cDAO = classRepository.getAllClasses();
		ArrayList<Class> classes = new ArrayList<>();
		for (ClassDAO c : cDAO)
			classes.add(c.map());
		return classes;
	}

	@Override
	public String createClass(Integer classNumber) {
		if (classNumber == null || classNumber > 5 || classNumber < 1)
			return null;
		ArrayList<ClassDAO> cDAO = classRepository.getAllClasses();
		long n = cDAO.stream().filter(c -> c.getName().contains(classNumber.toString())).count();
		n = n + (int) 'A';
		String newName = classNumber + Character.toString((char) n);
		classRepository.createNewClass(newName);
		return newName;
	}

	@Override
	public void updateClass(UpdateClassRequest request) {
		if (request == null || request.getC() == null || request.getStudents() == null || request.getC().getId() == null
				|| request.getC().getId() < 1)
			return;
		List<StudentDAO> deletedStudents;
		deletedStudents = studentRepository.getStudentsByClassId(request.getC().getId());
		Map<Integer, StudentDAO> dSMap = deletedStudents.stream().collect(Collectors.toMap(StudentDAO::getId, s -> s));

		for (StudentInfoResponse s : request.getStudents()) {
			if (dSMap.containsKey(s.getId()))
				dSMap.remove(s.getId());
			studentRepository.updateStudentClass(request.getC().getId(), s.getId());
		}

		for (StudentDAO s : dSMap.values()) {
			studentRepository.updateStudentClass(null, s.getId());
		}
	}

	@Override
	public void createExcelClasses(List<CreateClassesStudentRequest> classesStudents) {

		/* check null */
		if (classesStudents == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		/* check all not null */
		classesStudents.forEach(cs -> {
			if (cs.getClasse() == null || cs.getStudent() == null)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Some columns are empty");
		});

		/* check the pattern of the class name */
		classesStudents.forEach(cs -> {
			cs.setClasse(cs.getClasse().toUpperCase());
			if (!cs.getClasse().matches("[1-5][A-Z]"))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						"Class name: " + cs.getClasse() + " is not valid. Valid example are: 1A, 2B, 3F");
		});

		/* check if the students exist */
		List<String> allStudentsFiscalCode = new ArrayList<>();
		studentRepository.getAllStudents().forEach(fisc -> allStudentsFiscalCode.add(fisc.getFiscalCode()));

		classesStudents.forEach(stud -> {
			if (!allStudentsFiscalCode.contains(stud.getStudent()))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						"Student: " + stud.getStudent() + " does not exist in the database");
		});

		/* check if the class exist or create it */
		List<String> allClassesNames = new ArrayList<>();
		classRepository.getAllClasses().forEach(c -> allClassesNames.add(c.getName()));

		classesStudents.forEach(classe -> {
			if (!allClassesNames.contains(classe.getClasse()))
				classRepository.createNewClass(classe.getClasse());
		});

		/* update the student class */
		classesStudents.forEach(cs -> {
			Integer idClassInteger = classRepository.getClassByName(cs.getClasse()).getId();
			studentRepository.updateStudentClass(idClassInteger, cs.getStudent());
		});

	}

	private Boolean checkCommunicationDate(String communicationDate) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			// This parse can throw exception, but it's fine.
			LocalDate date = LocalDate.parse(communicationDate, formatter);
			LocalDate now = LocalDate.now();
			LocalDate lastSunday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));

			if (Boolean.TRUE.equals((date.isAfter(lastSunday) && (date.isBefore(now) || date.isEqual(now))))) {
				return Boolean.TRUE;
			}

			return Boolean.FALSE;
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	public void insertGeneralCommunications(GeneralCommunication request) {
		if (request == null || request.getDescription() == null || request.getDescription().isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		GeneralCommunicationDAO g = new GeneralCommunicationDAO();
		g.setDate(todayAsString);
		g.setDescription(request.getDescription());
		generalCommunicationRepository.save(g);
	}

	@Override
	public void updateGeneralCommunications(GeneralCommunication request) {
		if (request == null || request.getDescription() == null || request.getDescription().isBlank()
				|| request.getDate() == null || request.getDate().isBlank() || request.getId() == null
				|| request.getId() <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		GeneralCommunicationDAO g;
		
		try {
			Optional<GeneralCommunicationDAO> opt = generalCommunicationRepository.findById(request.getId());
			if(!opt.isPresent())
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
			g = opt.get();
		}catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(g.getDate().compareTo(request.getDate())!=0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		
		if (Boolean.FALSE.equals(checkCommunicationDate(request.getDate())))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		generalCommunicationRepository.save(request.map());
	}

	@Override
	public void deleteGeneralCommunications(GeneralCommunication request) {
		if (request == null || request.getId() == null || request.getDate() == null || request.getId() <= 0)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if (Boolean.FALSE.equals(checkCommunicationDate(request.getDate())))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		generalCommunicationRepository.deleteById(request.getId());
	}

	@Override
	public List<Subject> getAllSubjects() {
		List<SubjectDAO> sDAO = (List<SubjectDAO>) subjectRepository.findAll();
		List<Subject> s = new ArrayList<>();
		if (sDAO.isEmpty())
			return s;
		
		for (SubjectDAO subDAO : sDAO) {
			s.add(subDAO.map());
		}
		
		return s;
	}

	@Override
	public TeacherInfoResponse getTeacherByClassAndSubject(Integer subjectId, Integer classId) {
		if (subjectId == null || subjectId <= 0 || classId == null || classId <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		List<TeacherInfoResponse> t=teacherRepository.getTeachersByClassIdAndSubjectId(classId, subjectId);
		if(t.size()==1) {
			return t.get(0);
		}
		else {
			return null;
		}
	}

	@Override
	public void updateTeachingInClass(UpdateTeachingInClassRequest request) {
		if (request == null || request.getClassId() == null || request.getClassId() <= 0
				|| request.getSubjectId() == null || request.getSubjectId() <= 0
				|| (request.getTeacher() != null && request.getTeacher().getId() == null)
				|| (request.getTeacher() != null && request.getTeacher().getId() <= 0)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		Integer id = teacherSubjectClassRepository.getIdByClassAndSubject(request.getClassId(), request.getSubjectId());
		TeacherSubjectClassDAO tscDAO = new TeacherSubjectClassDAO();
		if (id != null) {
			tscDAO.setId(id);
			if(request.getTeacher()==null) {
				teacherSubjectClassRepository.deleteById(id);
				return;
			}
		}
		tscDAO.setClassRef(request.getClassId());
		tscDAO.setSubjectRef(request.getSubjectId());
		tscDAO.setTeacherRef(request.getTeacher().getId());
		teacherSubjectClassRepository.save(tscDAO);
	}

	@Override
	public List<TeacherInfoResponse> getAllTeachers() {
		return teacherRepository.getTeachersByClassIdAndSubjectId(null, null);
	}

	private void checkDateNotFuture(String stringDate) {
		try {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			// This parse can throw exception, but it's fine.
			LocalDate date = LocalDate.parse(stringDate, formatter);
			LocalDate now = LocalDate.now();
			if (date.isAfter(now))
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
	}

	@Override
	@Transactional
	public void addAccount(User request) {

		if(request == null) 
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if(request.getRole() == null || request.getRole().isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		String role = request.getRole();

		User user = request;

		if(user.getBirthdate() == null || user.getBirthdate().isBlank() || user.getFiscalCode() == null || user.getFiscalCode().isBlank()
				|| user.getName() == null || user.getName().isBlank() || user.getSurname() == null || user.getSurname().isBlank()
				|| user.getEmail() == null || user.getEmail().isBlank()
				|| !request.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]+")
				|| user.getGender() == null || user.getGender().isBlank() || (user.getGender().compareTo("M")!=0 && user.getGender().compareTo("F")!=0)
				|| !user.getBirthdate().matches("[0-9]{4}/[0-9]{2}/[0-9]{2}") || !user.getTelephoneN().matches("^[0-9]{10,12}$"))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if( !request.getFiscalCode().matches("[a-zA-Z0-9]{16}") )
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if ((!role.equals(Constants.TEACHER) && !role.equals(Constants.ADMINISTRATOR) && !role.equals(Constants.PRINCIPAL)))
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		if ( user.getTelephoneN() == null || user.getTelephoneN().isBlank())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		checkDateNotFuture(user.getBirthdate());

		Integer lastIdInteger = this.userRepository.getLastUserId();
		lastIdInteger++;

		String passwordHash = Long.toString(new java.util.Random().nextLong() & Long.MAX_VALUE, 36);		
		
		userRepository.insertUser(user.getEmail(), user.getName(), passwordHash, role, user.getSurname(), "U" + lastIdInteger);
		String username = userRepository.getLastUsername();
		
		Integer uId = userRepository.findByUsername(username).getId();
		
		if(role.equals(Constants.TEACHER)) {
			teacherRepository.insertTeacher(user.getBirthdate(), user.getFiscalCode(), user.getGender(), user.getTelephoneN(), uId);
		}
		else if(role.equals(Constants.ADMINISTRATOR)) {
			administratorRepository.insertAdministrator(user.getBirthdate(), user.getFiscalCode(), user.getGender(), uId, Boolean.FALSE);
		}
		else if(role.equals(Constants.PRINCIPAL)) {
			administratorRepository.insertAdministrator(user.getBirthdate(), user.getFiscalCode(), user.getGender(), uId, Boolean.TRUE);
		}
		
		EmailSystem.sendEmail(user.getEmail(), username, passwordHash);
	}

	@Override
	@Transactional
	public void addClassTimetable(TimeTableRequest request) {
		if(request == null || request.getClassId() == null || request.getClassId() < 0 || request.getTimetable() == null)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

		if(classRepository.findById(request.getClassId()).isEmpty())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Selected class does not exist");

		if(request.getTimetable().size() != 6)
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Uploaded file has wrong number of lines");

		//If timetable is already present, overwrite it
		try {
			timeTableRepository.deleteByClassId(request.getClassId());
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		List<TimeTableDAO> lessons = new ArrayList<>();

		int timeS = 0;
		for(TimeTableRow ttr : request.getTimetable()) {
			if(ttr == null)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST);

			TimeTableDAO ttDAO;
			Integer id = null;
			String[] subjects = new String[] {ttr.getMonday(), ttr.getTuesday(), ttr.getWednesday(), ttr.getThursday(), ttr.getFriday()};

			int day = 0;
			for(String subj : subjects) {
				ttDAO = new TimeTableDAO();
				ttDAO.setTimeSlot(timeS);
				ttDAO.setDayOfWeek(day);
				if(subj != null && !subj.isBlank()) {
					try {
						id = teacherSubjectClassRepository.getIdByClassIdAndSubjectName(request.getClassId(), subj);
					} catch(Exception e) {
						throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
					}

					if(id == null) {
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
								"Subject " + ttr.getMonday() + " is not taught in the selected class");
					}

					ttDAO.setTeachSubClassRef(id);
					lessons.add(ttDAO);
				}

				day++;
			}

			timeS++;
		}

		timeTableRepository.saveAll(lessons);

		List<Integer> check;

		try {
			check = timeTableRepository.getCountGroupByTimeAndDayAndTeacher();
		} catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		for(Integer i : check) {
			if(i > 1)
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						"Some lessons in the uploaded Time Table conflict with existing ones in other classes");
		}

	}

}
