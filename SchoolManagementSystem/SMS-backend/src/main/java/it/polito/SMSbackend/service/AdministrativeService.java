package it.polito.smsbackend.service;

import java.util.List;

import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.request.AddParentStudentRequest;
import it.polito.smsbackend.request.CreateClassesStudentRequest;
import it.polito.smsbackend.request.TimeTableRequest;
import it.polito.smsbackend.request.UpdateClassRequest;
import it.polito.smsbackend.request.UpdateTeachingInClassRequest;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TeacherInfoResponse;

public interface AdministrativeService {
	
	boolean checkIfParentExists(String token);
	
	boolean checkIfStudentExists(String token);
	
	void addParentsStudent(AddParentStudentRequest parentsStudent);
	
	List<StudentInfoResponse> getStudentsInClass(Integer classId);
	
	List<Class> getAllClasses();
	
	String createClass(Integer classNumber);
	
	void updateClass(UpdateClassRequest request);
	
	void createExcelClasses(List<CreateClassesStudentRequest> classesStudents);
	
	void insertGeneralCommunications(GeneralCommunication request);
	
	void updateGeneralCommunications(GeneralCommunication request);
	
	void deleteGeneralCommunications(GeneralCommunication request);
	
	List<Subject> getAllSubjects();
	
	TeacherInfoResponse getTeacherByClassAndSubject(Integer subjectId, Integer classId);
	
	void updateTeachingInClass(UpdateTeachingInClassRequest request);
	
	List<TeacherInfoResponse> getAllTeachers();
	
	void addAccount(User request);
	
	void addClassTimetable(TimeTableRequest request);
}
