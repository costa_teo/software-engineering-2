package it.polito.smsbackend.service;

import it.polito.smsbackend.dto.User;

public interface AuthenticationService {
	
	User login(String username, String password, String role);
	
	void logout(User user);
	
	Integer authenticateUser(String token);
	
	void changePassword(Integer userId, String newPassword);

}
