package it.polito.smsbackend.request;

import java.util.List;

import it.polito.smsbackend.dto.NewGrade;

public class InsertGradeRequest {

	private List<NewGrade> grades;
	private String description;
	private String data;
	private Integer timeslot;
	private Integer subjectId;
	private Integer teacherId;
	
	public List<NewGrade> getGrades() {
		return grades;
	}
	public String getDescription() {
		return description;
	}
	public String getData() {
		return data;
	}
	public Integer getTimeslot() {
		return timeslot;
	}
	public Integer getSubjectId() {
		return subjectId;
	}
	public Integer getTeacherId() {
		return teacherId;
	}
	public void setGrades(List<NewGrade> grades) {
		this.grades = grades;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setTimeslot(Integer timeslot) {
		this.timeslot = timeslot;
	}
	public void setSubjectId(Integer subjectId) {
		this.subjectId = subjectId;
	}
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	
	
}
