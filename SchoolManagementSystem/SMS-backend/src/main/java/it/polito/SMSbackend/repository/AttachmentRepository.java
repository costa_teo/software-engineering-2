package it.polito.smsbackend.repository;

import java.sql.Blob;
import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.AttachmentDAO;
import it.polito.smsbackend.dto.Attachment;

public interface AttachmentRepository extends CrudRepository<AttachmentDAO, Integer> {

	@Query(value = "SELECT new it.polito.smsbackend.dto.Attachment(a.id, a.fileName) FROM AttachmentDAO a WHERE a.assignmentRef = :assignmentId ORDER BY a.fileName", nativeQuery = false)
	ArrayList<Attachment> getAttachmentsByAssignmentIdOrderByName(@Param("assignmentId") Integer assignmentId);
	
	@Modifying
	@Transactional
	@Query(value="INSERT into ATTACHMENT (id,assignment_ref,file,file_Name) "
			+ "VALUES (attachment_seq.nextval, :assignmentId, :file, :name)", nativeQuery=true)
	void insertAttachment(@Param("assignmentId") int assignmentId,@Param("file") byte[] file,@Param("name") String name);

	@Query(value="SELECT COUNT(*) FROM ATTACHMENT a WHERE a.id = :id", nativeQuery = true)
	Integer checkAttachmentById(Integer id);
	
	@Query(value="SELECT file FROM ATTACHMENT a WHERE a.id = :id", nativeQuery = true)
	Blob getAttachmentById(Integer id);

	@Modifying
	@Transactional
	@Query(value="DELETE from ATTACHMENT where assignment_ref=:id", nativeQuery=true)
	void deleteByAssignmentId(@Param("id") Integer index);
}
