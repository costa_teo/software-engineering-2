package it.polito.smsbackend.dto;

import it.polito.smsbackend.common.AttendanceType;
import it.polito.smsbackend.dao.AttendanceDAO;

public class Attendance {

	private String date;
	private Integer timeslot;
	private AttendanceType type; 
	private Integer studentId;
	private Integer id;
	
	public Attendance(Integer id, String date, Integer timeslot, String type, Integer studentId) {
		super();
		this.date = date;
		this.timeslot = timeslot;
		this.studentId = studentId;
		this.id = id;
		
		switch (type) {
		case "Exit":
			this.type = AttendanceType.Exit;
			break;
		case "Enter":
			this.type = AttendanceType.Enter;
			break;
		case "Absence":
			this.type = AttendanceType.Absence;
			break ;

		default:
			throw new IllegalArgumentException("Unexpected value: " + type);
		}
		
	}
	
	public Attendance() {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Attendance(String date, Integer timeslot, AttendanceType type, Integer studentId) {
		super();
		this.date = date;
		this.timeslot = timeslot;
		this.studentId = studentId;
		this.type = type;
	}
	
	public Integer getTimeslot() {
		return timeslot;
		
	}
	public void setTimeslot(Integer timeslot) {
		this.timeslot = timeslot;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public AttendanceType getType() {
		return type;
	}
	public void setType(AttendanceType type) {
		this.type = type;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public AttendanceDAO map() {
		AttendanceDAO  dao = new AttendanceDAO();
		dao.setDate(date);
		dao.setTimeslot(timeslot);
		dao.setType(type.toString());
		dao.setStudentRef(studentId);
		return dao;
	}
	    	           
	    	          
}
