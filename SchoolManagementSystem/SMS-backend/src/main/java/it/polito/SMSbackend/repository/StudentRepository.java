package it.polito.smsbackend.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import it.polito.smsbackend.dao.StudentDAO;

public interface StudentRepository extends CrudRepository<StudentDAO, Integer> {
	
	@Query(value="SELECT * FROM STUDENT s, PARENT_STUDENT ps, PARENT p WHERE s.id = ps.student_ref AND ps.parent_ref = p.id AND p.user_ref = :id", nativeQuery = true)
	List<StudentDAO> getStudentsByUserId(@Param("id") Integer id);
	
	@Query(value="SELECT * FROM STUDENT", nativeQuery = true)
	List<StudentDAO> getAllStudents();
	
	@Query(value="SELECT * FROM STUDENT s WHERE (:cId IS NULL AND s.class_ref IS NULL) OR (:cId IS NOT NULL AND s.class_ref=:cId)", nativeQuery = true)
	List<StudentDAO> getStudentsByClassId(@Param("cId") Integer cId);
	
	@Query(value="SELECT * FROM STUDENT s WHERE s.fiscal_code = :fiscalCode", nativeQuery = true)
	StudentDAO getStudentByFiscalCode(@Param("fiscalCode") String fiscalCode);

	@Query(value="SELECT COUNT(*) FROM STUDENT p WHERE p.fiscal_code = :fiscalCode", nativeQuery = true)
	Integer checkStudentByFiscalId(String fiscalCode);
	
	@Query(value="SELECT COUNT(*) FROM STUDENT s WHERE s.id = :id", nativeQuery = true)
	Integer checkStudentById(Integer id);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE STUDENT SET CLASS_REF=:cId WHERE ID=:sId", nativeQuery=true)
	void updateStudentClass(@Param("cId") Integer cId, @Param("sId") Integer sId);
	
	@Modifying
	@Transactional
	@Query(value="UPDATE STUDENT SET CLASS_REF=:cId WHERE fiscal_code =:fiscal_code", nativeQuery=true)
	void updateStudentClass(@Param("cId") Integer cId, @Param("fiscal_code") String fiscalCode);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO STUDENT (id, birthdate, class_ref, entry_level, fiscal_code, gender, name, surname) "
			+ "VALUES (student_seq.nextval, :birthdate, :class_ref, :entry_level, :fiscal_code, :gender, :name, :surname)", nativeQuery=true)
	void insertStudent(@Param("birthdate") String birthdate, @Param("class_ref") Integer classRef, @Param("entry_level") Integer entryLevel,
			@Param("fiscal_code") String fiscalCode, @Param("gender") String gender, @Param("name") String name, @Param("surname") String surname);
	
}
