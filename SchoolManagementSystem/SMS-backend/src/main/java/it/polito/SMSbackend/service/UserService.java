package it.polito.smsbackend.service;

import java.sql.Blob;
import java.util.List;

import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.SupportMaterial;
import it.polito.smsbackend.dto.TimeTableRow;

public interface UserService {

	Blob downloadAttachment(Integer attachmentId);
	
	List<GeneralCommunication> getGeneralCommunications();
	
	Blob downloadSupportMaterial(Integer supportMaterialId);
	
	List<SupportMaterial> getSMaterialByClassIdAndSubjectId(Integer classId, Integer subjectId);

	List<TimeTableRow> getTimetableByClassId(Integer classId);

	List<TimeTableRow> getTimetableByTeacherId(Integer teacherId);
}
