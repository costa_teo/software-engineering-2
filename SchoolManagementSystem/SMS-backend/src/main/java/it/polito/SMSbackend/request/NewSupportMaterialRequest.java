package it.polito.smsbackend.request;

import org.springframework.web.multipart.MultipartFile;

public class NewSupportMaterialRequest {
	private String name;
	private MultipartFile file;
	private Integer subject_ref;
	private Integer class_ref;
	
	
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSubject_ref() {
		return subject_ref;
	}
	public void setSubject_ref(Integer subjectRef) {
		this.subject_ref = subjectRef;
	}
	public Integer getClass_ref() {
		return class_ref;
	}
	public void setClass_ref(Integer classRef) {
		this.class_ref = classRef;
	}
}
