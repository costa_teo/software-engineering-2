package it.polito.smsbackend.dao;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import it.polito.smsbackend.dto.Grade;

@Entity
@Table(name="GRADE")
public class GradeDAO {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "grade_generator")
	@SequenceGenerator(name="grade_generator", sequenceName = "grade_seq", allocationSize=1)
	private Integer id;
	@Column(nullable = false, name = "teacher_ref")
	private Integer teacherRef;
	@Column(nullable = false, name = "student_ref")
	private Integer studentRef;
	@Column(nullable = false, name = "subject_ref")
	private Integer subjectRef;
	private String description;
	@Column(nullable = false)
	private BigDecimal value;
	@Column(nullable = false)
	private String date;
	@Column(nullable = false)
	private Integer timeSlot;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTeacherRef() {
		return teacherRef;
	}

	public void setTeacherRef(Integer teacherRef) {
		this.teacherRef = teacherRef;
	}

	public Integer getStudentRef() {
		return studentRef;
	}

	public void setStudentRef(Integer studentRef) {
		this.studentRef = studentRef;
	}

	public Integer getSubjectRef() {
		return subjectRef;
	}

	public void setSubjectRef(Integer subjectRef) {
		this.subjectRef = subjectRef;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(Integer timeSlot) {
		this.timeSlot = timeSlot;
	}

	public Grade map() {
		Grade g = new Grade();
		g.setId(id);
		g.setDescription(description);
		g.setValue(value);
		g.setDate(date);
		g.setTimeslot(timeSlot);
		return g;
	}

}
