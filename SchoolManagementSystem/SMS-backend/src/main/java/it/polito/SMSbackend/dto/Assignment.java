package it.polito.smsbackend.dto;

public class Assignment {

	private Integer id;
	private String date;
	private String description;
	private String teacherName;
	private String subjectName;
	private String className;
	private Boolean hasAttachment;
	
	
	public Assignment(Integer id, String date, String description, String teacherName, String subjectName, String className,
			Boolean hasAttachment) {
		this.id = id;
		this.date = date;
		this.description = description;
		this.teacherName = teacherName;
		this.subjectName = subjectName;
		this.className=className;
		this.hasAttachment = hasAttachment;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Boolean getHasAttachment() {
		return hasAttachment;
	}

	public void setHasAttachment(Boolean hasAttachment) {
		this.hasAttachment = hasAttachment;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	
}
