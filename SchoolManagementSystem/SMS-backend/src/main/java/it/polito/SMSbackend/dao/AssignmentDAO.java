package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ASSIGNMENT")
public class AssignmentDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "assignment_generator")
	@SequenceGenerator(name="assignment_generator", sequenceName = "assignment_seq", allocationSize=1)
	private Integer id;
	private String date;
	private String description;
	@Column(nullable = false, name = "teach_sub_class_ref")
	private Integer teachSubClassRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getTeachSubClassRef() {
		return teachSubClassRef;
	}

	public void setTeachSubClassRef(Integer teachSubClassRef) {
		this.teachSubClassRef = teachSubClassRef;
	}
	
}
