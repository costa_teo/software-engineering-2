package it.polito.smsbackend.repository;

import org.springframework.data.repository.CrudRepository;
import it.polito.smsbackend.dao.GeneralCommunicationDAO;

public interface GeneralCommunicationRepository extends CrudRepository<GeneralCommunicationDAO, Integer> {

}
