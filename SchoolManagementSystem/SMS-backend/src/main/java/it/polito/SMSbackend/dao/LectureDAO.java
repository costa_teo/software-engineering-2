package it.polito.smsbackend.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import it.polito.smsbackend.dto.Lecture;

@Entity
@Table(name="LECTURE")
public class LectureDAO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lecture_generator")
	@SequenceGenerator(name="lecture_generator", sequenceName = "lecture_seq", allocationSize=1)
	private Integer id;
	private String date;
	private Integer timeSlot;
	private String description;
	@Column(nullable = false, name = "teach_sub_class_ref")
	private Integer teachSubClassRef;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(Integer timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getTeachSubClassRef() {
		return teachSubClassRef;
	}

	public void setTeachSubClassRef(Integer teachSubClassRef) {
		this.teachSubClassRef = teachSubClassRef;
	}

	public Lecture map() {
		Lecture l = new Lecture();
		l.setId(id);
		l.setDate(date);
		l.setDescription(description);
		l.setTimeSlot(timeSlot);
		return l;
	}

}
