package it.polito.smsbackend.request;

import java.util.List;

import it.polito.smsbackend.dto.TimeTableRow;

public class TimeTableRequest {
	
	private List<TimeTableRow> timetable;
	private Integer classId;
	
	public List<TimeTableRow> getTimetable() {
		return timetable;
	}
	
	public void setTimetable(List<TimeTableRow> timetable) {
		this.timetable = timetable;
	}
	
	public Integer getClassId() {
		return classId;
	}
	
	public void setClassId(Integer classId) {
		this.classId = classId;
	}

}
