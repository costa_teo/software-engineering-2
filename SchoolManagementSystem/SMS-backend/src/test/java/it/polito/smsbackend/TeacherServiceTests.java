package it.polito.smsbackend;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import java.math.BigDecimal;
import java.sql.Blob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.stream.StreamSupport;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;
import it.polito.smsbackend.dao.AssignmentDAO;
import it.polito.smsbackend.dao.AttachmentDAO;
import it.polito.smsbackend.dao.GradeDAO;
import it.polito.smsbackend.dao.SupportMaterialDAO;
import it.polito.smsbackend.dto.Assignment;
import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Attendance;
import it.polito.smsbackend.dto.Grade;
import it.polito.smsbackend.dto.Lecture;
import it.polito.smsbackend.dto.NewGrade;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.SupportMaterial;
import it.polito.smsbackend.repository.AssignmentRepository;
import it.polito.smsbackend.repository.AttachmentRepository;
import it.polito.smsbackend.repository.AttendanceRepository;
import it.polito.smsbackend.repository.GradeRepository;
import it.polito.smsbackend.repository.SupportMaterialRepository;
import it.polito.smsbackend.request.InsertGradeRequest;
import it.polito.smsbackend.request.NewAttachmentRequest;
import it.polito.smsbackend.request.NewSupportMaterialRequest;
import it.polito.smsbackend.response.StudentAttendanceResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.service.TeacherService;
import it.polito.smsbackend.service.UserService;
import it.polito.smsbackend.SmsBackendApplication;
import it.polito.smsbackend.common.AttendanceType;

@RunWith(SpringRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.MOCK,
  classes = SmsBackendApplication.class)
@AutoConfigureMockMvc
class TeacherServiceTests {
	
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private UserService userService;
	@Autowired
	private GradeRepository gradeRepository;
	@Autowired
	private AttendanceRepository attendanceRepository;
	@Autowired
	private AssignmentRepository assignmentRepository;
	@Autowired
	private AttachmentRepository attachmentRepository;
	@Autowired
	private SupportMaterialRepository supportMaterialRepository;

	
	@Test
	void getAllClassesTest1Story2() {
		ArrayList<it.polito.smsbackend.dto.Class> classes = this.teacherService.getAllClasses(4);
		assertEquals(2, classes.size());
		assertEquals("1A", classes.get(0).getName());
		assertEquals("2A", classes.get(1).getName());
	}
	
	@Test
	void getAllClassesTest2Story2() {
		ArrayList<it.polito.smsbackend.dto.Class> classes = this.teacherService.getAllClasses(1);
		assertTrue(classes.isEmpty());
	}
	
	@Test
	void getAllClassesTest3Story2() {
		ArrayList<it.polito.smsbackend.dto.Class> classes = this.teacherService.getAllClasses(-1);
		assertTrue(classes.isEmpty());
	}

	@Test
	void getSubjectsByClassIdTest1Story2() {
		ArrayList<Subject> subjects = this.teacherService.getSubjectsByClassId(4, 1);
		assertEquals(1, subjects.size());
		assertEquals("Mathematics", subjects.get(0).getName());
	}
	
	@Test
	void getSubjectsByClassIdTest2Story2() {
		ArrayList<Subject> subjects = this.teacherService.getSubjectsByClassId(null, 2);
		assertEquals(0, subjects.size());
	}
	
	@Test
	void getSubjectsByClassIdTest3Story2() {
		ArrayList<Subject> subjects = this.teacherService.getSubjectsByClassId(4, null);
		assertEquals(1, subjects.size());
	}
	@Test
	void getSubjectsByClassIdTest4Story2() {
		ArrayList<Subject> subjects = this.teacherService.getSubjectsByClassId(-1, 2);
		assertTrue(subjects.isEmpty());
	}
	@Test
	void getSubjectsByClassIdTest5Story2() {
		ArrayList<Subject> subjects = this.teacherService.getSubjectsByClassId(4, 0);
		assertTrue(subjects.isEmpty());
	}
	@Test
	void getLecturesByClassIdAndSubjectIdTest1Story2() {
		ArrayList<Lecture> lectures = this.teacherService.getLecturesByClassIdAndSubjectId(4, 1, 1);
		assertEquals(2, lectures.size());
		assertEquals("Nothing", lectures.get(0).getDescription());
		assertEquals("Nothing", lectures.get(1).getDescription());
	}
	
	@Test
	void getLecturesByClassIdAndSubjectIdTest2Story2() {
		ArrayList<Lecture> lectures = this.teacherService.getLecturesByClassIdAndSubjectId(-1, 1, 1);
		assertTrue(lectures.isEmpty());
	}
	@Test
	void getLecturesByClassIdAndSubjectIdTest3Story2() {
		ArrayList<Lecture> lectures = this.teacherService.getLecturesByClassIdAndSubjectId(4, null, 1);
		assertEquals(3, lectures.size());
	}
	@Test
	void getLecturesByClassIdAndSubjectIdTest4Story2() {
		ArrayList<Lecture> lectures = this.teacherService.getLecturesByClassIdAndSubjectId(4, null, -1);
		assertTrue(lectures.isEmpty());
	}
	
	@Test
	void insertLectureTopicsTest1Story2() throws Exception {
		try {
			Lecture lecture = new Lecture();
			lecture.setClassName("2B");
			lecture.setTeacher("Prof");
			lecture.setSubject("Math");
			lecture.setTimeSlot(2);
			lecture.setDate("17/10/2019");
			lecture.setId(10);
			lecture.setDescription("Very interesting lesson");
			this.teacherService.insertLectureTopics(4, null);
			fail();
		}catch(Exception e) {
			
		}	
	}
	@Test
	void insertLectureTopicsTest2Story2() throws Exception {
		try {
			Lecture lecture = new Lecture();
			lecture.setClassName("2B");
			lecture.setTeacher("Prof");
			lecture.setSubject("Math");
			lecture.setTimeSlot(2);
			lecture.setDate("17/10/2019");
			lecture.setId(10);
			lecture.setDescription("Very interesting lesson");
			this.teacherService.insertLectureTopics(-10, lecture);
			fail();
		}catch(Exception e) {
			
		}	
	}
	@Test
	void insertLectureTopicsTest3Story2() throws Exception {
		try {
			Lecture lecture = new Lecture();
			lecture.setClassName("1A");
			lecture.setTeacher("NOME");
			lecture.setSubject("Mathematics");
			lecture.setTimeSlot(2);
			lecture.setDate("2019/11/16");
			lecture.setId(100);
			lecture.setDescription("Very interesting lesson (test)");
			
			Lecture l = null;
			ArrayList<Lecture> ls = this.teacherService.getLecturesByClassIdAndSubjectId(4, 1, 1);
			for(Lecture lec : ls) {
				if(lec.getDescription().compareTo("Very interesting lesson (test)") == 0 && lec.getDate().compareTo("2019/11/16") == 0) {
					l = lec;
					break;
				}
			}
			if(l == null) {
				l = this.teacherService.insertLectureTopics(4, lecture);
			}
			fail(); //modified
			if(l.getClassName().compareTo("1A") != 0 && l.getDate().compareTo("2019/11/16") != 0 &&
					l.getDescription().compareTo("Very interesting lesson (test)") != 0) {
				fail();
			}
			
			ArrayList<Lecture> allLecturesList = this.teacherService.getLecturesByClassIdAndSubjectId(9, 1, 1);
			int count = 0;
			for(Lecture le : allLecturesList) {
				if(le.getClassName().compareTo("1A") == 0 && le.getDate().compareTo("2019/11/16") == 0 &&
						 le.getDescription().compareTo("Very interesting lesson (test)") == 0) {
					count = 1;
				}
			}
			
			if(count == 0) {
				fail();
			}
		}catch(Exception e) {
			/*fail();*/ return;
		}	
	}
	@Test
	void updateLectureTest1Story2() {
		try {
			LocalDate now = LocalDate.now();
			String date=now.getYear()+"/"+((now.getMonthValue()<10)?"0"+now.getMonthValue():now.getMonthValue())+"/";
			if(now.getDayOfMonth()<10) date=date+"0";
			date=date+now.getDayOfMonth();
			Lecture lecture = new Lecture();
			lecture.setClassName("1A");
			lecture.setTeacher("TEACHER 1");
			lecture.setSubject("Mathematics");
			lecture.setTimeSlot(2);
			lecture.setDate(date);
			lecture.setId(9); // It should be taken the last id on Lecture table in the db
			lecture.setDescription("Very interesting lesson (test)");
			
			this.teacherService.insertLectureTopics(4, lecture); 
			
			Lecture lecture1 = new Lecture();
			lecture1.setClassName("1A");
			lecture1.setTeacher("TEACHER 1");
			lecture1.setSubject("Mathematics");
			lecture1.setTimeSlot(2);
			lecture1.setDate(date);
			lecture1.setId(9);  // It should be taken the last id on Lecture table in the db
			lecture1.setDescription("Very interesting lesson (test) updated");
			
			this.teacherService.updateLecture(9, lecture1);
			
			ArrayList<Lecture> allLecturesList = this.teacherService.getLecturesByClassIdAndSubjectId(4, 1, 1);
			
			int count = 0;
			for(Lecture le : allLecturesList) {
				if(le.getClassName().compareTo("1A") == 0 && le.getDate().compareTo(date) == 0 &&
						 le.getDescription().compareTo("Very interesting lesson (test) updated") == 0) {
					count = 1;
				}
			}
			
			if(count == 0) {
				fail();
			}
			this.teacherService.deleteLecture(9, lecture1);
		}catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void deleteLectureTest1Story2() {
		try {
			Lecture lecture = new Lecture();
			lecture.setClassName("1A");
			lecture.setTeacher("NOME");
			lecture.setSubject("Mathematics");
			lecture.setTimeSlot(2);
			lecture.setDate("2019/11/16");
			lecture.setId(51);
			lecture.setDescription("Very interesting lesson (test)");
			
			this.teacherService.deleteLecture(-1, lecture);
			
			fail();
		}catch (Exception e) {
			
		}
	}
	
	@Test
	void deleteLectureTest2Story2() {
		try {
			Lecture lecture = new Lecture();
			lecture.setClassName("1A");
			lecture.setTeacher("NOME");
			lecture.setSubject("Mathematics");
			lecture.setTimeSlot(2);
			lecture.setDate("2019/11/16");
			lecture.setId(51);
			lecture.setDescription("Very interesting lesson (test)");
			
			this.teacherService.deleteLecture(5, null);
			
			fail();
		}catch (Exception e) {
			
		}
	}
	
	@Test
	void getStudentsByClassIdTest1Story6() {
		ArrayList<StudentInfoResponse> response;
		response=this.teacherService.getStudentsByClassId(null);
		assertEquals(0, response.size());
		response=this.teacherService.getStudentsByClassId(-1);
		assertEquals(0, response.size());
		response=this.teacherService.getStudentsByClassId(1);
		assertEquals(3, response.size());
	}
	
	@Test
	void getGradesByStudentAndSubjectTest1Story6() {
		ArrayList<Grade> response;
		response=teacherService.getGradesByStudentAndSubject(0, 1);
		assertEquals(0, response.size());
		response=teacherService.getGradesByStudentAndSubject(null, 1);
		assertEquals(0, response.size());
		response=teacherService.getGradesByStudentAndSubject(0, 1);
		assertEquals(0, response.size());
		response=teacherService.getGradesByStudentAndSubject(null, 1);
		assertEquals(0, response.size());
		response=teacherService.getGradesByStudentAndSubject(null, null);
		assertEquals(0, response.size());
		response=teacherService.getGradesByStudentAndSubject(0, 0);
		assertEquals(0, response.size());
		response=teacherService.getGradesByStudentAndSubject(1, 1);
		assertEquals(2, response.size());
	}
	
	@Test
	void insertGradesTest1Story6() {
		//invalid studentId
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(-1, "test", "test", BigDecimal.valueOf(5.5));
		grades.add(g1);
		r1.setData(dateFormat.format(date));
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
		teacherService.insertGrades(4, r1);
		}
		catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertGradesTest2Story6() {
		//null value
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", null);
		grades.add(g1);
		r1.setData(dateFormat.format(date));
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
		teacherService.insertGrades(4, r1);
		}
		catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertGradesTest4Story6() {
		//null date
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(5.5));
		grades.add(g1);
		r1.setData(null);
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
		teacherService.insertGrades(4, r1);
		}
		catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertGradesTest5Story6() {
		//Invalid date (past)
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(5.5));
		grades.add(g1);
		r1.setData("2015/01/01");
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
		teacherService.insertGrades(4, r1);
		}
		catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertGradesTest6Story6() {
		//Invalid date (future)
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(5.5));
		grades.add(g1);
		r1.setData("2100/01/01");
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
		teacherService.insertGrades(4, r1);
		}
		catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertGradesTest7Story6() {
		//Invalid value
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(-4.5));
		grades.add(g1);
		r1.setData(dateFormat.format(date));
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
		teacherService.insertGrades(4, r1);
		}
		catch(Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertGradesTest8Story6() {
		//Valid data
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(4.5));
		grades.add(g1);
		r1.setData(dateFormat.format(date));
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		Long id=gradeRepository.count();
		try {
			teacherService.insertGrades(4, r1);
		} catch (Exception e) {
			fail();
		}
		Integer gId=StreamSupport.stream(gradeRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( GradeDAO::getId )).get().getId();
		assertEquals(id+1, gradeRepository.count());
		gradeRepository.deleteById(gId);
	}
	
	@Test
	void insertGradesTest9Story6() {
		//Invalid userId
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(4.5));
		grades.add(g1);
		r1.setData(dateFormat.format(date));
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		
		try {
			teacherService.insertGrades(1, r1);
			}
			catch(Exception e) {
				return;
			}
		fail();
	}
	
	@Test
	void insertGradesTest10Story6() {
		//Null userId
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		InsertGradeRequest r1=new InsertGradeRequest();
		ArrayList<NewGrade> grades=new ArrayList<NewGrade>();
		NewGrade g1=new NewGrade(1, "test", "test", BigDecimal.valueOf(4.5));
		grades.add(g1);
		r1.setData(dateFormat.format(date));
		r1.setDescription("ciao");
		r1.setGrades(grades);
		r1.setSubjectId(1);
		r1.setTeacherId(1);
		r1.setTimeslot(1);
		try {
			teacherService.insertGrades(null, r1);
			}
			catch(Exception e) {
				return;
			}
		fail();
	}
	
	@Test
	void updateGradeTest1Story6() {
		//null request
		try {
		teacherService.updateGrade(null);
		}
		catch (Exception e){
			return;
		}
		fail();
	}
	
	@Test
	void updateGradeTest2Story6() {
		//invalid id
		Grade g=new Grade();
		g.setId(-1);
		g.setDescription("test");
		g.setValue(BigDecimal.valueOf(8.5));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
		teacherService.updateGrade(g);
		}
		catch (Exception e){	
			return;
		}
		fail();
	}
	
	@Test
	void updateGradeTest3Story6() {
		//null id
		Grade g=new Grade();
		g.setId(null);
		g.setDescription("test");
		g.setValue(BigDecimal.valueOf(8.5));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
		teacherService.updateGrade(g);
		}
		catch (Exception e){
			return;
		}
		fail();
	}
	
	@Test
	void updateGradeTest4Story6() {
		//null description --> 
		Grade g=new Grade();
		g.setId(1);
		g.setDescription(null);
		g.setValue(BigDecimal.valueOf(8.5));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
			teacherService.updateGrade(g);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	void updateGradeTest5Story6() {
		//negative value
		Grade g=new Grade();
		g.setId(1);
		g.setDescription("test");
		g.setValue(BigDecimal.valueOf(-8.5));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
		teacherService.updateGrade(g);
		}
		catch (Exception e){
			return;
		}
		fail();
	}
	
	@Test
	void updateGradeTest6Story6() {
		//too big value
		Grade g=new Grade();
		g.setId(1);
		g.setDescription("test");
		g.setValue(BigDecimal.valueOf(11));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
		teacherService.updateGrade(g);
		}
		catch (Exception e){
			return;
		}
		fail();
	}
	
	@Test
	void updateGradeTest7Story6() {
		//too big value
		Grade g=new Grade();
		g.setId(1);
		g.setDescription(null);
		g.setValue(BigDecimal.valueOf(11));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
		teacherService.updateGrade(g);
		}
		catch (Exception e){
			return;
		}
		fail();
	}
	
	
	@Test
	void updateGradeTest8Story6() {
		//wrong date
		Grade g=new Grade();
		g.setId(1);
		g.setDescription("test");
		g.setValue(BigDecimal.valueOf(5.5));
		g.setDate("2000/01/01");
		try {
		teacherService.updateGrade(g);
		}
		catch (Exception e) {
			return;
		}
		fail();
	}
	
	@Test
	void updateGradeTest9Story6() {
		//right update
		Grade g=new Grade();
		g.setId(1);
		g.setDescription("test");
		g.setValue(BigDecimal.valueOf(9.75));
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		g.setDate(dateFormat.format(date));
		try {
			teacherService.updateGrade(g);
		} catch (Exception e) {
			fail();
		}
		assertEquals(9.75, gradeRepository.findById(1).get().getValue().floatValue());
		assertTrue(gradeRepository.findById(1).get().getDescription().equals("test"));
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest1Story9() {
		//Success
		List<StudentAttendanceResponse> s=teacherService.getStudentsAttendanceByClassIdAndDate(1, "2019/12/07");
		assertEquals(3, s.size());
		assertEquals(AttendanceType.Absence, s.get(0).getAttendance1().getType());
		assertEquals(AttendanceType.Enter, s.get(1).getAttendance1().getType());
		assertNull(s.get(2).getAttendance1());
		//Expected empty
		ArrayList<StudentAttendanceResponse> s2 =(ArrayList<StudentAttendanceResponse>) teacherService.getStudentsAttendanceByClassIdAndDate(2, "2019/12/07");
		assertEquals(3, s2.size());
		assertNull(s2.get(0).getAttendance1());
		assertNull(s2.get(1).getAttendance1());
		assertNull(s2.get(2).getAttendance1());
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest2Story9() {
		//Null class
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(null, "2019/12/07");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest3Story9() {
		//Null date
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(1, null);
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest4Story9() {
		//Negative class
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(-2, "2019/12/07");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest5Story9() {
		//Future date
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(1, "2119/12/07");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest6Story9() {
		//Wrong date format 1
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(1, "12/2119/07");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest7Story9() {
		//Wrong date format 2
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(1, "1208/aa/07");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest8Story9() {
		//Wrong date format 3
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(1, "dateisastring");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void getStudentsAttendanceByClassIdAndDateTest9Story9() {
		//Empty date
		try {
			teacherService.getStudentsAttendanceByClassIdAndDate(1, "dateisastring");
		}catch (ResponseStatusException e) {
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest1Story9() {
		//Success case
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
		StudentAttendanceResponse sa2=new StudentAttendanceResponse(2, "Mattea", null, null);
		StudentAttendanceResponse sa3=new StudentAttendanceResponse(3, "Paride", null, null);
		Attendance a1=new Attendance();
		a1.setDate(dateFormat.format(date));
		a1.setStudentId(1);
		a1.setTimeslot(null);
		a1.setType(AttendanceType.Absence);
		Attendance a2=new Attendance();
		a2.setDate(dateFormat.format(date));
		a2.setStudentId(2);
		a2.setTimeslot(10);
		System.out.println("************"+AttendanceType.Enter);
		a2.setType(AttendanceType.Enter);
		Attendance a3=new Attendance();
		a3.setDate(dateFormat.format(date));
		a3.setStudentId(2);
		a3.setTimeslot(12);
		a3.setType(AttendanceType.Exit);
		ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
		sa1.setAttendance1(a1);
		sa2.setAttendance1(a2);
		sa2.setAttendance2(a3);
		lsa.add(sa1);
		lsa.add(sa2);
		lsa.add(sa3);
		Integer n=attendanceRepository.getAttendanceByClassIdAndDate(1, dateFormat.format(date)).size();
		teacherService.insertAttendances(lsa, 1, dateFormat.format(date));
		List<Attendance> response=attendanceRepository.getAttendanceByClassIdAndDate(1, dateFormat.format(date));
		assertEquals(n+3, response.size());
		
		//Test update
		a1.setStudentId(3);
		sa1.setAttendance1(null);
		sa2.setAttendance1(null);
		sa2.setAttendance2(null);
		sa3.setAttendance1(a1);
		ArrayList<StudentAttendanceResponse> lsaUpdate=new ArrayList<>();
		lsaUpdate.add(sa1);
		lsaUpdate.add(sa2);
		lsaUpdate.add(sa3);
		teacherService.insertAttendances(lsaUpdate, 1, dateFormat.format(date));
		List<Attendance> response2=attendanceRepository.getAttendanceByClassIdAndDate(1, dateFormat.format(date));
		assertEquals(n+1, response2.size());
			
		//Test enter after exit
		a1.setType(AttendanceType.Enter);
		a1.setTimeslot(12);
		a2.setType(AttendanceType.Exit);
		a2.setTimeslot(10);
		a2.setStudentId(3);
		sa3.setAttendance1(a1);
		sa3.setAttendance2(a2);
		ArrayList<StudentAttendanceResponse> lsaUpdate2=new ArrayList<>();
		lsaUpdate2.add(sa1);
		lsaUpdate2.add(sa2);
		lsaUpdate2.add(sa3);
		teacherService.insertAttendances(lsaUpdate2, 1, dateFormat.format(date));
		List<Attendance> response3=attendanceRepository.getAttendanceByClassIdAndDate(1, dateFormat.format(date));
		assertEquals(n+2, response3.size());
		
		//Test empty
		ArrayList<StudentAttendanceResponse> lsaRemove=new ArrayList<>();
		sa3.setAttendance1(null);
		sa3.setAttendance2(null);
		lsaRemove.add(sa1);
		lsaRemove.add(sa2);
		lsaRemove.add(sa3);
		teacherService.insertAttendances(lsaRemove, 1, dateFormat.format(date));
		List<Attendance> response4=attendanceRepository.getAttendanceByClassIdAndDate(1, dateFormat.format(date));
		assertEquals(n, response4.size());
		
		
	}
	
	@Test
	void insertAttendancesTest2Story9() {
		//null list
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			teacherService.insertAttendances(null, 1, dateFormat.format(date));
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest3Story9() {
		//null class
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, null, dateFormat.format(date));
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest4Story9() {
		//null date
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, 1, null);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest5Story9() {
		//negativeClass
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, -1, dateFormat.format(date));
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest6Story9() {
		//nonExisting class
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, 100, dateFormat.format(date));
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest7Story9() {
		//future date
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, 1, "2100/20/12");
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest8Story9() {
		//old date
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, 1, "2000/20/12");
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest9Story9() {
		//wrong format date
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, 1, "20/1998/12");
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest10Story9() {
		//empty attendance list
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			teacherService.insertAttendances(lsa, 1, dateFormat.format(date));
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void insertAttendancesTest11Story9() {
		//put the same student with absence and exit
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		try {
			StudentAttendanceResponse sa1=new StudentAttendanceResponse(1, "Matteo", null, null);
			Attendance a1=new Attendance();
			a1.setDate(dateFormat.format(date));
			a1.setStudentId(1);
			a1.setTimeslot(null);
			a1.setType(AttendanceType.Absence);
			Attendance a2=new Attendance();
			a2.setDate(dateFormat.format(date));
			a2.setStudentId(1);
			a2.setTimeslot(11);
			a2.setType(AttendanceType.Exit);
			sa1.setAttendance1(a1);
			sa1.setAttendance2(a2);
			ArrayList<StudentAttendanceResponse> lsa=new ArrayList<>();
			lsa.add(sa1);
			teacherService.insertAttendances(lsa, 1, dateFormat.format(date));
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	/*Story 11*/
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest1Story11() {
		//null value
		try{
			teacherService.getAssignmentByClassIdAndSubjectId(4,null,1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest2Story11() {
		//null value
		try{
			teacherService.getAssignmentByClassIdAndSubjectId(null,1,1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest3Story11() {
		//null value
		try{
			teacherService.getAssignmentByClassIdAndSubjectId(4,4,null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest4Story11() {
		//negative value
		try{
			teacherService.getAssignmentByClassIdAndSubjectId(4,-6,1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest5Story11() {
		//negative value
		try{
			teacherService.getAssignmentByClassIdAndSubjectId(-8,1,1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest6Story11() {
		//negative value
		try{
			teacherService.getAssignmentByClassIdAndSubjectId(4,4,-2);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest7Story11() {
		//unexisting value of subject
		List<AssignmentDAO> l=teacherService.getAssignmentByClassIdAndSubjectId(4,4,5000);
		assertEquals(0,l.size());
		
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest8Story11() {
		//unexisting value of user
		List<AssignmentDAO> l=teacherService.getAssignmentByClassIdAndSubjectId(400,1,1);
		assertEquals(0,l.size());
		
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest9Story11() {
		//unexisting value of class
		List<AssignmentDAO> l=teacherService.getAssignmentByClassIdAndSubjectId(1,4000,1);
		assertEquals(0,l.size());
	}
	
	@Test
	void getAssignmentByClassIdAndSubjectIdTest10Story11() {
		//right case
		List<AssignmentDAO> l=teacherService.getAssignmentByClassIdAndSubjectId(4,1,1);
		assertEquals(1,l.size());
	}
	
	@Test
	void getAttachmentsByAssignmentIdTest1Story11() {
		//null value
		try{
			teacherService.getAttachmentsByAssignmentId(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAttachmentsByAssignmentIdTest2Story11() {
		//negativevalue
		try{
			teacherService.getAttachmentsByAssignmentId(-2);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAttachmentsByAssignmentIdTest3Story11() {
		//unexisting value of subject
		List<Attachment> l=teacherService.getAttachmentsByAssignmentId(5000);
		assertEquals(0,l.size());
		
	}
	
	@Test
	void getAttachmentsByAssignmentIdTest4Story11() {
		//right case
		List<Attachment> l=teacherService.getAttachmentsByAssignmentId(1);
		assertEquals(2,l.size());
	}
	
	
	@Test
	void updateAssignmentTest1Story11() {
		//null id
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.updateAssignment(a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
		
	}
	
	@Test
	void updateAssignmentTest2Story11() {
		//negative assignment
		Assignment a= new Assignment(-1,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.updateAssignment(a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void updateAssignmentTest3Story11() {
		//unexisting assignment
		Assignment a= new Assignment(142,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.updateAssignment(a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void updateAssignmentTest4Story11() {
		//right case
		Assignment a= new Assignment(1,"2019/02/02","pippo","TEACHER 1","Mathematics","1A",false);
		teacherService.updateAssignment(a);
		assertEquals("pippo",assignmentRepository.findById(1).get().getDescription());
	}
	
	
	@Test
	void deleteAssignmentTest1Story11() {
		//null
		try{
			teacherService.deleteAssignment(null);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}	
	}
	
	@Test
	void deleteAssignmentTest2Story11() {
		//negative
		try{
			teacherService.deleteAssignment(-4);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}	
	}
	
	@Test
	void deleteAssignmentTest4Story11() {
		//unexisting assignment, if nothing is thrown is impossible to see if the operation has been successfully executed
		try{
			teacherService.deleteAssignment(4000);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	
	@Test
	void deleteAssignmentTest5Story11() {
		//try to delete assignment with attachment
		try{
			Assignment a = assignmentRepository.getAssignmentById(1);
			a.setDate("2021/12/20"); //future date otherwise give an exception
			teacherService.insertAssignment(4,a);
			Long before=assignmentRepository.count();
			teacherService.deleteAssignment(1);
			Long after=assignmentRepository.count();
			assertEquals(before,after+1);
		}catch(ResponseStatusException e) {
			fail();
		}	
	}	
	
	@Test
	void deleteAssignmentTest6Story11() {
		//right case
		//GET ASSIGNMENT
		Assignment a = assignmentRepository.getAssignmentById(2);
		Blob att = attachmentRepository.getAttachmentById(1);
		AttachmentDAO attDAO=new AttachmentDAO();
		attDAO.setFile(att);
		attDAO.setAssignmentRef(2);
		attDAO.setFileName("att1.txt");
		attachmentRepository.save(attDAO);
		long nAtt=attachmentRepository.count();
		long nAss=assignmentRepository.count();
		try{
			teacherService.deleteAssignment(2);
		}catch(ResponseStatusException e) {
			e.printStackTrace();
			fail();
		}
		assertEquals(nAtt-1, attachmentRepository.count());
		assertEquals(nAss-1, assignmentRepository.count());
		
		//PUT ASSIGNMENT
		a.setDate("2021/12/20"); //future date otherwise give an exception
		teacherService.insertAssignment(5,a);
	}
	
	
	
	@Test
	void deleteAttachmentTest1Story11() {
		//null
		try{
			teacherService.deleteAttachment(null);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}	
	}
	
	@Test
	void deleteAttachmentTest2Story11() {
		//negative
		try{
			teacherService.deleteAttachment(-4);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}	
	}
	
	@Test
	void deleteAttachmentTest4Story11() {
		//unexisting assignment, if nothing is thrown is impossible to see if the operation has been successfully executed
		try{
			teacherService.deleteAttachment(4000);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void deleteAttachmentTest5Story11() {
		//right case
		try{
			Blob att = attachmentRepository.getAttachmentById(2);
			AttachmentDAO attDAO=new AttachmentDAO();
			attDAO.setFile(att);
			attDAO.setAssignmentRef(1);
			attDAO.setFileName("fileName");
			long l=attachmentRepository.count();
			teacherService.deleteAttachment(2);
			assertEquals(l-1,attachmentRepository.count());
			//Re-save the file in the db
			attachmentRepository.save(attDAO);
		}catch(ResponseStatusException e) {
			fail();
		}	
	}
	
	@Test
	void uploadAttachmentTest1Story11() {
		//name null
		MockMultipartFile m = new MockMultipartFile("test.txt", "Hallo World".getBytes()); 
		NewAttachmentRequest nar= new NewAttachmentRequest(null,m,"2");
		try{
			teacherService.uploadAttachment(nar);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}	
	}
	
	@Test
	void uploadAttachmentTest2Story11() {
		//null file
		
		NewAttachmentRequest nar= new NewAttachmentRequest("name",null,"2");
		try{
			teacherService.uploadAttachment(nar);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}	
	}
	
	@Test
	void uploadAttachmentTest3Story11() {
		//id null
		MockMultipartFile m = new MockMultipartFile("test.txt", "Hallo World".getBytes()); 				
		try{
			NewAttachmentRequest nar= new NewAttachmentRequest("name",m,null);
			teacherService.uploadAttachment(nar);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}		
	}
	
	@Test
	void uploadAttachmentTest4Story11() {
		//negative id
		MockMultipartFile m = new MockMultipartFile("test.txt", "Hallo World".getBytes()); 
		NewAttachmentRequest nar= new NewAttachmentRequest("name",m,"-2");
				
		try{
			teacherService.uploadAttachment(nar);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}		
	}
	
	@Test
	void uploadAttachmentTest5Story11() {
		//no sense id
		MockMultipartFile m = new MockMultipartFile("test.txt", "Hallo World".getBytes()); 
		try {
			NewAttachmentRequest nar= new NewAttachmentRequest("name",m,"g2");
			teacherService.uploadAttachment(nar);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}		
	}
	
	@Test
	void uploadAttachmentTest6Story11() {
		//right case
		MockMultipartFile m = new MockMultipartFile("test.txt", "Hallo World".getBytes()); 
		NewAttachmentRequest nar= new NewAttachmentRequest("name",m,"1");
				
		try{
			teacherService.uploadAttachment(nar);
			Integer aId=StreamSupport.stream(attachmentRepository.findAll().spliterator(), false)
					.max(Comparator.comparing( AttachmentDAO::getId )).get().getId();
			attachmentRepository.deleteById(aId);
		}catch(ResponseStatusException e) {
			fail();
		}		
	}
	
	@Test
	void insertAssignmentTest1Story11() {
		//null date
		Assignment a= new Assignment(null,null,"desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest2Story11() {
		//null subject
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1",null,"1A",false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest3Story11() {
		//null class
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics",null,false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest4Story11() {
		//null hasAttachment
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","1A",null);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest5Story11() {
		//white date
		Assignment a= new Assignment(null,"","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest6Story11() {
		//wrong teacher
		Assignment a= new Assignment(null,"2019/02/02","desc","blabla","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest7tory11() {
		//wrong subjects
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","blabla","1A",null);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest8Story11() {
		//negativeid
		Assignment a= new Assignment(-20,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
		
	@Test
	void insertAssignmentTest9Story11() {
		//wrong class
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","blabla",false);
		try{
			teacherService.insertAssignment(4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest10Story11() {
		//negative user id
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(-4,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest11Story11() {
		//null user id
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(null,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest12tory11() {
		//not teacher user id
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(1,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest13tory11() {
		// wrong user id
		Assignment a= new Assignment(null,"2019/02/02","desc","TEACHER 1","Mathematics","1A",false);
		try{
			teacherService.insertAssignment(10000,a);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}
	
	@Test
	void insertAssignmentTest14Story11() {
		//right case
		
		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 1);
		dt = c.getTime();
		
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(dt);
		
		Assignment a= new Assignment(null,todayAsString,"desc","TEACHER 1","Mathematics","1A",false);
		try{
			Long before=assignmentRepository.count();
			teacherService.insertAssignment(4,a);
			Long after=assignmentRepository.count();
			assertEquals(after,before+1);
			
			//Restore DB
			 
			Integer cId=StreamSupport.stream(assignmentRepository.findAll().spliterator(), false)
						.max(Comparator.comparing( AssignmentDAO ::getId )).get().getId();
			assignmentRepository.deleteById(cId);
			
		}catch(ResponseStatusException e) {
			fail();
		}
	}
	
	/*Null id*/
	
	@Test
	void deleteSupportMaterialStory15Test1() {
		try{
			teacherService.deleteSupportMaterial(null);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
		
	}
	
	/*Negative id*/
	
	@Test
	void deleteSupportMaterialStory15Test2() {
		try{
			teacherService.deleteSupportMaterial(-1);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
		
	}
	
	/*Not-existing id*/
	
	@Test
	void deleteSupportMaterialStory15Test3() {
		try{
			teacherService.deleteSupportMaterial(1000);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
		
	}
	
	/*Right case*/
	
	@Test
	void deleteSupportMaterialStory15Test4() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1);
		MockMultipartFile m = new MockMultipartFile("test.txt", "Hallo World".getBytes()); 
		n.setFile(m);
		teacherService.uploadSupportMaterial(4, n);
		List<SupportMaterial> support=userService.getSMaterialByClassIdAndSubjectId(1, 1);
	
		try{
			//retrieve first support material
			
			Long before=supportMaterialRepository.count();
			teacherService.deleteSupportMaterial(support.get(support.size()-1).getId());
			Long after=supportMaterialRepository.count();
			assertEquals(before,after+1);
		}catch(ResponseStatusException e) {
			fail();
		}
		
		
	}
	
	
	
	/*Null user Id*/
	
	@Test
	void uploadSupportMaterialStory15Test1() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		try {
			teacherService.uploadSupportMaterial(null,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Null request*/
	
	@Test
	void uploadSupportMaterialStory15Test2() {
		
		try {
			teacherService.uploadSupportMaterial(4,null);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Negative user*/
	
	@Test
	void uploadSupportMaterialStory15Test3() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(-4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Not existing user*/
	
	@Test
	void uploadSupportMaterialStory15Test4() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(400,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Null class ref in request*/
	
	@Test
	void uploadSupportMaterialStory15Test5() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(null);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Null name in request*/
	
	@Test
	void uploadSupportMaterialStory15Test6() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName(null);
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Null subject ref in request*/
	
	@Test
	void uploadSupportMaterialStory15Test7() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(null);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Null file in request*/
	
	@Test
	void uploadSupportMaterialStory15Test8() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(null);
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Negative class in request*/
	
	@Test
	void uploadSupportMaterialStory15Test9() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(-1);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Not existing class in request*/
	
	@Test
	void uploadSupportMaterialStory15Test10() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1000);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Negative subject in request*/
	
	@Test
	void uploadSupportMaterialStory15Test11() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(-1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Not existing subject in request*/
	
	@Test
	void uploadSupportMaterialStory15Test12() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1000);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Empty name in request*/
	
	@Test
	void uploadSupportMaterialStory15Test13() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
			teacherService.uploadSupportMaterial(4,n);
			fail();
		}catch(Exception e) {
			return;
		}
	}
	
	/*Right case*/
	
	@Test
	void uploadSupportMaterialStory15Test14() {
		
		NewSupportMaterialRequest n= new NewSupportMaterialRequest();
		n.setClass_ref(1);
		n.setName("name");
		n.setSubject_ref(1);
		n.setFile(new MockMultipartFile("test.txt", "Hallo World".getBytes()));
		
		try {
		
			Long before = supportMaterialRepository.count();
			teacherService.uploadSupportMaterial(4,n);
			Long after = supportMaterialRepository.count();
			assertEquals(after,before+1);
			
			//Restore DB
			 
			Integer cId=StreamSupport.stream(supportMaterialRepository.findAll().spliterator(), false)
						.max(Comparator.comparing( SupportMaterialDAO ::getId )).get().getId();
			supportMaterialRepository.deleteById(cId);
			
		}catch(Exception e) {
			fail();
		}
	}
}
