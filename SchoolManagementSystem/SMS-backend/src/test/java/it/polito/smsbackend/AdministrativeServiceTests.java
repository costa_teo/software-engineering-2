package it.polito.smsbackend;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.StreamSupport;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dao.GeneralCommunicationDAO;
import it.polito.smsbackend.dao.ParentDAO;
import it.polito.smsbackend.dao.ParentStudentDAO;
import it.polito.smsbackend.dao.StudentDAO;
import it.polito.smsbackend.dao.TeacherDAO;
import it.polito.smsbackend.dao.UserDAO;
import it.polito.smsbackend.dto.Class;
import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.Parent;
import it.polito.smsbackend.dto.Student;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.dto.User;
import it.polito.smsbackend.repository.ParentRepository;
import it.polito.smsbackend.repository.ParentStudentRepository;
import it.polito.smsbackend.repository.StudentRepository;
import it.polito.smsbackend.repository.TeacherRepository;
import it.polito.smsbackend.repository.TimeTableRepository;
import it.polito.smsbackend.repository.UserRepository;
import it.polito.smsbackend.request.AddParentStudentRequest;
import it.polito.smsbackend.request.CreateClassesStudentRequest;
import it.polito.smsbackend.request.TimeTableRequest;
import it.polito.smsbackend.repository.ClassRepository;
import it.polito.smsbackend.repository.GeneralCommunicationRepository;
import it.polito.smsbackend.request.UpdateClassRequest;
import it.polito.smsbackend.request.UpdateTeachingInClassRequest;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.response.TeacherInfoResponse;
import it.polito.smsbackend.service.AdministrativeService;
import it.polito.smsbackend.SmsBackendApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = SmsBackendApplication.class)
@AutoConfigureMockMvc

public class AdministrativeServiceTests {

	@Autowired
	private AdministrativeService administrativeService;
	@Autowired
	private ParentRepository parentRepository;
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ParentStudentRepository parentStudentRepository;
	@Autowired
	private GeneralCommunicationRepository generalCommunicationRepository;
	@Autowired
	private ClassRepository classRepository;
	@Autowired
	private TeacherRepository teacherRepository;
	@Autowired
	private TimeTableRepository timetableRepository;


	/*STORY 5*/

	@Test
	void getAllClassesTest1Story5() {
		List<Class> classes = this.administrativeService.getAllClasses();
		assertEquals(10, classes.size());

		for(Class c : classes) {
			if(c.getId()==1) assertEquals("1A", c.getName());
			if(c.getId()==2) assertEquals("2A", c.getName());
			if(c.getId()==3) assertEquals("3A", c.getName());
			if(c.getId()==4) assertEquals("4A", c.getName());
			if(c.getId()==5) assertEquals("5A", c.getName());
			if(c.getId()==6) assertEquals("1B", c.getName());
			if(c.getId()==7) assertEquals("2B", c.getName());
			if(c.getId()==8) assertEquals("3B", c.getName());
			if(c.getId()==9) assertEquals("4B", c.getName());
			if(c.getId()==10) assertEquals("5B", c.getName());
		}
	}

	@Test
	void getStudentsInClassesTest1Story5() {
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, students.size());
		for(StudentInfoResponse s : students) {
			if(s.getId()==1) { 
				assertEquals("Matteo", s.getName());
				assertEquals("Costa", s.getSurname());
			}
			if(s.getId()==2) { 
				assertEquals("Mattea", s.getName());
				assertEquals("Costa", s.getSurname());
			}
			if(s.getId()==3) { 
				assertEquals("Paride", s.getName());
				assertEquals("D'Angelo", s.getSurname());
			}
			if(s.getId()!=1 && s.getId()!=2 && s.getId()!=3) fail();
		}
	}

	@Test
	void getStudentsInClassesTest2Story5() {
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(2);
		assertEquals(3, students.size());
		for(StudentInfoResponse s : students) {
			if(s.getId()==4) { 
				assertEquals("Parida", s.getName());
				assertEquals("D'Angela", s.getSurname());
			}
			if(s.getId()==5) { 
				assertEquals("Paolo", s.getName());
				assertEquals("Destefanis", s.getSurname());
			}
			if(s.getId()==6) { 
				assertEquals("Paola", s.getName());
				assertEquals("Destefanis", s.getSurname());
			}
			if(s.getId()!=4 && s.getId()!=5 && s.getId()!=6) fail();
		}

	}

	@Test
	void getStudentsInClassesTest3Story5() {
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(3);
		assertEquals(2, students.size());
		for(StudentInfoResponse s : students) {
			if(s.getId()==7) { 
				assertEquals("Cristian", s.getName());
				assertEquals("Ferrero", s.getSurname());
			}
			if(s.getId()==8) { 
				assertEquals("Cristiana", s.getName());
				assertEquals("Ferrera", s.getSurname());
			}
			if(s.getId()!=7 && s.getId()!=8) fail();
		}

	}

	@Test
	void getStudentsInClassesTest4Story5() {
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(null);
		assertEquals(2, students.size());
		for(StudentInfoResponse s : students) {
			if(s.getId()==9) { 
				assertEquals("Filippo", s.getName());
				assertEquals("Fontan", s.getSurname());
			}
			if(s.getId()==10) { 
				assertEquals("Filippa", s.getName());
				assertEquals("Fontana", s.getSurname());
			}
			if(s.getId()!=9 && s.getId()!=10) fail();
		}
	}

	@Test
	void getStudentsInClassesTest6Story5() {
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(-2);
		assertEquals(0,students.size());
		students=this.administrativeService.getStudentsInClass(0);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(11);
		assertEquals(0, students.size());

	}

	@Test
	void getStudentsInClassesTest7Story5() {
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(4);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(5);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(6);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(7);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(8);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(9);
		assertEquals(0, students.size());
		students=this.administrativeService.getStudentsInClass(10);
		assertEquals(0, students.size());
	}

	@Test
	void UpdateClassTest1Story5() {

		this.administrativeService.updateClass(null);
		controlStudentAndClass();
	}

	@Test
	void UpdateClassTest2Story5() {

		UpdateClassRequest u= new UpdateClassRequest(null, null);

		this.administrativeService.updateClass(u);
		controlStudentAndClass();
	}	
	
	@Test
	void UpdateClassTest3Story5() {

		StudentInfoResponse students []=new StudentInfoResponse[3];
		List<StudentInfoResponse> st;
		Class c;
		UpdateClassRequest u;

		st=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, st.size());
		for (int i=0;i<3;i++) students[i]=st.get(i);
		c=new Class();
		c.setId(-1);
		u=new UpdateClassRequest(c, students);
		this.administrativeService.updateClass(u);
		controlStudentAndClass();
	}
	

	
	@Test
	void UpdateClassTest4Story5() {

		Class c;
		StudentInfoResponse students []=new StudentInfoResponse[3];
		List<StudentInfoResponse> st;
		UpdateClassRequest u;

		st=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, st.size());
		for (int i=0;i<3;i++) students[i]=st.get(i);
		c=new Class();
		c.setId(500);
		u=new UpdateClassRequest(c, students);
		try{
			this.administrativeService.updateClass(u);
			fail();
		}catch(DataIntegrityViolationException e ) {
			return;
		}

	}
	
	@Test
	void UpdateClassTest5Story5() {

		Class c;
		StudentInfoResponse students []=new StudentInfoResponse[3];
		List<StudentInfoResponse> st;
		UpdateClassRequest u;

		st=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, st.size());
		for (int i=0;i<3;i++) students[i]=st.get(i);
		c=new Class();
		c.setId(null);
		u=new UpdateClassRequest(c, students);
		this.administrativeService.updateClass(u);
		controlStudentAndClass();
	}

	@Test
	void UpdateClassTest6Story5() {

		Class c;
		StudentInfoResponse students [];
		UpdateClassRequest u;

		c = new Class();
		students=new StudentInfoResponse [3];
		u=new UpdateClassRequest(c,students);
		this.administrativeService.updateClass(u);
		controlStudentAndClass();

	}

	@Test
	void UpdateClassTest7Story5() {

		Class c;
		StudentInfoResponse students []=new StudentInfoResponse[3];
		StudentInfoResponse studentsprev []=new StudentInfoResponse[3];
		List<StudentInfoResponse> stprev,st;
		UpdateClassRequest u;

		stprev=this.administrativeService.getStudentsInClass(2);
		st=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, stprev.size());
		assertEquals(3, st.size());

		for (int i=0;i<3;i++) { students[i]=st.get(i); studentsprev[i]=stprev.get(i); }

		c=this.administrativeService.getAllClasses().get(1);		
		u=new UpdateClassRequest(c, students);
		this.administrativeService.updateClass(u);

		List<StudentInfoResponse> sir=this.administrativeService.getStudentsInClass(2);
		assertEquals(3, sir.size());
		for(StudentInfoResponse s : sir) {
			if(s.getId()==1) { 
				assertEquals("Matteo", s.getName());
				assertEquals("Costa", s.getSurname());
			}
			if(s.getId()==2) { 
				assertEquals("Mattea", s.getName());
				assertEquals("Costa", s.getSurname());
			}
			if(s.getId()==3) { 
				assertEquals("Paride", s.getName());
				assertEquals("D'Angelo", s.getSurname());
			}
			if(s.getId()!=1 && s.getId()!=2 && s.getId()!=3) fail();
		}

		//restore db
		//Metto nella classe i vecchi studenti
		u=new UpdateClassRequest(c, studentsprev);
		this.administrativeService.updateClass(u);;
		//Poi metto gli studenti nella classe dove erano prima
		c=this.administrativeService.getAllClasses().get(0);
		u=new UpdateClassRequest(c, students);
		this.administrativeService.updateClass(u);
	}

	@Test
	void UpdateClassTest8Story5() {

		Class c;
		StudentInfoResponse students [];
		StudentInfoResponse studentsprev []=new StudentInfoResponse[3];
		UpdateClassRequest u;
		List<StudentInfoResponse> stprev;

		stprev=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, stprev.size());		
		for (int i=0;i<3;i++)  studentsprev[i]=stprev.get(i);

		students=new StudentInfoResponse[0];
		c=this.administrativeService.getAllClasses().get(0);
		u=new UpdateClassRequest(c, students);

		this.administrativeService.updateClass(u);
		assertEquals(0,this.administrativeService.getStudentsInClass(1).size());

		u=new UpdateClassRequest(c, studentsprev);
		this.administrativeService.updateClass(u);
	}

	@Test
	void createClassTest1Story5() {
		String c=this.administrativeService.createClass(1);
		assertEquals("1C",c);
		c=this.administrativeService.createClass(2);
		assertEquals("2C",c);
		c=this.administrativeService.createClass(3);
		assertEquals("3C",c);
		c=this.administrativeService.createClass(4);
		assertEquals("4C",c);
		c=this.administrativeService.createClass(5);
		assertEquals("5C",c);
		c=this.administrativeService.createClass(5);
		assertEquals("5D",c);
		classRepository.deleteById(16);
		classRepository.deleteById(15);
		classRepository.deleteById(14);
		classRepository.deleteById(13);
		classRepository.deleteById(12);
		classRepository.deleteById(11);
	}

	@Test
	void createClassTest2Story5() {
		String c=this.administrativeService.createClass(0);
		assertNull(c);
		c=this.administrativeService.createClass(6);
		assertNull(c);
		c=this.administrativeService.createClass(null);
		assertNull(c);
		c=this.administrativeService.createClass(-1);
		assertNull(c);
	}
	
	public void controlStudentAndClass() {

		List<Class> classes;
		classes = this.administrativeService.getAllClasses();
		assertEquals(10, classes.size());
		List<StudentInfoResponse> students=this.administrativeService.getStudentsInClass(1);
		assertEquals(3, students.size());
		students=this.administrativeService.getStudentsInClass(2);
		assertEquals(3, students.size());
		students=this.administrativeService.getStudentsInClass(3);
		assertEquals(2, students.size());
		students=this.administrativeService.getStudentsInClass(null);
		assertEquals(2, students.size());
		
	}

	/*STORY 3 - 4*/


	@Test 
	void checkIfParentExistsTest1Story3() {
		assertFalse(administrativeService.checkIfParentExists(null));
		assertFalse(administrativeService.checkIfParentExists("aaaaaa00a00a000x"));
		assertFalse(administrativeService.checkIfParentExists("a")); //substring of existing fiscalId
		assertTrue(administrativeService.checkIfParentExists("aaaaaa00a00a000a"));
	}

	@Test 
	void checkIfStudentExistsTest1Story4() {
		assertFalse(administrativeService.checkIfStudentExists(null));
		assertFalse(administrativeService.checkIfStudentExists("ssssss00s00s000x"));
		assertFalse(administrativeService.checkIfStudentExists("s")); //Substring of existing fiscalId
		assertTrue(administrativeService.checkIfStudentExists("ssssss00s00s000a"));
	}

	
	@Test
	void addParentsStudentTest1Story4() {
		//SUCCESS CASE
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("desrrtep34@live.it");
		p1.setFiscalCode("provaparenttryss"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000k");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("desfftep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1970/12/20");
		s1.setEntryLevel(6);
		s1.setFiscalCode("provastudent1234");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);
		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (Exception e) {
			e.printStackTrace();
			fail();
			return;
		}
		Long nSafter=studentRepository.count();
		Long nPafter=parentRepository.count();
		Long nUafter=userRepository.count();
		assertEquals(11, nSafter);
		assertEquals(11, nUafter);
		assertEquals(5, nPafter);

		//Restore db
		Integer PsId=StreamSupport.stream(parentStudentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( ParentStudentDAO::getId )).get().getId();
		parentStudentRepository.deleteById(PsId);
		PsId=StreamSupport.stream(parentStudentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( ParentStudentDAO::getId )).get().getId();
		parentStudentRepository.deleteById(PsId);
		Integer pId=StreamSupport.stream(parentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( ParentDAO::getId )).get().getId();
		parentRepository.deleteById(pId);
		pId=StreamSupport.stream(parentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( ParentDAO::getId )).get().getId();
		parentRepository.deleteById(pId);
		Integer uId=StreamSupport.stream(userRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( UserDAO::getId )).get().getId();
		userRepository.deleteById(uId);
		uId=StreamSupport.stream(userRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( UserDAO::getId )).get().getId();
		userRepository.deleteById(uId);
		Integer sId=StreamSupport.stream(studentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( StudentDAO::getId )).get().getId();
		studentRepository.deleteById(sId);
	}
	
	@Test
	void addParentsStudentTest2Story4() {
		//Same fiscal code for both the parents (existing one)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep3@live.it");
		p1.setFiscalCode("aaaaaa00a00a000a"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000a");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void addParentsStudentTest3Story4() {
		//Same fiscal code for both the parents (non existing one)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("provaUguali"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("provaUguali");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest4Story4() {
		//Wrong format of email
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33live.it");
		p1.setFiscalCode("aaaaaa00a00a000l"); 
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(null);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest5Story4() {
		//no parents
		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(null);
		parentsStudent.setParents2(null);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest6Story4() {
		//no student
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000a"); 
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(null);
		parentsStudent.setStudent(null);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void addParentsStudentTest7aStory4() {
		//null field 1 (birthday parent)
		Parent p1=new Parent();
		p1.setBirthdate(null);
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7bStory4() {
		//null field 2 (email parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail(null);
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7dStory4() {
		//null field 4 (gender parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender(null);
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7eStory4() {
		//null field 5 (name parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName(null);
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7fStory4() {
		//null field 6 (surname parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Paolo");
		p1.setSurname(null);
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7gStory4() {
		//null field 7 (telephone parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Paolo");
		p1.setSurname("Destefanis");
		p1.setTelephoneN(null);

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7hStory4() {
		//null field 8 (birthday parent2)
		Parent p2=new Parent();
		p2.setBirthdate(null);
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void addParentsStudentTest7iStory4() {
		//null field 9 (email parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail(null);
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}


	@Test
	void addParentsStudentTest7mStory4() {
		//null field 11 (gender parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender(null);
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7nStory4() {
		//null field 12 (name parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName(null);
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7oStory4() {
		//null field 13 (surname parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Paolo");
		p2.setSurname(null);
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7pStory4() {
		//null field 14 (telephone parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Paolo");
		p2.setSurname("Destefanis");
		p2.setTelephoneN(null);

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7qStory4() {
		//null field 15 (student birthdate)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate(null);
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7rStory4() {
		//null field 16 (student entryLevel)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(null);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7sStory4() {
		//null field 17 (student fiscalCode)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode(null);
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7tStory4() {
		//null field 18 (student gender)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender(null);
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7uStory4() {
		//null field 19 (student name)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName(null);
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest7vStory4() {
		//null field 20 (student surname)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname(null);

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8aStory4() {
		//"" field 1 (birthday parent)
		Parent p1=new Parent();
		p1.setBirthdate("");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8bStory4() {
		//"" field 2 (email parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8cStory4() {
		//"" field 3 (fiscalCode parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8dStory4() {
		//"" field 4 (gender parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8eStory4() {
		//"" field 5 (name parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8fStory4() {
		//"" field 6 (surname parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Paolo");
		p1.setSurname("");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8gStory4() {
		//"" field 7 (telephone parent)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x");
		p1.setGender("M");
		p1.setName("Paolo");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8hStory4() {
		//"" field 8 (birthday parent2)
		Parent p2=new Parent();
		p2.setBirthdate("");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8iStory4() {
		//"" field 9 (email parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8lStory4() {
		//"" field 10 (fiscalCode parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8mStory4() {
		//"" field 11 (gender parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8nStory4() {
		//"" field 12 (name parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8oStory4() {
		//"" field 13 (surname parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Paolo");
		p2.setSurname("");
		p2.setTelephoneN("0123456789");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8pStory4() {
		//"" field 14 (telephone parent2)
		Parent p2=new Parent();
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep23@live.it");
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setGender("M");
		p2.setName("Paolo");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("");

		Parent p1= new Parent();
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep2@live.it");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8qStory4() {
		//"" field 15 (student birthdate)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8rStory4() {
		//"" field 16 (student entryLevel)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(0);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8sStory4() {
		//"" field 18 (student fiscalCode)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8tStory4() {
		//"" field 18 (student gender)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8uStory4() {
		//"" field 19 (student name)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest8vStory4() {
		//"" field 20 (student surname)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000x"); //aaaaaa00a00a000x
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest9Story4() {
		//SUCCESS CASE JUST ONE PARENT
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep3@live.it");
		p1.setFiscalCode("succes00a00a000x");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(null);
		parentsStudent.setStudent(s1);

		Long nS=studentRepository.count();
		Long nP=parentRepository.count();
		Long nU=userRepository.count();
		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			fail();
			return;
		}
		Long nSafter=studentRepository.count();
		Long nPafter=parentRepository.count();
		Long nUafter=userRepository.count();
		//Long nPSafter=userRepository.count();
		assertEquals(nS, nSafter);
		assertEquals(nP+1, nPafter);
		assertEquals(nU+1, nUafter);

		//Restore DB
		Integer PsId=StreamSupport.stream(parentStudentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( ParentStudentDAO::getId )).get().getId();
		parentStudentRepository.deleteById(PsId);
		Integer pId=StreamSupport.stream(parentRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( ParentDAO::getId )).get().getId();
		parentRepository.deleteById(pId);
		Integer uId=StreamSupport.stream(userRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( UserDAO::getId )).get().getId();
		userRepository.deleteById(uId);
	}


	@Test
	void addParentsStudentTest10aStory4() {
		//future date as birthdate (parent1)
		Parent p1=new Parent();
		p1.setBirthdate("2080/12/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000a");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();

	}


	@Test
	void addParentsStudentTest10bStory4() {
		//future date as birthdate (parent2)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setBirthdate("2080/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1996/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();

	}

	@Test
	void addParentsStudentTest10cStory4() {
		//future date as birthdate (student)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000a");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("2080/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000p");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}


	@Test
	void addParentsStudentTest10dStory4() {
		//worng format date as birthdate (student)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000a");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("data/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000p");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}


	@Test
	void addParentsStudentTest10eStory4() {
		//worng format date as birthdate (parent1)
		Parent p1=new Parent();
		p1.setBirthdate("19/1980/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000a");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("2000/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest10fStory4() {
		//worng format date as birthdate (parent2)
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000x");
		p2.setBirthdate("12/1980/44");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("2000/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}



	@Test
	void addParentsStudentTest11aStory4() {
		//wrong entry level student (too high --> too low done in the emptyness test).
		//it should be between 6 and 10
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep34@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y");
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000a");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep3@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("2080/12/20");
		s1.setEntryLevel(11);
		s1.setFiscalCode("ssssss00s00s000x");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();

	}


	@Test
	void addParentsStudentTest12aStory4() {
		//wrong gender parent1
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y"); //aaaaaa00a00a000a
		p1.setGender("G");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000b");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest12bStory4() {
		//wrong gender parent2
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000a"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("G");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest12cStory4() {
		//wrong gender student
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000a"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000b");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000k");
		s1.setGender("G");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}


	@Test
	void addParentsStudentTest13aStory4() {
		//wrong telephone parent1
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000b");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest13bStory4() {
		//wrong telephone parent2
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000a"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789111");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest13cStory4() {
		//negative telephone
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000y"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("-012345678");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000b");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("0123456789");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addParentsStudentTest13dStory4() {
		//not-numeri telephone parent2
		Parent p1=new Parent();
		p1.setBirthdate("1980/12/20");
		p1.setEmail("destep33@live.it");
		p1.setFiscalCode("aaaaaa00a00a000a"); //aaaaaa00a00a000a
		p1.setGender("M");
		p1.setName("Diego");
		p1.setSurname("Destefanis");
		p1.setTelephoneN("0123456789");

		Parent p2= new Parent();
		p2.setFiscalCode("aaaaaa00a00a000y");
		p2.setBirthdate("1980/12/20");
		p2.setEmail("destep34@live.it");
		p2.setGender("M");
		p2.setName("Diego");
		p2.setSurname("Destefanis");
		p2.setTelephoneN("012345abcd");

		Student s1=new Student();
		s1.setBirthdate("1997/12/20");
		s1.setEntryLevel(9);
		s1.setFiscalCode("ssssss00s00s000a");
		s1.setGender("M");
		s1.setName("Paolo");
		s1.setSurname("Destefanis");

		AddParentStudentRequest parentsStudent=new AddParentStudentRequest();
		parentsStudent.setParents(p1);
		parentsStudent.setParents2(p2);
		parentsStudent.setStudent(s1);

		try {
			administrativeService.addParentsStudent(parentsStudent);
		}
		catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void testExcelClassCompositionStory5Test1() {
		try{
			administrativeService.createExcelClasses(null);
			fail();
		}
		catch(Exception e){
			return;
		}
	}

	@Test
	void testExcelClassCompositionStory5Test2() {
		ArrayList<CreateClassesStudentRequest> list = new ArrayList<>();
		list.add(new CreateClassesStudentRequest("ssssss00s00s000a", null));
		try{
			administrativeService.createExcelClasses(null);
			fail();
		}
		catch(Exception e){
			return;
		}
	}

	@Test
	void testExcelClassCompositionStory5Test3() {
		ArrayList<CreateClassesStudentRequest> list = new ArrayList<>();
		list.add(new CreateClassesStudentRequest(null, "1A"));
		try{
			administrativeService.createExcelClasses(null);
			fail();
		}
		catch(Exception e){
			return;
		}
	}

	/*Right case*/

	@Test
	void testGetAllTeachersStory30Test1() {
		List<TeacherInfoResponse> allTeachers=administrativeService.getAllTeachers();
		assertEquals(4,allTeachers.size());

		for(TeacherInfoResponse t : allTeachers) {
			if(t.getId()==1) { assertEquals("TEACHER 1", t.getName()); assertEquals("COGNOME", t.getSurname());}
			if(t.getId()==2) { assertEquals("TEACHER 2", t.getName()); assertEquals("COGNOME", t.getSurname());}
			if(t.getId()==3) { assertEquals("TEACHER 3", t.getName()); assertEquals("COGNOME", t.getSurname());}
			if(t.getId()==4) { assertEquals("TEACHER 4", t.getName()); assertEquals("COGNOME", t.getSurname());}

		}
	}

	/*Null class*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test1() {

		try {
			administrativeService.getTeacherByClassAndSubject(null, 1);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null subject*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test2() {

		try {
			administrativeService.getTeacherByClassAndSubject(1,null);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Negative class*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test3() {

		try {
			administrativeService.getTeacherByClassAndSubject(-1,1);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Negative subject*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test4() {

		try {
			administrativeService.getTeacherByClassAndSubject(1,-1);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Right case*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test5() {

		try {
			TeacherInfoResponse teacher=administrativeService.getTeacherByClassAndSubject(1,1);
			assertEquals("TEACHER 1", teacher.getName()); 
			assertEquals("COGNOME", teacher.getSurname());
			assertEquals(1,teacher.getId());

		}catch(Exception e) {
			fail();
		}
	}

	/*No existing subject*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test6() {

		try {
			TeacherInfoResponse t =administrativeService.getTeacherByClassAndSubject(1,300);
			assertEquals(null,t);
		}catch(Exception e) {
			fail();
		}
	}

	/*No existing class*/

	@Test
	void testgetTeacherByClassAndSubjectStory30Test7() {

		try {
			TeacherInfoResponse t =administrativeService.getTeacherByClassAndSubject(1000,1);
			assertEquals(null,t);
		}catch(Exception e) {
			fail();
		}
	}


	/*Right case*/

	@Test
	void testGetAllTSubjectsStory30Test1() {
		List<Subject> allSubjects=administrativeService.getAllSubjects();
		assertEquals(9,allSubjects.size());

		for(Subject s : allSubjects) {
			if(s.getId()==1) assertEquals("Mathematics", s.getName());
			if(s.getId()==2) assertEquals("Literature", s.getName()); 
			if(s.getId()==3) assertEquals("English", s.getName()); 
			if(s.getId()==4) assertEquals("History", s.getName());
			if(s.getId()==5) assertEquals("Philosophy", s.getName());
			if(s.getId()==6) assertEquals("Latin", s.getName()); 
			if(s.getId()==7) assertEquals("Phisical Education", s.getName()); 
			if(s.getId()==8) assertEquals("Science", s.getName());
			if(s.getId()==9) assertEquals("Phisics", s.getName());

		}
	}

	/*Null class*/

	@Test
	void testUpdateTeachingInClassStory30Test1() {
		try {
			TeacherInfoResponse t = new TeacherInfoResponse("name", "surname", 10);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(null, 1, t);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null subject*/

	@Test
	void testUpdateTeachingInClassStory30Test2() {
		try {
			TeacherInfoResponse t = new TeacherInfoResponse("name", "surname", 10);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, null, t);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null teacherInfoResponse*/

	@Test
	void testUpdateTeachingInClassStory30Test3() {
		try {
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, 1, null);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null id in teacherInfoResponse*/

	@Test
	void testUpdateTeachingInClassStory30Test4() {
		try {
			TeacherInfoResponse t = new TeacherInfoResponse("name", "surname", null);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, 1, t);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Negative class*/

	@Test
	void testUpdateTeachingInClassStory30Test5() {
		try {
			TeacherInfoResponse t = new TeacherInfoResponse("name", "surname", 10);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(-1, 1, t);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Negative subject*/

	@Test
	void testUpdateTeachingInClassStory30Test6() {
		try {
			TeacherInfoResponse t = new TeacherInfoResponse("name", "surname", 10);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(-1, null, t);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Negative id in teacherInfoResponse*/

	@Test
	void testUpdateTeachingInClassStory30Test7() {
		try {
			TeacherInfoResponse t = new TeacherInfoResponse("name", "surname", -1);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, 1, t);
			administrativeService.updateTeachingInClass(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null request*/

	@Test
	void testUpdateTeachingInClassStory30Test8() {
		try {
			administrativeService.updateTeachingInClass(null);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Right case*/

	@Test
	void testUpdateTeachingInClassStory30Test9() {

		try {

			TeacherInfoResponse tbefore=administrativeService.getTeacherByClassAndSubject(1,1);
			TeacherInfoResponse t = new TeacherInfoResponse("TEACHER 3", "COGNOME", 3);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, 1, t);
			UpdateTeachingInClassRequest uRestore = new UpdateTeachingInClassRequest(1, 1, tbefore);

			administrativeService.updateTeachingInClass(u);

			/*Control success change*/

			TeacherInfoResponse tNow=administrativeService.getTeacherByClassAndSubject(1,1);
			assertEquals(3,tNow.getId());
			assertEquals("TEACHER 3",tNow.getName());
			assertEquals("COGNOME",tNow.getSurname());

			/*Restore DB*/

			administrativeService.updateTeachingInClass(uRestore);

		}catch(Exception e) {
			fail();
		}
	}


	/*Not Existing Teacher*/

	@Test
	void testUpdateTeachingInClassStory30Test10() {

		try {

			TeacherInfoResponse t = new TeacherInfoResponse("TEACHER 3", "COGNOME", 300);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, 1, t);
			administrativeService.updateTeachingInClass(u);
			fail();

		}catch(Exception e) {
			return;
		}
	}

	/*Not Existing Class*/

	@Test
	void testUpdateTeachingInClassStory30Test11() {

		try {
			TeacherInfoResponse t = new TeacherInfoResponse("TEACHER 3", "COGNOME", 3);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(300, 1, t);
			administrativeService.updateTeachingInClass(u);
			fail();

		}catch(Exception e) {
			return;
		}
	}

	/*Not Existing Subject*/

	@Test
	void testUpdateTeachingInClassStory30Test12() {

		try {
			TeacherInfoResponse t = new TeacherInfoResponse("TEACHER 3", "COGNOME", 3);
			UpdateTeachingInClassRequest u = new UpdateTeachingInClassRequest(1, 1000, t);
			administrativeService.updateTeachingInClass(u);
			fail();

		}catch(Exception e) {
			return;
		}
	}

	/*Null general communication*/

	@Test
	void insertGeneralCommunicationsStory12Test1() {
		try {
			administrativeService.insertGeneralCommunications(null);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null description in generalCommunication */

	@Test
	void insertGeneralCommunicationsStory12Test2() {
		GeneralCommunication g = new GeneralCommunication();
		try {
			administrativeService.insertGeneralCommunications(g);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Empty description in generalCommunication */

	@Test
	void insertGeneralCommunicationsStory12Test3() {
		GeneralCommunication g = new GeneralCommunication();
		g.setDescription("");
		try {
			administrativeService.insertGeneralCommunications(g);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Right case*/

	@Test
	void insertGeneralCommunicationsStory12Test4() {
		GeneralCommunication g = new GeneralCommunication();
		g.setDescription("Desc");
		Long nbefore=generalCommunicationRepository.count();
		try {
			administrativeService.insertGeneralCommunications(g);
		}catch(Exception e) {
			e.printStackTrace();
			fail();
		}

		Long nafter=generalCommunicationRepository.count();
		assertEquals(nafter,nbefore+1);

		//Restore DB

		Integer cId=StreamSupport.stream(generalCommunicationRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( GeneralCommunicationDAO ::getId )).get().getId();
		generalCommunicationRepository.deleteById(cId);

	}

	/*Null communication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test1(){
		try {
			administrativeService.deleteGeneralCommunications(null);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null id in communication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test2(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(null);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null date in communication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test3(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate(null);
		c.setDescription("d");
		c.setId(4);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Id<0 in communication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test4(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(-2);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Date < 1 week in commmunication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test5(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate("2018/02/02");
		c.setDescription("d");
		c.setId(1);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Nonexisting commmunication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test6(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(40);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong date in commmunication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test7(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate("wrongDate");
		c.setDescription("d");
		c.setId(40);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Empty date in commmunication*/

	@Test 
	void deleteGeneralCommunicationsStory12Test8(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate("");
		c.setDescription("d");
		c.setId(40);

		try {
			administrativeService.deleteGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Right case*/

	@Test 
	void deleteGeneralCommunicationsStory12Test9(){

		GeneralCommunication g = new GeneralCommunication();
		g.setDescription("Desc");
		administrativeService.insertGeneralCommunications(g);
		Long nbefore=generalCommunicationRepository.count();

		List<GeneralCommunicationDAO> gcDAO = (List<GeneralCommunicationDAO>) generalCommunicationRepository.findAll();
		List<GeneralCommunication> gcs = new ArrayList<>();
		for (GeneralCommunicationDAO gc : gcDAO)
			gcs.add(gc.map());

		try {
			administrativeService.deleteGeneralCommunications(gcs.get((gcs.size())-1));
		}catch(Exception e) {
			fail();
		}

		Long nafter=generalCommunicationRepository.count();
		assertEquals(nafter,nbefore-1);

	}

	/*Null request*/

	@Test 
	void updateGeneralCommunicationsStory12Test1(){
		try {
			administrativeService.updateGeneralCommunications(null);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null id in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test2(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(null);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null data in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test3(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate(null);
		c.setDescription("d");
		c.setId(4);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null description in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test4(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription(null);
		c.setId(4);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Id<0 in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test5(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(-1);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Empty description in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test6(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("");
		c.setId(4);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Empty date in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test7(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate("");
		c.setDescription("d");
		c.setId(4);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong date in request*/

	@Test 
	void updateGeneralCommunicationsStory12Test8(){

		GeneralCommunication c= new GeneralCommunication();
		c.setDate("wrongdate");
		c.setDescription("d");
		c.setId(4);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Different date from db and request */

	@Test 
	void updateGeneralCommunicationsStory12Test9(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(3);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Not Existing id*/

	@Test 
	void updateGeneralCommunicationsStory12Test10(){

		GeneralCommunication c= new GeneralCommunication();
		Date today = new Date();
		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(today);
		c.setDate(todayAsString);
		c.setDescription("d");
		c.setId(40);

		try {
			administrativeService.updateGeneralCommunications(c);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Different date*/

	@Test 
	void updateGeneralCommunicationsStory12Test11(){

		List<GeneralCommunicationDAO> gcDAO = (List<GeneralCommunicationDAO>) generalCommunicationRepository.findAll();
		List<GeneralCommunication> gcs = new ArrayList<>();

		for (GeneralCommunicationDAO gc : gcDAO)
			gcs.add(gc.map());

		Date dt = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 1);
		dt = c.getTime();

		String pattern = "yyyy/MM/dd";
		DateFormat df = new SimpleDateFormat(pattern);
		String todayAsString = df.format(dt);

		gcs.get(0).setDescription("new description");
		gcs.get(0).setDate(todayAsString);

		try {
			administrativeService.updateGeneralCommunications(gcs.get(0));
			fail();
		}catch(Exception e) {
			return;
		}

	}

	/*Right case*/

	@Test 
	void updateGeneralCommunicationsStory12Test12(){

		GeneralCommunication g = new GeneralCommunication();
		g.setDescription("description");
		administrativeService.insertGeneralCommunications(g);


		List<GeneralCommunicationDAO> gcDAO = (List<GeneralCommunicationDAO>) generalCommunicationRepository.findAll();
		ArrayList<GeneralCommunication> gcs = new ArrayList<>();

		for (GeneralCommunicationDAO gc : gcDAO)
			gcs.add(gc.map());

		gcs.get(gcs.size()-1).setDescription("new description");

		try {
			administrativeService.updateGeneralCommunications(gcs.get(gcs.size()-1));
		}catch(Exception e) {
			fail();
		}

		//check

		gcDAO = (List<GeneralCommunicationDAO>) generalCommunicationRepository.findAll();
		gcs = new ArrayList<>();

		for (GeneralCommunicationDAO gc : gcDAO)
			gcs.add(gc.map());

		assertEquals("new description",gcs.get(gcs.size()-1).getDescription());


		//Restore DB

		Integer cId=StreamSupport.stream(generalCommunicationRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( GeneralCommunicationDAO ::getId )).get().getId();
		generalCommunicationRepository.deleteById(cId);
	}

	/*Null user*/

	@Test
	void addAccountStory30Test1() {
		try {
			administrativeService.addAccount(null);
			fail();
		}catch(Exception e) {
			return;
		}
	}


	/*Null date*/

	@Test
	void addAccountStory30Test2() {

		User u = new User();
		u.setBirthdate(null);
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null email*/

	@Test
	void addAccountStory30Test3() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail(null);
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null fiscal code*/

	@Test
	void addAccountStory30Test4() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode(null);
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null gender*/

	@Test
	void addAccountStory30Test5() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender(null);
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null name*/

	@Test
	void addAccountStory30Test6() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName(null);
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null surname*/

	@Test
	void addAccountStory30Test7() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname(null);
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null telephone*/

	@Test
	void addAccountStory30Test8() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN(null);
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Null role*/

	@Test
	void addAccountStory30Test9() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole(null);

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White date*/

	@Test
	void addAccountStory30Test10() {

		User u = new User();
		u.setBirthdate("");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White email*/

	@Test
	void addAccountStory30Test11() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White fiscal code*/

	@Test
	void addAccountStory30Test12() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White gender*/

	@Test
	void addAccountStory30Test13() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White name*/

	@Test
	void addAccountStory30Test14() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White surname*/

	@Test
	void addAccountStory30Test15() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White telephone*/

	@Test
	void addAccountStory30Test16() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*White role*/

	@Test
	void addAccountStory30Test17() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong date*/

	@Test
	void addAccountStory30Test18() {

		User u = new User();
		u.setBirthdate("blabla");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Future date*/

	@Test
	void addAccountStory30Test19() {

		User u = new User();
		u.setBirthdate("2040/01/01");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong email no @ */

	@Test
	void addAccountStory30Test20() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong email no .*/

	@Test
	void addAccountStory30Test21() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@liveit");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong email no data*/

	@Test
	void addAccountStory30Test22() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("@.");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong fiscal code*/

	@Test
	void addAccountStory30Test23() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaaa00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}


	/*Wrong gender*/

	@Test
	void addAccountStory30Test24() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("K");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong telephone no numeric*/

	@Test
	void addAccountStory30Test25() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("pippo");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong telephone no enough digits*/

	@Test
	void addAccountStory30Test26() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@liveit");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("1234");
		u.setRole("TEACHER");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong role*/

	@Test
	void addAccountStory30Test27() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("role");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Wrong role case sensitive*/

	@Test
	void addAccountStory30Test28() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("aaaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("teacher");

		try {
			administrativeService.addAccount(u);
			fail();
		}catch(Exception e) {
			return;
		}
	}

	/*Right case*/

	@Test
	void addAccountStory30Test29() {

		User u = new User();
		u.setBirthdate("1980/12/20");
		u.setEmail("destep23@live.it");
		u.setFiscalCode("bfaaaa00a00a000x");
		u.setGender("M");
		u.setName("Diego");
		u.setSurname("Destefanis");
		u.setTelephoneN("0123456789");
		u.setRole("TEACHER");
		Long before=userRepository.count();

		administrativeService.addAccount(u);
		Long after=userRepository.count();
		assertEquals(before,after-1);

		//Restore DB

		Integer idt=StreamSupport.stream(teacherRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( TeacherDAO ::getId )).get().getId();
		teacherRepository.deleteById(idt);

		Integer idu=StreamSupport.stream(userRepository.findAll().spliterator(), false)
				.max(Comparator.comparing( UserDAO ::getId )).get().getId();
		userRepository.deleteById(idu);

	}

	//Story 14

	@Test
	void addClassTimetableStory14Test1() {
		//Success case
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Literature");
			r.setTuesday("History");
			r.setWednesday("History");
			r.setThursday("Mathematics");
			r.setFriday("English");
			l.add(r);
		}
		t.setTimetable(l);
		long n = timetableRepository.count();
		try {
			administrativeService.addClassTimetable(t);
		}catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		assertEquals(n+30,timetableRepository.count());
	}

	
	@Test
	void addClassTimetableStory14Test1bis() {
		//Success case with override
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(1);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		long n = timetableRepository.count();
		administrativeService.addClassTimetable(t);
		assertEquals(n,timetableRepository.count());
	}
    
	@Test
	void addClassTimetableStory14Test1tris() {
		//Wrong case: a teacher in the same timeslot in two classes
		//In the db generator you can see that the class 1 has math for each timeslot of monday
		//Here I set the class 2 but I give the same subject for monday ( that is taught by the same teacher in the two classes)
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	

	@Test
	void addClassTimetableStory14Test2() {
		//Missin a row of timeslot (5 row)
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<5;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test3() {
		//Not existent subject
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Maths");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test4() {
		//Absence of a subject taught in the class but not present in the timetable
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("History");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test5() {
		//Null class
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(null);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test6() {
		//Negative class Id
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(-1);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test7() {
		//Not existing class
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(10);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test8() {
		//Null list
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(1);
		t.setTimetable(null);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test9() {
		try {
			administrativeService.addClassTimetable(null);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}

	@Test
	void addClassTimetableStory14Test10() {
		//Null subject
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<6;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday(null);
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}


	@Test
	void addClassTimetableStory14Test11() {
		//Null row
		TimeTableRequest t=new TimeTableRequest();
		t.setClassId(2);
		List<TimeTableRow> l=new ArrayList<>();
		for(int i=0;i<5;i++) {
			TimeTableRow r=new TimeTableRow();
			r.setMonday("Mathematics");
			r.setTuesday("Mathematics");
			r.setWednesday("English");
			r.setThursday("History");
			r.setFriday("Literature");
			l.add(r);
		}
		l.add(null);
		t.setTimetable(l);
		try {
			administrativeService.addClassTimetable(t);
		}catch (ResponseStatusException e){
			return;
		}catch (Exception e) {
			e.printStackTrace();
		}
		fail();
	}
	
}
