package it.polito.smsbackend;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;


import it.polito.smsbackend.dto.GeneralCommunication;
import it.polito.smsbackend.dto.SupportMaterial;
import it.polito.smsbackend.dto.TimeTableRow;
import it.polito.smsbackend.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.MOCK,
		classes = SmsBackendApplication.class)
@AutoConfigureMockMvc
public class UserServiceTests {

	@Autowired
	private UserService userService;

	@Test
	void downloadAttachmentTest1Story7() {
		//null
		try {
			userService.downloadAttachment(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	@Test
	void downloadAttachmentTest2Story7() {
		//negative
		try {
			userService.downloadAttachment(-1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	@Test
	void downloadAttachmentTest3Story7() {
		//too big id
		try {
			userService.downloadAttachment(100);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	/*Right case*/

	@Test 
	void getGeneralCommunicationsTest1Story12(){
		List<GeneralCommunication> communications=userService.getGeneralCommunications();
		assertEquals(3,communications.size());

		for(GeneralCommunication g : communications) {
			if(g.getId()==1) {assertEquals("2019/12/02", g.getDate()); assertEquals("General Communication 1",g.getDescription());}
			if(g.getId()==2) {assertEquals("2019/12/28", g.getDate()); assertEquals("General Communication 2",g.getDescription());}
			if(g.getId()==3) {assertEquals("2019/12/29", g.getDate()); assertEquals("General Communication 3",g.getDescription());}
		}
	}

	/*Null id*/

	@Test
	void downloadSupportMaterialStory15Test1() {
		try {
			userService.downloadSupportMaterial(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	/*Negative id*/

	@Test
	void downloadSupportMaterialStory15Test2() {
		try {
			userService.downloadSupportMaterial(-1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	/*Too big id*/

	@Test
	void downloadSupportMaterialStory15Test3() {

		try {
			userService.downloadSupportMaterial(100);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	/*Null class*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test1() {
		try{
			userService.getSMaterialByClassIdAndSubjectId(null,1);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}

	/*Null subject*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test2() {
		try{
			userService.getSMaterialByClassIdAndSubjectId(1,null);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}

	/*Negative class*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test3() {
		try{
			userService.getSMaterialByClassIdAndSubjectId(-1,1);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}

	/*Null subject*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test4() {
		try{
			userService.getSMaterialByClassIdAndSubjectId(1,-1);
			fail();
		}catch(ResponseStatusException e) {
			return;
		}
	}

	/*Not existing class*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test5() {
		try{
			List<SupportMaterial> support=userService.getSMaterialByClassIdAndSubjectId(1000,1);
			assertEquals(0,support.size());
		}catch(ResponseStatusException e) {
			fail();
		}
	}

	/*Not existing subject*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test6() {
		try{
			List<SupportMaterial> support=userService.getSMaterialByClassIdAndSubjectId(1,1000);
			assertEquals(0,support.size());
		}catch(ResponseStatusException e) {
			fail();
		}
	}

	/*Right case*/

	@Test
	void getSMaterialByClassIdAndSubjectIdStory15Test7() {
		try{
			List<SupportMaterial> support=userService.getSMaterialByClassIdAndSubjectId(1,1);
			assertEquals(2,support.size());
		}catch(ResponseStatusException e) {
			fail();
		}
	}
	
	/*Story 14*/
	
	@Test
	void getTimetableByClassStory14Test1() {
		//Success case
		List<TimeTableRow> l=userService.getTimetableByClassId(1);
		assertEquals(6, l.size());
		assertEquals("Mathematics", l.get(0).getMonday());
		for(int i=0;i<5;i++) {
			assertNotNull(l.get(i).getMonday());
			assertNotNull(l.get(i).getTuesday());
			assertNotNull(l.get(i).getWednesday());
			assertNotNull(l.get(i).getThursday());
			assertNotNull(l.get(i).getFriday());
		}
	}
	
	@Test
	void getTimetableByClassStory14Test2() {
		//Null class
		try {
			userService.getTimetableByClassId(null);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void getTimetableByClassStory14Test3() {
		//Negative class
		try {
			userService.getTimetableByClassId(-1);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void getTimetableByClassStory14Test4() {
		//Not existent class
		try {
			userService.getTimetableByClassId(20);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void getTimetableByTeacherStory14Test1() {
		//Success case
		List<TimeTableRow> l=userService.getTimetableByTeacherId(1);
		assertEquals(6, l.size());
		assertEquals("1A: Mathematics", l.get(0).getMonday());
	}
	
	@Test
	void getTimetableByTeacherStory14Test2() {
		//Null teacher
		try {
			userService.getTimetableByTeacherId(null);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void getTimetableByTeacherStory14Test3() {
		//negative id
		try {
			userService.getTimetableByTeacherId(-1);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
	
	@Test
	void getTimetableByTeacherStory14Test4() {
		//not existent teacher
		try {
			userService.getTimetableByTeacherId(30);
		}catch (ResponseStatusException e){
			return;
		}
		fail();
	}
}
