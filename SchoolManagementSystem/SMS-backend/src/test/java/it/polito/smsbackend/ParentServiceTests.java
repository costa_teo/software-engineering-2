package it.polito.smsbackend;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import it.polito.smsbackend.dto.Attachment;
import it.polito.smsbackend.dto.Attendance;
import it.polito.smsbackend.dto.Grade;
import it.polito.smsbackend.dto.Subject;
import it.polito.smsbackend.response.AssignmentsBySubjectResponse;
import it.polito.smsbackend.response.GradesBySubjectResponse;
import it.polito.smsbackend.response.StudentInfoResponse;
import it.polito.smsbackend.service.ParentService;
import it.polito.smsbackend.SmsBackendApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.MOCK,
  classes = SmsBackendApplication.class)
@AutoConfigureMockMvc
class ParentServiceTests {
	
	@Autowired
	private ParentService parentService;
	
	@Test
	void getGradesTest1Story1() {
		ArrayList<GradesBySubjectResponse> grades = this.parentService.getGrades(1);
		assertEquals(2, grades.size());
		for(GradesBySubjectResponse gbs : grades) {
			if(gbs.getSubject().compareTo("Literature") == 0) {
				assertEquals("Literature", gbs.getSubject());
				assertEquals("TEACHER 2", gbs.getTeacher());
				for(Grade g : gbs.getGrades()) {
					if(g.getId()==5)assertEquals(8.0, g.getValue().floatValue());
					if(g.getId()==6)assertEquals(10.0, g.getValue().floatValue());
					if(g.getId()!=5 && g.getId()!=6 ) fail();
				}
			}
			else if(gbs.getSubject().compareTo("Mathematics") == 0) {
				assertEquals("Mathematics", gbs.getSubject());
				assertEquals("TEACHER 1", gbs.getTeacher());
				assertEquals(5.50, gbs.getGrades().get(0).getValue().floatValue());
				assertEquals(7.50, gbs.getGrades().get(1).getValue().floatValue());
				if(gbs.getGrades().size()!=2) fail();
			}
			else {
				fail();
			}			
		}
	}
	
	@Test
	void getGradesTest2Story1() {
		ArrayList<GradesBySubjectResponse> grades = this.parentService.getGrades(0);
		assertEquals(0, grades.size());
		ArrayList<GradesBySubjectResponse> grades1 = this.parentService.getGrades(-3);
		assertEquals(0, grades1.size());
	}
	
	@Test
	void getGradesTest3Story1() {
		ArrayList<GradesBySubjectResponse> grades = this.parentService.getGrades(null);
		assertEquals(0, grades.size());
	}
	
	@Test
	void getStudentsTest1Story1() {
		ArrayList<StudentInfoResponse> students1 = parentService.getStudents(1);
		assertEquals(2, students1.size());
		ArrayList<StudentInfoResponse> students2 = parentService.getStudents(2);
		assertEquals(3, students2.size());
		for(StudentInfoResponse s : students2) {
			if( s.getId() == 1) {
				assertEquals("Matteo", s.getName());
				assertEquals("Costa", s.getSurname());
			}
			else if( s.getId() == 3) {
				assertEquals("Paride", s.getName());
				assertEquals("D'Angelo", s.getSurname());
			}
			else if( s.getId() == 4) {
				assertEquals("Parida", s.getName());
				assertEquals("D'Angela", s.getSurname());
			}
			else {
				fail();
			}
		}	
	}
	
	@Test
	void getStudentsTest2Story1() {
		try {
			ArrayList<StudentInfoResponse> students = parentService.getStudents(null);
			assertEquals(0, students.size());
		}catch(Exception e) {
			fail();
		}
	}
	
	@Test
	void getStudentsTest3Story1() {
		try {
			ArrayList<StudentInfoResponse> students = parentService.getStudents(-1);
			assertEquals(0, students.size());
			ArrayList<StudentInfoResponse> students1 = parentService.getStudents(0);
			assertEquals(0, students1.size());
		}catch(Exception e) {
			fail();
		}
	}
	
	@Test
	void getAssignmentsTest1Story7() {
		//Success case
		ArrayList<AssignmentsBySubjectResponse> a=parentService.getAssignments(1);
		assertEquals(4, a.size());
		assertEquals(1, a.get(0).getAssignments().size());
		assertEquals(1, a.get(1).getAssignments().size());
		assertEquals(1, a.get(2).getAssignments().size());
		assertEquals(1, a.get(3).getAssignments().size());
		//no assignments
		ArrayList<AssignmentsBySubjectResponse> b=parentService.getAssignments(9);
		assertEquals(0, b.size());
	}
	
	@Test
	void getAssignmentsTest2Story7() {
		//null id
		try {
		parentService.getAssignments(null);
		fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAssignmentsTest3Story7() {
		//negative id
		try {
		parentService.getAssignments(-2);
		fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAttachmentsTest1Story7() {
		//null
		try {
			parentService.getAttachments(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAttachmentsTest2Story7() {
		//negative
		try {
			parentService.getAttachments(-2);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	
	
	@Test
	void getAttachmentsTest3Story7() {
		//Success case
		ArrayList<Attachment> a=parentService.getAttachments(1);
		assertEquals(2, a.size());
		ArrayList<Attachment> a2=parentService.getAttachments(200);
		assertEquals(0, a2.size());
	}
	

	/*Story 8*/
	
	@Test
	void getAttendancesTest1Story8() {
		//null
		try {
			parentService.getAttendances(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAttendancesTest2Story8() {
		//negative
		try {
			parentService.getAttendances(-6);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	@Test
	void getAttendancesTest3Story8() {
		//invalid
		List<Attendance> attendances=parentService.getAttendances(6000);
		assertEquals(0, attendances.size());
	}
	
	@Test
	void getAttendancesTest4Story8() {
		//Success case
		List<Attendance> attendances=parentService.getAttendances(1);
		assertEquals(6, attendances.size());
	}
	
	/*Null student*/
	
	@Test
	void getClassTest1Story16() {
	
		try {
			parentService.getClass(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	

	/*Negative student*/
	
	@Test
	void getClassTest2Story16() {
	
		try {
			parentService.getClass(-1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	/*Not existing student*/
	
	@Test
	void getClassTest3Story16() {
	
		try {
			parentService.getClass(1000);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	/*Right case*/
	
	@Test
	void getClassTest4Story16() {
	
		try {
			it.polito.smsbackend.dto.Class c =parentService.getClass(1);
			assertEquals(1,c.getId());
			assertEquals("1A",c.getName());
		}
		catch (ResponseStatusException e){
			fail();
		}
	}
	
	/*Null class*/
	
	@Test
	void getSubjectsByClassIdTest1Story16() {
	
		try {
			parentService.getSubjectsByClassId(null);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	

	/*Negative class*/
	
	@Test
	void getSubjectsByClassIdTest2Story16() {
	
		try {
			parentService.getSubjectsByClassId(-1);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}
	
	/*Not existing class*/
	
	@Test
	void getSubjectsByClassIdTest3Story16() {
	
		try {
			parentService.getSubjectsByClassId(1000);
			fail();
		}
		catch (ResponseStatusException e){
			return;
		}
	}

	/*Right case*/
	
	@Test
	void getSubjectsByClassIdTest4Story16() {
	
		try {
			List <Subject> s=parentService.getSubjectsByClassId(1);
			assertEquals(4,s.size());
		}
		catch (ResponseStatusException e){
			fail();
		}
	}
	
}
