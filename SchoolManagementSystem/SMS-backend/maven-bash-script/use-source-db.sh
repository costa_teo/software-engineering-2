#!/bin/bash

#### ATTENZIONE #####
#### Questo file deve essere editato con un editor che usa la convenzione UNIX ######
#### EOL End of LINE MUST be = \n. Windows editor use EOL = \r\n; (use LF instead) ############

#-----directories------
TMPDIR=./tmp
DBDIRECTORY=./src/main/resources

#-----files------
DBTMP=./tmp/myDB.mv.db
DBOLD=./src/main/resources/myDB.mv.db


############ CHECKS ################
if [ ! -f "$DBTMP" ]; then
#    echo "[BASH] ERROR DB SOURCE IS NEEDED IN PATH $TMPDIR"    
	exit
fi

#-----remove old test file if exists------
if [ -f "DBOLD" ]; then
    echo "[BASH] removing old $DBOLD file" 
	rm $DBOLD
fi


#copy DBTMP in the working dir
echo "[BASH] copying $DBTMP in $DBDIRECTORY"
echo "[BASH]"   
echo "[BASH]"   
echo "[BASH]"   #delay, i know it not makes sense
cp $DBTMP $DBDIRECTORY


#-----tmp dir if exists------
if [ -d "$TMPDIR" ]; then
    echo "[BASH] removing $TMPDIR directory"    
	rm -R $TMPDIR
fi
