#!/bin/bash

#### ATTENZIONE #####
#### Questo file deve essere editato con un editor che usa la convenzione UNIX ######
#### EOL End of LINE MUST be = \n. Windows editor use EOL = \r\n; (use LF instead) ############

#-----directories------
TMPDIR=./tmp
DBDIRECTORY=./src/main/resources

#-----files------
DBFILE=./src/main/resources/myDB.mv.db
DBTEST=./src/test/resources/myDB.mv.db

############ CHECKS ################
if [ ! -f "$DBTEST" ]; then
    echo "[BASH] ERROR DB TEST IS NEEDED IN PATH $DBTEST"    
	exit 1
fi



#-----remove old tmp dir if exists------
if [ -d "$TMPDIR" ]; then
    echo "[BASH] removing $TMPDIR directory"    
	rm -R $TMPDIR
fi

#-----create tmp dir------
echo "[BASH] making $TMPDIR"
mkdir $TMPDIR

#-----copy original db file in tmp and remove it, if exist------
if test -f "$DBFILE"; then
    echo "[BASH] copying $DBFILE file in $TMPDIR"
	cp $DBFILE $TMPDIR
	echo "[BASH] removing $DBFILE" from $DBDIRECTORY
	rm $DBFILE
fi

#copy DBTEST in the working dir
echo "[BASH] copying $DBTEST in $DBDIRECTORY"
cp $DBTEST $DBDIRECTORY




