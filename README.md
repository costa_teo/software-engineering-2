# Software engineering 2 project repository

Pull the application 
> **docker pull se2polito/team-c**

In order to run the application first of all run Docker specifying the correct port mapping by using the command:
> **docker run se2polito/team-c**

After starting Docker and after that the application has started you can access the login page through the port 443 (https://localhost:443).

If you want to have access with one of the three available roles the following credentials could be provided:

1. **To enter as an Administrative Officer:**

-   Administrator ( Super Administrator )

    Username: `A1` or the email: `E-A1` 

    Password: `A1`

    Role button: `Administrator`

-   Administrator ( Normal Administrator )

    Username: `A2` or the email: `E-A2` 

    Password: `A2`

    Role button: `Administrator`

2. **To enter as a Teacher:**

    Username: `T1` or the email: `E-T1`

    Password: `T1`

    Role button: `Teacher`

3. **To enter as a Parent:**

    Username: `P1` or the email `E-P1`

    Password: `P1`

    Role button: `Parent`

The application available on Docker is up-to-date with the complete version reached after the fourth and last sprint, therefore it has implemented stories from 0 to 17, story 25, story 28 and story 30.
